
#ifndef ROCK_H
#define ROCK_H
//for model inputs


class ROCK{
public:
	ROCK();
	double k(int, int) const; //get permeability
	void k(int, int, double); //set permeability
	double porosity();
	void porosity(double);
	double Pct();
	void Pct(double);
	double lamda();
	void lamda(double);
	double x();
	void x(double);
	double y();
	void y(double);
	double z();
	void z(double);
	double porevolume();
	void porevolume(double);
	double oilip();
	void oilip(double);
	void clear();
private:
	int numberofregions_;
	double porosity_, x_, y_, z_, porevolume_, Pct_, lamda_, oilip_;
	double k_[3][3];

};


#endif