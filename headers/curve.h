
#ifndef CURVE_H
#define CURVE_H
#include <vector>
#include "node.h"

class CURVE{
public:
	CURVE();
	int node(int); //get node
	void addnode(int); //add one node
	void markbuilt(int);
	int markbuilt();

private:
	std::vector<int> node_;
	int markbuilt_;
};

#endif