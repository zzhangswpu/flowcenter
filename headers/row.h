
#ifndef ROW_H
#define ROW_H

#include<vector>

class ROW{
public:
	void addvalue(double, int);
	std::vector<double> valuevec();
	std::vector<int> positionvec();
private:
	std::vector<double> valuevec_;
	std::vector<int> positionvec_;
};

#endif
