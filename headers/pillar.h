
#ifndef PILLAR_H
#define PILLAR_H
//for pillars to generate corner-point grid

#include <vector>

class PILLAR{
public:
	PILLAR();
	void initialise(int); //set number of pillar nodes
	int numberofnodes();
	int pillarnode(int);
	void pillarnode(int, int);
	int pillarmarker(int);
	void pillarmarker(int, int);
	double x();
	void x(double);
	double y();
	void y(double);
	void addsuelement(int);
	std::vector<int> suelement();

private:
	std::vector<int> pillarnodelist_, pillarmarkerlist_;
	double x_, y_;
	std::vector<int> suelement_;
};



#endif