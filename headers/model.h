
#ifndef MODEL_H
#define MODEL_H
//for model inputs

#include <vector>
#include<fstream>
#include"bsurface.h"
#include"well.h"
#include"propertyarea.h"
#include"tetrahedron.h"
#include"mathzz.h"

class MODEL{
public:

	unsigned int numberofpropertyareas() const;
	void numberofpropertyareas(int);
	void addpropertyarea(PROPERTYAREA);
	PROPERTYAREA propertyarea(int);
	void modifypropertyarea(int, PROPERTYAREA);

	void numberofwells(int i);
	void modifywell(int, WELL);

	void addbsurface(BSURFACE);
	BSURFACE bsurface(int);
	int numberofbsurfaces();

	void addwell(WELL);
	WELL well(int);
	int numberofwells() const;
	void inputwellgeometry(std::vector<std::vector<NODE>>);
	void generatewell(double);
	void computeporevolume(std::vector<TETRAHEDRON>);

	void resetbsurface(int, BSURFACE);
	void meshedgelength(double);
	double meshedgelength();

	void clear();
	void clearpropertyareas();
	void clearwells();
private:
	char tetgencommand_[20], trianglecommand_[20];
	std::vector<BSURFACE> bsurfacelist;
	std::vector<WELL> welllist;
	std::vector<PROPERTYAREA> propertyarealist;
	MATHZZ mathzz_;
	double meshedgelength_;
};


#endif