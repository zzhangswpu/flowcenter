

#ifndef EDGE_H
#define EDGE_H
#include<iostream>
#include <vector>

class EDGE{
public:
	EDGE();

	unsigned int node(unsigned int) const; //get node
	void node(unsigned int, unsigned int); //set node 1 and 2

	void sx(double); //set projections
	void sy(double);
	void sz(double);

	double sx();
	double sy();
	double sz();

	void area(double);
	double area();

	void length(double);
	double length();

	void flux(double); //positive means point to i2
	double flux();

	void fluxc(double);
	double fluxc();

	void fluxh(double);
	double fluxh();

	void trans(double);
	double trans();

	double k(int, int) const; //get permeability
	void k(int, int, double); //set permeability

	void addsuele(int);
	int suele(int);
	int numberofsuele();
	void fmmv(int, double);
	double fmmv(int);

private:
	unsigned int node1[2]; // for edge
	double sx_, sy_, sz_, area_, length_, fluxh_, k_[3][3], fluxc_, trans_, flux_, fmmv_[3];
	std::vector<int> suele_;
};

#endif