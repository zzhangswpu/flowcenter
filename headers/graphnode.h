#ifndef GRAPHNODE_H
#define GRAPHNODE_H

//general graphnode; not specific for DFS

#include <vector>

class GRAPHNODE{
public:
	GRAPHNODE();
	int numberofedgeout(); //return number of edges from this node
	int numberofedgein(); //return number of edges to this node

	void addedgeout(int); //add edges from this node
	int edgeout(int); // edge out

	void addedgein(int); //add edges towards this node
	int edgein(int); // edge in

	void marknodein(int);//jnode that in the current path flows into the current node
	int marknodein();

	int status();
	void status(int);

	void countedgeout(); //count which edge to go; 0 means no edge; 1 means the first edge. every visit=>count++
	int printcountedgeout();

	void markcycle(int); // size 0 means no cycle, positive means cycles attached. Maybe more than 1 cycles
	std::vector<int> markcycle();

	void markmatrix(int); //0 means not in big cycle matrix; 1 means in
	int markmatrix();

	void markfromgraphb(int);//which tof or tracer boundary the current graphnode connects to
	int markfromgraphb();

	void clear(); //clear vector edge_

private:
	std::vector<int> edgeout_, edgein_, markcycle_;
	int marknodein_, countedgeout_, status_, markmatrix_, markfromgraphb_, marktracerfromb_;
};



#endif