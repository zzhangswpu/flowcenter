
//for all calculations
#ifndef MATHZZ_H
#define MATHZZ_H

#include <vector>
#include<cmath>
#include <iostream>
#include"matrix.h"
#include"node.h"


class MATHZZ{
public:
	double volume_tetra_signed(double x[4], double y[4], double z[4]); //volume of a tetrahedron signed(positive or negative)
	double volume_tetra_positive(double x[4], double y[4], double z[4]);//volume of a tetrahedron positive
	double area_triangle(double x[3], double y[3], double z[3]); //area of a triangle by coordinates of 3 nodes
	double area_quad(double x[4], double y[4], double z[4]);
	double determinant3(double, double, double, double, double, double, double, double, double);//3x3 matrix determinant row1, row2, row3
	double determinant4(double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double);
	double determinant2(double, double, double, double);//2x2 matrix
	std::vector<double> crossproduct(double *a, double *b);
	std::vector<double> crossproduct(std::vector<double>, std::vector<double>);
	std::vector<double> unitnormal(double *a, double *b);
	std::vector<double> unitnormal(std::vector<double>, std::vector<double>);
	double dotproduct(std::vector<double>, std::vector<double>);
	double dotproduct(double *a, double *b);
	double area_vector(double *a, double *b); //area of the parallelogram by vectors a and b
	double area_vector(std::vector<double>, std::vector<double>);
	double vectorlength(double *a);
	double vectorlength(std::vector<double>);
	std::vector<double> adversevector(std::vector < double>);
	std::vector<double> projectionvector(std::vector<double>, std::vector<double>);
	std::vector<double> Gaussianelimination(MATRIX, std::vector<double> );
	double twonodedistance(NODE, NODE);
	double twonodedistance2D(double, double, double, double);
	std::vector<std::vector<double>> inverse33matrix(std::vector<std::vector<double>>);


private:
	double Sx, Sy, Sz, area_;
};

#endif