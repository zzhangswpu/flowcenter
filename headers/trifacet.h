

#ifndef TRIFACET_H
#define TRIFACET_H
#include<vector>
#include<iostream>

class TRIFACET{               //Triangle
public:
	unsigned int node(unsigned int) const; //get node
	void node(unsigned int, unsigned int); //set nodes

	void markbsurface(int); //mark which bsurface
	int markbsurface();

	double area() const; //get area
	void area(double); //set area

	double parametricarea() const;
	void parametricarea(double);

	long element() const; //get ajacent element
	void element(long); //set ajacent element

	void unitnormalvec(std::vector<double>); //set normal vector (normals by default point to inside of the domain)
	std::vector<double> unitnormalvec(); //get normal vector

private:
	unsigned int node_[3], numberofvertex_; // 3 for triangle
	int markbsurface_;
	double area_, area_parametric;
	long element_;
	std::vector<double>unitnormalvec_;
};

#endif