

#ifndef LINEARFUNC_H
#define LINEARFUNC_H
#include"node.h"

class LINEARFUNC{
public:
	void a(double);
	void b(double);
	void c(double);
	void d(double);

	double value(double, double, double);
private:
	double value_, a_, b_, c_, d_;
};

#endif