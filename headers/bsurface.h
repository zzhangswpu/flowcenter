
#ifndef BSURFACE_H
#define BSURFACE_H
//boundary conditions surfaces
#include <iostream>
#include <vector>

class BSURFACE{
public:
	BSURFACE();
	void type(int); //type  0 noflow 1 pressure 2 velocity
	int type();
	void P(double);
	double P();
	double influx();//volumetric flow rate per unit area into the model
	void influx(double);
	void sign(int);//injector (1) or producer (-1) or 0 no-flow
	int sign();

private:
	double value_, influx_, P_;
	int type_, sign_;

};


#endif