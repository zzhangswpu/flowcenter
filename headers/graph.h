
#ifndef GRAPH_H
#define GRAPH_H
//for wells (pressure-controled and rate-controled)

#include <vector>

class GRAPH{
public:
	GRAPH();
	void tracingsign(int); //1 from injector -1 reverse graph from producer
	int tracingsign();
	void ntracingb(int);
	int ntracingb();
	void clear();

private:
	int tracingsign_, ntracingb_;

};


#endif