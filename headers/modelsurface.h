
#ifndef MODELSURFACE_H
#define MODELSURFACE_H
//corresponds to coons-patch surfaces
#include <iostream>
#include <vector>

class MODELSURFACE{
public:
	//MODELSURFACE();

	void addinternalneighbor(int);
	int internalneighbor(int);
	void addinternalneighbordir(int);
	int internalneighbordir(int);
	int numberofinternalneighbor();

private:
	std::vector<int> internalneighbor_, internalneighbordir_;


};


#endif