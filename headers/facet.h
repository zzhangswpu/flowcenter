
#ifndef FACET_H
#define FACET_H
#include <vector>
class FACET{               //Triangle OR Quadrilateral
public:
	unsigned int node(unsigned int) const; //get node
	void node(unsigned int, unsigned int); //set nodes

	unsigned int numberofvertex() const; //get number of vertex
	void numberofvertex(unsigned int); //set number of vertex

	void markbsurface(int);
	int markbsurface();

	void area(double);
	double area();

	double parametricarea() const;
	void parametricarea(double);

	double flatarea() const;
	void flatarea(double);

	long element() const; //get ajacent element
	void element(long); //set ajacent element

	void unitnormalvec(std::vector<double>); //set normal vector (normals by default point to inside of the domain)
	std::vector<double> unitnormalvec(); //get normal vector

	void clear();

private:
	unsigned int node_[4], numberofvertex_;; // 3 for triangle, 4 for quadriangle
	int markbsurface_;
	double area_, area_parametric, area_flat;
	long element_;
	std::vector<double>unitnormalvec_;
};

#endif