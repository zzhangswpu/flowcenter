

#ifndef GEOSURFACE_H
#define GEOSURFACE_H
//corresponds to coons-patch surfaces
#include <iostream>
#include <vector>

class GEOSURFACE{
public:
	GEOSURFACE();

	int nnode(); //return number of nodes
	int ncurve(); //return number of curves


	void addnode(int); //add a global node number
	int node(int); //returns the global node number

	void addcurve(int);
	int curve(int);

	void addcurvemark(int); //1 south 2 north 3 west 4 east -1 internal alligned with north-south boundary curves -2 internal alligned with east-west
	int curvemark(int);
	void addcurvelocation(double); //distance from south (v=0) or west (u=0) in terms of parameter
	double curvelocation(int);

	void mark(int); //mark horizontal or vertical 1 horizontal 2 vertical
	int mark();

private:
	std::vector<int> node_, curve_, curvemark_;
	std::vector<double> curvelocation_;
	int mark_;


};


#endif