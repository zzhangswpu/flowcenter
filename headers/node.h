
#ifndef NODE_H
#define NODE_H

#include<vector>

class NODE{
public:
	NODE();
	//coordinates and mark
	double x() const;
	double y() const;
	double z() const;
	void x(double);
	void y(double);
	void z(double);

	//fluid and rock properties
	//double rho() const; //density

	//void rho(double);
	//double mu_w() const; //dynamic viscosity only useful for visualisation
	//void mu_w(double);
	//double mu_o() const; //dynamic viscosity
	//void mu_o(double);
	double k(int, int); //get permeability nodal permeability only useful for visualisation
	void k(int, int, double); //set permeability
	double porosity() const;
	void porosity(double);
	double Pc() const; //capillary pressure
	void Pc(double);
	double Sw() const; //Saturation (wetting-phase)
	void Sw(double);
	double mobiw() const; //mobility of oil
	void mobiw(double);
	double mobio() const; //mobility of water
	void mobio(double);
	double mobit(); //total mobility
	void mobit(double);
	double fracw();
	void fracw(double);
	double Po() const;  //oil pressure; water pressure=Po-Pc
	void Po(double);
	double Pw() const;
	double coef() const;
	void coef(double);
	double source() const; //source term; positive for source and negative for sink
	void source(double);

	double tof() const; //forward time of flight
	void tof(double);

	double tof_back() const; //backward time of flight
	void tof_back(double);

	double tof_total() const; //total time of flight

	double tracer(int) const; //tracer of the flowb or tracerb (same) for forward flow field case
	void tracer(int, double);
	double tracer_back(int) const; //tracer of the flowb or tracerb (same) for backward flow field case
	void tracer_back(int, double);
	void tracersize(int);
	int tracersize();
	void tracersize_back(int);
	int tracersize_back();

	void markbsurface(int); //number of boundary
	int markbsurface();

	void markwell(int);//mark well
	int markwell();

	void marktracingb(int); //number of tracer boundary
	int marktracingb();

	void markbc(int); //FOR ELLIPTIC EQ: bc type 0 noflow 1 Dirichlet, 2 Neumann
	int markbc();

	void markbc_Sw(int);
	int markbc_Sw();
	//double Ux() const; //Nodal Darcy velocity
	//double Uy() const;
	//double Uz() const;
	//void Ux(double);
	//void Uy(double);
	//void Uz(double);

	void dualvolume(double); //dual volume of the node
	double dualvolume();

	//double dpdx() const; //pressure derivative
	//double dpdy() const;
	//double dpdz() const;
	//void dpdx(double);
	//void dpdy(double);
	//void dpdz(double);

	void addelement(int); //for elements surrounding nodes
	int element(int);
	int numberofelement();

	void addsunode(int); //for node surrounding node
	int sunode(int);
	int numberofsunode();

	void addsutriangle(int); //for node surrounding node
	int sutriangle(int);
	int numberofsutriangle();

	void addmarksunode(int); //for marking when creating edges
	int marksunode(int);
	void marksunode(int, int);

	void addedgeout(int); //for edge from this node
	int edgeout(int);
	int numberofedgeout();

	void addedgein(int); //for edge into this node
	int edgein(int);
	int numberofedgein();

	void addedgeinflux(int); //for edge into this node
	int edgeinflux(int);
	int numberofedgeinflux();
	void clearedgeinflux();

	void addedgeoutflux(int); //for edge from this node
	int edgeoutflux(int);
	int numberofedgeoutflux();
	void clearedgeoutflux();

	void addedgeinfluxh(int); //for edge into this node
	int edgeinfluxh(int);
	int numberofedgeinfluxh();
	void clearedgeinfluxh();

	void addedgeoutfluxh(int); //for edge from this node
	int edgeoutfluxh(int);
	int numberofedgeoutfluxh();
	void clearedgeoutfluxh();



//***for graph search************************************************************

	int status();
	void status(int);

	void marknodein(int);//jnode that in the current path flows into the current node
	int marknodein();

	void markcycle(int); // size 0 means no cycle, positive means cycles attached. Maybe more than 1 cycles
	std::vector<int> markcycle();

	int markmaxtracer(); //tracer max mark
	void markmaxtracer(int);

	int markmaxtracer_back(); //tracer max mark
	void markmaxtracer_back(int);

	void computetracerpair();
	double tracerpair(int, int);

	void computewellpairPV();
	double wellpairPV(int, int);

	void computewellpairflowrate();
	double wellpairflowrate(int, int);

	void influx(double);
	double influx();

	void cleargraphinfo();

private:
	double x_, y_, z_, tof_, tof_back_, porosity_, influx_;
	int markbsurface_, markwell_, marktracingb_, markbc_, markbc_Sw_, status_, marknodein_, markmaxtracer_, markmaxtracer_back_;
	double rho_, coef_, Po_, source_, dualvolume_, Pc_;
	double Sw_, mobiw_, mobio_, mobit_, fracw_;
	double k_[3][3];
	//mu_w_, mu_o_, Sw_, Krw_, Kro_, Ux_, Uy_, Uz_, dpdx_, dpdy_, dpdz_;
	std::vector<int> element_, sunode_, marksunode_, edgeout_, edgein_, edgeoutflux_, edgeinflux_,edgeoutfluxh_, edgeinfluxh_, markcycle_;
	std::vector<int> sutriangle_;
	std::vector<double> tracer_, tracer_back_;
	std::vector<std::vector<double>> tracerpair_, wellpairpv_, wellpairflowrate_;
};

#endif
