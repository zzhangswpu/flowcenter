
#ifndef UVLINE_H
#define UVLINE_H

#include<vector>

class UVLINE{
public:
	UVLINE();
	void neighborsurface(int);
	void direction(int);
	int neighborsurface();
	int direction();
	void addinternalnode(int);
	int internalnode(int);
	int numberofinternalnode();
	void endnode(int, int);
	int endnode(int);//endnode(0): u or v =0; endnode(1):u or v = 1
	void neighborline(int, int);
	int neighborline(int); //correspond to endnode
	void markb(int); //mark boundary line or not
	int markb();
	void length(double); //length cannot be obtained from endnodes because uvline can be curved
	double length();


private:
	int neighborsurface_, direction_, markb_;
	int endnode_[2], neighborline_[2];
	std::vector<int> internalnode_;
	double length_;
};








#endif