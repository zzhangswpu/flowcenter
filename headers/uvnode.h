
#ifndef UVNODE_H
#define UVNODE_H

class UVNODE{
public:
	UVNODE();
	void u_origin(double);
	void v_origin(double);
	void u_current(double);
	void v_current(double);
	void u_inter(double);
	void v_inter(double);
	void u_flat(double);
	void v_flat(double);
	double u_origin();
	double v_origin();
	double u_current();
	double v_current();
	double u_inter();
	double v_inter();
	double u_flat();
	double v_flat();

	void x(double);
	void y(double);
	void z(double);
	double x();
	double y();
	double z();
	void markoriginsurface(int);
	int markoriginsurface();

	void flag(int);
	int flag();

private:
	int markoriginsurface_, flag_;
	double u_origin_, v_origin_, u_current_, v_current_, u_inter_, v_inter_, x_, y_, z_, u_flat_, v_flat_;
};











#endif