
/*parasurface illustrate
           v=1 (N or 2)
--------------->--------------
|                            |
|                            |
^  u=0 (W or 3)              ^ u=1 (E or 4)
|                            |
|                            |
--------------->--------------
          v=0 (S or 1)
when refering to direction it is 1, 2, 3, 4; but the numbering of neighbors start from 0;
*/




#ifndef PARASURFACE_H
#define PARASURFACE_H

#include <vector>
#include "uvnode.h"
#include "facet.h"
#include "mathzz.h"
class PARASURFACE{
public:
	PARASURFACE();
	void clear();
	int hid();//marking horizontal or vertical
	void hid(int);
	int markbc();
	void markbc(int);
	int nu();
	void nu(int);
	int nv();
	void nv(int);
	double lengthu(int);
	void lengthu(int, double);
	double lengthv(int);
	void lengthv(int, double);
	int uvnodematrix(int, int);
	void uvnodematrix(int, int, int);
	void buildskeleton();
	void neighbor(int, int); //neighbor(0) is the neighbor on dirction S (or 1) neighbor 0 1 2 3 is s(1), n(2), w(3), e(4)
	int neighbor(int);
	void adduvnode(UVNODE);
	UVNODE uvnode(int);
	UVNODE uvnode(int, int);
	FACET facet(int);
	int numberofuvnodes();
	void skeleton2parametric();
	void skeleton2flat();
	int numberoffacets();
	void buildfacets();
	UVNODE node3d(double, double);
	UVNODE node3d_fromflat(double, double);
	UVNODE node3d_fromxy(double, double);
	std::vector<double> uvforxyz(double, double, double);
	std::vector<double> flatuvforxyz(double, double, double);
private:
	std::vector<std::vector<int>> uvnodematrix_; //structured nodes
	int nu_, nv_, hid_, neighbor_[4], markbc_;
	std::vector<double>lengthu_, lengthv_;
	std::vector<UVNODE> uvnodelist;
	std::vector<FACET> facetlist;
	MATHZZ mathzz_;
};






#endif