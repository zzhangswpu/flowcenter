
#ifndef MATRIX_H
#define MATRIX_H
//for square matrix
#include <iostream>
#include <vector>

class MATRIX{
public:
	MATRIX();
	void size(int);
	int size();
	void entry(int, int, double);
	double entry(int, int);

	void mutiplyrow(int, double);
	void swaprows(int, int);
	void addrowbyrow(int, int, double);
	void clear();


private:
	std::vector<std::vector<double>> matrix_;
};



#endif