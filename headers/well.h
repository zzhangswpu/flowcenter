
#ifndef WELL_H
#define WELL_H
//for wells (pressure-controled and rate-controled)
#include <iostream>
#include <vector>
#include "node.h"


class WELL{
public:
	WELL();
	/*void addnode(NODE);
	int numberofnodes();*/
	void type(int); //type 1 pressure-controled; type 2 rate-controled
	int type();
	void value(double);
	double value();
	/*NODE node(int);*/
	void computelength();
	double length();
	void sign(int); //injector or producer
	int sign();
	void clear();
    std::vector<NODE> nodelist();
	std::vector<NODE> geonodelist();

	void nodelist(std::vector<NODE>);
	void geonodelist(std::vector<NODE>);

private:
	double value_, length_;
	std::vector<NODE> nodelist_, geonodelist_;
	int type_, sign_;

};



#endif