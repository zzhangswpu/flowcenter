
#ifndef FLOWB_H
#define FLOWB_H
//for flow boundaries including wells and flowbsurface; can define inflowboundaries, outflowboundaries, tofb, tracerb

class FLOWB{
public:
	FLOWB();
	void mark(int); //mark number of well or bsurface
	int mark();
	void type(int); //type 1 well 2 bsurface
	int type();

private:
	int type_, mark_;

};


#endif