/************************************************************************************************************************************************
* This software contains mesh generation, flow solver , flow diagnostics and fast marching method.                                              *
* Author: Zhao Zhang, Petroleum Engineering School, Southwest Petroleum University, China                                                       *
* Contact: zzhang6666@163.com                                                                                           *
*************************************************************************************************************************************************/
//***keep it as simple and stupid as possible. No need to be very automatic!!!!

#ifndef REGION_H
#define REGION_H

#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include"tetgen.h"
#include"node.h"
#include"tetrahedron.h"
#include"facet.h"
#include"edge.h"
#include"mathzz.h"
#include"graphedge.h"
#include"graphnode.h"
#include"matrix.h"
#include"samg.h"
#include"well.h"
#include"bsurface.h"
#include"graph.h"
#include"geosurface.h"
#include"curve.h"
#include"segment.h"
#include"modelsurface.h"
#include"uvnode.h"
#include"uvline.h"
#include"meshinfo.h"
#include"cpgelement.h"
#include"skeleton.h"
#include"parasurface.h"
#include"flowb.h"
#include"row.h"
#include"rock.h"
#include"pillar.h"
#include <math.h>
#include "_hypre_utilities.h"
#include "HYPRE_krylov.h"
#include "HYPRE.h"
#include "HYPRE_parcsr_ls.h"
#include <random>
extern "C" {
#include"triangle.h"
}

/*
NOTE: inputs in userinput use mD, cP, bar
      functions for meshing postprocess are for unstructured tetrahedral mesh unless containing "CPG" for CPG
	  1mD=10^-15m^2; 1cp = 0.001pa*s;
*/


class REGION{//function names standard is important
public:
	
	REGION();
//User Input Module
	void userinput_unstructured_poly_SFFMM(const char*); //for unstructured poly SF FMM
	void userinput_unstructured_skt_MF(const char*); //for unstructured skt MF
	void userinput_unstructured_skt_SF(const char*); //for unstructured skt SF
	void userinput_cpg_skt_SF(const char*); //for cpg skt SF
	void userinput_unstructured_poly_SF(const char*); //for unstructured poly SF
	void userinput_upscale(char*, int, int);
	void userinput_unstructured_msh_SF(const char*);
	void setviscosity_o(double);
	void setviscosity_w(double);
	void setct(double);//psi^-1
	void setSiw(double);
	void setdensity_o(double);
	void setdensity_w(double);
	void createrocklist(int);
	void readrock_perm(char*);
	void readrock_porosity(char*);
	void readrock_Siw(char*);
	void readrock_xyz(char*);
	void readproperty_mf(char*);
	void createnoflowbsurfaces(int);
	void setfmmstartwell(int);
	void wellmethod(int);//only used in function:wellconditon
	int wellmethod();
	void readwellgeometry(char*);
	void readwellcondition(char*);
	void readboundarycondition(char*);
	void modifyresolutiontype(int, double);
	void inputsurfacetype(int);
	int inputsurfacetype();
	void numberofphases(int);
	int numberofphases();
	void settetgencommand(char*);
	void settrianglecommand(char*);
	void ftofbutton(int);
	int ftofbutton();
	void btofbutton(int);
	int btofbutton();
	void ftracerbutton(int);
	int ftracerbutton();
	void btracerbutton(int);
	int btracerbutton();
	void vis_regionidbutton(int);
	int vis_regionidbutton();
	void vis_permbutton(int);
	int vis_permbutton();
	void vis_porobutton(int);
	int vis_porobutton();
	void dtofbutton(int);
	int dtofbutton();
	void dtofanabutton(int);
	int dtofanabutton();
	void welltestbutton(int);
	int welltestbutton();
	void zscale(double);
	void graphtracingsign(int);

//CPG All-------------------------------------------------------------------------------------------------------
	void properties_cpgfv();
	void wellcondition_cpgfv(int, int, int);
	
	void computepressure_cpgfv();//steady-state with pressure controls
	void computeflux_cpgfv();
	void steadytemperature();

	void dt2cfl_cpg_tpfa_car(double);
	void updatecpgeleSw_tpfa_car(double);
	void initialcpgSw();
	void cpgSw2mobi();
	void simresult_cpgfv_mf(char*);
	int checkwaterbreak_cpg();

	void cpgtrans_car_sp();
	void cpgtrans_car_temperature();
	void tracingboundarycondition_cpgfv();
	void rescalecpgftof(double);
	void rescalecpgbtof(double);
	void writecornerpointgridGRDECL(char*);//in eclipse CPG format: GRDECL
	void writeresult_cpgfv(char*); //write VTK result for cpg fv
	void writeresult_cpgfv_log2t(char*); //write VTK result for cpg fv
	
	//CPG MESH MODULE
	void buildcpg_parallel();//using (horizontal) parasurfacelist; store CPG like unstructured mesh. works for parallel surfaces
	void buildcpg_ordering(); //use pillars; works for stratigraphical models
	void buildcpg_cut();
	//***CPG mesh post-process for CVFE
	void CPG_postprocessCVFE();
	void buildedgesandedgesunodeCPG();
	void bfacetareaandnormalCPG();
	void buildcarmesh(double, double, double, double, double, double, int, int, int);
	void buildcpgfaces_car();
	void car2tetgen();

	//CPG FLOW DIAGNOSTICS MODULE by FV
	void tofboundarycondition_cpgfv();
	void tracerboundarycondition_cpgfv();
	void buildgraph_cpgfv();
	void reordercpgelements();
	void DFSgraphnode(int);
	void DFSgraphnode_reversed(int);
	void DFSgraphnode_rh(int);
	void computetof_cpgfv_local();
	void computetracer_cpgfv_local();
	void markcpgmaxtracer();
	void derivedquantities_cpgfv();
	void derivedquantities_cpgfv_energy();
	void cpgtracersize(int);
	void cpgtracersize_back(int);
	void cpgreverseflux();
	//CPG FMM
	std::vector<int> fbset0_well_cpg(int, std::vector<int>);
	std::vector<int> fbset0_wells_cpg(std::vector<int>);
	std::vector<int> fbset0_wells_cpg_back(std::vector<int>);
	void markcpgtracer_fmm(std::vector<int>);
	void markcpgtracermax_fmm(std::vector<int>);
	void markcpgtracer_fmm_back(std::vector<int>);
	void markcpgtracermax_fmm_back(std::vector<int>);
	void interpolatecpgdtof2point();
	void computeaveragetof_cpg();
	void computeaveragetofback_cpg();
	void computeaveragetof_cpg_analytical();
	void cpgtofbacksec2year();
	void cpgpascal2psi();

	void cpgflux2fmmv();
	void fmm_cartesian2d_iso();
	void fmm_cartesian3d(std::vector<int>);
	void dfmm_cartesian3d();
	void dfmmdiagnostic_cpg_tof(std::vector<int>);
	void dfmmdiagnostic_cpg_tracer(std::vector<int>, int, std::vector<int>);
	void dfmmdiagnostic_cpg_tof_exact(std::vector<int>);
	void dfmmdiagnostic_cpg_btof_exact(std::vector<int>);
	void dfmmdiagnostic_cpg_btof_exact_reversetoforder(std::vector<int>);
	void dfmmdiagnostic_cpg_tracer_exact(std::vector<int>, int, std::vector<int>);//need modify according to tof_exact
	void dfmmdiagnostic_cpg_tracer_exact_toforder(std::vector<int>, std::vector<int>);
	void dfmmdiagnostic_cpg_btracer_exact_btoforder(std::vector<int>, std::vector<int>);
	void dfmmdiagnostic_cpg_btracer_exact_reversetoforder(std::vector<int>, std::vector<int>);
	void cpgflux_removefacearea_car(double, double, double);
	void inoutflowb_wells(std::vector<int>, std::vector<int>);
	void averagemodelproperties_cpg();
//****For simulation
	void compute_cvfeexupmdnodelist();
	void computeedgefluxhw_cvfeexupmd();//md fw using velocity in fluxh (mdu)
	void computeedgefluxhw_cvfeexupmd_upmdnodelist();

	void computeedgefluxw_cvfeexup1d();//1d fw subface-spu (spu)
	void computeedgefluxw_cvfeexupmd();//md fw using velocity in flux (mdu)
	void computeedgefluxw_cvfeexupmd2();//mdup fw using subface normal in flux (bad mdu)
	void computeedgefluxw_cvfeexupmd3();//mdup fw using velocity in fluxh and cvfeflux in flux (for edge-mpu)
	void computeedgefluxw_cvfeexupmd4();//hybrid mdup fw using velocity and subface-up1d (mpu)
	void writeSwwithx(char*);
	void averageedgelength();
	void initialnodalSw();
	
	void nodalSw2mobi();
	void nodalSw2mobi(int);
	void nodalSw2eleSw();
	void eleSw2mobi();
	
	void nodalmobi2elementalmobi();
	void nodalSwbc();
	void nodeinoutfluxedges_h();
	void nodeinoutfluxedges_c();
	void nodeinoutfluxedges_hmonotone();
	void nodeinoutfluxedges();
	void nodeinoutfluxedges_monotone();
	void updatenodalSw_highorder1(double);
	void updatenodalSw_flux(double);//edge-upwind
	void updatenodalSw_flux_monotone(double);//monotone-edge-upwind
	
	void updatenodalSw_fluxw(double);
	void updatenodalSw_fluxw_edgesdirect(double);
	void updatenodalSw_fluxhybrid(double);
	void updatenodalSw_antiflux_kuzminlimit(double);
	void updatenodalSw_antiflux_kuzminlimit_w(double);
	double cfl2dt_cvfe(double);
	double cfl2dt_tpfa(double);
	void dt2cfl_cvfe(double);
	void dt2cfl_fe(double);
	void dt2cfl_tpfa(double);
	
	void simresult_cvfe_mf(char*);
	
	void computeedgeantiflux();
	void computenodalSwbound();
	void computenodalSwbound_pguide();
	int checkwaterbreak();
	
	double totalSw_car();
	double totalSw();
	void partitionSwshare_5spot_car();
	void partitionSwshare_5spot();//default is cvfetet
	void repairlocalextreme();
	int checksteadystate();
	void setlimitc(double);
	void setlimitg(double);
	void setcar_const_poro(double);
	void setcar_const_perm(double, double, double);
	void settet_const_poro(double);
	void settet_const_perm(double, double, double);
	void setcar_perm_paper2();
	void setcar_perm_paperfmm();
	void setcar_perm_paperdfmm();
	
	void setcar_perm_paperupmd();
	void settet_permporo_paperfmm();
	void setcar_permporo_paperfmm2();
	void setcar_permporo_paperEFMMC();
	void setcar_perm_rand();
	void setcar_perm_rand_paperdfmm();
	void setcar_well_nomodel(int, int, int, int, double);
	void setcar_well_nomodel(int, int, int, double, int, double);
	void setcar_pbc(int, double, int);
	void setcar_perm_spemodel2(char*, int, int, int);
	void setcar_poro_spemodel2(char*, int, int, int);
	
	
	void computeedgeflux_w();
	void computeedgefluxh_w();
	void computeedgefluxh_cvfe_w();
	void computeedgeflux_cvfe_w();
	
	//For Main
	MESHINFO meshinfo;
	void paramodel();
	//void generatemesh();
	void modelpreparation();
	void modelpreparation_upscale();
	void steadystateflowsolver();
	void flowdiagnostics_tracing();
	void forwardtracing();
	void backwardtracing();
	void clearregion();
	void writeresult(char*);
	

//For paper tracing algorithm
	void flowdiagnostics_matrix();
	void flowdiagnostics_ex5spot_findcycle();
	void properties_cvfe_sf_khscaledpaper();
	void properties_cvfe_sf_randompaper();
	void properties_cvfe_sf_paper_cylinder_full();
	void properties_cvfe_sf_paper_cylinder();
	void outputcycles();
	void wellcondition_paper1();
	void testsorting();
	



//IO MODULE---------------------------------------------------------------------------------------------------------------------------------------------
	//***read and write skeleton
	void meshdimension_max_av();
	void meshdimension_max();
	void readskeleton(const char*);
	void writeskeletonVTK_triangle(char*);
	void writeskeletonVTK_structured(char*);
	void writeskeletonVTK_parametric(char*);
	void writeskeletonVTK_flat(char*);
	void writeskeletonVTK_triangle_para(char*);
	
	//***reading volume mesh***
	void readvolumemeshVTK(std::string);//read formal VTK tetrahedral mesh output from Tetgen & calculate element's volume
	void readmarker(std::string);//read point markers from .node file output from Tetgen
	void readflowfield(std::string);
	void readvolumemeshMSH(char*); //read volume mesh in MSH format, output from gmsh
	void readtrifacies(char*); //read trifacies after readvolumemeshVTK
	//***reading and outputing surface mesh***
	void readsurfacemeshPOLY(char*);//read surface mesh in Tetgen's poly format; also for GUI
	void readsurfacemeshVTK(char*);//read surface mesh in VTK format and pass to tetgenio in
	void writesurfacemeshVTK(const char*);//write surface mesh in VTK format; now triangle or quad
	void triangle_outputVTK(const char*);
	void writesurfacemeshPOLY(char*); //write surface mesh in POLY format; now quad
	void writepslgPOLY(char*);
	void writepslgvtk(char*);
	void writepslgvtkxyz(char*);
	//***output volume mesh and result***
	void writevolumemesh(std::string);//write VTK tetrahedral mesh
	void writeresult_cvfe_sf(char*); //write VTK mesh and result for tetrahedral
	void writeresult_cvfe_sf_log2t(char*);//write VTK mesh and result for tetrahedral(logT)
	void writeresult_cvfe_mf(char*);
	void writemeshandproperty_tet(char*);
	void writecornerpointgridVTK(const char*); //in unstructured format
    //***output flow diagnostics info***
	void writeflowdiagnostics(char*);

//PARAMETRIC SURFACE MODULE-----------------------------------------------------------------------------------------------------
	void buildparasurfacefacets_all();
	void skeleton2parametric_all();
	void buildverticalskeleton();
	void parasurfaceneighbours();
	void parasurfaceneighbours_b();
	void reorderhsurfaces();

//UNSTRUCTURED MESH MODULE-----------------------------------------------------------------------------------------------------
	void unstructuredsurfacemesh();
	void buildtriangularsurfacemesh(); //internal intersect only!
	void buildtrisurfacemesh(); //out intersect only (no internal uvline)
	void pslg2triangle();
	void surfacemesh2tetgen(); //pass information in surface mesh to tetgen internally
	void well2tetgen();	
	//***tetrahedralise and build unstructured volume mesh***
	
	void verticalwellrefine();
	void wellgeonodes2addin();
	void calltetgen_in2out();  //call tetgen internally using tetgen.h and tetgen1.5.1-library.lib
	void calltetgen_in2mid();
	void calltetgenrefine_mid2out();
	void calltetgenrefine_mid2out(tetgenio);
	void tetgen2region();
	void tetgen2region_car();
	void volumemesh2surfacemesh();
	//***tetrahedral mesh post-process
	void tetrahedralmesh_postprocessCVFE();
	void bfacetareaandnormal();
	void bsurfacemarkface2node();
	void computeelementalvolume(); //elemental volume for tetrahedral mesh
	void computeelementalfacenormalandarea();
	void nodalcontrolvolumeandelementsunode();
	void bfacetelement();
	void elementsuelement();
	double distancebetweenelements(int, int);
	double distance2elementface(int, int);
	//***mesh post-process for CV based on tetrahedral mesh
	void nodesunode();
	void buildedgesandedgesunode();
	void computeedgelength();


//WELLS
	void welldiscretisation();
	//BOUNDARY CONDITION
	void boundarycondition2node(); //set boundary condition
	void wellcondition2node();
	void markwellnode();
	void markwellnode_paper2();
	void markwellnode_paper2b();
	void markwellnode_paper2c();
	void markwellnode_papermesh();
	void flowb2bsurface();
	void upscalebsurface(int);
	void clearnodesbc();
	void tracingbset();
	void para2internalandnoflowbsurface();

//LINEAR EQUATION SYSTEM SOLVERS-------------------------------------------------------------------------------------------------
	//void tolerance(double, double);
	void callsamg(); //calculate a, ia, ja, f for samg input
	void callsamg_advection();
	void callsamg_advection_tracer();
	//void callagmg();
//UNSTRUCTURED MESH STEADY-STATE FLOW SOLVER MODULE by CVFE----------------------------------------------------------------------------------------------
	void properties_cvfe_sf(); //set k, mu, porosity
	void elementalmobit_sf();		
	void shapefunction(); //calculate shape functions of all nodes in all elements
	void computepressure_cvfe();
	void computevelocity_cvfe_sf(); //get elemental velocity
	//***UNSTRUCTURED MESH MULTIPHASE PROPERTIES
	void properties_cvfe_mf();
	void computecapillarypressure();
	void computesaturation();
	void computerelativepermeabilities();
	void computevelocity_cvfe_mf();
	void computevelocity_cvfe_w();
	void oilinplace();
//UNSTRUCTURED MESH STEADY-STATE FLOW SOLVER MODULE by TPFA
	void computeedgetrans_tpfa_sf();
	void computeedgetrans_tpfa_mf();
	void computeedgetrans_tpfa_mf_nodalperm();
	void computepressure_tpfa();

//UNSTRUCTURED MESH FLOW DIAGNOSTICS MODULE-------------------------------------------------------------------------------------------
	
	void inoutflowb(); //find injectors and producers (wells and bsurface)
	void tracingboundarycondition(); //boundary condition for tracer calculation	
	//**graph operations***
	void integratecycles(int); //build bigcycle based on connected cycles from graphnode i;
	void reordernodes();
	void DFSnode(int);
	void reordernodes_cycle();
	void DFSnode_cycle(int);
	void integratecyclesnode(int);
	//***TOF and tracer calculations***
	void computeedgefluxh_cvfe();
	void computeedgeflux_cvfe();
	void computeedgeflux_edgefd();
	void computetofcvfe_matrix();
	void marknodesmaxtracer();
	void computetracercvfe_matrix();
	void buildgraph_flux();
	void buildgraph_cvfeflux();
	void buildgraph_fluxh_monotone();
	void buildgraph_tpfa();

	void computetofcvfe_local();
	void computetracercvfe_local();
	void derivedquantities_tracer();
	void derivedquantities_both();
	void computeedgesxsysz_and_k();
	void nodeproperty2edge();
	void computeedgeflux_tpfa();
	void computetoftpfa_local();
	void computetracertpfa_local();
	void computeedgeflux_fct();
	void computetoffct_local_globalfct();
	void computetracerfct_local_globalfct();
	void computetoffct_local_localfct();
	void computetracerfct_local_localfct();

//Upscale Module
	void upscaling(int, char*);

//Fast Marching Method MODULE (don't change other functions)--------------------------------------------------------------------
	
	void wellcondition_fmm();
	void writeresult_fmm_cartesian(char*);
	void writeresult_fmm_tet(char*);
	void writedtof(char*);
	void writedtofana(char*);
	void fmm_tet_sed(std::vector<int>); //single edge
	
	void fmm_tet_sedfv();
	void fmm_tet_medmixed();
	void fmm_tet_med_elebased(); //multi edge (3 in 3d) more work
	void fmm_tet_med_nodebased();
	void fmm_tet_fe_nodebased();
	void fmm_tet_fe();
	void fmm_tet_fefirst();
	std::vector<int> fbset0_ele(int);
	std::vector<int> fbset0_node(double, double, double);
	std::vector<int> fbset0_verticalwell(double, double);
	std::vector<int> fbset0_verticalwell_car(double, double);
	void computeedgesxsysz_and_fmmv();
	void computeedgefmmv();
	void computeaveragetof_tet();
	void computeaveragetofback_tet();
	void computeaveragePo_tet();
	
	//new
	void welltestanat_isohomo_fmmpaper2();
	void welltestanat_isohomo_fmmpaper2car();
	void fmm_tet_sed_homov(std::vector<int>); //single edge
	void computeaveragefmmv();
	void computeaveragefmmvcar();
	void averagefmmv2iso();
	void analyticalt_homo_fmmpaperex1();
	void analyticalt_homo_fmmpaperex2();
	void analyticaldtof_homo_fmmpaper2();
	void analyticaldtof_homo_fmmpaper2car();
	void setfmmv_tet_paperfmm1ex2();
	void setpermporo_tet_paperEFMMC();
	void setfmmv_tet_properties();
	void computenodalc_fmm();
	void clearnodalfmminfo();
	void fmm_correction();
	void nodaltofsec2year();
	void nodaltofsec2day();
	void nodalpascal2psi();
//Well test analysis
	void initialnodalPo(double);
	void nodaldtof2t();
	void nodaldtofana2t();
	void computefmmdvt(double, int);
	void computefmmdvt_cpg(double, int);
	void computewelltestderivative_fmmt(double, int, double);
	void computewelltestderivative_fmmcpg(double, int, double);
	void updatenodaldp_fmm(double, double, double, double, int);
	void writefmmdvt(char*);
	void writefmmdvt_cpg(char*);
	void writefmmddp(char*);
	void writefmmddp_cpg(char*);
	void writefmmddpwf_cpg(char*);
	void writefmmdpc(char*); //well nodes pressure drop over time
	void writefmmdpc_cpg(char*);
	void writefmmwellp_homo_semiana(char*);
	void computemaxtof();
	void reordernodebyt();

//*******use public variables for convenience*********************
	//***cartesian grid
	double limit_c, limit_g, dt;
	int car_nx, car_ny, car_nz;
	//***corner-point grid
	std::vector<CPGELEMENT> cpgelementlist;
	std::vector<CPGFACE> cpgfacelist; //removed cpgface because still need to store which face is which cpgface for cpgelements, too complex
	std::vector<PILLAR> pillarlist;
	std::vector<std::vector<std::vector<int>>> cpgnodematrix, cpgelementmatrix;
	//***tetrahedral mesh
	std::vector<NODE> nodelist; //create a vector to store node class and input nodes
	std::vector<TETRAHEDRON>elementlist; //create a vector to store tetrahedron class
	std::vector<FACET>facetlist_input;//input facets list
	std::vector<FACET>bfacetlist; //3d mesh boundary facets	
	//***buttons***
	int linearsolverbutton_, ftofbutton_, dtofbutton_, dtofanabutton_, ftracerbutton_, btofbutton_, btracerbutton_;
	int vis_regionidbutton_, vis_permbutton_, vis_porobutton_, vis_saturationbutton_, temperaturebutton_;
	int meshtype_, numberofphases_, wellmethod_, inputsurfacetype_;
	int trifacesbutton_, welltestbutton_;
	//***properties
	double mu_, mu_o, mu_w, mu_g, rho_o, rho_w, rho_m, Siw_, kk_f, kk_m, cp_w, cp_m;
	double h_fwl, g_, fvf_o, fvf_in; //free water level, gravity, formation volume factor for oil and water;
	double oilreserve_, ct_;//total compressibility
	std::vector<double> dvt, dvt2, dvt3, ddp, ddp2, ddp3, dpc, dpc2, dpc3, dpwf, ddpwf;

private: 

	//***RRM integration***
	double dim_x, dim_y, dim_z;
	//***for paper only
	std::vector<double> nodeedgemark, nodecvalue;
	//***user input***
	std::vector<BSURFACE> flowbsurfacelist;
	std::vector<int> flowbsurfacemarklist;
	
	//***CPU time***
	double time_total1_, time_total2_, time_flow1_, time_flow2_, time_mesh1_, time_mesh2_, time_cv1_, time_cv2_;
    //***tolerance***
	double tol_;	
	
	//***for meshing ***
	double zscale_;
	struct triangulateio trianglein, triangleout, trianglevor;
	char tetgencommand[20], trianglecommand[20];
	std::vector<FACET> facetlist, facetlist_local;
	std::vector<PARASURFACE> parasurfacelist;
	std::vector<GEOSURFACE> geosurfacelist;
	std::vector<CURVE> curvelist;
	std::vector<NODE> surfacenodelist, surfacenodelist_local, regionnodelist;
	std::vector<EDGE> surfaceedgelist, edgelist;
	std::vector<UVNODE> uvnodelist, uvnodelist_out;
	std::vector<SEGMENT> segmentlist, segmentlist_all;
	std::vector<int> hsurfaceid, bsurfaceid;
	int ngeosurface, nsurfacenode, nfacet, nregion, elementneighbormark_, bfacetelementmark_;
	int nbfacet, nedge, nbnode;
	std::vector<int> bnodelist;
	std::vector<std::vector<int>> lpoed; //ordered edge points of a tetrahedron
	std::vector<std::vector<int>> ilfe; //ordered faces around an edge
	std::vector<std::vector<int>> lpofa;//face's nodes
	
	
    //***boundary conditions of facets***
	std::vector<std::vector<long>> bfacetBC;
	std::vector < std::vector<long> > face2tetlist;

    //***for boundary condition surfaces; wells; propertyareas and other userinputs***
	std::vector<BSURFACE> bsurfacelist;
	std::vector<WELL> welllist;
	std::vector<ROCK> rocklist; //global fluid properties, different rock property areas
	//***for upscaling***
	int upscaletype_, upscaledirection_;

	//***tof and tracer***
	int marktoftracersameb_;
	GRAPH graph;
	std::vector<FLOWB> tofblist, tracingblist, inflowblist, outflowblist;
	std::vector<int> markintree, notintreenodes;
	//***flow diagnostics***
	double totalLc;
	std::vector<std::vector<double>> wellpairPV, WAF, Lc;
	std::vector<std::vector<std::vector<double>>> Fc, Sc, t_D, Ev, Fc2, Sc2, t_D2, Ev2, t_D3, Ev3; //flow capacity; storage capacity 
	std::vector<double> sv, dv, totalFc, totalSc, totalt_D, totalEv;
	
	
	

    //***graph traversal***
	std::vector<GRAPHEDGE> graphedgelist;
	std::vector<GRAPHNODE> graphnodelist;
	std::vector<int> bigcycle;
	GRAPHEDGE graphedge_;
	GRAPHNODE graphnode_;
	std::vector<int> graphbeleset, preordering, postordering, reversepostordering, nodeorder;
	std::vector<std::vector<int>> cycles;
	std::vector<int> markinside;
	std::vector<int> graphbnodeset;
	int ngnode, markb_;
    //***tetgen***
	tetgenio in, mid, out, addin; //lower case classes in tetgen.h
	tetgenio::facet *ff;
	tetgenio::polygon *p;
	
    //***compressedrowmatrix***
	double *a, *f, *u;
	int *ja, *ia;

    //***samg input***	
	int nnu, nna, matrix, nsys;
	int *iu, *iscale, *ip;

    //***samg output***
	int ncyc_done, ierr;
	double res_out, res_in;
	
	//***Math operations***
	MATHZZ mathzz_;

	//***FMM***
	std::vector<int> startnodeset_;
	std::vector<double> nodalerrorvec, nodalc;
	int startwellnumber_;
	double fmm_avvx, fmm_avvy, fmm_avvz;

	//***for debugging
	int izz;

	//***Simulation
	std::vector<double> nodalSwmax, nodalSwmin, oldnodalSw;
	std::vector<int> producernodes, producercpgelelist;
	std::vector<int> upmdnodelist;
	std::vector<double> upmdweightinglist;

};























#endif