
#ifndef CPGELEMENT_H
#define CPGELEMENT_H

#include<vector>
#include"node.h"
#include"cpgface.h"


class CPGELEMENT{ //degenerated cpgelement treated as 8 point normal cpgelement
public:
	CPGELEMENT();
	int node(int); //get node 0-7
	void node(int, int); //set node
	int layer();
	void layer(int);

	int status();
	void status(int);

	double volume(); //get volume
	void volume(double); //set volume

	double porosity() const; //get porosity
	void porosity(double); //set porosity

	double k(int, int) const; //get permeability
	void k(int, int, double); //set permeability

	double mu() const; //get mu
	void mu(double); //set mu

	double source(); //volume source (incompressible) inside element that is not elemental boundary
	void source(double);

	void neighbor(int, int);
	int neighbor(int);

	void markwell(int);
	int markwell();

	void markbc(int);
	int markbc();

	void markbc_Sw(int);
	int markbc_Sw();

	void Po(double);
	double Po();

	void mobio(double);
	double mobio();

	void mobiw(double);
	double mobiw();

	void mobit(double);
	double mobit();

	void Sw(double);
	double Sw();

	void centnode(NODE);
	NODE centnode();

	//void cpgface(int, int);
	//int cpgface(int);

	void marktracingb(int); //number of tracer boundary
	int marktracingb();

	void tof(double);
	double tof();

	void tof_back(double);
	double tof_back();

	double tof_total();

	void pbh(double); //bottom hole press
	double pbh();

	double tracer(int); //tracer
	void tracer(int, double);
	void tracersize(int);
	int tracersize();
	int markmaxtracer(); //tracer max mark
	void markmaxtracer(int);

	double tracer_back(int); //tracer
	void tracer_back(int, double);
	void tracersize_back(int);
	int tracersize_back();
	int markmaxtracer_back(); //tracer max mark
	void markmaxtracer_back(int);

	void computetracerpair();
	double tracerpair(int, int);
	void computewellpairPV();
	double wellpairPV(int, int);
	void computewellpairflowrate();
	double wellpairflowrate(int, int);
	void fmmv(int, double);
	double fmmv(int);


	double trans(int);
	void trans(int, double);

	double flux(int);
	void flux(int, double);
	double porevolume();

	void clear();

	double kk; //effective thermal conductivity of rock and fluid
	double thermaltrans[6];
	double temperature;

	double dx, dy, dz;

private:
	int nodelist[8], layer_, markwell_, markbc_, markbc_Sw_, marktracingb_, markmaxtracer_, markmaxtracer_back_, status_;
	double volume_, mu_, porosity_, Po_, source_, tof_, tof_back_, pbh_, Sw_, mobio_, mobiw_, mobit_;
	double k_[3][3], fmmv_[3], trans_[6], flux_[6];
	int neighbor_[6];
	//int cpgface_[6];
	NODE node_;
	std::vector<double> tracer_, tracer_back_;
	std::vector<std::vector<double>> tracerpair_, wellpairpv_, wellpairflowrate_;
};






#endif