

#ifndef CPGFACE_H
#define CPGFACE_H

#include "node.h"

class CPGFACE{
public:
	CPGFACE();
	void area(double);
	double area();
	void centnode(NODE);
	NODE centnode();
	void unitnormalvec(std::vector<double>); //set normal vector (normals by default point to outside of the element)
	std::vector<double> unitnormalvec(); //get normal vector
	void trans(double);
	double trans();


private:
	double area_, trans_;
	NODE node_;
	std::vector<double>unitnormalvec_;


};


#endif