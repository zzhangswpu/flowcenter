
#ifndef ELEMENT_H
#define ELEMENT_H

#include"mathzz.h"
#include <iostream>

class TETRAHEDRON{
public:
	TETRAHEDRON();

	unsigned int node(unsigned int) const; //get node
	void node(unsigned int, unsigned int); //set node

	unsigned int neighbor(unsigned int) const; //get neighbour; neighbour i is opposite to element's node i (i=0-3).
	void neighbor(unsigned int, int); //set neighbour

	double volume() const; //get signed volume (here all ELEMENTs are rhs so volume>0)
	void volume(double); //set signed volume

	double porosity() const; //get porosity
	void porosity(double); //set porosity

	int status();
	void status(int);

	double k(int, int) const; //get permeability
	void k(int, int, double); //set permeability

	double mobit() const;
	void mobit(double);
	double mobio() const;
	void mobio(double);
	double mobiw() const;
	void mobiw(double);
	double fracw();
	void fracw(double);

	double Sw() const;
	void Sw(double);

	double coef() const; //get coef
	void coef(double); //set coef

	int markregion() const; //mark region
	void markregion(int); //mark region

	void Na(unsigned int, double); //set shape function coefficient a
	void Nb(unsigned int, double); //set shape function coefficient b
	void Nc(unsigned int, double); //set shape function coefficient c
	void Nd(unsigned int, double); //set shape function coefficient d

	double Na(unsigned int) const; //get shape function coefficient a
	double Nb(unsigned int) const; //b
	double Nc(unsigned int) const; //c
	double Nd(unsigned int) const; //d

	double Ux_t() const; //oil Darcy velocity
	double Uy_t() const;
	double Uz_t() const;
	void Ux_t(double);
	void Uy_t(double);
	void Uz_t(double);

	double Ux_w() const; //water Darcy velocity
	double Uy_w() const;
	double Uz_w() const;
	void Ux_w(double);
	void Uy_w(double);
	void Uz_w(double);


	int face(unsigned int, unsigned int); //face number (0-3) and face node(0-2)
	int face_local(unsigned int, unsigned int); //face number (0-3) and face local node(0-2)
	double face_normalx(unsigned int);//x coordinate of normal (unit)
	double face_normaly(unsigned int);
	double face_normalz(unsigned int);
	void face_normalx(unsigned int, double);//set x coordinate of unit normal
	void face_normaly(unsigned int, double);//set x coordinate of normal
	void face_normalz(unsigned int, double);//set x coordinate of normal

	void face_area(unsigned int, double); //set face's area
	double face_area(unsigned int); //get face's area

	void fmmv(int, double);
	double fmmv(int);
	/*//for FV
    double face_flux(unsigned int);//facial flux
	void face_flux(unsigned int, double);//set facial flux

	int marktofb(); //tof boundary mark
	void marktofb(int);

	int marktracerb(); //tracer boundary mark
	void marktracerb(int);

	int markmaxtracer(); //tracer max mark
	void markmaxtracer(int);

	double tof() const; //time of flight
	void tof(double);

	double tracer(int) const; //tracer
	void tracer(int, double);
	void tracersize(int);
	int tracersize();

	int face_markfluxcalculated(int);//0 not calculated yet 1 calculated already
	void face_markfluxcalculated(int, int);
	*/



private:
	unsigned int node_[4];
	int neighbor_[4], i, j, markregion_, status_;
	double volume_, mobit_, mobio_, mobiw_, coef_; //initial volume=0
	double k_[3][3], porosity_, Sw_, fracw_, fmmv_[3];
	double a[4], b[4], c[4], d[4], Ux_t_, Uy_t_, Uz_t_, Ux_w_, Uy_w_, Uz_w_; //shape function coefficients and velocity
	double normalx_[4], normaly_[4], normalz_[4], face_area_[4];

	//for FV
	//int marktofb_, marktracerb_, markfluxcalculated_[4], markmaxtracer_;
	//double tof_, flux_[4];
	//std::vector<double> tracer_;
};

#endif