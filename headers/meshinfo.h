
#ifndef MESHINFO_H
#define MESHINFO_H

class MESHINFO{
public:
	MESHINFO();

	int type(); //1 means unstructured tetrahedral; 2 means cpg (Cartesian included);
	void type(int);

	void xmin(double);
	double xmin();
	void xmax(double);
	double xmax();

	void ymin(double);
	double ymin();
	void ymax(double);
	double ymax();

	void zmin(double);
	double zmin();
	void zmax(double);
	double zmax();

	void xminav(double);
	double xminav();
	void xmaxav(double);
	double xmaxav();

	void yminav(double);
	double yminav();
	void ymaxav(double);
	double ymaxav();

	void zminav(double);
	double zminav();
	void zmaxav(double);
	double zmaxav();

	int cpgdimension(int); //0 for x, 1 for y, 2 for z
	void cpgdimension(int, int);//dimension is number of elements per direction per layer

	int naddnodes();
	void naddnodes(int);

	int resolutiontype();
	void resolutiontype(int);

	double guideedgelength();
	void guideedgelength(double);

	int resolutionxnumber();
	void resolutionxnumber(int);

private:
	double xmax_, xmin_, ymax_, ymin_, zmax_, zmin_, xmaxav_, xminav_, ymaxav_, yminav_, zmaxav_, zminav_, guideedgelength_;
	int dimension[3], type_, naddnodes_, resolutiontype_, rxnumber_;
};
















#endif