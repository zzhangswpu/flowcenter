

#include"uvline.h"

UVLINE::UVLINE(){
	neighborline_[0] = -1;
	neighborline_[1] = -1;
}

void UVLINE::neighborsurface(int i){
	neighborsurface_ = i;
}
void UVLINE::direction(int i){
	direction_ = i;
}
int UVLINE::neighborsurface(){
	return(neighborsurface_);
}
int UVLINE::direction(){
	return(direction_);
}
void UVLINE::addinternalnode(int i){
	internalnode_.push_back(i);
}
int UVLINE::internalnode(int i){
	return(internalnode_[i]);
}

int UVLINE::numberofinternalnode(){
	return(internalnode_.size());
}

void UVLINE::endnode(int i, int j){
	endnode_[i] = j;
}

int UVLINE::endnode(int i){
	return(endnode_[i]);
}

void UVLINE::neighborline(int i, int j){
	neighborline_[i] = j;
}

int UVLINE::neighborline(int i){
	return(neighborline_[i]);
}

void UVLINE::markb(int i){
	markb_ = i;
}

int UVLINE::markb(){
	return(markb_);
}

void UVLINE::length(double d){
	length_ = d;
}

double UVLINE::length(){
	return(length_);
}