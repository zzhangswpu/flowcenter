

#include "graphnode.h"

GRAPHNODE::GRAPHNODE(){
	marknodein_ = -1;
	countedgeout_ = -1;

	status_ = 0;
	markmatrix_ = 0;
	markfromgraphb_ = -1;
	//markcycle_ is a vector whose initial size is zero
}

void GRAPHNODE::clear(){
	edgeout_.clear();
	edgein_.clear();
	markcycle_.clear();
	marknodein_ = -1;
	countedgeout_ = -1;
	status_ = 0;
	markmatrix_ = 0;
}

int GRAPHNODE::numberofedgeout(){
	return(edgeout_.size());
}

int GRAPHNODE::numberofedgein(){
	return(edgein_.size());
}

void GRAPHNODE::addedgeout(int i){
	edgeout_.push_back(i);
}

int GRAPHNODE::edgeout(int j){
	return(edgeout_[j]);
}

void GRAPHNODE::addedgein(int i){
	edgein_.push_back(i);
}

int GRAPHNODE::edgein(int j){
	return(edgein_[j]);
}

void GRAPHNODE::marknodein(int i){
	marknodein_ = i;
}
int GRAPHNODE::marknodein(){
	return(marknodein_);
}

void GRAPHNODE::countedgeout(){
	countedgeout_ ++;
}
int GRAPHNODE::printcountedgeout(){
	return(countedgeout_);
}

int GRAPHNODE::status(){
	return(status_);
}

void GRAPHNODE::status(int i){
	status_ = i;
}

std::vector<int> GRAPHNODE::markcycle(){
	return(markcycle_);
}

void GRAPHNODE::markcycle(int i){
	markcycle_.push_back(i);
}


int GRAPHNODE::markmatrix(){
	return(markmatrix_);
}

void GRAPHNODE::markmatrix(int i){
	markmatrix_ = i;
}

int GRAPHNODE::markfromgraphb(){
	return(markfromgraphb_);
}

void GRAPHNODE::markfromgraphb(int i){
	markfromgraphb_ = i;
}
