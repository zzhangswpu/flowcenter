
#include"trifacet.h"

unsigned int TRIFACET::node(unsigned int i) const{
	if (i > 2 || i < 0){
		std::cout << "error get node index of trifacet > 2 or negative"<<std::endl;
		std::exit(EXIT_FAILURE);
	}
	else{
		return (node_[i]);
	}
}

void TRIFACET::node(unsigned int i, unsigned int node_in){ //set facet's node numbers
	if (i > 2 || i < 0){
		std::cout << "error set node index of trifacet > 2 or negative"<<std::endl;
		std::exit(EXIT_FAILURE);
	}
	else{
		node_[i] = node_in;
	}
}

void TRIFACET::markbsurface(int mark_in){
	markbsurface_ = mark_in;
}

int TRIFACET::markbsurface(){
	return (markbsurface_);
}

double TRIFACET::area() const{ // get volume
	return area_;
}

void TRIFACET::area(double v){ // set volume
	area_ = v;
}

double TRIFACET::parametricarea() const{ // get volume
	return area_parametric;
}

void TRIFACET::parametricarea(double v){ // set volume
	area_parametric = v;
}

void TRIFACET::element(long input){
	element_ = input;
}
long TRIFACET::element() const{
	return(element_);
}

void TRIFACET::unitnormalvec(std::vector<double> vec){
	unitnormalvec_ = vec;
}

std::vector<double> TRIFACET::unitnormalvec(){
	return (unitnormalvec_);
}