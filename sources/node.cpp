

#include"node.h"

// for coordinates and mark
NODE::NODE(){
	int i, j;
	markbsurface_ = -1;
	markwell_ = -1;
	marktracingb_ = -1;
	markbc_ = -2;
	markbc_Sw_ = 0; //no saturation bc
	rho_ = 0.0;
	Sw_ = 0;
	/*mu_w_ = 0.0;
	mu_o_ = 0.0;*/
	Po_ = 0.0;
	Pc_ = 0;
	/*Ux_ = 0;
	Uy_ = 0;
	Uz_ = 0;
	dpdx_=0;
	dpdy_=0;
	dpdz_=0;
	*/
	source_ = 0.0;
	tof_ = 0; //not reached
	tof_back_ = 0;
	dualvolume_ = 0;
	porosity_ = 0;
	status_ = 0;
	marknodein_ = -1;
	for (i = 0; i < 3; i++){
		for (j = 0; j < 3; j++){
			k_[i][j] = 0.0;
		}
	}
	influx_ = 0;
	mobio_ = 0;
	mobiw_ = 0;
}

void NODE::clearedgeinflux(){
	edgeinflux_.clear();
}

void NODE::clearedgeoutflux(){
	edgeoutflux_.clear();
}

void NODE::clearedgeinfluxh(){
	edgeinfluxh_.clear();
}

void NODE::clearedgeoutfluxh(){
	edgeoutfluxh_.clear();
}

double NODE::x() const{
	return (x_);
}

double NODE::y() const{
	return (y_);
}

double NODE::z() const{
	return (z_);
}

void NODE::x(double x_in){
	x_ = x_in;
}

void NODE::y(double y_in){
	y_ = y_in;
}

void NODE::z(double z_in){
	z_ = z_in;
}

double NODE::coef() const{
	return (coef_);
}

double NODE::Po() const{
	return (Po_);
}
//double NODE::Pw() const{
//	return (Po_);//neglect Pc_
//}

double NODE::Pw() const{
	return (Po_ - Pc_);
}

double NODE::Pc() const{
	return(Pc_);
}

void NODE::Pc(double d){
	Pc_ = d;
}

double NODE::Sw() const{
	return(Sw_);
}

void NODE::Sw(double d){
	Sw_ = d;
}

double NODE::mobiw() const{
	return(mobiw_);
}

void NODE::mobiw(double d){
	mobiw_ = d;
}

double NODE::mobio() const{
	return(mobio_);
}

void NODE::mobio(double d){
	mobio_ = d;
}

double NODE::mobit(){
	return (mobit_);
}

void NODE::mobit(double d){
	mobit_ = d;
}

double NODE::fracw(){
	return (fracw_);
}

void NODE::fracw(double d){
	fracw_ = d;
}

double NODE::source() const{
	return (source_);
}

//double NODE::mu_w() const{
//	return (mu_w_);
//}
//
//void NODE::mu_w(double input){
//	mu_w_ = input;
//}
//
//double NODE::mu_o() const{
//	return (mu_o_);
//}
//
//void NODE::mu_o(double input){
//	mu_o_ = input;
//}

double NODE::porosity() const{ // time of flight
	return (porosity_);
}

void NODE::porosity(double input){
	porosity_ = input;
}

double NODE::tof() const{ // time of flight
	return (tof_);
}

double NODE::tof_back() const{ // time of flight
	return (tof_back_);
}

double NODE::tof_total() const{ // time of flight
	return (tof_ + tof_back_);
}

void NODE::tof(double input){
	tof_ = input;
}

void NODE::tof_back(double input){
	tof_back_ = input;
}

double NODE::tracer(int i) const { // tracer vector
	return (tracer_[i]);
}

void NODE::tracer(int i, double input) { //same order as the tracer boundaries
	tracer_[i] = input;
}

void NODE::tracersize(int i) {
	tracer_.resize(i);
}

int NODE::tracersize() {
	return(tracer_.size());
}


double NODE::tracer_back(int i) const { // tracer vector
	return (tracer_back_[i]);
}

void NODE::tracer_back(int i, double input) { //same order as the tracer boundaries
	tracer_back_[i] = input;
}

void NODE::tracersize_back(int i) {
	tracer_back_.resize(i);
}

int NODE::tracersize_back() {
	return(tracer_back_.size());
}

double NODE::k(int i, int j){ // get permeability
	return k_[i][j];
}

void NODE::k(int i, int j, double input){ // set permeability
	k_[i][j] = input;
}

void NODE::coef(double input){
	coef_ = input;
}

void NODE::Po(double input){
	Po_ = input;
}

void NODE::source(double input){
	source_ = input;
}

void NODE::markbsurface(int mark_in){
	markbsurface_ = mark_in;
}

int NODE::markbsurface(){
	return (markbsurface_);
}


void NODE::marktracingb(int mark_in){
	marktracingb_ = mark_in;
}

int NODE::marktracingb(){
	return (marktracingb_);
}

void NODE::markwell(int mark_in){
	markwell_ = mark_in;
}

int NODE::markwell(){
	return (markwell_);
}

int NODE::markmaxtracer() {
	return(markmaxtracer_);

}
void NODE::markmaxtracer(int i) {
	markmaxtracer_ = i;
}

int NODE::markmaxtracer_back() {
	return(markmaxtracer_back_);

}
void NODE::markmaxtracer_back(int i) {
	markmaxtracer_back_ = i;
}

void NODE::markbc(int mark_in){
	markbc_ = mark_in;
}

int NODE::markbc(){
	return (markbc_);
}

void NODE::markbc_Sw(int mark_in){
	markbc_Sw_ = mark_in;
}

int NODE::markbc_Sw(){
	return (markbc_Sw_);
}

//double NODE::Ux() const{
//	return (Ux_);
//}
//
//double NODE::Uy() const{
//	return (Uy_);
//}
//
//double NODE::Uz() const{
//	return (Uz_);
//}
//
//void NODE::Ux(double input){
//	Ux_ = input;
//}
//
//void NODE::Uy(double input){
//	Uy_ = input;
//}
//
//void NODE::Uz(double input){
//	Uz_ = input;
//}
//
//double NODE::dpdx() const{
//	return (dpdx_);
//}
//
//double NODE::dpdy() const{
//	return (dpdy_);
//}
//
//double NODE::dpdz() const{
//	return (dpdz_);
//}
//
//void NODE::dpdx(double input){
//	dpdx_ = input;
//}
//
//void NODE::dpdy(double input){
//	dpdy_ = input;
//}
//
//void NODE::dpdz(double input){
//	dpdz_ = input;
//}

void NODE::dualvolume(double a){
	dualvolume_=a;
}

double NODE::dualvolume() {
	return (dualvolume_);
}

void NODE::addelement(int ie){
	element_.push_back(ie);
}
int NODE::element(int i){
	return(element_[i]);
}
int NODE::numberofelement(){
	return(element_.size());
}

void NODE::addsunode(int i){
	sunode_.push_back(i);
}

int NODE::sunode(int i){
	return(sunode_[i]);
}

int NODE::numberofsunode(){
	return(sunode_.size());
}

void NODE::addmarksunode(int i){
	marksunode_.push_back(i);
}

int NODE::marksunode(int i){
	return(marksunode_[i]);
}

void NODE::marksunode(int i, int j){
	marksunode_[i] = j;
}

void NODE::addsutriangle(int i){
	sutriangle_.push_back(i);
}

int NODE::sutriangle(int i){
	return(sutriangle_[i]);
}

int NODE::numberofsutriangle(){
	return(sutriangle_.size());
}

void NODE::addedgeout(int i){
	edgeout_.push_back(i);
}
int NODE::edgeout(int i){
	return(edgeout_[i]);
}
int NODE::numberofedgeout(){
	return(edgeout_.size());
}

void NODE::addedgein(int i){
	edgein_.push_back(i);
}
int NODE::edgein(int i){
	return(edgein_[i]);
}
int NODE::numberofedgein(){
	return(edgein_.size());
}

void NODE::addedgeoutflux(int i){
	edgeoutflux_.push_back(i);
}
int NODE::edgeoutflux(int i){
	return(edgeoutflux_[i]);
}
int NODE::numberofedgeoutflux(){
	return(edgeoutflux_.size());
}

void NODE::addedgeinflux(int i){
	edgeinflux_.push_back(i);
}
int NODE::edgeinflux(int i){
	return(edgeinflux_[i]);
}
int NODE::numberofedgeinflux(){
	return(edgeinflux_.size());
}

void NODE::addedgeoutfluxh(int i){
	edgeoutfluxh_.push_back(i);
}
int NODE::edgeoutfluxh(int i){
	return(edgeoutfluxh_[i]);
}
int NODE::numberofedgeoutfluxh(){
	return(edgeoutfluxh_.size());
}

void NODE::addedgeinfluxh(int i){
	edgeinfluxh_.push_back(i);
}
int NODE::edgeinfluxh(int i){
	return(edgeinfluxh_[i]);
}
int NODE::numberofedgeinfluxh(){
	return(edgeinfluxh_.size());
}


void NODE::cleargraphinfo(){
	edgeinflux_.clear();
	edgeoutflux_.clear();
	edgeoutfluxh_.clear();
	edgeinfluxh_.clear();
	status_ = 0;
	marknodein_ = -1;
	markcycle_.clear();
}

int NODE::status(){
	return(status_);
}

void NODE::status(int i){
	status_ = i;
}

void NODE::marknodein(int i){
	marknodein_ = i;
}
int NODE::marknodein(){
	return(marknodein_);
}

std::vector<int> NODE::markcycle(){
	return(markcycle_);
}

void NODE::markcycle(int i){
	markcycle_.push_back(i);
}

void NODE::computetracerpair(){
	int i, j;
	tracerpair_.resize(tracer_.size());
	for (i = 0; i < tracer_.size(); i++){
		tracerpair_[i].resize(tracer_back_.size());
	}
	for (i = 0; i < tracer_.size(); i++){
		for (j = 0; j < tracer_back_.size(); j++){
			tracerpair_[i][j] = tracer_[i] * tracer_back_[j];
		}
	}
}

double NODE::tracerpair(int i, int j){
	return(tracerpair_[i][j]);
}

void NODE::computewellpairPV(){
	int i, j;
	wellpairpv_.resize(tracer_.size());
	for (i = 0; i < tracer_.size(); i++){
		wellpairpv_[i].resize(tracer_back_.size());
	}
	for (i = 0; i < tracer_.size(); i++){
		for (j = 0; j < tracer_back_.size(); j++){
			wellpairpv_[i][j] = tracerpair_[i][j]*porosity_*dualvolume_;
		}
	}

}

double NODE::wellpairPV(int i, int j){
	return(wellpairpv_[i][j]);
}

void NODE::computewellpairflowrate(){
	int i, j;
	wellpairflowrate_.resize(tracer_.size());
	for (i = 0; i < tracer_.size(); i++){
		wellpairflowrate_[i].resize(tracer_back_.size());
	}
	for (i = 0; i < tracer_.size(); i++){
		for (j = 0; j < tracer_back_.size(); j++){
			wellpairflowrate_[i][j] = wellpairpv_[i][j]/tof_total();
		}
	}
}

double NODE::wellpairflowrate(int i, int j){
	return(wellpairflowrate_[i][j]);
}

void NODE::influx(double a){
	influx_ = a;
}

double NODE::influx(){
	return(influx_);
}