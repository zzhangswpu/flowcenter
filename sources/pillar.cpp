

#include "pillar.h"

PILLAR::PILLAR(){

}

void PILLAR::initialise(int i){
	pillarnodelist_.resize(i);
	pillarmarkerlist_.resize(i);
}

int PILLAR::numberofnodes(){
	return(pillarnodelist_.size());
}

int PILLAR::pillarnode(int i){
	return(pillarnodelist_[i]);
}

void PILLAR::pillarnode(int i, int j){
	pillarnodelist_[i] = j;
}

int PILLAR::pillarmarker(int i){
	return(pillarmarkerlist_[i]);
}

void PILLAR::pillarmarker(int i, int j){
	pillarmarkerlist_[i] = j;
}

double PILLAR::x(){
	return(x_);
}

double PILLAR::y(){
	return(y_);
}

void PILLAR::x(double d){
	x_ = d;
}

void PILLAR::y(double d){
	y_ = d;
}

void PILLAR::addsuelement(int i){
	suelement_.push_back(i);
}

std::vector<int> PILLAR::suelement(){
	return suelement_;
}