#include"cpgface.h"

CPGFACE::CPGFACE(){
	area_ = 0;
}

void CPGFACE::area(double d){
	area_ = d;
}

double CPGFACE::area(){
	return(area_);
}

void CPGFACE::centnode(NODE n){
	node_ = n;
}

NODE CPGFACE::centnode(){
	return(node_);
}

void CPGFACE::unitnormalvec(std::vector<double> vec){
	unitnormalvec_ = vec;
}

std::vector<double> CPGFACE::unitnormalvec(){
	return (unitnormalvec_);
}


void CPGFACE::trans(double d){
	trans_ = d;
}

double CPGFACE::trans(){
	return(trans_);
}