
#include"cpgelement.h"

CPGELEMENT::CPGELEMENT(){
	int i, j;
	status_ = 0;
	tof_ = 0; //not reached
	tof_back_ = 0;
	volume_ = 0;
	mu_ = 0;
	porosity_ = 1.0;
	Po_ = 0;
	Sw_ = 0;
	for (i = 0; i < 3; i++){
		fmmv_[i] = 0;
		for (j = 0; j < 3; j++){
			k_[i][j] = 0.0;
		}
	}
	for (i = 0; i < 6; i++){
		neighbor_[i] = -100;
		flux_[i] = 0;
		trans_[i] = 0;
	}
	for (i = 0; i < 8; i++){
		nodelist[i]=-1;
	}
	markwell_ = -1;

	marktracingb_ = -1;
	markbc_ = -2;
	markbc_Sw_ = -1;
}

void CPGELEMENT::clear(){
	int i, j;
	volume_ = 0;
	mu_ = 0;
	markbc_Sw_ = -1;
	porosity_ = 1.0;
	Po_ = 0;
	for (i = 0; i < 3; i++){
		for (j = 0; j < 3; j++){
			k_[i][j] = 0.0;
		}
	}
	for (i = 0; i < 6; i++){
		neighbor_[i] = -100;
		flux_[i] = 0;
		trans_[i] = 0;
	}
	for (i = 0; i < 8; i++){
		nodelist[i] = -1;
	}
}

double CPGELEMENT::porevolume(){
	return porosity_*volume_;
}

double CPGELEMENT::trans(int i){
	return trans_[i];
}

void CPGELEMENT::trans(int i, double d){
	trans_[i] = d;
}

double CPGELEMENT::flux(int i){
	return flux_[i];
}

void CPGELEMENT::flux(int i, double d){
	flux_[i] = d;
}

int CPGELEMENT::node(int i){
	return(nodelist[i]);
}

void CPGELEMENT::node(int i, int inode){
	nodelist[i] = inode;
}

int CPGELEMENT::layer(){
	return(layer_);
}

void CPGELEMENT::layer(int i){
	layer_ = i;
}

int CPGELEMENT::status(){
	return(status_);
}

void CPGELEMENT::status(int i){
	status_ = i;
}

double CPGELEMENT::porosity() const{ // get porosity
	return porosity_;
}

void CPGELEMENT::porosity(double v){ // set porosity
	porosity_ = v;
}

double CPGELEMENT::k(int i, int j) const{ // get permeability
	return k_[i][j];
}

void CPGELEMENT::k(int i, int j, double input){ // set permeability
	k_[i][j] = input;
}

double CPGELEMENT::mu() const{ // get viscosity for calculation
	return mu_;
}

void CPGELEMENT::mu(double input){ // set viscosity
	mu_ = input;
}

double CPGELEMENT::source(){
	return source_;
}

void CPGELEMENT::source(double input){
	source_ = input;
}

double CPGELEMENT::pbh(){
	return pbh_;
}

void CPGELEMENT::pbh(double input){
	pbh_ = input;
}

void CPGELEMENT::neighbor(int i, int j){
	neighbor_[i] = j;
}

int CPGELEMENT::neighbor(int i){
	return(neighbor_[i]);
}


void CPGELEMENT::markwell(int i){
	markwell_ = i;
}

int CPGELEMENT::markwell(){
	return(markwell_);
}

void CPGELEMENT::markbc(int i){
	markbc_ = i;
}

int CPGELEMENT::markbc(){
	return(markbc_);
}

void CPGELEMENT::markbc_Sw(int i){
	markbc_Sw_ = i;
}

int CPGELEMENT::markbc_Sw(){
	return(markbc_Sw_);
}

void CPGELEMENT::Po(double d){
	Po_ = d;
}

double CPGELEMENT::Po(){
	return(Po_);
}

void CPGELEMENT::mobio(double d){
	mobio_ = d;
}

double CPGELEMENT::mobio(){
	return(mobio_);
}

void CPGELEMENT::mobiw(double d){
	mobiw_ = d;
}

double CPGELEMENT::mobiw(){
	return(mobiw_);
}

void CPGELEMENT::mobit(double d){
	mobit_ = d;
}

double CPGELEMENT::mobit(){
	return(mobit_);
}

void CPGELEMENT::Sw(double d){
	Sw_ = d;
}

double CPGELEMENT::Sw(){
	return(Sw_);
}

void CPGELEMENT::volume(double d){
	volume_ = d;
}

double CPGELEMENT::volume(){
	return(volume_);
}

void CPGELEMENT::centnode(NODE n){
	node_ = n;
}

NODE CPGELEMENT::centnode(){
	return(node_);
}

void CPGELEMENT::marktracingb(int i){
	marktracingb_ = i;
}

int CPGELEMENT::marktracingb(){
	return(marktracingb_);
}

void CPGELEMENT::tof(double d){
	tof_ = d;
}

double CPGELEMENT::tof(){
	return(tof_);
}

void CPGELEMENT::tof_back(double d){
	tof_back_ = d;
}

double CPGELEMENT::tof_back(){
	return(tof_back_);
}

double CPGELEMENT::tof_total(){
	return(tof_ + tof_back_);
}

double CPGELEMENT::tracer(int i) { // tracer vector
	return (tracer_[i]);
}

void CPGELEMENT::tracer(int i, double input) { //same order as the tracer boundaries
	tracer_[i] = input;
}

void CPGELEMENT::tracersize(int i) {
	tracer_.resize(i);
}

int CPGELEMENT::tracersize() {
	return(tracer_.size());
}

void CPGELEMENT::markmaxtracer(int i){
	markmaxtracer_ = i;
}

int CPGELEMENT::markmaxtracer(){
	return(markmaxtracer_);
}


double CPGELEMENT::tracer_back(int i) { // backward tracer vector
	return (tracer_back_[i]);
}

void CPGELEMENT::tracer_back(int i, double input) { //same order as the tracer boundaries
	tracer_back_[i] = input;
}

void CPGELEMENT::tracersize_back(int i) {
	tracer_back_.resize(i);
}

int CPGELEMENT::tracersize_back() {
	return(tracer_back_.size());
}

void CPGELEMENT::markmaxtracer_back(int i){
	markmaxtracer_back_ = i;
}

int CPGELEMENT::markmaxtracer_back(){
	return(markmaxtracer_back_);
}

void CPGELEMENT::computetracerpair(){
	int i, j;
	tracerpair_.clear();
	tracerpair_.resize(tracer_.size());
	for (i = 0; i < tracer_.size(); i++){
		tracerpair_[i].resize(tracer_back_.size());
	}
	for (i = 0; i < tracer_.size(); i++){
		for (j = 0; j < tracer_back_.size(); j++){
			tracerpair_[i][j] = tracer_[i] * tracer_back_[j];
		}
	}
}
double CPGELEMENT::tracerpair(int i, int j){
	return(tracerpair_[i][j]);
}

void CPGELEMENT::fmmv(int i, double d){
	fmmv_[i] = d;
}

double CPGELEMENT::fmmv(int i){
	return fmmv_[i];
}

void CPGELEMENT::computewellpairPV(){
	int i, j;
	wellpairpv_.clear();
	wellpairpv_.resize(tracer_.size());
	for (i = 0; i < tracer_.size(); i++){
		wellpairpv_[i].resize(tracer_back_.size());
	}
	for (i = 0; i < tracer_.size(); i++){
		for (j = 0; j < tracer_back_.size(); j++){
			wellpairpv_[i][j] = tracerpair_[i][j] * porosity_*volume_;
		}
	}
}


double CPGELEMENT::wellpairPV(int i, int j){
	return(wellpairpv_[i][j]);
}

void CPGELEMENT::computewellpairflowrate(){
	int i, j;
	wellpairflowrate_.resize(tracer_.size());
	for (i = 0; i < tracer_.size(); i++){
		wellpairflowrate_[i].resize(tracer_back_.size());
	}
	for (i = 0; i < tracer_.size(); i++){
		for (j = 0; j < tracer_back_.size(); j++){
			wellpairflowrate_[i][j] = wellpairpv_[i][j] / tof_total();//if wellpairpv_ is energy[J], then flowrate is power.
		}
	}
}

double CPGELEMENT::wellpairflowrate(int i, int j){
	return(wellpairflowrate_[i][j]);
}