

#include "graph.h"

GRAPH::GRAPH(){
	tracingsign_ = 0;
	ntracingb_ = 0;
}

void GRAPH::clear(){
	tracingsign_ = 0;
	ntracingb_ = 0;
}

void GRAPH::tracingsign(int a){
	tracingsign_ = a;
}

int GRAPH::tracingsign(){
	return(tracingsign_);
}

void GRAPH::ntracingb(int a){
	ntracingb_ = a;
}

int GRAPH::ntracingb(){
	return(ntracingb_);
}