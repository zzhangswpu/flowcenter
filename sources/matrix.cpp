

#include "matrix.h"

MATRIX::MATRIX(){

}


void MATRIX::size(int size){
	int i;
	matrix_.resize(size);
	for (i = 0; i < size; i++){
		matrix_[i].resize(size);
	}
}

int MATRIX::size(){
	return(matrix_.size());
}

void MATRIX::entry(int i, int j, double d){
	if (i<0 || j<0 || i>matrix_.size() - 1 || j>matrix_.size() - 1){
		std::cout << "wrong matrix index" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	matrix_[i][j] = d;
}

double MATRIX::entry(int i, int j){
	if (i<0 || j<0 || i>matrix_.size() - 1 || j>matrix_.size() - 1){
		std::cout << "wrong matrix index" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	return(matrix_[i][j]);
}

void MATRIX::mutiplyrow(int i, double d){
	int j;
	for (j = 0; j < matrix_[i].size(); j++){
		matrix_[i][j] = matrix_[i][j] * d;
	}
}

void MATRIX::swaprows(int r1, int r2){
	std::vector<double> cache = matrix_[r1];
	matrix_[r1] = matrix_[r2];
	matrix_[r2] = cache;
}

void MATRIX::addrowbyrow(int r1, int r2, double d){
	int i;
	for (i = 0; i < matrix_[r1].size(); i++){
		matrix_[r1][i] = matrix_[r1][i] + matrix_[r2][i] * d;
	}
}

void MATRIX::clear(){
	matrix_.clear();
}

