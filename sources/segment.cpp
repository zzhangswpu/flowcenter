


#include "segment.h"

int SEGMENT::node(int i){
	return (node_[i]);
}

void SEGMENT::node(int i, int node_in){
	if (i > 1 || i < 0){
		std::cout << "set node index i out of range " << i << std::endl;
		std::exit(EXIT_FAILURE);
	}
	node_[i] = node_in;
}