

#include"facet.h"

unsigned int FACET::node(unsigned int i) const{
	return (node_[i]);
}

void FACET::node(unsigned int i, unsigned int node_in){ //set facet's node numbers
	node_[i] = node_in;
}

unsigned int FACET::numberofvertex() const{
	return (numberofvertex_);
}

void FACET::numberofvertex(unsigned int input){
	numberofvertex_ = input;
}

void FACET::markbsurface(int mark_in){
	markbsurface_ = mark_in;
}

int FACET::markbsurface(){
	return (markbsurface_);
}


void FACET::clear(){
	markbsurface_ = 0;
	numberofvertex_ = 0;
	area_ = 0;
}

double FACET::area(){
	return(area_);
}

void FACET::area(double d){
	area_ = d;
}

double FACET::flatarea() const{
	return(area_flat);
}

void FACET::flatarea(double d){
	area_flat = d;
}

double FACET::parametricarea() const{ // get volume
	return area_parametric;
}

void FACET::parametricarea(double v){ // set volume
	area_parametric = v;
}

void FACET::element(long input){
	element_ = input;
}
long FACET::element() const{
	return(element_);
}

void FACET::unitnormalvec(std::vector<double> vec){
	unitnormalvec_ = vec;
}

std::vector<double> FACET::unitnormalvec(){
	return (unitnormalvec_);
}