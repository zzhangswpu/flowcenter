
#include"parasurface.h"

PARASURFACE::PARASURFACE(){
	int i;
	for (i = 0; i < 4; i++){
		neighbor_[i] = -1;
	}
	hid_ = -1;
}

int PARASURFACE::hid(){
	return(hid_);
}

void PARASURFACE::hid(int i){
	hid_ = i;
}

int PARASURFACE::markbc() {
	return(markbc_);
}

void PARASURFACE::markbc(int i) {
	markbc_ = i;
}


int PARASURFACE::nu(){
	return(nu_);
}

int PARASURFACE::nv(){
	return(nv_);
}

void PARASURFACE::nu(int i){
	nu_ = i;
}

void PARASURFACE::nv(int i){
	nv_ = i;
}

double PARASURFACE::lengthu(int i){
	return(lengthu_[i]);
}

double PARASURFACE::lengthv(int i){
	return(lengthv_[i]);
}

void PARASURFACE::lengthu(int i, double d){
	lengthu_[i] = d;
}

void PARASURFACE::lengthv(int i, double d){
	lengthv_[i] = d;
}

void PARASURFACE::buildskeleton(){
	int i;
	uvnodematrix_.resize(nu_);
	for (i = 0; i < nu_; i++){
		uvnodematrix_[i].resize(nv_);
	}
	lengthu_.resize(nv_);
	lengthv_.resize(nu_);
}

void PARASURFACE::uvnodematrix(int i, int j, int nn){
	uvnodematrix_[i][j] = nn;
}

int PARASURFACE::uvnodematrix(int i, int j){
	return(uvnodematrix_[i][j]);
}

void PARASURFACE::adduvnode(UVNODE n) {
	uvnodelist.push_back(n);
}

UVNODE PARASURFACE::uvnode(int i) {
	return(uvnodelist[i]);
}

UVNODE PARASURFACE::uvnode(int i, int j) {
	int k = uvnodematrix(i, j);
	return(uvnodelist[k]);
}

FACET PARASURFACE::facet(int i) {
	return(facetlist[i]);
}

void PARASURFACE::neighbor(int i, int n){
	neighbor_[i] = n;
}

int PARASURFACE::neighbor(int i){
	return(neighbor_[i]);
}

int PARASURFACE::numberofuvnodes() {
	return(uvnodelist.size());
}

int PARASURFACE::numberoffacets() {
	return(facetlist.size());
}


void PARASURFACE::skeleton2flat() {
	int i, j, k, inode3, inode1, inode2;
	UVNODE n1, n2, n3;
	double lengthvec[3], u_, v_, lu, lv;

	//for u
	for (j = 0; j < nv_; j++) {
		lu = 0;
		for (i = 1; i < nu_; i++) { //nodes from left to right
			n1 = uvnode(i - 1, j);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();
			lu = lu + mathzz_.vectorlength(lengthvec);
		}
		lengthu(j, lu);
		inode3 = uvnodematrix(0, j);
		uvnodelist[inode3].u_flat(0);
		for (i = 1; i < nu_; i++) {
			n1 = uvnode(i - 1, j);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();

			u_ = mathzz_.vectorlength(lengthvec) + n1.u_flat(); //another parametric way but need to seal surface contacts
			inode2 = uvnodematrix(i, j);
			uvnodelist[inode2].u_flat(u_);
		}
	}
	//for v
	for (i = 0; i < nu_; i++) {
		lv = 0;
		for (j = 1; j < nv_; j++) { //nodes from front to back
			n1 = uvnode(i, j - 1);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();
			lv = lv + mathzz_.vectorlength(lengthvec);
		}
		lengthv(i, lv);
		inode3 = uvnodematrix(i, 0);
		uvnodelist[inode3].v_flat(0);
		for (j = 1; j < nv_; j++) {
			n1 = uvnode(i, j - 1);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();

			v_ = mathzz_.vectorlength(lengthvec) + n1.v_flat();
			inode2 = uvnodematrix(i, j);
			uvnodelist[inode2].v_flat(v_);
		}
	}
}

void PARASURFACE::skeleton2parametric() {
	int i, j, k, inode3, inode1, inode2;
	UVNODE n1, n2, n3;
	double lengthvec[3], u_, v_, lu, lv;

	//for u
	for (j = 0; j < nv_; j++) {
		lu = 0;
		for (i = 1; i < nu_; i++) { //nodes from left to right
			n1 = uvnode(i - 1, j);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();
			lu = lu + mathzz_.vectorlength(lengthvec);
		}
		lengthu(j, lu);
		inode3 = uvnodematrix(0, j);
		uvnodelist[inode3].u_origin(0);
		for (i = 1; i < nu_; i++) {
			n1 = uvnode(i - 1, j);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();
			u_ = mathzz_.vectorlength(lengthvec) / lengthu(j) + n1.u_origin();
			inode2 = uvnodematrix(i, j);
			uvnodelist[inode2].u_origin(u_);
		}
	}
	//for v
	for (i = 0; i < nu_; i++) {
		lv = 0;
		for (j = 1; j < nv_; j++) { //nodes from front to back
			n1 = uvnode(i, j - 1);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();
			lv = lv + mathzz_.vectorlength(lengthvec);
		}
		lengthv(i, lv);
		inode3 = uvnodematrix(i, 0);
		uvnodelist[inode3].v_origin(0);
		for (j = 1; j < nv_; j++) {
			n1 = uvnode(i, j - 1);
			n2 = uvnode(i, j);
			lengthvec[0] = n2.x() - n1.x();
			lengthvec[1] = n2.y() - n1.y();
			lengthvec[2] = n2.z() - n1.z();
			v_ = mathzz_.vectorlength(lengthvec) / lengthv(i) + n1.v_origin();
			inode2 = uvnodematrix(i, j);
			uvnodelist[inode2].v_origin(v_);
		}
	}
}

void PARASURFACE::buildfacets() {
	int i, j, n00, n[4];
	double x[3], y[3], z[3];
	FACET bfacet_;

	for (j = 0; j < nv_ - 1; j++) {
		for (i = 0; i < nu_ - 1; i++) {
			n[0] = uvnodematrix(i, j); //cycle anticlock
			n[1] = uvnodematrix(i + 1, j);
			n[2] = uvnodematrix(i + 1, j + 1);
			n[3] = uvnodematrix(i, j + 1);
			//first triangle
			bfacet_.node(0, n[0]);
			bfacet_.node(1, n[1]);
			bfacet_.node(2, n[2]);
			x[0] = uvnodelist[n[0]].u_origin();
			y[0] = uvnodelist[n[0]].v_origin();
			z[0] = 0;
			x[1] = uvnodelist[n[1]].u_origin();
			y[1] = uvnodelist[n[1]].v_origin();
			z[1] = 0;
			x[2] = uvnodelist[n[2]].u_origin();
			y[2] = uvnodelist[n[2]].v_origin();
			z[2] = 0;
			bfacet_.parametricarea(mathzz_.area_triangle(x, y, z));//parametric area
			x[0] = uvnodelist[n[0]].u_flat();
			y[0] = uvnodelist[n[0]].v_flat();
			z[0] = 0;
			x[1] = uvnodelist[n[1]].u_flat();
			y[1] = uvnodelist[n[1]].v_flat();
			z[1] = 0;
			x[2] = uvnodelist[n[2]].u_flat();
			y[2] = uvnodelist[n[2]].v_flat();
			z[2] = 0;
			bfacet_.flatarea(mathzz_.area_triangle(x, y, z));//flat area
			x[0] = uvnodelist[n[0]].x();
			y[0] = uvnodelist[n[0]].y();
			z[0] = uvnodelist[n[0]].z();
			x[1] = uvnodelist[n[1]].x();
			y[1] = uvnodelist[n[1]].y();
			z[1] = uvnodelist[n[1]].z();
			x[2] = uvnodelist[n[2]].x();
			y[2] = uvnodelist[n[2]].y();
			z[2] = uvnodelist[n[2]].z();
			bfacet_.area(mathzz_.area_triangle(x, y, z)); //3d area
			facetlist.push_back(bfacet_);
			//second triangle
			bfacet_.node(0, n[0]);
			bfacet_.node(1, n[3]);
			bfacet_.node(2, n[2]);
			x[0] = uvnodelist[n[0]].u_origin();
			y[0] = uvnodelist[n[0]].v_origin();
			z[0] = 0;
			x[1] = uvnodelist[n[3]].u_origin();
			y[1] = uvnodelist[n[3]].v_origin();
			z[1] = 0;
			x[2] = uvnodelist[n[2]].u_origin();
			y[2] = uvnodelist[n[2]].v_origin();
			z[2] = 0;
			bfacet_.parametricarea(mathzz_.area_triangle(x, y, z));//parametric area
			x[0] = uvnodelist[n[0]].u_flat();
			y[0] = uvnodelist[n[0]].v_flat();
			z[0] = 0;
			x[1] = uvnodelist[n[3]].u_flat();
			y[1] = uvnodelist[n[3]].v_flat();
			z[1] = 0;
			x[2] = uvnodelist[n[2]].u_flat();
			y[2] = uvnodelist[n[2]].v_flat();
			z[2] = 0;
			bfacet_.flatarea(mathzz_.area_triangle(x, y, z));//flat area
			x[0] = uvnodelist[n[0]].x();
			y[0] = uvnodelist[n[0]].y();
			z[0] = uvnodelist[n[0]].z();
			x[1] = uvnodelist[n[3]].x();
			y[1] = uvnodelist[n[3]].y();
			z[1] = uvnodelist[n[3]].z();
			x[2] = uvnodelist[n[2]].x();
			y[2] = uvnodelist[n[2]].y();
			z[2] = uvnodelist[n[2]].z();
			bfacet_.area(mathzz_.area_triangle(x, y, z)); //3d area
			facetlist.push_back(bfacet_);
		}
	}
}

UVNODE PARASURFACE::node3d(double u_, double v_) {
	int nu, nv, i, j, flag;
	double x_, y_, z_, xx[3], yy[3], zz[3], area_, area[3], nn[3];
	UVNODE uvnode_;
	/*
	if (u_ < 0){
	u_ = 0;
	}
	else if (u_>1){
	u_ = 1;
	}
	if (v_ < 0){
	v_ = 0;
	}
	else if (v_>1){
	v_ = 1;
	}*/

	uvnode_.u_origin(u_);
	uvnode_.v_origin(v_);
	flag = 0;
	//find the facet that the node is on

	for (i = 0; i < facetlist.size(); i++) {
		area_ = facetlist[i].parametricarea();
		for (j = 0; j < 3; j++) {
			nn[j] = facetlist[i].node(j);
		}
		xx[0] = u_;
		yy[0] = v_;
		zz[0] = 0;
		zz[1] = 0;
		zz[2] = 0;
		xx[1] = uvnodelist[nn[1]].u_origin();
		yy[1] = uvnodelist[nn[1]].v_origin();
		xx[2] = uvnodelist[nn[2]].u_origin();
		yy[2] = uvnodelist[nn[2]].v_origin();
		area[0] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].u_origin();
		yy[1] = uvnodelist[nn[0]].v_origin();
		xx[2] = uvnodelist[nn[2]].u_origin();
		yy[2] = uvnodelist[nn[2]].v_origin();
		area[1] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].u_origin();
		yy[1] = uvnodelist[nn[0]].v_origin();
		xx[2] = uvnodelist[nn[1]].u_origin();
		yy[2] = uvnodelist[nn[1]].v_origin();
		area[2] = mathzz_.area_triangle(xx, yy, zz);
		if (std::abs(area[0] + area[1] + area[2] - area_)<0.0000001*std::sqrt(area_)) {
			flag = 1;
			x_ = (uvnodelist[nn[0]].x()*area[0] + uvnodelist[nn[1]].x()*area[1] + uvnodelist[nn[2]].x()*area[2]) / area_;
			y_ = (uvnodelist[nn[0]].y()*area[0] + uvnodelist[nn[1]].y()*area[1] + uvnodelist[nn[2]].y()*area[2]) / area_;
			z_ = (uvnodelist[nn[0]].z()*area[0] + uvnodelist[nn[1]].z()*area[1] + uvnodelist[nn[2]].z()*area[2]) / area_;
			break;
		}
	}

	if (flag == 1) {
		uvnode_.x(x_);
		uvnode_.y(y_);
		uvnode_.z(z_);
	}
	else {
		std::cout << "not on the parasurface error" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	return(uvnode_);
}

UVNODE PARASURFACE::node3d_fromflat(double u_, double v_) {
	int nu, nv, i, j, flag;
	double x_, y_, z_, xx[3], yy[3], zz[3], area_, area[3], nn[3];
	UVNODE uvnode_;

	uvnode_.u_flat(u_);
	uvnode_.v_flat(v_);
	flag = 0;
	//find the facet that the node is on

	for (i = 0; i < facetlist.size(); i++) {
		area_ = facetlist[i].flatarea();
		for (j = 0; j < 3; j++) {
			nn[j] = facetlist[i].node(j);
		}
		xx[0] = u_;
		yy[0] = v_;
		zz[0] = 0;
		zz[1] = 0;
		zz[2] = 0;
		xx[1] = uvnodelist[nn[1]].u_flat();
		yy[1] = uvnodelist[nn[1]].v_flat();
		xx[2] = uvnodelist[nn[2]].u_flat();
		yy[2] = uvnodelist[nn[2]].v_flat();
		area[0] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].u_flat();
		yy[1] = uvnodelist[nn[0]].v_flat();
		xx[2] = uvnodelist[nn[2]].u_flat();
		yy[2] = uvnodelist[nn[2]].v_flat();
		area[1] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].u_flat();
		yy[1] = uvnodelist[nn[0]].v_flat();
		xx[2] = uvnodelist[nn[1]].u_flat();
		yy[2] = uvnodelist[nn[1]].v_flat();
		area[2] = mathzz_.area_triangle(xx, yy, zz);
		if (std::abs(area[0] + area[1] + area[2] - area_)<0.0000001*std::sqrt(area_)) {
			flag = 1;
			x_ = (uvnodelist[nn[0]].x()*area[0] + uvnodelist[nn[1]].x()*area[1] + uvnodelist[nn[2]].x()*area[2]) / area_;
			y_ = (uvnodelist[nn[0]].y()*area[0] + uvnodelist[nn[1]].y()*area[1] + uvnodelist[nn[2]].y()*area[2]) / area_;
			z_ = (uvnodelist[nn[0]].z()*area[0] + uvnodelist[nn[1]].z()*area[1] + uvnodelist[nn[2]].z()*area[2]) / area_;
			break;
		}
	}

	if (flag == 1) {
		uvnode_.x(x_);
		uvnode_.y(y_);
		uvnode_.z(z_);
	}
	else {
		std::cout << "not on the parasurface error" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	return(uvnode_);
}

UVNODE PARASURFACE::node3d_fromxy(double x_, double y_) {
	int i, j, flag;
	double z_, xx[3], yy[3], zz[3], area_, area[3], nn[3];
	UVNODE uvnode_;

	uvnode_.x(x_);
	uvnode_.y(y_);
	flag = 0;
	//find the facet that the node is on

	for (i = 0; i < facetlist.size(); i++) {

		for (j = 0; j < 3; j++) {
			nn[j] = facetlist[i].node(j);
		}
		xx[0] = x_;
		yy[0] = y_;
		zz[0] = 0;
		zz[1] = 0;
		zz[2] = 0;
		xx[1] = uvnodelist[nn[1]].x();
		yy[1] = uvnodelist[nn[1]].y();
		xx[2] = uvnodelist[nn[2]].x();
		yy[2] = uvnodelist[nn[2]].y();
		area[0] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].x();
		yy[1] = uvnodelist[nn[0]].y();
		xx[2] = uvnodelist[nn[2]].x();
		yy[2] = uvnodelist[nn[2]].y();
		area[1] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].x();
		yy[1] = uvnodelist[nn[0]].y();
		xx[2] = uvnodelist[nn[1]].x();
		yy[2] = uvnodelist[nn[1]].y();
		area[2] = mathzz_.area_triangle(xx, yy, zz);
		xx[0] = uvnodelist[nn[2]].x();
		yy[0] = uvnodelist[nn[2]].y();
		area_ = mathzz_.area_triangle(xx, yy, zz);
		if (std::abs(area[0] + area[1] + area[2] - area_)<0.0000001*std::sqrt(area_)) {
			flag = 1;
			x_ = (uvnodelist[nn[0]].x()*area[0] + uvnodelist[nn[1]].x()*area[1] + uvnodelist[nn[2]].x()*area[2]) / area_;
			y_ = (uvnodelist[nn[0]].y()*area[0] + uvnodelist[nn[1]].y()*area[1] + uvnodelist[nn[2]].y()*area[2]) / area_;
			z_ = (uvnodelist[nn[0]].z()*area[0] + uvnodelist[nn[1]].z()*area[1] + uvnodelist[nn[2]].z()*area[2]) / area_;
			break;
		}
	}

	if (flag == 1) {
		uvnode_.flag(1);
		uvnode_.x(x_);
		uvnode_.y(y_);
		uvnode_.z(z_);
	}
	else {
		uvnode_.flag(0);
	}

	return(uvnode_);
}

std::vector<double> PARASURFACE::uvforxyz(double x, double y, double z) {
	std::vector<double> vec;
	double u_, v_;
	int nu, nv, i, j, flag;
	double xx[3], yy[3], zz[3], area_, area[3], nn[3];

	flag = 0;
	//find the facet that the node is on
	for (i = 0; i < facetlist.size(); i++) {
		area_ = facetlist[i].area();
		for (j = 0; j < 3; j++) {
			nn[j] = facetlist[i].node(j);
		}
		xx[0] = x;
		yy[0] = y;
		zz[0] = z;

		xx[1] = uvnodelist[nn[1]].x();
		yy[1] = uvnodelist[nn[1]].y();
		zz[1] = uvnodelist[nn[1]].z();
		xx[2] = uvnodelist[nn[2]].x();
		yy[2] = uvnodelist[nn[2]].y();
		zz[2] = uvnodelist[nn[2]].z();
		area[0] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].x();
		yy[1] = uvnodelist[nn[0]].y();
		zz[1] = uvnodelist[nn[0]].z();
		xx[2] = uvnodelist[nn[2]].x();
		yy[2] = uvnodelist[nn[2]].y();
		zz[2] = uvnodelist[nn[2]].z();
		area[1] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].x();
		yy[1] = uvnodelist[nn[0]].y();
		zz[1] = uvnodelist[nn[0]].z();
		xx[2] = uvnodelist[nn[1]].x();
		yy[2] = uvnodelist[nn[1]].y();
		zz[2] = uvnodelist[nn[1]].z();
		area[2] = mathzz_.area_triangle(xx, yy, zz);

		if (std::abs(area[0] + area[1] + area[2] - area_)<0.0001*std::sqrt(area_)) {
			flag = 1;
			u_ = (uvnodelist[nn[0]].u_origin()*area[0] + uvnodelist[nn[1]].u_origin()*area[1] + uvnodelist[nn[2]].u_origin()*area[2]) / area_;
			v_ = (uvnodelist[nn[0]].v_origin()*area[0] + uvnodelist[nn[1]].v_origin()*area[1] + uvnodelist[nn[2]].v_origin()*area[2]) / area_;
			break;
		}
	}

	if (flag == 1) {
		vec.push_back(u_);
		vec.push_back(v_);
	}
	else {
		vec.push_back(-1);
		vec.push_back(-1);
	}

	return(vec);
}

std::vector<double> PARASURFACE::flatuvforxyz(double x, double y, double z) {
	std::vector<double> vec;
	double u_, v_;
	int nu, nv, i, j, flag;
	double xx[3], yy[3], zz[3], area_, area[3], nn[3];

	flag = 0;
	//find the facet that the node is on
	for (i = 0; i < facetlist.size(); i++) {
		area_ = facetlist[i].area();
		for (j = 0; j < 3; j++) {
			nn[j] = facetlist[i].node(j);
		}
		xx[0] = x;
		yy[0] = y;
		zz[0] = z;

		xx[1] = uvnodelist[nn[1]].x();
		yy[1] = uvnodelist[nn[1]].y();
		zz[1] = uvnodelist[nn[1]].z();
		xx[2] = uvnodelist[nn[2]].x();
		yy[2] = uvnodelist[nn[2]].y();
		zz[2] = uvnodelist[nn[2]].z();
		area[0] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].x();
		yy[1] = uvnodelist[nn[0]].y();
		zz[1] = uvnodelist[nn[0]].z();
		xx[2] = uvnodelist[nn[2]].x();
		yy[2] = uvnodelist[nn[2]].y();
		zz[2] = uvnodelist[nn[2]].z();
		area[1] = mathzz_.area_triangle(xx, yy, zz);
		xx[1] = uvnodelist[nn[0]].x();
		yy[1] = uvnodelist[nn[0]].y();
		zz[1] = uvnodelist[nn[0]].z();
		xx[2] = uvnodelist[nn[1]].x();
		yy[2] = uvnodelist[nn[1]].y();
		zz[2] = uvnodelist[nn[1]].z();
		area[2] = mathzz_.area_triangle(xx, yy, zz);

		if (std::abs(area[0] + area[1] + area[2] - area_)<0.0001*std::sqrt(area_)) {
			flag = 1;
			u_ = (uvnodelist[nn[0]].u_flat()*area[0] + uvnodelist[nn[1]].u_flat()*area[1] + uvnodelist[nn[2]].u_flat()*area[2]) / area_;
			v_ = (uvnodelist[nn[0]].v_flat()*area[0] + uvnodelist[nn[1]].v_flat()*area[1] + uvnodelist[nn[2]].v_flat()*area[2]) / area_;
			break;
		}
	}

	if (flag == 1) {
		vec.push_back(u_);
		vec.push_back(v_);
	}
	else {
		vec.push_back(-1);
		vec.push_back(-1);
	}

	return(vec);
}

void PARASURFACE::clear() {
	uvnodematrix_.clear();
	lengthu_.clear();
	lengthv_.clear();
	uvnodelist.clear();
	facetlist.clear();
}