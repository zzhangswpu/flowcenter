
#include "bsurface.h"

BSURFACE::BSURFACE(){
	type_ = -2; //internal by default
	P_ = 0;
	influx_ = 0;
	sign_ = 0;
}

void BSURFACE::type(int a){
	type_ = a;
}

int BSURFACE::type(){
	return(type_);
}

void BSURFACE::P(double a){
	P_ = a;
}

double BSURFACE::P(){
	return(P_);
}
void BSURFACE::influx(double a){
	influx_ = a;
}

double BSURFACE::influx(){
	return(influx_);
}

void BSURFACE::sign(int i){
	sign_ = i;
}

int BSURFACE::sign(){
	return(sign_);
}