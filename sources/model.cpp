
#include "model.h"



void MODEL::clear(){
	propertyarealist.clear();
	bsurfacelist.clear();
	welllist.clear();
}

void MODEL::addpropertyarea(PROPERTYAREA p){
	propertyarealist.push_back(p);
}
void MODEL::modifypropertyarea(int i, PROPERTYAREA p){
	if (i >= propertyarealist.size()){
		std::cout << "error modify propertyarea int i > propertyarealist size" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	propertyarealist[i] = p;
}

PROPERTYAREA MODEL::propertyarea(int i){
	return(propertyarealist[i]);
}

unsigned int MODEL::numberofpropertyareas() const{
	return(propertyarealist.size());
}
void MODEL::numberofpropertyareas(int i){
	propertyarealist.resize(i);
}

void MODEL::addbsurface(BSURFACE b){
	bsurfacelist.push_back(b);
}

BSURFACE MODEL::bsurface(int i){
	return(bsurfacelist[i]);
}

int MODEL::numberofbsurfaces(){
	return(bsurfacelist.size());
}

void MODEL::addwell(WELL w){
	welllist.push_back(w);
}

WELL MODEL::well(int i){
	return(welllist[i]);
}

int MODEL::numberofwells() const{
	return(welllist.size());
}

void MODEL::computeporevolume(std::vector<TETRAHEDRON> tetlist){
	int i, iregion;
	std::vector<double> rv;
	rv.resize(propertyarealist.size());
	for (i = 0; i < tetlist.size(); i++){
		iregion = tetlist[i].markregion();
		rv[iregion] = rv[iregion] + tetlist[i].volume()*propertyarealist[iregion].porosity();
	}
	for (i = 0; i < rv.size(); i++){
		propertyarealist[i].porevolume(rv[i]);
	}
}

void MODEL::inputwellgeometry(std::vector<std::vector<NODE>> wellgeonodelist) {
	std::ifstream inputrrm;
	NODE node_;
	int i, j, nwell, nn, np, k;
	double x_, y_, z_, dd, x1, x2, y1, y2, z1, z2, dd2;

	dd = meshedgelength_;

	nwell = wellgeonodelist.size();
	if (nwell != numberofwells()) {
		std::cout << "well number in .well and userinput different" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	for (i = 0; i < nwell; i++) {
		nn = wellgeonodelist[i].size(); //number of nodes for this well
		for (j = 0; j < nn - 1; j++){
			np = int(mathzz_.twonodedistance(wellgeonodelist[i][j], wellgeonodelist[i][j + 1]) / dd);
			if (np == 0){
				np = 1;
			}
			for (k = 0; k < np; k++){
				x_ = (k *wellgeonodelist[i][j + 1].x() + (np - k) *wellgeonodelist[i][j].x()) / np;
				y_ = (k *wellgeonodelist[i][j + 1].y() + (np - k) *wellgeonodelist[i][j].y()) / np;
				z_ = (k *wellgeonodelist[i][j + 1].z() + (np - k) *wellgeonodelist[i][j].z()) / np;
				node_.x(x_);
				node_.y(y_);
				node_.z(z_);
				welllist[i].addnode(node_);
			}
		}
		welllist[i].addnode(wellgeonodelist[i][nn - 1]);
		welllist[i].computelength();
	}
}

void MODEL::resetbsurface(int i, BSURFACE b){
	bsurfacelist[i] = b;
}

void MODEL::meshedgelength(double d){
	meshedgelength_ = d;
}

double MODEL::meshedgelength(){
	return(meshedgelength_);
}

void MODEL::numberofwells(int i){
	welllist.resize(i);
}

void MODEL::modifywell(int i, WELL w){
	if (i >= welllist.size()){
		std::cout << "error modify well int i > welllist size" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	welllist[i] = w;
}

void MODEL::clearpropertyareas(){
	propertyarealist.clear();
}

void MODEL::clearwells(){
	welllist.clear();
}



