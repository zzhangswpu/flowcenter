

#include "row.h"


void ROW::addvalue(double d, int i){
	int j, mark;
	mark = 0;
	for (j = 0; j < positionvec_.size(); j++){
		if (i == positionvec_[j]){
			valuevec_[j] = valuevec_[j] + d;
			mark = 1;
			break;
		}
	}
	if (mark == 0){
		positionvec_.push_back(i);
		valuevec_.push_back(d);
	}

}

std::vector<double> ROW::valuevec(){
	return(valuevec_);
}

std::vector<int> ROW::positionvec(){
	return(positionvec_);
}