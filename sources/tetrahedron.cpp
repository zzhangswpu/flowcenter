

#include"TETRAHEDRON.h"


//TETRAHEDRON
TETRAHEDRON::TETRAHEDRON(){
	status_ = 0;
	mobit_ = 0;
	fracw_ = 0;
	volume_ = 0;
	/*mu_w_ = 0;
	mu_o_ = 0;*/
	Ux_t_ = 0;
	Uy_t_ = 0;
	Uz_t_ = 0;
	Ux_w_ = 0;
	Uy_w_ = 0;
	Uz_w_ = 0;
	markregion_ = -1;
	//tof_ = 0;
	//marktofb_ = -1;
	//marktracerb_ = -1;
	porosity_ = 1.0;
	Sw_ = 0;

	for (i = 0; i < 3; i++){
		fmmv_[i] = 0;
		for (j = 0; j < 3; j++){
			k_[i][j] = 0.0;
		}
	}
	for (i = 0; i < 4; i++){
		//markfluxcalculated_[i] = 0;
		neighbor_[i] = -1;
	}
	mobit_ = 0;
	mobio_ = 0;
	mobiw_ = 0;
}



unsigned int TETRAHEDRON::node(unsigned int i) const{ //get TETRAHEDRON's node numbers
	return (node_[i]);
}
void TETRAHEDRON::node(unsigned int i, unsigned int node_in){ //set TETRAHEDRON's node numbers, i is local while node_in is global
	node_[i] = node_in;
}

unsigned int TETRAHEDRON::neighbor(unsigned int i) const{
	return(neighbor_[i]);
}
void TETRAHEDRON::neighbor(unsigned int i, int j){
	neighbor_[i] = j;
}

double TETRAHEDRON::volume() const{ // get volume
	return volume_;
}

void TETRAHEDRON::volume(double v){ // set volume
	if (v <= 0){
		std::cout << "negative or zero volume; maybe volume calculation not rhs; or forget to use abs. Volume is "<< v << std::endl;
		std::exit(EXIT_FAILURE);
	}
	volume_ = v;
}

double TETRAHEDRON::Sw() const{ // get porosity
	return Sw_;
}

void TETRAHEDRON::Sw(double v){ // set porosity
	Sw_ = v;
}

double TETRAHEDRON::porosity() const{ // get porosity
	return porosity_;
}

void TETRAHEDRON::porosity(double v){ // set porosity
	porosity_ = v;
}

double TETRAHEDRON::mobit() const{ // get total mobility
	return mobit_;
}

void TETRAHEDRON::mobit(double v){ // set total mobility
	mobit_ = v;
}

double TETRAHEDRON::fracw(){
	return fracw_;
}

void TETRAHEDRON::fracw(double v){
	fracw_ = v;
}


double TETRAHEDRON::mobio() const{ // get total mobility
	return mobio_;
}

void TETRAHEDRON::mobio(double v){ // set total mobility
	mobio_ = v;
}

double TETRAHEDRON::mobiw() const{ // get total mobility
	return mobiw_;
}

void TETRAHEDRON::mobiw(double v){ // set total mobility
	mobiw_ = v;
}

//double TETRAHEDRON::krw() const{
//	return krw_;
//}
//
//double TETRAHEDRON::kro() const{
//	return kro_;
//}
//
//void TETRAHEDRON::krw(double d){
//	krw_ = d;
//}
//
//void TETRAHEDRON::kro(double d){
//	kro_ = d;
//}

double TETRAHEDRON::k(int i, int j) const{ // get permeability
	return k_[i][j];
}

void TETRAHEDRON::k(int i, int j, double input){ // set permeability
	k_[i][j] = input;
}

//double TETRAHEDRON::mobi(int i, int j) const{ // get mobi
//	return mobi_[i][j];
//}
//
//void TETRAHEDRON::mobi(int i, int j, double input){ // set mobi
//	mobi_[i][j] = input;
//}

//double TETRAHEDRON::mu_w() const{ // get viscosity for calculation
//	return mu_w_;
//}
//
//void TETRAHEDRON::mu_w(double input){ // set viscosity
//	mu_w_ = input;
//}

//double TETRAHEDRON::mu_o() const{ // get viscosity for calculation
//	return mu_o_;
//}
//
//void TETRAHEDRON::mu_o(double input){ // set viscosity
//	mu_o_ = input;
//}
//
//double TETRAHEDRON::rho_o() const{
//	return rho_o_;
//}
//
//void TETRAHEDRON::rho_o(double input){
//	rho_o_ = input;
//}

//double TETRAHEDRON::rho_w() const{
//	return rho_w_;
//}
//
//void TETRAHEDRON::rho_w(double input){
//	rho_w_ = input;
//}

//double TETRAHEDRON::Siw() const{
//	return Siw_;
//}
//
//void TETRAHEDRON::Siw(double input){
//	Siw_ = input;
//}
//
//double TETRAHEDRON::Sw() const{
//	return Sw_;
//}
//
//void TETRAHEDRON::Sw(double input){
//	Sw_ = input;
//}
//
//double TETRAHEDRON::Pct() const{
//	return Pct_;
//}
//
//void TETRAHEDRON::Pct(double input){
//	Pct_ = input;
//}
//
//double TETRAHEDRON::Pc() const{
//	return Pc_;
//}
//
//void TETRAHEDRON::Pc(double input){
//	Pc_ = input;
//}

//double TETRAHEDRON::lamda() const{
//	return lamda_;
//}
//
//void TETRAHEDRON::lamda(double input){
//	lamda_ = input;
//}

double TETRAHEDRON::coef() const{ // get coef
	return coef_;
}

void TETRAHEDRON::coef(double input){ // set coef
	coef_ = input;
}

int TETRAHEDRON::markregion() const{
	return(markregion_);
}

void TETRAHEDRON::markregion(int input){
	markregion_ = input;
}

//for shape function
void TETRAHEDRON::Na(unsigned int i, double value){
	a[i] = value;
}

void TETRAHEDRON::Nb(unsigned int i, double value){
	b[i] = value;
}

void TETRAHEDRON::Nc(unsigned int i, double value){
	c[i] = value;
}

void TETRAHEDRON::Nd(unsigned int i, double value){
	d[i] = value;
}

double TETRAHEDRON::Na(unsigned int i) const{
	return(a[i]);
}

double TETRAHEDRON::Nb(unsigned int i) const{
	return(b[i]);
}

double TETRAHEDRON::Nc(unsigned int i) const{
	return(c[i]);
}

double TETRAHEDRON::Nd(unsigned int i) const{
	return(d[i]);
}

int TETRAHEDRON::status(){
	return status_;
}

void TETRAHEDRON::status(int i){
	status_ = i;
}

double TETRAHEDRON::Ux_t() const{
	return (Ux_t_);
}

double TETRAHEDRON::Uy_t() const{
	return (Uy_t_);
}

double TETRAHEDRON::Uz_t() const{
	return (Uz_t_);
}

void TETRAHEDRON::Ux_t(double input){
	Ux_t_ = input;
}

void TETRAHEDRON::Uy_t(double input){
	Uy_t_ = input;
}

void TETRAHEDRON::Uz_t(double input){
	Uz_t_ = input;
}


double TETRAHEDRON::Ux_w() const{
	return (Ux_w_);
}

double TETRAHEDRON::Uy_w() const{
	return (Uy_w_);
}

double TETRAHEDRON::Uz_w() const{
	return (Uz_w_);
}

void TETRAHEDRON::Ux_w(double input){
	Ux_w_ = input;
}

void TETRAHEDRON::Uy_w(double input){
	Uy_w_ = input;
}

void TETRAHEDRON::Uz_w(double input){
	Uz_w_ = input;
}


int TETRAHEDRON::face(unsigned int i1, unsigned int i2){
	if (i1<0 || i1>3){
		std::cout << "wrong i1" << std::endl;
		std::exit(EXIT_FAILURE);
		return(-1);
	}
	if (i2<0 || i2>2){
		std::cout << "wrong i2" << std::endl;
		std::exit(EXIT_FAILURE);
		return(-1);
	}
	if (i1 == 0){
		if (i2 == 0){
			return(node_[1]);
		}
		if (i2 == 1){
			return(node_[2]);
		}
		if (i2 == 2){
			return(node_[3]);
		}
	}
	if (i1 == 1){
		if (i2 == 0){
			return(node_[0]);
		}
		if (i2 == 1){
			return(node_[3]);
		}
		if (i2 == 2){
			return(node_[2]);
		}
	}
	if (i1 == 2){
		if (i2 == 0){
			return(node_[1]);
		}
		if (i2 == 1){
			return(node_[3]);
		}
		if (i2 == 2){
			return(node_[0]);
		}
	}
	if (i1 == 3){
		if (i2 == 0){
			return(node_[0]);
		}
		if (i2 == 1){
			return(node_[2]);
		}
		if (i2 == 2){
			return(node_[1]);
		}
	}

}

int TETRAHEDRON::face_local(unsigned int i1, unsigned int i2){
	if (i1<0 || i1>3){
		std::cout << "wrong i1" << std::endl;
	}
	if (i2<0 || i2>2){
		std::cout << "wrong i2" << std::endl;
	}
	if (i1 == 0){
		if (i2 == 0){
			return(1);
		}
		if (i2 == 1){
			return(2);
		}
		if (i2 == 2){
			return(3);
		}
	}
	if (i1 == 1){
		if (i2 == 0){
			return(0);
		}
		if (i2 == 1){
			return(3);
		}
		if (i2 == 2){
			return(2);
		}
	}
	if (i1 == 2){
		if (i2 == 0){
			return(1);
		}
		if (i2 == 1){
			return(3);
		}
		if (i2 == 2){
			return(0);
		}
	}
	if (i1 == 3){
		if (i2 == 0){
			return(0);
		}
		if (i2 == 1){
			return(2);
		}
		if (i2 == 2){
			return(1);
		}
	}

}

void TETRAHEDRON::face_normalx(unsigned int i, double d){
	normalx_[i] = d;
}
void TETRAHEDRON::face_normaly(unsigned int i, double d){
	normaly_[i] = d;
}
void TETRAHEDRON::face_normalz(unsigned int i, double d){
	normalz_[i] = d;
}
double TETRAHEDRON::face_normalx(unsigned i){
	return(normalx_[i]);
}
double TETRAHEDRON::face_normaly(unsigned i){
	return(normaly_[i]);
}
double TETRAHEDRON::face_normalz(unsigned i){
	return(normalz_[i]);
}

void TETRAHEDRON::face_area(unsigned int i, double d){
	face_area_[i] = d;
}
double TETRAHEDRON::face_area(unsigned int i){
	return(face_area_[i]);
}

void TETRAHEDRON::fmmv(int i, double d){
	fmmv_[i] = d;
}

double TETRAHEDRON::fmmv(int i){
	return fmmv_[i];
}

/*
double TETRAHEDRON::tof() const{ // time of flight
	return (tof_);
}

void TETRAHEDRON::tof(double input){
	tof_ = input;
}

double TETRAHEDRON::tracer(int i) const{ // tracer vector
	return (tracer_[i]);
}

void TETRAHEDRON::tracer(int i, double input){ //same order as the tracer boundaries
	tracer_[i] = input;
}

void TETRAHEDRON::tracersize(int i){
	tracer_.resize(i);
}

int TETRAHEDRON::tracersize(){
	return(tracer_.size());
}
void TETRAHEDRON::face_flux(unsigned int i, double d){
	flux_[i] = d;
}
double TETRAHEDRON::face_flux(unsigned i){
	return(flux_[i]);
}

int TETRAHEDRON::marktofb(){
	return(marktofb_);

}
void TETRAHEDRON::marktofb(int i){
	marktofb_ = i;
}

int TETRAHEDRON::marktracerb(){
	return(marktracerb_);

}
void TETRAHEDRON::marktracerb(int i){
	marktracerb_ = i;
}

int TETRAHEDRON::markmaxtracer(){
	return(markmaxtracer_);

}
void TETRAHEDRON::markmaxtracer(int i){
	markmaxtracer_ = i;
}

int TETRAHEDRON::face_markfluxcalculated(int i){
	return(markfluxcalculated_[i]);
}

void TETRAHEDRON::face_markfluxcalculated(int i, int j){
	markfluxcalculated_[i] = j;
}*/