

#include "meshinfo.h"

MESHINFO::MESHINFO(){
	resolutiontype_ = 1;
	guideedgelength_ = 20;
	rxnumber_ = 40;
}


void MESHINFO::xmax(double d){
	xmax_ = d;
}

double MESHINFO::xmax(){
	return(xmax_);
}

void MESHINFO::xmin(double d){
	xmin_ = d;
}

double MESHINFO::xmin(){
	return(xmin_);
}

void MESHINFO::ymax(double d){
	ymax_ = d;
}

double MESHINFO::ymax(){
	return(ymax_);
}

void MESHINFO::ymin(double d){
	ymin_ = d;
}

double MESHINFO::ymin(){
	return(ymin_);
}

void MESHINFO::zmax(double d){
	zmax_ = d;
}

double MESHINFO::zmax(){
	return(zmax_);
}

void MESHINFO::zmin(double d){
	zmin_ = d;
}

double MESHINFO::zmin(){
	return(zmin_);
}

int MESHINFO::cpgdimension(int i){
	return(dimension[i]);
}

void MESHINFO::cpgdimension(int i, int j){
	dimension[i] = j;
}

int MESHINFO::type(){
	return(type_);
}

void MESHINFO::type(int i){
	type_ = i;
}

int MESHINFO::naddnodes(){
	return(naddnodes_);
}

void MESHINFO::naddnodes(int i){
	naddnodes_ = i;
}

void MESHINFO::xmaxav(double d){
	xmaxav_ = d;
}

double MESHINFO::xmaxav(){
	return(xmaxav_);
}

void MESHINFO::xminav(double d){
	xminav_ = d;
}

double MESHINFO::xminav(){
	return(xminav_);
}

void MESHINFO::ymaxav(double d){
	ymaxav_ = d;
}

double MESHINFO::ymaxav(){
	return(ymaxav_);
}

void MESHINFO::yminav(double d){
	yminav_ = d;
}

double MESHINFO::yminav(){
	return(yminav_);
}

void MESHINFO::zmaxav(double d){
	zmaxav_ = d;
}

double MESHINFO::zmaxav(){
	return(zmaxav_);
}

void MESHINFO::zminav(double d){
	zminav_ = d;
}

double MESHINFO::zminav(){
	return(zminav_);
}

int MESHINFO::resolutiontype(){
	return resolutiontype_;
}

void MESHINFO::resolutiontype(int i){
	resolutiontype_ = i;
}

double MESHINFO::guideedgelength(){
	return guideedgelength_;
}

void MESHINFO::guideedgelength(double d){
	guideedgelength_ = d;
}

int MESHINFO::resolutionxnumber(){
	return rxnumber_;
}

void MESHINFO::resolutionxnumber(int i){
	rxnumber_ = i;
}