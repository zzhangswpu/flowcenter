

#include "flowb.h"

FLOWB::FLOWB(){
	type_ = 0;
	mark_ = -1;
}

void FLOWB::type(int a){
	type_ = a;
}

int FLOWB::type(){
	return(type_);
}

void FLOWB::mark(int a){
	mark_ = a;
}

int FLOWB::mark(){
	return(mark_);
}

