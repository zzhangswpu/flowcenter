

#include"linearfunc.h"

void LINEARFUNC::a(double v){
	a_ = v;
}

void LINEARFUNC::b(double v){
	b_ = v;
}

void LINEARFUNC::c(double v){
	c_ = v;
}

void LINEARFUNC::d(double v){
	d_ = v;
}

double LINEARFUNC::value(double x, double y, double z){
	value_ = a_*x + b_*y + c_*z + d_;
	return(value_);
}

