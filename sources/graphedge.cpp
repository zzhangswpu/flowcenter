


#include "graphedge.h"

GRAPHEDGE::GRAPHEDGE(){
	graphnode_[0] = 0;
	graphnode_[1] = 0;
}

void GRAPHEDGE::graphnode(int i, int j){
	if (i > 1 || i < 0){
		std::cout << "set graphnode index i out of range " << i << std::endl;
		std::exit(EXIT_FAILURE);
	}
	graphnode_[i] = j;
}

int GRAPHEDGE::graphnode(int i){
	if (i > 1 || i < 0){
		std::cout << "get graphnode index i out of range " << i << std::endl;
		std::exit(EXIT_FAILURE);
	}
	return(graphnode_[i]);
}