

#include"mathzz.h"

double MATHZZ::volume_tetra_signed(double x[4], double y[4], double z[4]){  //equivalent to volume_tetra by determinant 4
    double vol6, vol4;
	Sx = (y[1] - y[0])*(z[2] - z[0]) - (z[1] - z[0])*(y[2] - y[0]);
	Sy = (z[1] - z[0])*(x[2] - x[0]) - (x[1] - x[0])*(z[2] - z[0]);
	Sz = (x[1] - x[0])*(y[2] - y[0]) - (y[1] - y[0])*(x[2] - x[0]);
	vol6 = (x[3] - x[0])*Sx + (y[3] - y[0])*Sy + (z[3] - z[0])*Sz; //volume of the hexahedron created by vectors 2-1, 3-1, 4-1
	vol4 = vol6 / 6.0; //volume of the tetrahedron

	return (vol4);
}

std::vector<std::vector<double>> MATHZZ::inverse33matrix(std::vector<std::vector<double>> m){
	std::vector<std::vector<double>> n;
	n.resize(3);
	for (int i = 0; i < 3; i++){
		n[i].resize(3);
	}
	double a11 = m[0][0];
	double a12 = m[0][1];
	double a13 = m[0][2];
	double a21 = m[1][0];
	double a22 = m[1][1];
	double a23 = m[1][2];
	double a31 = m[2][0];
	double a32 = m[2][1];
	double a33 = m[2][2];
	double length_ = determinant3(a11, a12, a13, a21, a22, a23, a31, a32, a33);
	n[0][0] = determinant2(a22, a23, a32, a33) / length_;
	n[0][1] = determinant2(a13, a12, a33, a32) / length_;
	n[0][2] = determinant2(a12, a13, a22, a23) / length_;
	n[1][0] = determinant2(a23, a21, a33, a31) / length_;
	n[1][1] = determinant2(a11, a13, a31, a33) / length_;
	n[1][2] = determinant2(a13, a11, a23, a21) / length_;
	n[2][0] = determinant2(a21, a22, a31, a32) / length_;
	n[2][1] = determinant2(a12, a11, a32, a31) / length_;
	n[2][2] = determinant2(a11, a12, a21, a22) / length_;
	return n;

}

double MATHZZ::determinant2(double a11, double a12, double a21, double a22){
	return(a11*a22 - a12*a21);
}

double MATHZZ::volume_tetra_positive(double x[4], double y[4], double z[4]){  //equivalent to volume_tetra by determinant 4
	double vol6, vol4;
	Sx = (y[1] - y[0])*(z[2] - z[0]) - (z[1] - z[0])*(y[2] - y[0]);
	Sy = (z[1] - z[0])*(x[2] - x[0]) - (x[1] - x[0])*(z[2] - z[0]);
	Sz = (x[1] - x[0])*(y[2] - y[0]) - (y[1] - y[0])*(x[2] - x[0]);
	vol6 = (x[3] - x[0])*Sx + (y[3] - y[0])*Sy + (z[3] - z[0])*Sz; //volume of the hexahedron created by vectors 2-1, 3-1, 4-1
	vol4 = vol6 / 6.0; //volume of the tetrahedron

	return (std::abs(vol4));
}


/*double MATHZZ::volume_tetra_positive(double x[4], double y[4], double z[4]){
	double vol;
	vol = determinant4(1, x[0], y[0], z[0], 1, x[1], y[1], z[1], 1, x[2], y[2], z[2], 1, x[3], y[3], z[3]) / 6.0;

	return(std::abs(vol));
}*/

double MATHZZ::determinant3(double a11, double a12, double a13, double a21, double a22, double a23, double a31, double a32, double a33){
	return (a11*a22*a33 + a12*a23*a31 + a13*a21*a32 - a13*a22*a31 - a11*a23*a32 - a12*a21*a33);
}

double MATHZZ::determinant4(double a11, double a12, double a13, double a14, double a21, double a22, double a23, double a24, double a31, double a32, double a33, double a34, double a41, double a42, double a43, double a44){
	double m1, m2, m3, m4;
	m1 = determinant3(a22, a23, a24, a32, a33, a34, a42, a43, a44);
	m2 = determinant3(a12, a13, a14, a32, a33, a34, a42, a43, a44);
	m3 = determinant3(a12, a13, a14, a22, a23, a24, a42, a43, a44);
	m4 = determinant3(a12, a13, a14, a22, a23, a24, a32, a33, a34);
	return(a11*m1 - a21*m2 + a31*m3 - a41*m4);
}


double MATHZZ::area_triangle(double x[3], double y[3], double z[3]){//always positive
	double v1[3], v2[3];
	v1[0] = x[1] - x[0];
	v1[1] = y[1] - y[0];
	v1[2] = z[1] - z[0];
	v2[0] = x[2] - x[0];
	v2[1] = y[2] - y[0];
	v2[2] = z[2] - z[0];
	area_ = 0.5*area_vector(v1, v2);
	return (area_);
}

double MATHZZ::area_quad(double x[4], double y[4], double z[4]){
	double xx[3], yy[3], zz[3], xxx[3], yyy[3], zzz[3];
	int i;
	for (i = 0; i < 3; i++){
		xx[i] = x[i];
		yy[i] = y[i];
		zz[i] = z[i];
		xxx[i] = x[i + 1];
		yyy[i] = y[i + 1];
		zzz[i] = z[i + 1];
	}
	return(area_triangle(xx, yy, zz)+area_triangle(xxx, yyy, zzz));
}


std::vector<double> MATHZZ::crossproduct(double *a, double *b){
	std::vector<double> Product;
	Product.resize(3);
	//Cross product formula
	Product[0] = (a[1] * b[2]) - (a[2] * b[1]);
	Product[1] = (a[2] * b[0]) - (a[0] * b[2]);
	Product[2] = (a[0] * b[1]) - (a[1] * b[0]);

	return Product;
}

std::vector<double> MATHZZ::crossproduct(std::vector<double> a, std::vector<double> b){
	std::vector<double> Product;
	Product.resize(3);
	//Cross product formula
	Product[0] = (a[1] * b[2]) - (a[2] * b[1]);
	Product[1] = (a[2] * b[0]) - (a[0] * b[2]);
	Product[2] = (a[0] * b[1]) - (a[1] * b[0]);

	return Product;
}



double MATHZZ::area_vector(double *a, double *b){//always positive
	std::vector<double> p;
	p = crossproduct(a, b);
	return(std::sqrt(p[0] * p[0] + p[1] * p[1] + p[2] * p[2]));
}

double MATHZZ::area_vector(std::vector<double> a, std::vector<double> b){
	std::vector<double> p;
	p = crossproduct(a, b);
	return(std::sqrt(p[0] * p[0] + p[1] * p[1] + p[2] * p[2]));
}


std::vector<double> MATHZZ::unitnormal(double *a, double *b){
	std::vector<double>unitnormal_;
	double length;
	unitnormal_.resize(3);
	unitnormal_ = crossproduct(a, b);
	length = sqrt(unitnormal_[0]*unitnormal_[0]+unitnormal_[1]*unitnormal_[1]+unitnormal_[2]*unitnormal_[2]);
	unitnormal_[0] = unitnormal_[0] / length;
	unitnormal_[1] = unitnormal_[1] / length;
	unitnormal_[2] = unitnormal_[2] / length;
	return unitnormal_;
}

std::vector<double> MATHZZ::unitnormal(std::vector<double> a, std::vector<double> b){
	std::vector<double>unitnormal_;
	double length;
	unitnormal_.resize(3);
	unitnormal_ = crossproduct(a, b);
	length = sqrt(unitnormal_[0] * unitnormal_[0] + unitnormal_[1] * unitnormal_[1] + unitnormal_[2] * unitnormal_[2]);
	unitnormal_[0] = unitnormal_[0] / length;
	unitnormal_[1] = unitnormal_[1] / length;
	unitnormal_[2] = unitnormal_[2] / length;
	return unitnormal_;
}

double MATHZZ::dotproduct(std::vector<double> vec1, std::vector<double> vec2){
	return(vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2]);
}

double MATHZZ::dotproduct(double *vec1, double *vec2){
	return(vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2]);
}

double MATHZZ::vectorlength(double *a){
	return(sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]));
}

std::vector<double> MATHZZ::Gaussianelimination(MATRIX m, std::vector<double> rhs){
	std::vector<double> u;
	double a, b;
	int i, j;

	//upper triangulation
	for (i = 0; i < m.size()-1; i++){
		if (m.entry(i, i) == 0){
			std::cout << "zero matrix diagonal entry in Gaussian elimination" << std::endl;
			return(u);
		}
		else{
			a = m.entry(i, i);
			for (j = i + 1; j < m.size(); j++){
				b = m.entry(j, i);
				m.addrowbyrow(j, i, -b / a);
				rhs[j] = rhs[j]-rhs[i] * b / a;
			}
		}
	}
	u.resize(m.size());

	for (i = 0; i < m.size() - 1; i++){
		if (abs(m.entry(i, i)) < 1e-15){
			std::cout << "warning: very small diagonal element after Gaussian Elimination" << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}

	//backward substitution
	for (i = m.size() - 1; i >= 0; i--){ //for row i
		for (j = i+1; j < m.size(); j++){ //move known values to rhs
			rhs[i] = rhs[i] - m.entry(i,j) * u[j];
		}
		u[i] = rhs[i] / m.entry(i,i);
	}
	return(u);

}

double MATHZZ::vectorlength(std::vector<double> a){
	return(sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]));
}

std::vector<double> MATHZZ::adversevector(std::vector<double> vec){
	std::vector<double> adverse;
	adverse.resize(3);
	if (vec.size() != 3){
		std::cout << "adverse vector input wrong" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	adverse[0] = -vec[0];
	adverse[1] = -vec[1];
	adverse[2] = -vec[2];
	return(adverse);
}

std::vector<double> MATHZZ::projectionvector(std::vector<double> a, std::vector<double> b){//a projected to along b
	std::vector<double> vec;
	double dd;
	vec.resize(3);
	dd = dotproduct(a, b) / (b[0] * b[0] + b[1] * b[1] + b[2] * b[2]);
	vec[0] = dd*b[0];
	vec[1] = dd*b[1];
	vec[2] = dd*b[2];
	return(vec);

}

double MATHZZ::twonodedistance(NODE m, NODE n){
	return(sqrt((m.x() - n.x())*(m.x() - n.x()) + (m.y() - n.y())*(m.y() - n.y()) + (m.z() - n.z())*(m.z() - n.z())));
}

double MATHZZ::twonodedistance2D(double x1, double y1, double x2, double y2){
	return(sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}