

#include "ROCK.h"

ROCK::ROCK(){
	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			k_[i][j] = 0.0;
		}
	}
}

double ROCK::k(int i, int j) const{ // get permeability
	return k_[i][j];
}

void ROCK::k(int i, int j, double input){ // set permeability
	k_[i][j] = input;
}


double ROCK::porosity(){
	return(porosity_);
}

void ROCK::porosity(double d){
	porosity_ = d;
}

double ROCK::Pct(){
	return(Pct_);
}

void ROCK::Pct(double d){
	Pct_ = d;
}

double ROCK::lamda(){
	return(lamda_);
}

void ROCK::lamda(double d){
	lamda_ = d;
}

double ROCK::x(){
	return(x_);
}

void ROCK::x(double d){
	x_ = d;
}

double ROCK::y(){
	return(y_);
}

void ROCK::y(double d){
	y_ = d;
}

double ROCK::z(){
	return(z_);
}

void ROCK::z(double d){
	z_ = d;
}

void ROCK::porevolume(double p){
	porevolume_ = p;
}

double ROCK::porevolume(){
	return(porevolume_);
}

void ROCK::oilip(double p){
	oilip_ = p;
}

double ROCK::oilip(){
	return(oilip_);
}

