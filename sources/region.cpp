

#include "region.h"
#include"samg.h"


REGION::REGION(){

	elementneighbormark_ = 0;
	bfacetelementmark_ = 0;
	welltestbutton_ = 0;
	linearsolverbutton_ = 1;
	ftofbutton_ = 0;
	ftracerbutton_ = 0;
	btofbutton_ = 0;
	btracerbutton_ = 0;
	ct_ = 1.0e-5 / 6895;
	vis_regionidbutton_ = 0;
	vis_permbutton_ = 0;
	vis_porobutton_ = 0;
	nbfacet = 0;
	wellmethod_ = 0; //no wells
	inputsurfacetype_ = 1; //default from skt

	trifacesbutton_ = 0;
	vis_saturationbutton_ = 0;
	h_fwl = 0.0;
	g_ = 9.81;
	oilreserve_ = 0;
	zscale_ = 1.0;
	dtofbutton_ = 0;
	dtofanabutton_ = 0;
}



void REGION::repairlocalextreme(){
	for (int inode = 0; inode < nodelist.size(); inode++){
		double max = 0;
		double min = 1;
		double avr = 0;
		int ct = 0;
		for (int i = 0; i < nodelist[inode].numberofedgein(); i++){
			ct++;
			int ied = nodelist[inode].edgein(i);
			int jnode = edgelist[ied].node(0) + edgelist[ied].node(1) - inode;
			double sw = nodelist[jnode].Sw();
			avr = avr + sw;
			if (sw>max){
				max = sw;
			}
			else if (sw < min){
				min = sw;
			}
		}
		for (int i = 0; i < nodelist[inode].numberofedgeout(); i++){
			ct++;
			int ied = nodelist[inode].edgeout(i);
			int jnode = edgelist[ied].node(0) + edgelist[ied].node(1) - inode;
			double sw = nodelist[jnode].Sw();
			avr = avr + sw;
			if (sw>max){
				max = sw;
			}
			else if (sw < min){
				min = sw;
			}
		}
		avr = avr / ct;
		if (nodelist[inode].Sw()>max || nodelist[inode].Sw() < min){
			nodelist[inode].Sw(avr);
		}
	}
}


void REGION::ftofbutton(int a){
	ftofbutton_ = a;
}

int REGION::ftofbutton(){
	return(ftofbutton_);
}

void REGION::dtofbutton(int a){
	dtofbutton_ = a;
}

int REGION::dtofbutton(){
	return(dtofbutton_);
}

void REGION::dtofanabutton(int a){
	dtofanabutton_ = a;
}

int REGION::dtofanabutton(){
	return(dtofanabutton_);
}

void REGION::welltestbutton(int a){
	welltestbutton_ = a;
}

int REGION::welltestbutton(){
	return(welltestbutton_);
}

void REGION::ftracerbutton(int a){
	ftracerbutton_ = a;
}

int REGION::ftracerbutton(){
	return(ftracerbutton_);
}

void REGION::btofbutton(int a){
	btofbutton_ = a;
}

int REGION::btofbutton(){
	return(btofbutton_);
}

void REGION::btracerbutton(int a){
	btracerbutton_ = a;
}

int REGION::btracerbutton(){
	return(btracerbutton_);
}

void REGION::vis_permbutton(int a){
	vis_permbutton_ = a;
}

int REGION::vis_permbutton(){
	return(vis_permbutton_);
}

void REGION::vis_porobutton(int a){
	vis_porobutton_ = a;
}

int REGION::vis_porobutton(){
	return(vis_porobutton_);
}

void REGION::vis_regionidbutton(int a){
	vis_regionidbutton_ = a;
}

int REGION::vis_regionidbutton(){
	return(vis_regionidbutton_);
}

void REGION::clearregion(){
	//only some int or doule variables in region.h are not reset
	elementneighbormark_ = 0;
	bfacetelementmark_ = 0;
	cpgfacelist.clear();
	linearsolverbutton_ = 2; //1 means SAMG 2 means Hypre
	ftofbutton_ = 0;
	ftracerbutton_ = 0;
	btofbutton_ = 0;
	btracerbutton_ = 0;

	vis_regionidbutton_ = 0;
	vis_permbutton_ = 0;
	vis_porobutton_ = 0;
	nbfacet = 0;
	wellmethod_ = 0;
	inputsurfacetype_ = 1; //default from skt

	trifacesbutton_ = 0;
	vis_saturationbutton_ = 0;
	h_fwl = 0.0;
	g_ = 9.81;
	oilreserve_ = 0;
	zscale_ = 1.0;

	oilreserve_ = 0;

	elementneighbormark_ = 0;
	bfacetelementmark_ = 0;
	nodelist.clear();
	elementlist.clear();
	bfacetlist.clear();
	bfacetBC.clear();
	tofblist.clear();
	tracingblist.clear();
	parasurfacelist.clear();
	facetlist.clear();
	facetlist_local.clear();
	geosurfacelist.clear();
	curvelist.clear();
	surfacenodelist.clear();
	regionnodelist.clear();
	surfaceedgelist.clear();
	edgelist.clear();
	uvnodelist.clear();
	segmentlist.clear();
	hsurfaceid.clear();
	bnodelist.clear();
	facetlist_input.clear();
	bfacetlist.clear();
	cpgelementlist.clear();
	face2tetlist.clear();
	graphedgelist.clear();
	graphnodelist.clear();
	bigcycle.clear();
	graphbeleset.clear(); preordering.clear(); postordering.clear(); reversepostordering.clear();
	cycles.clear();
	graphbnodeset.clear();
	rocklist.clear();
	bsurfacelist.clear();
	welllist.clear();
	flowbsurfacelist.clear();
	flowbsurfacemarklist.clear();
	meshtype_ = 0;
	wellmethod_ = 0;
	inputsurfacetype_ = 1;
	surfacenodelist_local.clear();
	segmentlist_all.clear();
	bsurfaceid.clear();
	lpoed.clear();
	ilfe.clear();
	lpofa.clear();
	marktoftracersameb_ = -1;
	graph.clear();
	inflowblist.clear(); outflowblist.clear();
	wellpairPV.clear(); WAF.clear(); Lc.clear();
	Fc.clear(); Sc.clear();
	sv.clear(); dv.clear();
	nodelist.clear();
	pillarlist.clear();

}

void REGION::wellmethod(int i){
	wellmethod_ = i;
}

int REGION::wellmethod(){
	return(wellmethod_);
}

void REGION::unstructuredsurfacemesh(){
	buildtriangularsurfacemesh();
	surfacemesh2tetgen();
}

void REGION::paramodel(){
	reorderhsurfaces();
	buildverticalskeleton();
	para2internalandnoflowbsurface();
	skeleton2parametric_all();
	buildparasurfacefacets_all(); //after skeleton2parametric because need calculate trifacets's area //writeskeletonVTK("model1skeleton2.vtk");
	//writeskeletonVTK("skeleton.vtk");
	parasurfaceneighbours();
}
void REGION::reorderhsurfaces(){
	//re-order horizontal surfaces bottom to top
	int itmp, i, j;
	for (i = 1; i < hsurfaceid.size(); i++){ //straight insertion sort
		itmp = hsurfaceid[i];
		for (j = i - 1; j >= 0 && parasurfacelist[hsurfaceid[j]].uvnode(int(parasurfacelist[hsurfaceid[j]].nu() / 2), int(parasurfacelist[hsurfaceid[j]].nv() / 2)).z() > parasurfacelist[itmp].uvnode(int(parasurfacelist[itmp].nu() / 2), int(parasurfacelist[itmp].nv() / 2)).z(); j--){
			hsurfaceid[j + 1] = hsurfaceid[j];
		}
		hsurfaceid[j + 1] = itmp;
	}
}

void REGION::verticalwellrefine(){
	int nnode = out.numberofpoints;
	//out.numberofpointmtrs = out.numberofpoints;
	//out.pointmtrlist = new REAL[out.numberofpointmtrs];

	std::vector<std::vector<NODE>> wellsnodelist;
	wellsnodelist.resize(welllist.size());
	for (int i = 0; i < nnode; i++){// reason:more than one well & maybe more added nodes than in TETGENIO::addin
		int flag_ = 0;
		NODE node_;
		//out.pointmtrlist[i+1] = meshinfo.guideedgelength();
		node_.x(out.pointlist[i * 3]);
		node_.y(out.pointlist[i * 3+1]);
		node_.z(out.pointlist[i * 3+2]);
		for (int iwell = 0; iwell < welllist.size(); iwell++){
			std::vector<NODE> geonodelist_ = welllist[iwell].geonodelist();
			for (int j = 0; j < geonodelist_.size() - 1; j++){
				NODE node1_ = geonodelist_[j];
				NODE node2_ = geonodelist_[j + 1];
				if (std::abs(mathzz_.twonodedistance(node_, node1_) + mathzz_.twonodedistance(node_, node2_) - mathzz_.twonodedistance(node1_, node2_)) < 0.00001){
					wellsnodelist[iwell].push_back(node_);
					flag_ = 1;
					break;
				}
			}
			if (flag_ == 1){
				//out.pointmtrlist[i] = 0.5*meshinfo.guideedgelength();
				break;
			}
		}
	}
	for (int iwell = 0; iwell < welllist.size(); iwell++){
		//reorder all well nodes
		for (int i = 1; i < wellsnodelist[iwell].size(); i++){ //straight insertion sort
			NODE itmp = wellsnodelist[iwell][i];
			int j;
			for (j = i - 1; j >= 0 && wellsnodelist[iwell][j].z() > itmp.z(); j--){
				wellsnodelist[iwell][j + 1] = wellsnodelist[iwell][j];
			}
			wellsnodelist[iwell][j + 1] = itmp;
		}
	}
	for (int iwell = 0; iwell < welllist.size(); iwell++){
		int limit = wellsnodelist[iwell].size() - 1;
		for (int i = 0; i < limit; i++){
			double xx = 0.5*(wellsnodelist[iwell][i].x() + wellsnodelist[iwell][i + 1].x());
			double yy = 0.5*(wellsnodelist[iwell][i].y() + wellsnodelist[iwell][i + 1].y());
			double zz = 0.5*(wellsnodelist[iwell][i].z() + wellsnodelist[iwell][i + 1].z());
			NODE node_;
			node_.x(xx); node_.y(yy); node_.z(zz);
			wellsnodelist[iwell].push_back(node_);
		}
		welllist[iwell].nodelist(wellsnodelist[iwell]);
	}

	well2tetgen();
	calltetgenrefine_mid2out();
}

void REGION::wellgeonodes2addin(){

	for (int iwell = 0; iwell < welllist.size(); iwell++){
		welllist[iwell].nodelist(welllist[iwell].geonodelist());
	}
	well2tetgen();
}

void REGION::modelpreparation(){
	if (meshinfo.type() == 1 && numberofphases_==1){ //unstructured
		tetrahedralmesh_postprocessCVFE();
		//properties_cvfe_sf();
		properties_cvfe_sf_khscaledpaper();
		//properties_cvfe_sf_randompaper();
		//properties_cvfe_sf_cyclepaper();
		elementalmobit_sf();
		flowb2bsurface();
		boundarycondition2node();
		if (welllist.size() > 0){
			markwellnode();
			wellcondition2node(); //well boundary condition overwrite surface boundary condition
		}
		oilinplace();
	}
	else if (meshinfo.type() == 1 && numberofphases_ == 2){ //unstructured
		tetrahedralmesh_postprocessCVFE();
		properties_cvfe_mf();
		computecapillarypressure();
		computesaturation();
		computerelativepermeabilities();
		//computemobility_cvfe_mf();
		boundarycondition2node();
		if (welllist.size() > 0){
			markwellnode();
			wellcondition2node(); //well boundary condition overwrite surface boundary condition
		}
		oilinplace();
	}

}

void REGION::oilinplace(){ //change to node based
	int i, iregion;
	std::vector<double> rv, ro;
	rv.resize(rocklist.size());
	ro.resize(rocklist.size());
	oilreserve_ = 0;
	/*for (i = 0; i < elementlist.size(); i++){
		iregion = elementlist[i].markregion();
		rv[iregion] = rv[iregion] + elementlist[i].volume()*rocklist[iregion].porosity();
		rv[iregion] = rv[iregion] + elementlist[i].volume()*(1.0 - elementlist[i].Sw());
		oilreserve_ = oilreserve_ + elementlist[i].volume()*(1.0 - elementlist[i].Sw());
	}*/
	for (i = 0; i < rv.size(); i++){
		rocklist[i].porevolume(rv[i]);
		rocklist[i].oilip(ro[i]);// in rm3
	}

	oilreserve_ = oilreserve_*6.2898; // in stb
}



void REGION::modelpreparation_upscale(){
	meshdimension_max_av();
	properties_cvfe_sf();
	elementalmobit_sf();
}
void REGION::upscalebsurface(int idir_){
	BSURFACE bsurface_;
	bsurface_.type(0);
	for (int i = 0; i < 6; i++){
		bsurfacelist[bsurfaceid[i]] = bsurface_;
	}
	if (idir_ == 1){
		bsurfacelist[bsurfaceid[4]].type(1);
		bsurfacelist[bsurfaceid[4]].P(100 * 1.0e+5);
		bsurfacelist[bsurfaceid[4]].sign(1);
		bsurfacelist[bsurfaceid[5]].type(1);
		bsurfacelist[bsurfaceid[5]].P(0);
		bsurfacelist[bsurfaceid[5]].sign(-1);
	}
	else if (idir_ == 2){
		bsurfacelist[bsurfaceid[2]].type(1);
		bsurfacelist[bsurfaceid[2]].P(100 * 1.0e+5);
		bsurfacelist[bsurfaceid[2]].sign(1);
		bsurfacelist[bsurfaceid[3]].type(1);
		bsurfacelist[bsurfaceid[3]].P(0);
		bsurfacelist[bsurfaceid[3]].sign(-1);
	}
	else if (idir_ == 3){
		bsurfacelist[bsurfaceid[0]].type(1);
		bsurfacelist[bsurfaceid[0]].P(100 * 1.0e+5);
		bsurfacelist[bsurfaceid[0]].sign(1);
		bsurfacelist[bsurfaceid[1]].type(1);
		bsurfacelist[bsurfaceid[1]].P(0);
		bsurfacelist[bsurfaceid[1]].sign(-1);
	}
}

void REGION::steadystateflowsolver(){
	if (meshinfo.type() == 1 && numberofphases_==1){
		computepressure_cvfe();
		computevelocity_cvfe_sf();
	}
	else if (meshinfo.type() == 1 && numberofphases_ == 2){
		computepressure_cvfe();
		computevelocity_cvfe_mf();
	}
	else{
		computepressure_cpgfv();
	}
}


void REGION::flowdiagnostics_ex5spot_findcycle(){
	inoutflowb();
	//computeedgeflux_cvfe();
	//flow diagnostics based on TOF
	graph.tracingsign(-1);//forward TOF
	//graph.tracingsign(-1);//backward TOF
	tracingboundarycondition();
	buildgraph_cvfeflux();
	reordernodes_cycle();
	outputcycles();
}



void REGION::flowdiagnostics_tracing(){ //unstructured mesh
	inoutflowb();
	computeedgefluxh_cvfe();
	if (ftofbutton_ == 1 || ftracerbutton_ == 1){
		double toftime1 = GetTickCount64();
		graph.tracingsign(1);
		tracingboundarycondition();
		buildgraph_fluxh_monotone();
		reordernodes();
		if (ftofbutton_ == 1){
			computetofcvfe_local();
			double toftime2 = GetTickCount64();
			std::cout << "tof time spent is " << (toftime2 - toftime1) / CLOCKS_PER_SEC << std::endl;
		}
		if (ftracerbutton_ == 1){
			computetracercvfe_local();
			marknodesmaxtracer();
		}
	}
	if (btofbutton_ == 1 || btracerbutton_ == 1){
		graph.tracingsign(-1);
		tracingboundarycondition();
		buildgraph_fluxh_monotone();
		reordernodes();
		if (btofbutton_ == 1){
			computetofcvfe_local();
		}
		if (btracerbutton_ == 1){
			computetracercvfe_local();
			marknodesmaxtracer();
		}
	}
}

void REGION::computeflux_cpgfv(){
	int ie, j, je;
	double flux_;
	std::cout << "compute flux cpg sp" << std::endl;
	for (ie = 0; ie < cpgelementlist.size(); ie++){
		for (j = 0; j < 6; j++){
			je = cpgelementlist[ie].neighbor(j);
			if (je >= 0){
				flux_ = (cpgelementlist[ie].Po() - cpgelementlist[je].Po())*cpgelementlist[ie].trans(j);//-dp/dx
				cpgelementlist[ie].flux(j, flux_);
			}
		}
	}
}



void REGION::flowdiagnostics_matrix(){
	int i;

	inoutflowb();
//	computeedgeflux_cvfe();

	if (ftofbutton_ == 1 || ftracerbutton_ == 1){
		double toftime1 = GetTickCount64();
		graph.tracingsign(1);
		tracingboundarycondition();
		if (ftofbutton_ == 1){
			std::cout << "compute ftof*****"<< std::endl;
			computetofcvfe_matrix();
			double toftime2 = GetTickCount64();
			std::cout << "tof time spent is " << (toftime2 - toftime1) / CLOCKS_PER_SEC << std::endl;
		}
		if (ftracerbutton_ == 1){
			std::cout << "compute ftracer*****" << std::endl;
			computetracercvfe_matrix();
			marknodesmaxtracer();
		}
	}
	if (btofbutton_ == 1 || btracerbutton_ == 1){
		graph.tracingsign(-1);
		tracingboundarycondition();
		if (btofbutton_ == 1){
			std::cout << "compute btof*****" << std::endl;
			computetofcvfe_matrix();
		}
		if (btracerbutton_ == 1){
			std::cout << "compute btracer*****" << std::endl;
			computetracercvfe_matrix();
			marknodesmaxtracer();
		}
	}
}

void REGION::derivedquantities_both(){ //automatically
	int i, j, inode, iedge, n1, n2, nnode, itmp, ii;
	double allcvfeinflux, alledgeinflux, avpv, avwa, avwa2, Fc_total, Sc_total, Fc_m, Sc_m;
	std::vector<double> cvfeinflux, outflux;
	std::vector<int> nodeorder;

	nnode = nodelist.size();
	sv.clear(); dv.clear();
	wellpairPV.resize(inflowblist.size());
	WAF.resize(inflowblist.size()); //define all variables (tracer and TOF) for outputting
	Fc.resize(inflowblist.size()); //define all variables (tracer and TOF) for outputting
	Sc.resize(inflowblist.size());
	Lc.resize(inflowblist.size());
	sv.resize(inflowblist.size());
	dv.resize(outflowblist.size());
	for (i = 0; i < inflowblist.size(); i++){
		wellpairPV[i].resize(outflowblist.size());
		WAF[i].resize(outflowblist.size());
		Fc[i].resize(outflowblist.size());
		Sc[i].resize(outflowblist.size());
		Lc[i].resize(outflowblist.size());
	}

	cvfeinflux.resize(nnode);
	outflux.resize(nnode);
	nodeorder.resize(nnode);
	//check notcalculated tracers
	if (ftracerbutton_ == 0){
		if (inflowblist.size() == 1){
			for (inode = 0; inode < nnode; inode++){
				nodelist[inode].tracersize(1);
				nodelist[inode].tracer(0, 1);
			}
		}
		else{
			std::cout << "inflowblist>=1 but ftracer not computed, cannot derive quantities based on ftracers" << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}
	if (btracerbutton_ == 0){
		if (outflowblist.size() == 1){
			for (inode = 0; inode < nnode; inode++){
				nodelist[inode].tracersize_back(1);
				nodelist[inode].tracer_back(0, 1);
			}
		}
		else{
			std::cout << "outflowblist>=1 but btracer not computed, cannot derive quantities based on btracers" << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}

	//compute swept volume and drainage volume
	for (inode = 0; inode < nnode; inode++){
		for (i = 0; i < inflowblist.size(); i++){
			if (nodelist[inode].tracer(i) < 0){
				std::cout << "tracer negative " << inode << " " << nodelist[inode].tracer(i) << std::endl;
			}
			sv[i] = sv[i] + nodelist[inode].tracer(i)*nodelist[inode].dualvolume()*nodelist[inode].porosity();
		}
		for (j = 0; j < outflowblist.size(); j++){
			if (nodelist[inode].tracer_back(j) < 0){
				std::cout << "tracer negative " << inode << " " << nodelist[inode].tracer_back(j) << std::endl;
			}
			dv[j] = dv[j] + nodelist[inode].tracer_back(j)*nodelist[inode].dualvolume()*nodelist[inode].porosity();
		}
	}

	//compute influx for each node
	for (iedge = 0; iedge < nedge; iedge++){
		n1 = edgelist[iedge].node(0);
		n2 = edgelist[iedge].node(1);
		if (edgelist[iedge].flux() >= 0){
			cvfeinflux[n2] = cvfeinflux[n2] + edgelist[iedge].flux();
			outflux[n1] = outflux[n1] + edgelist[iedge].flux();
		}
		if (edgelist[iedge].flux() < 0){
			cvfeinflux[n1] = cvfeinflux[n1] - edgelist[iedge].flux();
			outflux[n2] = outflux[n2] - edgelist[iedge].flux();
		}
	}

	for (i = 0; i < nnode; i++){
		if (cvfeinflux[i] == 0){//for boundary nodes
			cvfeinflux[i] = outflux[i];
		}
	}

	//compute all flux; compute tracerpair, wellpairpv and flow rate for each node
	allcvfeinflux = 0;
	for (inode = 0; inode < nnode; inode++){
		allcvfeinflux = allcvfeinflux + cvfeinflux[inode];
		nodelist[inode].computetracerpair();
		nodelist[inode].computewellpairPV();
		nodelist[inode].computewellpairflowrate();
	}

	//compute wellpairPV and WAF
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			for (inode = 0; inode < nnode; inode++){
				wellpairPV[i][j] = wellpairPV[i][j] + nodelist[inode].wellpairPV(i, j);
				WAF[i][j] = WAF[i][j] + nodelist[inode].tracerpair(i, j)*cvfeinflux[inode];
			}
			WAF[i][j] = WAF[i][j] / allcvfeinflux;
		}
	}
	std::cout << "computing TOF-based derived quantities" << std::endl;
	//order all nodes in ascending total tof----expensive for large mesh

	if (ftofbutton_ == 0 || btofbutton_ == 0){
		std::cout << "both ftof and btof should be computed" << std::endl;
	}

	for (i = 0; i < nnode; i++){
		nodeorder[i] = i;
	}

	for (i = 1; i < nodeorder.size(); i++){ //straight insertion sort
		itmp = nodeorder[i];
		for (j = i - 1; j >= 0 && nodelist[nodeorder[j]].tof_total() > nodelist[itmp].tof_total(); j--){
			nodeorder[j + 1] = nodeorder[j];
		}
		nodeorder[j + 1] = itmp;
	}

	//for each well pair compute F and Phi and Lc
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			Fc_m = 0;
			Sc_m = 0;
			for (ii = 0; ii < nodeorder.size(); ii++){
				inode = nodeorder[ii];
				if (nodelist[inode].tracerpair(i, j)>0){//term is 0 if tracerpair value=0
					Fc_m = Fc_m + nodelist[inode].wellpairflowrate(i, j);
					Sc_m = Sc_m + nodelist[inode].wellpairPV(i, j);
					Fc[i][j].push_back(Fc_m);
					Sc[i][j].push_back(Sc_m);
				}
			}
			for (ii = 0; ii < Fc[i][j].size(); ii++){
				Fc[i][j][ii] = Fc[i][j][ii] / Fc_m;
				Sc[i][j][ii] = Sc[i][j][ii] / Sc_m;
			}
			//Lorenz Coefficient
			Lc[i][j] = Fc[i][j][0] * Sc[i][j][0];
			for (ii = 1; ii < Fc[i][j].size(); ii++){
				Lc[i][j] = Lc[i][j] + (Fc[i][j][ii] + Fc[i][j][ii - 1])*(Sc[i][j][ii] - Sc[i][j][ii - 1]);
			}
			Lc[i][j] = Lc[i][j] - 1;
		}
	}
}

void REGION::testsorting(){ //automatically
	int i, j, inode, iedge, n1, n2, nnode, itmp, ii;
	double allcvfeinflux, alledgeinflux, avpv, avwa, avwa2, Fc_total, Sc_total, Fc_m, Sc_m;
	std::vector<double> cvfeinflux, outflux;
	std::vector<int> nodeorder;
	nnode = nodelist.size();

	nodeorder.resize(nnode);

	//order all nodes in ascending total tof----expensive for large mesh
	std::cout << "Sorting started" << std::endl;
	for (i = 0; i < nnode; i++){
		nodeorder[i] = i;
	}
	double sorttime1 = GetTickCount64();
	for (i = 1; i < nodeorder.size(); i++){ //straight insertion sort
		itmp = nodeorder[i];
		for (j = i - 1; j >= 0 && nodelist[nodeorder[j]].tof_total() > nodelist[itmp].tof_total(); j--){
			nodeorder[j + 1] = nodeorder[j];
		}
		nodeorder[j + 1] = itmp;
	}
	double sorttime2 = GetTickCount64();
	std::cout << "Sorting time spent is " << (sorttime2 - sorttime1) / CLOCKS_PER_SEC <<" for "<<nnode<<" nodes"<< std::endl;
}


void REGION::derivedquantities_tracer(){ //automatically
	int i, j, inode, iedge, n1, n2, nnode, itmp, ii;
	double allcvfeinflux, alledgeinflux, avpv, avwa, avwa2, Fc_total, Sc_total, Fc_m, Sc_m;
	std::vector<double> cvfeinflux, outflux;
	std::vector<int> nodeorder;
	nnode = nodelist.size();
	sv.clear(); dv.clear();
	wellpairPV.resize(inflowblist.size());
	WAF.resize(inflowblist.size()); //define all variables (tracer and TOF) for outputting
	Fc.resize(inflowblist.size()); //define all variables (tracer and TOF) for outputting
	Sc.resize(inflowblist.size());
	Lc.resize(inflowblist.size());
	sv.resize(inflowblist.size());
	dv.resize(outflowblist.size());
	for (i = 0; i < inflowblist.size(); i++){
		wellpairPV[i].resize(outflowblist.size());
		WAF[i].resize(outflowblist.size());
		Fc[i].resize(outflowblist.size());
		Sc[i].resize(outflowblist.size());
		Lc[i].resize(outflowblist.size());
	}
	cvfeinflux.clear();
	outflux.clear();
	cvfeinflux.resize(nnode);
	outflux.resize(nnode);
	nodeorder.resize(nnode);
	//check notcalculated tracers
	if (ftracerbutton_ == 0){
		if (inflowblist.size() == 1){
			for (inode = 0; inode < nnode; inode++){
				nodelist[inode].tracersize(1);
				nodelist[inode].tracer(0, 1);
			}
		}
		else{
			std::cout << "inflowblist>=1 but ftracer not computed, cannot derive quantities based on ftracers" << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}
	if (btracerbutton_ == 0){
		if (outflowblist.size() == 1){
			for (inode = 0; inode < nnode; inode++){
				nodelist[inode].tracersize_back(1);
				nodelist[inode].tracer_back(0, 1);
			}
		}
		else{
			std::cout << "outflowblist>=1 but btracer not computed, cannot derive quantities based on btracers" << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}


	//compute swept volume and drainage volume
	for (inode = 0; inode < nnode; inode++){
		for (i = 0; i < inflowblist.size(); i++){
			if (nodelist[inode].tracer(i) < 0){
				std::cout << "tracer negative " << inode << " " << nodelist[inode].tracer(i) << std::endl;
			}
			sv[i] = sv[i] + nodelist[inode].tracer(i)*nodelist[inode].dualvolume()*nodelist[inode].porosity();
		}
		for (j = 0; j < outflowblist.size(); j++){
			if (nodelist[inode].tracer_back(i) < 0){
				std::cout << "tracer negative " << inode << " " << nodelist[inode].tracer_back(i) << std::endl;
			}
			dv[j] = dv[j] + nodelist[inode].tracer_back(j)*nodelist[inode].dualvolume()*nodelist[inode].porosity();
		}
	}

	//compute influx for each node
	for (iedge = 0; iedge < nedge; iedge++){
		n1 = edgelist[iedge].node(0);
		n2 = edgelist[iedge].node(1);
		if (edgelist[iedge].flux() >= 0){
			cvfeinflux[n2] = cvfeinflux[n2] + edgelist[iedge].flux();
			outflux[n1] = outflux[n1] + edgelist[iedge].flux();
		}
		if (edgelist[iedge].flux() < 0){
			cvfeinflux[n1] = cvfeinflux[n1] - edgelist[iedge].flux();
			outflux[n2] = outflux[n2] - edgelist[iedge].flux();
		}
	}

	for (i = 0; i < nnode; i++){
		if (cvfeinflux[i] == 0){//for boundary nodes
			cvfeinflux[i] = outflux[i];
		}
	}



	/*
	for (iedge = 0; iedge < nedge; iedge++){
	n1 = edgelist[iedge].node(0);
	n2 = edgelist[iedge].node(1);
	if (edgelist[iedge].flux() >= 0){
	edgeinflux[n2] = edgeinflux[n2] + edgelist[iedge].fluxcvfe();
	}
	if (edgelist[iedge].flux() < 0){
	edgeinflux[n1] = edgeinflux[n1] - edgelist[iedge].fluxcvfe();
	}
	}
	*/

	//compute all flux; compute tracerpair, wellpairpv and flow rate for each node
	allcvfeinflux = 0;
	for (inode = 0; inode < nnode; inode++){
		allcvfeinflux = allcvfeinflux + cvfeinflux[inode];
		nodelist[inode].computetracerpair();
		nodelist[inode].computewellpairPV();
		nodelist[inode].computewellpairflowrate();
	}

	//compute wellpairPV and WAF
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			for (inode = 0; inode < nnode; inode++){
				wellpairPV[i][j] = wellpairPV[i][j] + nodelist[inode].wellpairPV(i, j);
				WAF[i][j] = WAF[i][j] + nodelist[inode].tracerpair(i, j)*cvfeinflux[inode];
			}
			WAF[i][j] = WAF[i][j] / allcvfeinflux;
		}
	}

}

void REGION::derivedquantities_cpgfv(){ //automatically cpg
	std::cout << "Compute derived quantities for flow diagnostics...";
	int i, j, ie, je, iface, n1, n2, nele, itmp, ii;
	double allinflux, avpv, avwa, avwa2, Fc_total, Sc_total, Fc_m, Sc_m, flux_;
	std::vector<double> influx, outflux;
	std::vector<int> eleorder;
	nele = cpgelementlist.size();
	sv.clear(); dv.clear(); wellpairPV.clear(); WAF.clear(); Lc.clear();
	Fc.clear(); Sc.clear(); t_D.clear(); Ev.clear(); Fc2.clear(); Sc2.clear(); t_D2.clear(); Ev2.clear(); t_D3.clear(); Ev3.clear();

	wellpairPV.resize(inflowblist.size());
	WAF.resize(inflowblist.size());
	Fc.resize(inflowblist.size());//define all variables (tracer and TOF) for outputting
	Fc2.resize(inflowblist.size());
	Sc.resize(inflowblist.size());
	Sc2.resize(inflowblist.size());
	Lc.resize(inflowblist.size());
	sv.resize(inflowblist.size());
	dv.resize(outflowblist.size());
	t_D.resize(inflowblist.size());
	Ev.resize(inflowblist.size());
	t_D2.resize(inflowblist.size());
	Ev2.resize(inflowblist.size());
	t_D3.resize(inflowblist.size());
	Ev3.resize(inflowblist.size());
	for (i = 0; i < inflowblist.size(); i++){
		wellpairPV[i].resize(outflowblist.size());
		WAF[i].resize(outflowblist.size());
		Fc[i].resize(outflowblist.size());
		Sc[i].resize(outflowblist.size());
		Lc[i].resize(outflowblist.size());
		t_D[i].resize(outflowblist.size());
		Ev[i].resize(outflowblist.size());
		Fc2[i].resize(outflowblist.size());
		Sc2[i].resize(outflowblist.size());
		t_D2[i].resize(outflowblist.size());
		Ev2[i].resize(outflowblist.size());
		t_D3[i].resize(outflowblist.size());
		Ev3[i].resize(outflowblist.size());
	}
	//influx.clear(); no need to clear since they are local variables
	//outflux.clear();
	influx.resize(nele);
	outflux.resize(nele);
	eleorder.resize(nele);

	//compute swept volume and drainage volume
	for (ie = 0; ie < nele; ie++){
		for (i = 0; i < inflowblist.size(); i++){
			sv[i] = sv[i] + cpgelementlist[ie].tracer(i)*cpgelementlist[ie].volume()*cpgelementlist[ie].porosity();
		}
		for (j = 0; j < outflowblist.size(); j++){
			dv[j] = dv[j] + cpgelementlist[ie].tracer_back(j)*cpgelementlist[ie].volume()*cpgelementlist[ie].porosity();
		}
	}

	//compute influx for each cpgelement
	for (ie = 0; ie < nele; ie++){
		for (iface = 0; iface < 6; iface++){
			je = cpgelementlist[ie].neighbor(iface);
			if (je >= 0){
				flux_ = cpgelementlist[ie].flux(iface);
				if (flux_ < -0){
					influx[ie] = influx[ie] - flux_;
				}
				else if (flux_>0){
					outflux[ie] = outflux[ie] + flux_;
				}
			}
		}
		if (influx[ie] == 0){ //for boundary cpgelements
			influx[ie] = outflux[ie];
		}
	}


	//compute all flux; compute tracerpair, wellpairpv and flow rate for each node
	allinflux = 0;
	for (ie = 0; ie < nele; ie++){
		allinflux = allinflux + influx[ie];
		cpgelementlist[ie].computetracerpair();
		cpgelementlist[ie].computewellpairPV();
		cpgelementlist[ie].computewellpairflowrate();
	}

	//compute wellpairPV and WAF
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			for (ie = 0; ie < nele; ie++){
				wellpairPV[i][j] = wellpairPV[i][j] + cpgelementlist[ie].wellpairPV(i, j);
				WAF[i][j] = WAF[i][j] + cpgelementlist[ie].tracerpair(i, j)*influx[ie];
			}
			WAF[i][j] = WAF[i][j] / allinflux;
		}
	}

	//order all nodes in ascending total tof
	for (i = 0; i < nele; i++){
		eleorder[i] = i;
	}

	for (i = 1; i < eleorder.size(); i++){ //straight insertion sort
		itmp = eleorder[i];
		for (j = i - 1; j >= 0 && cpgelementlist[eleorder[j]].tof_total() > cpgelementlist[itmp].tof_total(); j--){
			eleorder[j + 1] = eleorder[j];
		}
		eleorder[j + 1] = itmp;
	}
	totalFc.clear(); totalSc.clear(); totalEv.clear(); totalt_D.clear(); totalLc = 0;
	totalFc.resize(nele); totalSc.resize(nele); totalEv.resize(nele); totalt_D.resize(nele);
	//compute total F and Phi and Lc for whole model
	Fc_m = 0; Sc_m = 0;
	for (ii = 0; ii < eleorder.size(); ii++){
		ie = eleorder[ii];
		Fc_m = Fc_m + cpgelementlist[ie].porosity()*cpgelementlist[ie].volume()/cpgelementlist[ie].tof_total();
		Sc_m = Sc_m + cpgelementlist[ie].porosity()*cpgelementlist[ie].volume();
		totalFc[ii] = Fc_m;
		totalSc[ii] = Sc_m;
	}
	for (ii = 0; ii < totalFc.size(); ii++){
		totalFc[ii] = totalFc[ii] / Fc_m;
		totalSc[ii] = totalSc[ii] / Sc_m;
	}

	totalLc = totalFc[0] * totalSc[0];
	for (ii = 1; ii < totalFc.size(); ii++){
		totalLc = totalLc + (totalFc[ii] + totalFc[ii - 1])*(totalSc[ii] - totalSc[ii - 1]);
	}
	totalLc = totalLc - 1;

	//for each well pair compute F and Phi and Lc
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			Fc_m = 0;
			Sc_m = 0;
			for (ii = 0; ii < eleorder.size(); ii++){
				ie = eleorder[ii];
				if (cpgelementlist[ie].tracerpair(i, j)>0){//term is 0 if tracerpair value=0
					Fc_m = Fc_m + cpgelementlist[ie].wellpairflowrate(i, j);
					Sc_m = Sc_m + cpgelementlist[ie].wellpairPV(i, j);
					Fc[i][j].push_back(Fc_m);
					Sc[i][j].push_back(Sc_m);
				}
			}
			for (ii = 0; ii < Fc[i][j].size(); ii++){
				Fc[i][j][ii] = Fc[i][j][ii] / Fc_m;
				Sc[i][j][ii] = Sc[i][j][ii] / Sc_m;
			}
			//Lorenz Coefficient
			Lc[i][j] = Fc[i][j][0] * Sc[i][j][0];
			for (ii = 1; ii < Fc[i][j].size(); ii++){
				Lc[i][j] = Lc[i][j] + (Fc[i][j][ii] + Fc[i][j][ii - 1])*(Sc[i][j][ii] - Sc[i][j][ii - 1]);
			}
			Lc[i][j] = Lc[i][j] - 1;
		}
	}
	//compute total Ev and t_D
	for (ii = 1; ii < totalFc.size(); ii++){
		if (totalFc[ii] == totalFc[ii - 1]){
			totalt_D[ii] = totalt_D[ii - 1];
			totalEv[ii] = totalEv[ii - 1];
		}
		else{
			totalt_D[ii] = (totalSc[ii] - totalSc[ii - 1]) / (totalFc[ii] - totalFc[ii - 1]);
			totalEv[ii] = totalSc[ii] + (1 - totalFc[ii])*totalt_D[ii];
		}
	}

	//compute sweep efficiency and dimensionless time based on F-Phi plot
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			t_D[i][j].resize(Fc[i][j].size());
			Ev[i][j].resize(Fc[i][j].size());
			for (ii = 1; ii < Fc[i][j].size(); ii++){
				if (Fc[i][j][ii] == Fc[i][j][ii - 1]){
					t_D[i][j][ii] = t_D[i][j][ii - 1];
					Ev[i][j][ii] = Ev[i][j][ii - 1];
				}
				else{
					t_D[i][j][ii] = (Sc[i][j][ii] - Sc[i][j][ii - 1]) / (Fc[i][j][ii] - Fc[i][j][ii - 1]);
					Ev[i][j][ii] = Sc[i][j][ii] + (1 - Fc[i][j][ii])*t_D[i][j][ii];
				}
			}
			/*for (ii = 1; ii < Fc[i][j].size(); ii++){
				if ((t_D[i][j][ii] - t_D[i][j][ii - 1])*(t_D[i][j][ii] - t_D[i][j][ii + 1]) > 0){
					t_D[i][j][ii] = t_D[i][j][ii - 1];
					Ev[i][j][ii] = Ev[i][j][ii - 1];
				}
			}*/
		}
	}
	/*for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			int nplus = Fc[i][j].size() / 200;
			for (ii = 0; ii < Fc[i][j].size(); ii = ii + nplus){
				Fc2[i][j].push_back(Fc[i][j][ii]);
				Sc2[i][j].push_back(Sc[i][j][ii]);
				Ev2[i][j].push_back(Ev[i][j][ii]);
				t_D2[i][j].push_back(t_D[i][j][ii]);
			}
			for (ii = 0; ii < Ev2[i][j].size(); ii++){
				if ((t_D2[i][j][ii] - t_D2[i][j][ii - 1])*(t_D2[i][j][ii] - t_D2[i][j][ii + 1]) < 0){
					t_D3[i][j].push_back(t_D2[i][j][ii]);
					Ev3[i][j].push_back(Ev2[i][j][ii]);
				}
			}
		}
	}*/
	std::cout << "Done"<<std::endl;
}

void REGION::derivedquantities_cpgfv_energy(){ //automatically cpg
	std::cout << "Compute Energy diagnostics derived quantities...";
	int i, j, ie, je, iface, n1, n2, nele, itmp, ii;
	double allinflux, avpv, avwa, avwa2, Fc_total, Sc_total, Fc_m, Sc_m, flux_;
	std::vector<double> influx, outflux;
	std::vector<int> eleorder;
	nele = cpgelementlist.size();
	sv.clear(); dv.clear(); wellpairPV.clear(); WAF.clear(); Lc.clear();
	Fc.clear(); Sc.clear(); t_D.clear(); Ev.clear(); Fc2.clear(); Sc2.clear(); t_D2.clear(); Ev2.clear(); t_D3.clear(); Ev3.clear();
	wellpairPV.resize(inflowblist.size());
	WAF.resize(inflowblist.size());
	Fc.resize(inflowblist.size());//define all variables (tracer and TOF) for outputting
	Fc2.resize(inflowblist.size());
	Sc.resize(inflowblist.size());
	Sc2.resize(inflowblist.size());
	Lc.resize(inflowblist.size());
	sv.resize(inflowblist.size());
	dv.resize(outflowblist.size());
	t_D.resize(inflowblist.size());
	Ev.resize(inflowblist.size());
	t_D2.resize(inflowblist.size());
	Ev2.resize(inflowblist.size());
	t_D3.resize(inflowblist.size());
	Ev3.resize(inflowblist.size());
	for (i = 0; i < inflowblist.size(); i++){
		wellpairPV[i].resize(outflowblist.size());
		WAF[i].resize(outflowblist.size());
		Fc[i].resize(outflowblist.size());
		Sc[i].resize(outflowblist.size());
		Lc[i].resize(outflowblist.size());
		t_D[i].resize(outflowblist.size());
		Ev[i].resize(outflowblist.size());
		Fc2[i].resize(outflowblist.size());
		Sc2[i].resize(outflowblist.size());
		t_D2[i].resize(outflowblist.size());
		Ev2[i].resize(outflowblist.size());
		t_D3[i].resize(outflowblist.size());
		Ev3[i].resize(outflowblist.size());
	}
	influx.resize(nele);
	outflux.resize(nele);
	eleorder.resize(nele);

	//compute swept energy and drainage energy
	for (ie = 0; ie < nele; ie++){
		for (i = 0; i < inflowblist.size(); i++){
			sv[i] = sv[i] + cpgelementlist[ie].tracer(i)*cpgelementlist[ie].porevolume()*rho_w*cp_w*cpgelementlist[ie].temperature;//cp is effective cp*rho
		}
		for (j = 0; j < outflowblist.size(); j++){
			dv[j] = dv[j] + cpgelementlist[ie].tracer_back(j)*cpgelementlist[ie].porevolume()*rho_w*cp_w*cpgelementlist[ie].temperature;
		}
	}

	//compute influx power for each cpgelement
	for (ie = 0; ie < nele; ie++){
		double cpti = rho_w*cp_w*cpgelementlist[ie].temperature;
		for (iface = 0; iface < 6; iface++){
			je = cpgelementlist[ie].neighbor(iface);
			if (je >= 0){
				double cptj = rho_w*cp_w*cpgelementlist[je].temperature;
				flux_ = cpgelementlist[ie].flux(iface)*(cpti+cptj)*0.5;
				if (flux_ < -0){
					influx[ie] = influx[ie] - flux_;
				}
				else if (flux_>0){
					outflux[ie] = outflux[ie] + flux_;
				}
			}
		}
		if (influx[ie] == 0){ //for boundary cpgelements
			influx[ie] = outflux[ie];
		}
	}


	//compute all flux; compute tracerpair, wellpairpv and flow rate for each node
	allinflux = 0;
	for (ie = 0; ie < nele; ie++){
		allinflux = allinflux + influx[ie];
		cpgelementlist[ie].computetracerpair();
		cpgelementlist[ie].computewellpairPV();
		cpgelementlist[ie].computewellpairflowrate();
	}

	//compute wellpairPV(energy) and WAF
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			for (ie = 0; ie < nele; ie++){
				wellpairPV[i][j] = wellpairPV[i][j] + cpgelementlist[ie].wellpairPV(i, j)*rho_w*cp_w*cpgelementlist[ie].temperature;//well pair energy
				WAF[i][j] = WAF[i][j] + cpgelementlist[ie].tracerpair(i, j)*influx[ie];//well pair influx power
			}
			WAF[i][j] = WAF[i][j] / allinflux;
		}
	}

	//order all nodes in ascending total tof
	for (i = 0; i < nele; i++){
		eleorder[i] = i;
	}

	for (i = 1; i < eleorder.size(); i++){ //straight insertion sort
		itmp = eleorder[i];
		for (j = i - 1; j >= 0 && cpgelementlist[eleorder[j]].tof_total() > cpgelementlist[itmp].tof_total(); j--){
			eleorder[j + 1] = eleorder[j];
		}
		eleorder[j + 1] = itmp;
	}

	totalFc.clear(); totalSc.clear(); totalEv.clear(); totalt_D.clear(); totalLc = 0;
	totalFc.resize(nele); totalSc.resize(nele); totalEv.resize(nele); totalt_D.resize(nele);
	//compute total F and Phi and Lc for whole model
	Fc_m = 0; Sc_m = 0;
	for (ii = 0; ii < eleorder.size(); ii++){
		ie = eleorder[ii];
		Fc_m = Fc_m + rho_w*cp_w*cpgelementlist[ie].temperature*cpgelementlist[ie].porevolume() / cpgelementlist[ie].tof_total();
		Sc_m = Sc_m + rho_w*cp_w*cpgelementlist[ie].temperature*cpgelementlist[ie].porevolume();
		totalFc[ii] = Fc_m;
		totalSc[ii] = Sc_m;
	}
	for (ii = 0; ii < totalFc.size(); ii++){
		totalFc[ii] = totalFc[ii] / Fc_m;
		totalSc[ii] = totalSc[ii] / Sc_m;
	}

	totalLc = totalFc[0] * totalSc[0];
	for (ii = 1; ii < totalFc.size(); ii++){
		totalLc = totalLc + (totalFc[ii] + totalFc[ii - 1])*(totalSc[ii] - totalSc[ii - 1]);
	}
	totalLc = totalLc - 1;

	//for each well pair compute F and Phi and Lc
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			Fc_m = 0;
			Sc_m = 0;
			for (ii = 0; ii < eleorder.size(); ii++){
				ie = eleorder[ii];
				if (cpgelementlist[ie].tracerpair(i, j)>0){//term is 0 if tracerpair value=0
					Fc_m = Fc_m + cpgelementlist[ie].wellpairflowrate(i, j)*rho_w*cp_w*cpgelementlist[ie].temperature;//power
					Sc_m = Sc_m + cpgelementlist[ie].wellpairPV(i, j)*rho_w*cp_w*cpgelementlist[ie].temperature;//energy
					Fc[i][j].push_back(Fc_m);
					Sc[i][j].push_back(Sc_m);
				}
			}
			for (ii = 0; ii < Fc[i][j].size(); ii++){
				Fc[i][j][ii] = Fc[i][j][ii] / Fc_m;
				Sc[i][j][ii] = Sc[i][j][ii] / Sc_m;
			}
			//Lorenz Coefficient
			Lc[i][j] = Fc[i][j][0] * Sc[i][j][0];
			for (ii = 1; ii < Fc[i][j].size(); ii++){
				Lc[i][j] = Lc[i][j] + (Fc[i][j][ii] + Fc[i][j][ii - 1])*(Sc[i][j][ii] - Sc[i][j][ii - 1]);
			}
			Lc[i][j] = Lc[i][j] - 1;
		}
	}
	//compute total Ev and t_D
	for (ii = 1; ii < totalFc.size(); ii++){
		if (totalFc[ii] == totalFc[ii - 1]){
			totalt_D[ii] = totalt_D[ii - 1];
			totalEv[ii] = totalEv[ii - 1];
		}
		else{
			totalt_D[ii] = (totalSc[ii] - totalSc[ii - 1]) / (totalFc[ii] - totalFc[ii - 1]);
			totalEv[ii] = totalSc[ii] + (1 - totalFc[ii])*totalt_D[ii];
		}
	}
	//compute sweep efficiency and dimensionless time based on F-Phi plot
	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			t_D[i][j].resize(Fc[i][j].size());
			Ev[i][j].resize(Fc[i][j].size());
			for (ii = 1; ii < Fc[i][j].size(); ii++){
				if (Fc[i][j][ii] == Fc[i][j][ii - 1]){
					t_D[i][j][ii] = t_D[i][j][ii - 1];
					Ev[i][j][ii] = Ev[i][j][ii - 1];
				}
				else{
					t_D[i][j][ii] = (Sc[i][j][ii] - Sc[i][j][ii - 1]) / (Fc[i][j][ii] - Fc[i][j][ii - 1]);
					Ev[i][j][ii] = Sc[i][j][ii] + (1 - Fc[i][j][ii])*t_D[i][j][ii];
				}
			}
			/*for (ii = 1; ii < Fc[i][j].size(); ii++){
			if ((t_D[i][j][ii] - t_D[i][j][ii - 1])*(t_D[i][j][ii] - t_D[i][j][ii + 1]) > 0){
			t_D[i][j][ii] = t_D[i][j][ii - 1];
			Ev[i][j][ii] = Ev[i][j][ii - 1];
			}
			}*/
		}
	}
	/*for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			int nplus = Fc[i][j].size() / 200;
			for (ii = 0; ii < Fc[i][j].size(); ii = ii + nplus){
				Fc2[i][j].push_back(Fc[i][j][ii]);
				Sc2[i][j].push_back(Sc[i][j][ii]);
				Ev2[i][j].push_back(Ev[i][j][ii]);
				t_D2[i][j].push_back(t_D[i][j][ii]);
			}
			for (ii = 0; ii < Ev2[i][j].size(); ii++){
				if ((t_D2[i][j][ii] - t_D2[i][j][ii - 1])*(t_D2[i][j][ii] - t_D2[i][j][ii + 1]) < 0){
					t_D3[i][j].push_back(t_D2[i][j][ii]);
					Ev3[i][j].push_back(Ev2[i][j][ii]);
				}
			}
		}
	}*/
	std::cout << "Done" << std::endl;
}

void REGION::averagemodelproperties_cpg(){
	double avkx = 0;
	double avky = 0;
	double avkz = 0;
	double avporo = 0;
	for (int i = 0; i < cpgelementlist.size(); i++){
		avkx = avkx + cpgelementlist[i].k(0, 0);
		avky = avky + cpgelementlist[i].k(1, 1);
		avkz = avkz + cpgelementlist[i].k(2, 2);
		avporo = avporo + cpgelementlist[i].porosity();
	}
	avkx = avkx / cpgelementlist.size();
	avky = avky / cpgelementlist.size();
	avkz = avkz / cpgelementlist.size();
	avporo = avporo / cpgelementlist.size();
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].k(0, 0, avkx);
		cpgelementlist[i].k(1, 1, avky);
		cpgelementlist[i].k(2, 2, avkz);
		cpgelementlist[i].porosity(avporo);
	}
}

void REGION::writeflowdiagnostics(char* filename){
	std::ofstream out;
	int i, j, ii;
	std::cout << "outputting flow diagnostics information" << std::endl;
	out.open(filename);
	out << "Flow Diagnostic Derived Quantities" << std::endl;
	out << "total hydrocarbon reserve is " << oilreserve_ << " stb (formation volume factor 1)" << std::endl;
	for (i = 0; i < inflowblist.size(); i++){
		out << "injector " << i << " is ";
		if (inflowblist[i].type() == 1){
			out << "well " << inflowblist[i].mark() << std::endl;
		}
		else if (inflowblist[i].type() == 2){
			out << "boundary surface " << inflowblist[i].mark();
		}
		out << " swept volume is " << sv[i] << " m3" << std::endl;
	}
	for (j = 0; j < outflowblist.size(); j++){
		out << "producer " << j << " is ";
		if (outflowblist[j].type() == 1){
			out << "well " << outflowblist[j].mark() << std::endl;
		}
		else if (outflowblist[j].type() == 2){
			out << "boundary surface " << outflowblist[j].mark();
		}
		out << " drainage volume is " << dv[j] << " m3" << std::endl;
	}
	out << "Total Lorenz Coefficient: " << totalLc << std::endl;
	out << "Total Flow/Storage Capacity" << std::endl;
	for (ii = 0; ii < totalFc.size(); ii++){
		out << totalFc[ii] << " " << totalSc[ii] << std::endl;
	}
	out << "Total Sweep efficiency vs dimensionless time " << std::endl;
	for (ii = 0; ii < totalt_D.size(); ii++){
		out << totalEv[ii] << " " << totalt_D[ii] << std::endl;
	}

	for (i = 0; i < inflowblist.size(); i++){
		for (j = 0; j < outflowblist.size(); j++){
			out << "For injector-producer pair " << i << " " << j << std::endl;
			out << "Pore Volume: " << wellpairPV[i][j] << " m3" << std::endl;
			out << "Well Allocation Factor: " << WAF[i][j] << std::endl;
			out << "Lorenz Coefficient: " << Lc[i][j] << std::endl;
			out << "Flow/Storage Capacity" << std::endl;
			for (ii = 0; ii < Fc[i][j].size(); ii++){
				out << Fc[i][j][ii] << " " << Sc[i][j][ii] << std::endl;
			}
			out << "Sweep efficiency vs dimensionless time " << std::endl;
			for (ii = 0; ii < t_D[i][j].size(); ii++){
				out << Ev[i][j][ii] << " " << t_D[i][j][ii] << std::endl;
			}
			/*int nplus = Fc[i][j].size() / 1000;
			for (ii = 0; ii < Fc[i][j].size(); ii=ii+nplus){
				out << Fc[i][j][ii] << " " << Sc[i][j][ii] << std::endl;
			}
			out << "Sweep efficiency vs dimensionless time " << std::endl;
			for (ii = 0; ii < Fc[i][j].size(); ii = ii + nplus){
				out << Ev[i][j][ii] << " " << t_D[i][j][ii] << std::endl;
			}*/
		}
	}
}

//void REGION::tolerance(double input2){

	//tolerance2 = input2; //for velocity
//}


void REGION::userinput_unstructured_skt_MF(const char* inputfile){//single or two-phase; for input skt (unstructured&cpg); no key word
	int i, number_, type_, length_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, resolutiontype_, nx, ny, nz;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, x, y, z, rvalue_;
	double rho_o_, mu_w_, Siw_, mu_o_, rho_w_, Pct_, lamda_;

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;
	std::ifstream inputrrm;
	ROCK rock_;

	//default inputs for unstructured+skt+MF
	vis_regionidbutton_ = 1;
	vis_permbutton_ = 1;
	linearsolverbutton_ = 2;
	inputsurfacetype_ == 1;//skt
	numberofphases_ = 2; //two-phase
	meshinfo.type(1);//unstructured tetrahedral mesh
	strcpy(tetgencommand, "pq1.5/10Ann");
	strcpy(trianglecommand, "pa");
	inputrrm.open(inputfile);

	//user inputs (need also provide well file)
	inputrrm >> resolutiontype_ >> rvalue_;
	if (resolutiontype_ == 1){
		meshinfo.resolutiontype(1);
		meshinfo.resolutionxnumber(rvalue_);
	}
	else if (resolutiontype_ == 2){
		meshinfo.resolutiontype(2);
		meshinfo.guideedgelength(rvalue_);
	}


	//number of regions of different property
	//free water level; oil FVF (hydrostatic pressure); injection FVF (hydrodynamic pressure)
	inputrrm >> number_ >> h_fwl >> fvf_o >> fvf_in;

	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o_ >> mu_w_ >> rho_o_ >> rho_w_ >> Siw_ >> Pct_ >> lamda_ >> x >> y >> z;
		permlow_ = permlow_*0.987e-15;
		permhigh_ = permhigh_*0.987e-15;
		mu_o_ = mu_o_*1.0e-3;
		mu_w_ = mu_w_*1.0e-3;
		Pct_ = Pct_*1.0e+5; //bar to si
		rock_.porosity(porosity_);
		//rock_.Siw(Siw_);
		rock_.Pct(Pct_);
		rock_.lamda(lamda_);
		rock_.x(x);
		rock_.y(y);
		rock_.z(z);
		rocklist.push_back(rock_);
	}

	//number of wells
	inputrrm >> number_;

	//for wells:
	for (i = 0; i < number_; i++){
		well_.clear();
		inputrrm >> type_ >> value_ >> sign_;
		well_.type(type_); //1:press, 2:volume flow rate , 3:bottom hole press
		well_.sign(sign_); //1 injector -1 producer
		if (type_ == 1 || type_ == 3){
			value_ = value_*1.0e+5;
		}
		else if (type_ == 2){
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
		}
		well_.value(value_);
		welllist.push_back(well_);
	}

	//for flowbsurfaces
	inputrrm >> number_;
	for (i = 0; i < number_; i++){
		inputrrm >> mark_ >> type_ >> value_ >> sign_; //mark_ is the number of the bsurface; sign_=1 is inflow, -1 is outflow
		flowbsurfacemarklist.push_back(mark_); //if type==2 value change to flow rate also need to change Neumann BC related codes
		bsurface_.type(type_);                 //set ux, uy, uz not good
		bsurface_.sign(sign_);
		if (type_ == 1){
			value_ = value_*1.0e+5;
			bsurface_.P(value_);
		}
		else if (type_ == 2){
			//value_ = value_ / 6.2898*fvf_in;//bbl to meter3 * averaged formation volume factor
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
			bsurface_.influx(value_);
		}
		flowbsurfacelist.push_back(bsurface_); //flowbsurfacemarklist reorder in buildverticalpara
	}

	std::cout << "User Input Done" << std::endl;
}

void REGION::userinput_unstructured_skt_SF(const char* inputfile){//single or two-phase; for input skt (unstructured&cpg); no key word
	int i, number_, type_, length_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, resolutiontype_, nx, ny, nz;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, x, y, z, rvalue_;
	double rho_o_, mu_w_, Siw_, rho_w_, Pct_, lamda_;

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	//default inputs for unstructured+skt+SF
	inputsurfacetype_ == 1;//skt
	numberofphases_ = 1; //single-phase
	meshinfo.type(1);//unstructured tetrahedral mesh
	wellmethod(1);
	strcpy(tetgencommand, "pq1.5/10Ann");
	strcpy(trianglecommand, "pa");
	inputrrm.open(inputfile);

	//user inputs
	inputrrm >> resolutiontype_ >> rvalue_;
	if (resolutiontype_ == 1){
		meshinfo.resolutiontype(1);
		meshinfo.resolutionxnumber(rvalue_);
	}
	else if (resolutiontype_ == 2){
		meshinfo.resolutiontype(2);
		meshinfo.guideedgelength(rvalue_);
	}

	//number of regions of different property
	inputrrm >> number_;
	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o >> x >> y >> z;
		permlow_ = permlow_*0.987e-15; //mD to si
		permhigh_ = permhigh_*0.987e-15;
		mu_o = mu_o*1.0e-3; //cp to si
		rock_.k(0,0,permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		rock_.x(x);
		rock_.y(y);
		rock_.z(z);
		rocklist.push_back(rock_);
	}

	//number of wells
	inputrrm >> number_;

	//for wells:
	for (i = 0; i < number_; i++){
		well_.clear();
		inputrrm >> type_ >> value_ >> sign_;
		well_.type(type_); //1:press, 2:volume flow rate , 3:bottom hole press
		well_.sign(sign_); //1 injector -1 producer
		if (type_ == 1 || type_ == 3){
			value_ = value_*1.0e+5;
		}
		else if (type_ == 2){
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
		}
		well_.value(value_);
		welllist.push_back(well_);
	}

	//for flowbsurfaces
	inputrrm >> number_;

	for (i = 0; i < number_; i++){
		inputrrm >> mark_ >> type_ >> value_ >> sign_; //mark_ is the number of the bsurface; sign_=1 is inflow, -1 is outflow
		flowbsurfacemarklist.push_back(mark_); //if type==2 value change to flow rate also need to change Neumann BC related codes
		bsurface_.type(type_);                 //set ux, uy, uz not good
		bsurface_.sign(sign_);
		if (type_ == 1){
			value_ = value_*1.0e+5;
			bsurface_.P(value_);
		}
		else if (type_ == 2){
			//value_ = value_ / 6.2898*fvf_in;//bbl to meter3 * averaged formation volume factor
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
			bsurface_.influx(value_);
		}
		flowbsurfacelist.push_back(bsurface_); //flowbsurfacemarklist reorder in buildverticalpara
	}

	std::cout << "User Input Done" << std::endl;
}


void REGION::userinput_unstructured_poly_SF(const char* inputfile){//single or two-phase; for input poly (need mtr file); no key word
	int i, number_, type_, length_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, resolutiontype_, nx, ny, nz;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, x, y, z, rvalue_;
	double rho_o_, mu_w_, Siw_, mu_o_, rho_w_, Pct_, lamda_;

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	//default inputs for unstructured+skt+SF
	inputsurfacetype_ = 2;//poly
	numberofphases_ = 1; //single-phase
	meshinfo.type(1);//unstructured tetrahedral mesh
	strcpy(tetgencommand, "pq1.0/30mAnn"); //for poly
	wellmethod(3); //straight well position from .poly and .ui
	inputrrm.open(inputfile);

	//number of regions of different property
	inputrrm >> number_;
	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o_ >> x >> y >> z;
		permlow_ = permlow_*0.987e-15; //mD to si
		permhigh_ = permhigh_*0.987e-15;
		mu_o_ = mu_o_*1.0e-3; //cp to si
		rock_.k(0,0,permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		rock_.x(x);
		rock_.y(y);
		rock_.z(z);
		rocklist.push_back(rock_);
	}

	//number of wells
	inputrrm >> number_;
	//for wells:
	for (i = 0; i < number_; i++){
		well_.clear();
		inputrrm >> type_ >> value_ >> sign_;
		well_.type(type_); //1:press, 2:volume flow rate , 3:bottom hole press
		well_.sign(sign_); //1 injector -1 producer
		if (type_ == 1 || type_ == 3){
			value_ = value_*1.0e+5;
		}
		else if (type_ == 2){
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
		}
		well_.value(value_);
		inputrrm >> x >> y;
		node_.x(x);
		node_.y(y);
		node_.z(0);
		std::vector<NODE> nodelist_;
		nodelist_.clear();
		nodelist_.push_back(node_);
		well_.geonodelist(nodelist_);
		welllist.push_back(well_);
	}

	//for bsurfaces (need to be consistent with surfaces in poly)
	inputrrm >> number_;
	for (i = 0; i < number_; i++){
		inputrrm >> type_ >> value_ >> sign_; //if type==2 value change to flow rate also need to change Neumann BC related codes
		bsurface_.type(type_);                 //set ux, uy, uz not good
		bsurface_.sign(sign_);
		if (type_ == 1){
			value_ = value_*1.0e+5;
			bsurface_.P(value_);
		}
		else if (type_ == 2){
			//value_ = value_ / 6.2898*fvf_in;//bbl to meter3 * averaged formation volume factor
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
			bsurface_.influx(value_);
		}
		bsurfacelist.push_back(bsurface_);
	}

	std::cout << "User Input Done" << std::endl;
}

void REGION::settetgencommand(char* cc){
	strcpy(tetgencommand, cc);
}

void REGION::settrianglecommand(char* cc){
	strcpy(trianglecommand, cc);
}

void REGION::readproperty_mf(char* inputfile){//single or two-phase; for input poly (need mtr file); no key word
	int i, number_, type_, length_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, resolutiontype_, nx, ny, nz;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, x, y, z, rvalue_;
	double rho_o_, mu_w_, Siw_, mu_o_, rho_w_, Pct_, lamda_;

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	inputrrm.open(inputfile);

	inputrrm >> number_;

	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o_ >> mu_w_ >> rho_o_ >> rho_w_ >> Siw_ >> Pct_ >> lamda_ >> x >> y >> z;
		permlow_ = permlow_*0.987e-15;
		permhigh_ = permhigh_*0.987e-15;
		mu_o_ = mu_o_*1.0e-3;
		mu_w_ = mu_w_*1.0e-3;
		Pct_ = Pct_*1.0e+5; //bar to si
		rock_.k(0, 0, permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		//rock_.Siw(Siw_);
		rock_.Pct(Pct_);
		rock_.lamda(lamda_);
		rock_.x(x);
		rock_.y(y);
		rock_.z(z);
		rocklist.push_back(rock_);
	}


	//number of regions of different property
	inputrrm >> number_;
	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o_ >> x >> y >> z;
		permlow_ = permlow_*0.987e-15; //mD to si
		permhigh_ = permhigh_*0.987e-15;
		mu_o_ = mu_o_*1.0e-3; //cp to si
		rock_.k(0, 0, permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		rock_.x(x);
		rock_.y(y);
		rock_.z(z);
		rocklist.push_back(rock_);
	}

	std::cout << "read properties done" << std::endl;
}

void REGION::setviscosity_o(double d){
	mu_o = d; //cp to si
}
void REGION::setviscosity_w(double d){
	mu_w = d; //cp to si
}

void REGION::setct(double d){ //psi^-1 to pasca^-1
	ct_ = d/ 6895;
}

void REGION::setSiw(double d){
	Siw_ = d;
}

void REGION::setdensity_o(double d){
	rho_o = d;
}

void REGION::setdensity_w(double d){
	rho_w = d;
}

void REGION::setcar_const_poro(double d){
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].porosity(d);
	}
}

void REGION::setcar_poro_spemodel2(char* file, int nx, int ny, int nz){
	std::ifstream input;
	input.open(file);
	double aa;
	for (int k = 0; k <nz; k++){
		for (int j = 0; j < ny; j++){
			for (int i = 0; i < nx; i++){
				input >> aa;
				cpgelementlist[i + j*nx + k*nx*ny].porosity(aa);
			}
		}
	}


}

void REGION::setcar_perm_spemodel2(char* file, int nx, int ny, int nz){
	std::ifstream input;
	input.open(file);
	double aa;
	for (int k = 0; k <nz; k++){
		for (int j = 0; j < ny; j++){
			for (int i = 0; i < nx; i++){
				input >> aa;
				cpgelementlist[i + j*nx + k*nx*ny].k(0,0,aa*1e-15);
			}
		}
	}
	for (int k = 0; k <nz; k++){
		for (int j = 0; j < ny; j++){
			for (int i = 0; i < nx; i++){
				input >> aa;
				cpgelementlist[i + j*nx + k*nx*ny].k(1, 1, aa*1e-15);
			}
		}
	}
	for (int k = 0; k <nz; k++){
		for (int j = 0; j < ny; j++){
			for (int i = 0; i < nx; i++){
				input >> aa;
				cpgelementlist[i + j*nx + k*nx*ny].k(2, 2, aa*1e-15);
			}
		}
	}


}

void REGION::settet_const_poro(double porosity_){
	for (int ie = 0; ie < elementlist.size(); ie++){
		elementlist[ie].porosity(porosity_);
		for (int i = 0; i < 4; i++){
			int inode = elementlist[ie].node(i);
			nodelist[inode].porosity(nodelist[inode].porosity() + porosity_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}

}

void REGION::setcar_const_perm(double p1, double p2, double p3){
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].k(0, 0, p1);
		cpgelementlist[i].k(1, 1, p2);
		cpgelementlist[i].k(2, 2, p3);
	}
}

void REGION::settet_const_perm(double p1, double p2, double p3){
	for (int ie = 0; ie < elementlist.size(); ie++){
		elementlist[ie].k(0, 0, p1);
		elementlist[ie].k(1, 1, p2);
		elementlist[ie].k(2, 2, p3);
		for (int i = 0; i < 4; i++){
			int inode = elementlist[ie].node(i);
			nodelist[inode].k(0, 0, nodelist[inode].k(0, 0) + p1* 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			nodelist[inode].k(1, 1, nodelist[inode].k(1, 1) + p2* 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			nodelist[inode].k(2, 2, nodelist[inode].k(2, 2) + p3* 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}
}

void REGION::setcar_perm_paperfmm(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double x = cpgelementlist[i].centnode().x();
		double y = cpgelementlist[i].centnode().y();
		double z = cpgelementlist[i].centnode().z();
		if (x >= 200 && x <= 400 && y >= 100 && y <= 900){
			cpgelementlist[i].k(0, 0, 2 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 2 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.5 * 0.987e-15);
		}
		else if (x >= 600 && x <= 800 && y >= 100 && y <= 900){
			cpgelementlist[i].k(0, 0, 2 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 2 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.5 * 0.987e-15);
		}

		else{
			cpgelementlist[i].k(0, 0, 200 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 200 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 2 * 0.987e-15);
		}
	}
}

void REGION::setcar_perm_paperdfmm(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double x = cpgelementlist[i].centnode().x();
		double y = cpgelementlist[i].centnode().y();
		double z = cpgelementlist[i].centnode().z();
		if (x >= 40 && x <= 60 ){
			cpgelementlist[i].k(0, 0, 1e-15);//mD to si
			cpgelementlist[i].k(1, 1, 1e-15);
			cpgelementlist[i].k(2, 2, 1e-15);
		}
		else{
			cpgelementlist[i].k(0, 0, 100e-15);//mD to si
			cpgelementlist[i].k(1, 1, 100e-15);
			cpgelementlist[i].k(2, 2, 100e-15);
		}
	}
}

void REGION::setcar_perm_paperupmd(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double x = cpgelementlist[i].centnode().x();
		double y = cpgelementlist[i].centnode().y();
		double z = cpgelementlist[i].centnode().z();
		if (x >= 40 && x <= 60){
			cpgelementlist[i].k(0, 0, 1e-15);//mD to si
			cpgelementlist[i].k(1, 1, 1e-15);
			cpgelementlist[i].k(2, 2, 1e-15);
		}

		else{
			cpgelementlist[i].k(0, 0, 100e-15);//mD to si
			cpgelementlist[i].k(1, 1, 100e-15);
			cpgelementlist[i].k(2, 2, 100e-15);
		}
	}
}

void REGION::settet_permporo_paperfmm(){
	for (int i = 0; i < elementlist.size(); i++){
		elementlist[i].porosity(0.1);
		int n0 = elementlist[i].node(0);
		int n1 = elementlist[i].node(1);
		int n2 = elementlist[i].node(2);
		int n3 = elementlist[i].node(3);
		double x = 0.25*(nodelist[n0].x() + nodelist[n1].x() + nodelist[n2].x() + nodelist[n3].x());
		double y = 0.25*(nodelist[n0].y() + nodelist[n1].y() + nodelist[n2].y() + nodelist[n3].y());
		double z = 0.25*(nodelist[n0].z() + nodelist[n1].z() + nodelist[n2].z() + nodelist[n3].z());
		if (x >= 200 && x <= 400 && y >= 100 && y <= 900){
			elementlist[i].k(0, 0, 2 * 0.987e-15);//mD to si
			elementlist[i].k(1, 1, 2 * 0.987e-15);
			elementlist[i].k(2, 2, 0.5 * 0.987e-15);
		}
		else if (x >= 600 && x <= 800 && y >= 100 && y <= 900){
			elementlist[i].k(0, 0, 2 * 0.987e-15);//mD to si
			elementlist[i].k(1, 1, 2 * 0.987e-15);
			elementlist[i].k(2, 2, 0.5 * 0.987e-15);
		}

		else{
			elementlist[i].k(0, 0, 200 * 0.987e-15);//mD to si
			elementlist[i].k(1, 1, 200 * 0.987e-15);
			elementlist[i].k(2, 2, 2 * 0.987e-15);
		}
	}
}

void REGION::setcar_permporo_paperfmm2(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double x = cpgelementlist[i].centnode().x();
		double y = cpgelementlist[i].centnode().y();
		double z = cpgelementlist[i].centnode().z();
		if (x >= 200 && x <= 400 && y >= 100 && y <= 900){
			cpgelementlist[i].k(0, 0, 0.0001 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 0.0001 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.0001 * 0.987e-15);
			cpgelementlist[i].porosity(0.05);
		}
		else if (x >= 600 && x <= 800 && y >= 100 && y <= 900){
			cpgelementlist[i].k(0, 0, 0.0001 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 0.0001 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.0001 * 0.987e-15);
			cpgelementlist[i].porosity(0.05);
		}

		else{
			cpgelementlist[i].k(0, 0, 0.01 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 0.01 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.01 * 0.987e-15);
			cpgelementlist[i].porosity(0.1);
		}
	}
}

void REGION::setcar_permporo_paperEFMMC(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double x = cpgelementlist[i].centnode().x();
		double y = cpgelementlist[i].centnode().y();
		double z = cpgelementlist[i].centnode().z();
		if (x >= 400 && x <= 600 ){
			cpgelementlist[i].k(0, 0, 100 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 100 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 100 * 0.987e-15);
			cpgelementlist[i].porosity(0.1);
		}
		else{
			cpgelementlist[i].k(0, 0, 10 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 10 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 10 * 0.987e-15);
			cpgelementlist[i].porosity(0.1);
		}
	}
}


void REGION::cpgtofbacksec2year(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double t = cpgelementlist[i].tof_back();
		cpgelementlist[i].tof_back(t / 86400.0 / 365.0); //sec to year
	}
}

void REGION::nodaltofsec2year(){
	for (int i = 0; i < nodelist.size(); i++){
		double t = nodelist[i].tof();
		nodelist[i].tof(t / 86400.0 / 365.0); //sec to year
	}
}

void REGION::nodaltofsec2day(){
	for (int i = 0; i < nodelist.size(); i++){
		double t = nodelist[i].tof();
		nodelist[i].tof(t / 86400.0); //sec to day
	}
}

void REGION::cpgpascal2psi(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double p = cpgelementlist[i].Po();
		cpgelementlist[i].Po(p /6894.76); //pascal to psi
	}
}

void REGION::nodalpascal2psi(){
	for (int i = 0; i < nodelist.size(); i++){
		double p = nodelist[i].Po();
		nodelist[i].Po(p / 6894.76); //pascal to psi
	}
}

void REGION::setcar_perm_paper2(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double x = cpgelementlist[i].centnode().x();
		double y = cpgelementlist[i].centnode().y();
		double z = cpgelementlist[i].centnode().z();
		if (x >= 100 && x <= 500 && y >= 100 && y <= 200){
			cpgelementlist[i].k(0, 0, 1 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 1 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.2 * 0.987e-15);
		}
		else if (x >= 100 && x <= 500 && y >= 800 && y <= 900){
			cpgelementlist[i].k(0, 0, 1 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 1 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.2 * 0.987e-15);
		}
		else if (x >= 100 && x <= 200 && y >= 100 && y <= 400){
			cpgelementlist[i].k(0, 0, 1 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 1 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.2 * 0.987e-15);
		}
		else if (x >= 100 && x <= 200 && y >= 600 && y <= 900){
			cpgelementlist[i].k(0, 0, 1 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 1 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.2 * 0.987e-15);
		}
		else if (x >= 700 && x <= 800 && y >= 200 && y <= 800){
			cpgelementlist[i].k(0, 0, 1 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 1 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 0.2 * 0.987e-15);
		}
		else{
			cpgelementlist[i].k(0, 0, 100 * 0.987e-15);//mD to si
			cpgelementlist[i].k(1, 1, 100 * 0.987e-15);
			cpgelementlist[i].k(2, 2, 1 * 0.987e-15);
		}
	}
}

void REGION::createrocklist(int i){
	rocklist.resize(i);
}

void REGION::readrock_perm(char* inputfile){

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	inputrrm.open(inputfile);

	//number of regions of different property
	double permx_, permy_, permz_;
	int number_;
	inputrrm >> number_;
	if (number_ != rocklist.size()){
		std::cout << "create rocklist first" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	//permeability values in mD, viscosity in cP
	for (int i = 0; i < number_; i++){
		inputrrm >> permx_>>permy_>>permz_;
		rocklist[i].k(0, 0, permx_);
		rocklist[i].k(1, 1, permy_);
		rocklist[i].k(2, 2, permz_);
	}

	std::cout << "read rocks perm done" << std::endl;
}

void REGION::readrock_porosity(char* inputfile){

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	inputrrm.open(inputfile);

	//number of regions of different property
	double poro_;
	int number_;
	inputrrm >> number_;
	if (number_ != rocklist.size()){
		std::cout << "create rocklist first" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	//permeability values in mD, viscosity in cP
	for (int i = 0; i < number_; i++){
		inputrrm >> poro_;
		rocklist[i].porosity(poro_);
	}

	std::cout << "read rocks porosity done" << std::endl;
}

void REGION::readrock_xyz(char* inputfile){

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	inputrrm.open(inputfile);

	//number of regions of different property
	double x_, y_, z_;
	int number_;
	inputrrm >> number_;
	if (number_ != rocklist.size()){
		std::cout << "create rocklist first" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	//permeability values in mD, viscosity in cP
	for (int i = 0; i < number_; i++){
		inputrrm >> x_>>y_>>z_;
		rocklist[i].x(x_);
		rocklist[i].y(y_);
		rocklist[i].z(z_);
	}

	std::cout << "read rocks xyz done" << std::endl;
}

void REGION::initialnodalSw(){
	for (int i = 0; i < nodelist.size(); i++){
		if (nodelist[i].markbc_Sw() != 1){
			nodelist[i].Sw(Siw_);
		}
	}
	oldnodalSw.resize(nodelist.size());
}

void REGION::initialcpgSw(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		if (cpgelementlist[i].markbc_Sw() != 1){
			cpgelementlist[i].Sw(Siw_);
		}
	}
}

//void REGION::nodalSw2elementalmobit(){
//	//compute nodal total mobility with lamda=2
//	for (int i = 0; i < nodelist.size(); i++){
//		double sw = nodelist[i].Sw();
//		double a = (1 - sw) / (1 - Siw_);
//		double b = (sw - Siw_) / (1 - Siw_);
//		double kro = a*a*(1 - b*b);
//		double krw = b*b*b*b;
//		double vro = kro / mu_o;
//		double vrw = krw / mu_w;
//		nodelist[i].mobio(vro);
//		nodelist[i].mobiw(vrw);
//	}
//	for (int ie = 0; ie < elementlist.size(); ie++){
//		for (int i = 0; i < 4; i++){
//			int inode = elementlist[ie].node(i);
//			double mt = nodelist[inode].mobit();
//			elementlist[ie].mobit(elementlist[ie].mobit() + mt);
//		}
//	}
//}

void REGION::nodalSw2mobi(){

	for (int i = 0; i < nodelist.size(); i++){
		double sw = nodelist[i].Sw();
		double a = (1 - sw) / (1 - Siw_);
		double b = (sw - Siw_) / (1 - Siw_);
		double kro = a*a*(1 - b*b);
		double krw = b*b*b*b;
		double vro = kro / mu_o;
		double vrw = krw / mu_w;
		nodelist[i].mobio(vro);
		nodelist[i].mobiw(vrw);
		nodelist[i].mobit(vro + vrw);
		double fracw_ = vrw / (vro + vrw);
		nodelist[i].fracw(fracw_);
	}


	//for (int ie = 0; ie < elementlist.size(); ie++){
	//	for (int i = 0; i < 4; i++){
	//		int inode = elementlist[ie].node(i);
	//		//double sw = nodelist[inode].Sw();
	//		double mobit = nodelist[inode].mobit();
	//		//elementlist[ie].Sw(elementlist[ie].Sw() + sw*0.25);
	//		elementlist[ie].mobit(elementlist[ie].mobit() + 0.25*mobit);
	//	}
	//}
	//compute element total mobility with lamda=2
	/*for (int i = 0; i < elementlist.size(); i++){
		double sw = elementlist[i].Sw();
		double a = (1 - sw) / (1 - Siw_);
		double b = (sw - Siw_) / (1 - Siw_);
		double kro = a*a*(1 - b*b);
		double krw = b*b*b*b;
		double vro = kro / mu_o;
		double vrw = krw / mu_w;
		elementlist[i].mobio(vro);
		elementlist[i].mobiw(vrw);
		elementlist[i].mobit(vro + vrw);
	}*/
}

void REGION::nodalSw2mobi(int i){

		double sw = nodelist[i].Sw();
		double a = (1 - sw) / (1 - Siw_);
		double b = (sw - Siw_) / (1 - Siw_);
		double kro = a*a*(1 - b*b);
		double krw = b*b*b*b;
		double vro = kro / mu_o;
		double vrw = krw / mu_w;
		nodelist[i].mobio(vro);
		nodelist[i].mobiw(vrw);
		nodelist[i].mobit(vro + vrw);
		double fracw_ = vrw / (vro + vrw);
		nodelist[i].fracw(fracw_);

}

void REGION::cpgSw2mobi(){
	for (int i = 0; i < cpgelementlist.size(); i++){
		double sw = cpgelementlist[i].Sw();
		double a = (1 - sw) / (1 - Siw_);
		double b = (sw - Siw_) / (1 - Siw_);
		double kro = a*a*(1 - b*b);
		double krw = b*b*b*b;
		double vro = kro / mu_o;
		double vrw = krw / mu_w;
		cpgelementlist[i].mobio(vro);
		cpgelementlist[i].mobiw(vrw);
		cpgelementlist[i].mobit(vro + vrw);
	}
}

void REGION::nodalSw2eleSw(){
	for (int ie = 0; ie < elementlist.size(); ie++){
		for (int i = 0; i < 4; i++){
			int inode = elementlist[ie].node(i);
			double sw = nodelist[inode].Sw();
			elementlist[ie].Sw(elementlist[ie].Sw() + sw*0.25);
		}
	}
}

void REGION::eleSw2mobi(){
	//compute element total mobility with lamda=2
	for (int i = 0; i < elementlist.size(); i++){
		double sw = elementlist[i].Sw();
		double a = (1 - sw) / (1 - Siw_);
		double b = (sw - Siw_) / (1 - Siw_);
		double kro = a*a*(1 - b*b);
		double krw = b*b*b*b;
		double vro = kro / mu_o;
		double vrw = krw / mu_w;
		elementlist[i].mobio(vro);
		elementlist[i].mobiw(vrw);
		elementlist[i].mobit(vro + vrw);
	}
}


void REGION::nodalmobi2elementalmobi(){
	for (int ie = 0; ie < elementlist.size(); ie++){
		for (int i = 0; i < 4; i++){
			int inode = elementlist[ie].node(i);
			double mobit = nodelist[inode].mobit();
			double mobio = nodelist[inode].mobio();
			double mobiw = nodelist[inode].mobiw();
			double fracw_ = nodelist[inode].fracw();
			elementlist[ie].mobit(elementlist[ie].mobit() + 0.25*mobit);
			elementlist[ie].mobio(elementlist[ie].mobio() + 0.25*mobio);
			elementlist[ie].mobiw(elementlist[ie].mobiw() + 0.25*mobiw);
			//elementlist[ie].fracw(elementlist[ie].fracw() + 0.25*fracw_);
		}
		elementlist[ie].fracw(elementlist[ie].mobiw() / elementlist[ie].mobit());
	}
}

void REGION::readwellgeometry(char* inputfile){

	WELL well_;
	NODE node_;
	std::ifstream inputrrm;
	welllist.resize(0);
	inputrrm.open(inputfile);
	int nwn, number_;
	double x, y, z;
	//number of wells
	inputrrm >> number_;
	//for wells:
	for (int i = 0; i < number_; i++){
		well_.clear();
		inputrrm >> nwn;
		std::vector<NODE> nodelist_;
		for (int j = 0; j < nwn; j++){
			inputrrm >> x >> y >> z;
			node_.x(x);
			node_.y(y);
			node_.z(z);
			nodelist_.push_back(node_);
		}
		well_.geonodelist(nodelist_);
		welllist.push_back(well_);
	}

	std::cout << "Input Wells Geometry Done" << std::endl;
}

void REGION::readwellcondition(char* inputfile){

	std::ifstream inputrrm;

	inputrrm.open(inputfile);
	double type_, value_, sign_;
	int number_;
	//number of wells
	inputrrm >> number_;
	/*if (number_ != welllist.size()){
		std::cout << "ERROR need readwellgeometry before readwellcondition" << std::endl;
		std::exit(EXIT_FAILURE);
	}*/
	//for wells:
	for (int i = 0; i < number_; i++){
		inputrrm >> type_ >> value_ >> sign_; //value in bar
		welllist[i].type(type_);
		welllist[i].value(value_);
		welllist[i].sign(sign_);
	}

	std::cout << "read Wells condition Done" << std::endl;
}

void REGION::readboundarycondition(char* inputfile){
	int number_, type_, sign_, mark_; //sign_ = 1 is inflow, -1 is outflow
	double value_;
	BSURFACE bsurface_;
	std::ifstream inputrrm;
	inputrrm.open(inputfile);
	inputrrm >> number_;//number of flowbcondition include noflow; ignore internal; marker need to agree with .poly

	for (int i = 0; i < number_; i++){
		inputrrm >> mark_ >> type_ >> value_ >> sign_; //mark_ is the number of the bsurface (0 to max).
		bsurface_.type(type_);     //type_=0 is noflow
		bsurface_.sign(sign_);
		if (type_ == 1){
			bsurface_.P(value_);
		}
		else if (type_ == 2){
			bsurface_.influx(value_);
		}
		bsurfacelist.push_back(bsurface_);
	}
}

void REGION::createnoflowbsurfaces(int n){
	bsurfacelist.clear();
	BSURFACE bsurface_;
	bsurface_.type(0); //no-flow
	for (int i = 0; i < n; i++){
		bsurfacelist.push_back(bsurface_);
	}
}


void REGION::setfmmstartwell(int i){
	startwellnumber_ = 0;
}

void REGION::userinput_unstructured_poly_SFFMM(const char* inputfile){//single or two-phase; for input poly (need mtr file); no key word
	int i, number_, type_, length_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, resolutiontype_, nx, ny, nz;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, x, y, z, rvalue_;
	double rho_o_, mu_w_, Siw_, mu_o_, rho_w_, Pct_, lamda_;

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	inputrrm.open(inputfile);

	//number of regions of different property
	inputrrm >> number_;
	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o_ >> x >> y >> z;
		permlow_ = permlow_*0.987e-15; //mD to si
		permhigh_ = permhigh_*0.987e-15;
		mu_o_ = mu_o_*1.0e-3; //cp to si
		rock_.k(0,0,permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		rock_.x(x);
		rock_.y(y);
		rock_.z(z);
		rocklist.push_back(rock_);
	}

	//number of wells
	inputrrm >> number_;
	//for wells:
	for (i = 0; i < number_; i++){
		well_.clear();
		inputrrm >> x >> y;
		node_.x(x);
		node_.y(y);
		node_.z(0);
		std::vector<NODE> nodelist_;
		nodelist_.clear();
		nodelist_.push_back(node_);
		well_.geonodelist(nodelist_);
		welllist.push_back(well_);
	}
	inputrrm >> startwellnumber_;
	//for bsurfaces (need to be consistent with surfaces in poly)
	inputrrm >> number_;
	for (i = 0; i < number_; i++){
		inputrrm >> type_ >> value_ >> sign_; //if type==2 value change to flow rate also need to change Neumann BC related codes
		bsurface_.type(type_);                 //set ux, uy, uz not good
		bsurface_.sign(sign_);
		if (type_ == 1){
			value_ = value_*1.0e+5;
			bsurface_.P(value_);
		}
		else if (type_ == 2){
			//value_ = value_ / 6.2898*fvf_in;//bbl to meter3 * averaged formation volume factor
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
			bsurface_.influx(value_);
		}
		bsurfacelist.push_back(bsurface_);
	}

	std::cout << "User Input Done" << std::endl;
}

void REGION::userinput_unstructured_msh_SF(const char* inputfile){//single or two-phase; for input poly (need mtr file); no key word
	int i, number_, type_, length_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, resolutiontype_, nx, ny, nz;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, x, y, z, rvalue_;
	double rho_o_, mu_w_, Siw_, mu_o_, rho_w_, Pct_, lamda_;

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	//default inputs for unstructured+msh+SF+nowells (no well; no remesh)
	inputsurfacetype_ = 3;//msh: input volume mesh directly
	numberofphases_ = 1; //single-phase
	meshinfo.type(1);//unstructured tetrahedral mesh

	inputrrm.open(inputfile);

	//number of regions of different property
	inputrrm >> number_;
	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o_;
		permlow_ = permlow_*0.987e-15; //mD to si
		permhigh_ = permhigh_*0.987e-15;
		mu_o_ = mu_o_*1.0e-3; //cp to si
		rock_.k(0, 0, permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		rocklist.push_back(rock_);
	}

	//number of wells
	inputrrm >> number_;
	//for wells:
	for (i = 0; i < number_; i++){
		well_.clear();
		inputrrm >> type_ >> value_ >> sign_;
		well_.type(type_); //1:press, 2:volume flow rate , 3:bottom hole press
		well_.sign(sign_); //1 injector -1 producer
		if (type_ == 1 || type_ == 3){
			value_ = value_*1.0e+5;
		}
		else if (type_ == 2){
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
		}
		well_.value(value_);
		inputrrm >> x >> y;
		node_.x(x);
		node_.y(y);
		node_.z(0);
		std::vector<NODE> nodelist_;
		nodelist_.clear();
		nodelist_.push_back(node_);
		well_.geonodelist(nodelist_);
		welllist.push_back(well_);
	}

	//for bsurfaces (need to be consistent with surfaces in msh)
	inputrrm >> number_;
	for (i = 0; i < number_; i++){
		inputrrm >> type_ >> value_ >> sign_; //if type==2 value change to flow rate also need to change Neumann BC related codes
		bsurface_.type(type_);                 //set ux, uy, uz not good
		bsurface_.sign(sign_);
		if (type_ == 1){
			value_ = value_*1.0e+5;
			bsurface_.P(value_);
		}
		else if (type_ == 2){
			//value_ = value_ / 6.2898*fvf_in;//bbl to meter3 * averaged formation volume factor
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
			bsurface_.influx(value_);
		}
		bsurfacelist.push_back(bsurface_);
	}

	std::cout << "User Input Done" << std::endl;
}

void REGION::userinput_cpg_skt_SF(const char* inputfile){//single or two-phase; for input skt (unstructured&cpg); no key word
	int i, number_, type_, length_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, resolutiontype_, nx, ny, nz;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, x, y, z, rvalue_;
	double rho_o_, mu_w_, Siw_, mu_o_, rho_w_, Pct_, lamda_;

	WELL well_;
	BSURFACE bsurface_;
	NODE node_;

	std::ifstream inputrrm;
	ROCK rock_;

	//default inputs for cpg+skt+MF
	inputsurfacetype_ == 1;//skt
	numberofphases_ = 1; //single-phase
	meshinfo.type(2);//cpg
	inputrrm.open(inputfile);
	//user inputs

	inputrrm >> nx >> ny >> nz;
	meshinfo.cpgdimension(0, nx);
	meshinfo.cpgdimension(1, ny);
	meshinfo.cpgdimension(2, nz);

	//number of regions of different property
	inputrrm >> number_;

	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> mu_o_ ;
		permlow_ = permlow_*0.987e-15;
		permhigh_ = permhigh_*0.987e-15;
		mu_o_ = mu_o_*1.0e-3;
		rock_.k(0, 0, permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		rocklist.push_back(rock_);
	}

	//number of wells
	inputrrm >> number_;

	//for wells:
	for (i = 0; i < number_; i++){
		well_.clear();
		inputrrm >> type_ >> value_ >> sign_;
		well_.type(type_); //1:press, 2:volume flow rate , 3:bottom hole press
		well_.sign(sign_); //1 injector -1 producer
		if (type_ == 1 || type_ == 3){
			value_ = value_*1.0e+5;
		}
		else if (type_ == 2){
			value_ = value_ *fvf_in;//meter3 * averaged formation volume factor
		}
		well_.value(value_);
		inputrrm >> x >> y;
		node_.x(x);
		node_.y(y);
		node_.z(0);
		std::vector<NODE> nodelist_;
		nodelist_.clear();
		nodelist_.push_back(node_);
		well_.nodelist(nodelist_);
		welllist.push_back(well_);
	}

	std::cout << "User Input Done" << std::endl;
}

void REGION::userinput_upscale(char* inputfile, int upid_, int directionid_){
	//local variables
	int i, number_, type_, sign_, tofbtype_, mark_, ntofb, ntracingb, tof_sign, tracer_sign, n_, method_, rtype_, rnumber_;
	double value_, wellx_, welly_, wellz_, P_, Ux_, Uy_, Uz_, porosity_, permlow_, permhigh_, viscosity_, x, y, z, length_, rho_o_, rvalue_;
	std::string nouse;
	WELL well_;
	BSURFACE bsurface_;

	std::ifstream inputrrm;
	ROCK rock_;

	upscaletype_ = upid_;
	upscaledirection_ = directionid_;
	//default inputs
	vis_regionidbutton_ = 1;
	inputrrm.open(inputfile);
	linearsolverbutton_ = 2;
	numberofphases_ = 1;
	inputsurfacetype_ = 1;
	meshinfo.type(1);
	strcpy(tetgencommand, "pq1.5/10Ann");
	strcpy(trianglecommand, "pa");

	//user inputs
	getline(inputrrm, nouse); //skip one line
	inputrrm >> rtype_;
	meshinfo.resolutiontype(rtype_);
	if (rtype_ == 1){
		inputrrm >> rnumber_;
		meshinfo.resolutionxnumber(rnumber_);
	}
	else if (rtype_ == 2){
		inputrrm >> rvalue_;
		meshinfo.guideedgelength(rvalue_);
	}

	getline(inputrrm, nouse); //skip one line
	getline(inputrrm, nouse);
	//number of regions of different property
	inputrrm >> number_;
	getline(inputrrm, nouse); //skip one line
	getline(inputrrm, nouse);
	//permeability values in mD, viscosity in cP
	for (i = 0; i < number_; i++){ //for unstructured from skt
		inputrrm >> permlow_ >> permhigh_ >> porosity_ >> viscosity_ >> rho_o_ >> x >> y >> z;
		permlow_ = permlow_*0.987e-15;
		permhigh_ = permhigh_*0.987e-15;
		viscosity_ = viscosity_*1.0e-3;
		rock_.k(0, 0, permlow_);
		rock_.k(1, 1, permlow_);
		rock_.k(2, 2, permlow_);
		rock_.porosity(porosity_);
		rock_.x(x);
		rock_.y(y);
		rock_.z(z);
		rocklist.push_back(rock_);
	}

	//add upscaling bsurfaces
	if (upid_ == 1){//no-flow
		if (directionid_ == 1){//x
			mark_ = 4;
			type_ = 1;
			value_ = 100;
			value_ = value_*1.0e+5;
			sign_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 5;
			type_ = 1;
			value_ = 0;
			sign_ = -1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
		}
		else if (directionid_ == 2){//y
			mark_ = 2;
			type_ = 1;
			value_ = 100;
			value_ = value_*1.0e+5;
			sign_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 3;
			type_ = 1;
			value_ = 0;
			sign_ = -1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
		}
		else if (directionid_ == 3){//z
			mark_ = 0;
			type_ = 1;
			value_ = 100;
			value_ = value_*1.0e+5;
			sign_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 1;
			type_ = 1;
			value_ = 0;
			sign_ = -1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
		}
	}
	else if (upid_ == 2){
		if (directionid_ == 1){//x
			mark_ = 4;
			type_ = 1;
			value_ = 100;
			value_ = value_*1.0e+5;
			sign_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 5;
			type_ = 1;
			value_ = 0;
			sign_ = -1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			//pressure gradient bc for all other boundaries
			type_ = 4;
			bsurface_.type(type_);
			mark_ = 0;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 2;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 3;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
		}
		else if (directionid_ == 2){//y
			mark_ = 2;
			type_ = 1;
			value_ = 100;
			value_ = value_*1.0e+5;
			sign_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 3;
			type_ = 1;
			value_ = 0;
			sign_ = -1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			//pressure gradient bc for all other boundaries
			type_ = 4;
			bsurface_.type(type_);
			mark_ = 0;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 4;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 5;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
		}
		else if (directionid_ == 3){//z
			mark_ = 0;
			type_ = 1;
			value_ = 100;
			value_ = value_*1.0e+5;
			sign_ = 1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 1;
			type_ = 1;
			value_ = 0;
			sign_ = -1;
			flowbsurfacemarklist.push_back(mark_);
			bsurface_.type(type_);
			bsurface_.sign(sign_);
			bsurface_.P(value_);
			flowbsurfacelist.push_back(bsurface_);
			//pressure gradient bc for all other boundaries
			type_ = 4;
			bsurface_.type(type_);
			mark_ = 2;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 3;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 4;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
			mark_ = 5;
			flowbsurfacemarklist.push_back(mark_);
			flowbsurfacelist.push_back(bsurface_);
		}
	}
}


void REGION::welldiscretisation() {
	std::ifstream inputrrm;
	NODE node_;
	int i, j, nn, np, k;
	double x_, y_, z_, dd, x1, x2, y1, y2, z1, z2, dd2;

	dd = meshinfo.guideedgelength();

	for (i = 0; i < welllist.size(); i++) {
		std::vector<NODE> geonodelist_ = welllist[i].geonodelist();
		std::vector<NODE> nodelist_;
		nn = geonodelist_.size();
		for (j = 0; j < nn - 1; j++){
			np = int(mathzz_.twonodedistance(geonodelist_[j], geonodelist_[j + 1]) / dd);
			if (np == 0){
				np = 1;
			}
			for (k = 0; k < np; k++){
				x_ = (k *geonodelist_[j + 1].x() + (np - k) *geonodelist_[j].x()) / np;
				y_ = (k *geonodelist_[j + 1].y() + (np - k) *geonodelist_[j].y()) / np;
				z_ = (k *geonodelist_[j + 1].z() + (np - k) *geonodelist_[j].z()) / np;
				node_.x(x_);
				node_.y(y_);
				node_.z(z_);
				nodelist_.push_back(node_);
			}
		}
		nodelist_.push_back(geonodelist_[nn - 1]);
		welllist[i].nodelist(nodelist_);
	}
}

void REGION::readvolumemeshVTK(std::string filein ){
	int nvertex, i, j, n[4], nnode, nelement, mark_, type_;
	double x, y, z, xx4[4], yy4[4], zz4[4];
	char word1[20], word2[20], w3[20], w4[20], w5[20];
	std::string nouse;
	std::ifstream inputmesh;
	NODE node_;
	TETRAHEDRON tetrahedron_;
	inputmesh.open(filein);

	for (i = 0; i <= 3; i++){
		getline(inputmesh, nouse);
	};
	inputmesh >> word1 >> nnode >> word2;


	for (i = 0; i < nnode; i++){
		inputmesh >> x >> y >> z;
		node_.x(x);
		node_.y(y);
		node_.z(z);
		nodelist.push_back(node_);
	}

	getline(inputmesh, nouse);
	inputmesh >> word1 >> nelement >> word2;


	for (i = 0; i < nelement; i++){
		inputmesh >> nvertex >> n[0] >> n[1] >> n[2] >> n[3];
		for (j = 0; j < 4; j++){
			tetrahedron_.node(j, n[j]);
		}
		elementlist.push_back(tetrahedron_);
	};
	inputmesh >> word1 >> nelement;
	for (i = 0; i < nelement; i++){
		inputmesh >> type_;
	}
	inputmesh >> word1 >> nelement;
	inputmesh >> word1 >> word2 >> w3 >> w4 >> w5;
	for (i = 0; i < nelement; i++){
		inputmesh >> mark_;
		elementlist[i].markregion(mark_);
	}


	//reading VTK finish here

	//calculate and store volume for all elements
	for (i = 0; i < nelement; i++){
		for (j = 0; j < 4; j++){
			xx4[j] = nodelist[elementlist[i].node(j)].x();
			yy4[j] = nodelist[elementlist[i].node(j)].y();
			zz4[j] = nodelist[elementlist[i].node(j)].z();
		};
		elementlist[i].volume(mathzz_.volume_tetra_positive(xx4, yy4, zz4));
	};

}

void REGION::readtrifacies(char* file){
	std::ifstream input;
	int i, num, imark, i1, i2, i3, imax, mark_;
	FACET bfacet_;
	BSURFACE bsurface_;
	input.open(file);
	input >> num;
	imax = -1000;
	for (i = 0; i < num; i++){
		input >> imark >> i1 >> i2 >> i3;
		bfacet_.node(0, i1);
		bfacet_.node(1, i2);
		bfacet_.node(2, i3);
		bfacet_.markbsurface(imark); //mark numbering starts from 1 in input file
		bfacetlist.push_back(bfacet_);
		if (imark>imax){
			imax = imark;
		}
	}
	for (i = 0; i < imax+1; i++){ //from 0
		bsurfacelist.push_back(bsurface_); //default internal; internal or no-flow bc same in CVFEM
	}
	//below only for upscaling; otherwise flowbsurfacelist.size()==0
	for (i = 0; i < flowbsurfacelist.size(); i++){//for outer boundary bot-top-front-back-left-right is 0 1 2 3 4 5
		if (flowbsurfacemarklist[i] == 4){//left
			mark_ = bsurfacelist.size() - 2;
		}
		else if (flowbsurfacemarklist[i] == 5){//right
			mark_ = bsurfacelist.size() - 1;
		}
		bsurfacelist[mark_] = flowbsurfacelist[i];
	}


	nbfacet = bfacetlist.size();
}


void REGION::readvolumemeshMSH(char* filein){
	meshinfo.type(1); //tetrahedral mesh
	//firstly need to check the starting point is 0 or 1 for nodes, boundary surface marks, region marks for elements
	int i, j, inode, i1, i2, i3, imark, nelementtotal, n[4], nnode, nelement;
	double x, y, z;
	std::string nouse;
	std::ifstream inputmesh;
	NODE node_;
	TETRAHEDRON tetrahedron_;
	FACET bfacet_;
    inputmesh.open(filein);

	for (i = 0; i < 4; i++){
		getline(inputmesh, nouse);
	};
	inputmesh >> nnode;

	for (i = 0; i < nnode; i++){
		inputmesh >> inode >> x >> y >> z;
		node_.x(x);
		node_.y(y);
		node_.z(z);
		nodelist.push_back(node_);
	}

	for (i = 0; i < 3; i++){
		getline(inputmesh, nouse);
	};
	inputmesh >>nelementtotal;

	for (i = 0; i < nelementtotal; i++){
		inputmesh >> inode >> i1 >> i2 >> imark >> i3; //imark 1 left 4 right 2,3 mid
		if (i1 == 2){//triangle
			inputmesh >> n[0] >> n[1] >> n[2];
			for (j = 0; j < 3; j++){
				bfacet_.node(j, n[j]-1); //nodes numbering starts from 1 in input file
			}
			bfacet_.markbsurface(imark-1); //mark numbering starts from 1 in input file
			bfacetlist.push_back(bfacet_);
		}

		if (i1 == 4){
			inputmesh >> n[0] >> n[1] >> n[2] >> n[3];
			for (j = 0; j < 4; j++){
				tetrahedron_.node(j, n[j]-1); //nodes numbering starts from 1 in input file
			}
			tetrahedron_.markregion(imark-1); //mark numbering starts from 1 in input file
			elementlist.push_back(tetrahedron_);
		}
	};
	nelement = elementlist.size();
	nbfacet = bfacetlist.size();

	//reading MSH finish here
	meshdimension_max();
}

void REGION::volumemesh2surfacemesh(){
	int i, j, jnode, iregion, i0, i1, i2, i3, regionid, nnode, nelement;
	NODE node_;
	FACET facet_;
	double x_, y_, z_;

	std::vector<int> marknode; //map global node number to surface node number
	nnode = nodelist.size();
	nelement = elementlist.size();
	marknode.resize(nnode);
	for (i = 0; i < nnode; i++){
		marknode[i] = -1;
	}
	nregion = 10;
	std::cout << "there are " << nregion << " regions in this volume mesh" << std::endl;
	std::vector<int> markregion;
	markregion.resize(nregion);
	for (i = 0; i < nregion; i++){
		markregion[i] = -1;
	}

	surfacenodelist.clear();
	facetlist.clear();
	//use bfacet to creat surfacenodes and facetlist
	nsurfacenode = -1;
	for (i = 0; i < nbfacet; i++){
		for (j = 0; j < 3; j++){
			jnode = bfacetlist[i].node(j);
			if (marknode[jnode] == -1){
				nsurfacenode++;
				marknode[jnode] = nsurfacenode;
				x_ = nodelist[jnode].x();
				y_ = nodelist[jnode].y();
				z_ = nodelist[jnode].z();
				node_.x(x_);
				node_.y(y_);
				node_.z(z_);
				surfacenodelist.push_back(node_);
			}
			facet_.node(j, marknode[jnode]);
		}
		facet_.numberofvertex(3);
		facet_.markbsurface(bfacetlist[i].markbsurface());
		facetlist.push_back(facet_);
	}
	nfacet = facetlist.size();

	//create region nodes for denoting region
	for (regionid = 0; regionid < nregion; regionid++){
		for (i = 0; i < nelement; i++){
			iregion = elementlist[i].markregion();
			if (iregion == regionid){
				i0 = elementlist[i].node(0);
				i1 = elementlist[i].node(1);
				i2 = elementlist[i].node(2);
				i3 = elementlist[i].node(3);
				x_ = 0.25*(nodelist[i0].x() + nodelist[i1].x() + nodelist[i2].x() + nodelist[i3].x());
				y_ = 0.25*(nodelist[i0].y() + nodelist[i1].y() + nodelist[i2].y() + nodelist[i3].y());
				z_ = 0.25*(nodelist[i0].z() + nodelist[i1].z() + nodelist[i2].z() + nodelist[i3].z());
				node_.x(x_);
				node_.y(y_);
				node_.z(z_);
				regionnodelist.push_back(node_);
				break;
			}
		}
	}



}

void REGION::readflowfield(std::string filein) {
	std::ifstream flowfield;
	int itype, nnode2, nelement2, nvertex, i, j, n[4], nnode, nelement;
	double p_, ux_, uy_, uz_, tof_, x, y, z, xx4[4], yy4[4], zz4[4];
	char word1[20], word2[20];
	std::string nouse;
	NODE node_;
	TETRAHEDRON tetrahedron_;
	flowfield.open(filein);

	for (i = 0; i <= 3; i++) {
		getline(flowfield, nouse);
	};
	flowfield >> word1 >> nnode >> word2;


	for (i = 0; i < nnode; i++) {
		flowfield >> x >> y >> z;
		node_.x(x);
		node_.y(y);
		node_.z(z);
		nodelist.push_back(node_);
	}

	getline(flowfield, nouse);
	flowfield >> word1 >> nelement >> word2;


	for (i = 0; i < nelement; i++) {
		flowfield >> nvertex >> n[0] >> n[1] >> n[2] >> n[3];
		for (j = 0; j < 4; j++) {
			tetrahedron_.node(j, n[j]);
		}

		elementlist.push_back(tetrahedron_);
	};

	//read element type
	getline(flowfield, nouse);
	flowfield >> word1 >> nelement2;
	for (i = 0; i < nelement2; i++) {
		flowfield >> itype;
	}

	//read nodal pressure
	getline(flowfield, nouse);
	flowfield >> word1 >> nnode2;
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	for (i = 0; i < nnode2; i++) {
		flowfield >> p_;
		nodelist[i].Po(p_);
	}
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	for (i = 0; i < nnode2; i++) {
		flowfield >> tof_;
	}
	/*getline(flowfield, nouse);
	getline(flowfield, nouse);
	for (i = 0; i < nnode2; i++) {
		flowfield >> ux_>>uy_>>uz_;
		nodelist[i].Ux(ux_);
		nodelist[i].Uy(uy_);
		nodelist[i].Uz(uz_);
	}*/
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	for (i = 0; i < nelement; i++) {
		flowfield >> ux_>>uy_>>uz_;
		elementlist[i].Ux_t(ux_);
		elementlist[i].Uy_t(uy_);
		elementlist[i].Uz_t(uz_);
	}
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	getline(flowfield, nouse);
	for (i = 0; i < nelement; i++) {
		flowfield >> tof_;
	}




	//reading VTK finish here

	//calculate and store volume for all elements
	for (i = 0; i < nelement; i++) {
		for (j = 0; j < 4; j++) {
			xx4[j] = nodelist[elementlist[i].node(j)].x();
			yy4[j] = nodelist[elementlist[i].node(j)].y();
			zz4[j] = nodelist[elementlist[i].node(j)].z();
		};
		elementlist[i].volume(mathzz_.volume_tetra_positive(xx4, yy4, zz4));
	};
}

void REGION::readmarker(std::string filein){
	int i, mark_, nnode;
	double number1, number2, number3, x, y, z;
	std::ifstream inputmesh2;
	inputmesh2.open(filein);
	inputmesh2 >> nnode >> number1 >> number2 >> number3;
	for (i = 0; i < nnode; i++){
		inputmesh2 >> number1 >> x >> y >> z >> mark_;

		nodelist[i].markbsurface(mark_);
	}
}

void REGION::readsurfacemeshPOLY(char* filein){

	in.load_plc(filein, 1); //tetgenbehavior::POLY=1
	//or equavalently:
	//in.load_poly(filein);
	//in.load_mtr(filein);
}

void REGION::readsurfacemeshVTK(char* filein){
	int nvertex, nnode_input, nfacet_input, i, j, n[4], mark_;
	std::vector<NODE> nodelist_input;
	double number1, x, y, z;
	char word1[20], word2[20], word3[20];
	std::string nouse;
	std::ifstream inputmesh, inputmeshmarker;
	NODE node_;
	FACET facet_;
	inputmesh.open(filein);
	inputmeshmarker.open("boxmarker.vtk");
	for (i = 0; i < 4; i++){
		getline(inputmesh, nouse);
	};
	inputmesh >> word1 >> nnode_input >> word2;

	for (i = 0; i < nnode_input; i++){
		inputmesh >> x >> y >> z;
		node_.x(x);
		node_.y(y);
		node_.z(z);
		nodelist_input.push_back(node_);
	}

	inputmesh >> word1 >> nfacet_input >> number1;

	for (i = 0; i < nfacet_input; i++){
		inputmesh >> nvertex;
		facet_.numberofvertex(nvertex);
		if (nvertex == 3){
			inputmesh >> n[0] >> n[1] >> n[2];
			for (j = 0; j < 3; j++){
				facet_.node(j, n[j]);
			}
		}
		if (nvertex == 4){
			inputmesh >> n[0] >> n[1] >> n[2] >> n[3];
			for (j = 0; j < 4; j++){
				facet_.node(j, n[j]);
			}
		}
		facetlist_input.push_back(facet_);
	};

	inputmesh >> word1 >> number1;
	inputmesh >> word1 >> word2 >> word3 >> number1;
	inputmesh >> word1 >> word2;

	for (i = 0; i < nnode_input; i++){
		inputmesh >> mark_;
		nodelist_input[i].markbsurface(mark_);
	}
	inputmesh >> word1 >> number1;
	inputmesh >> word1 >> word2 >> word3 >> number1;
	inputmesh >> word1 >> word2;

	for (i = 0; i < nfacet_input; i++){
		inputmesh >> mark_;
		facetlist_input[i].markbsurface(mark_);
	}

	inputmesh.close();

	//pass the infomation in surface to in (follow the method in tetgen user manual)
	in.firstnumber = 0;
	in.numberofpoints = nnode_input;
	in.pointlist = new REAL[in.numberofpoints * 3];
	in.pointmarkerlist = new int[in.numberofpoints];
	std::cout << in.numberofpoints;
	for (i = 0; i < in.numberofpoints; i++){ //loop over all points
		in.pointlist[i * 3] = nodelist_input[i].x();
		in.pointlist[i * 3 + 1] = nodelist_input[i].y();
		in.pointlist[i * 3 + 2] = nodelist_input[i].z();
		in.pointmarkerlist[i] = nodelist_input[i].markbsurface();
	}
	in.numberoffacets = nfacet_input;
	in.facetlist = new tetgenio::facet[in.numberoffacets];
	in.facetmarkerlist = new int[in.numberoffacets];
	for (i = 0; i < in.numberoffacets; i++){ //loop over all facets on  the surface
		ff = &in.facetlist[i];
		ff->numberofpolygons = 1;
		ff->polygonlist = new tetgenio::polygon[ff->numberofpolygons];
		ff->numberofholes = 0;
		ff->holelist = NULL;
		p = &ff->polygonlist[0];
		p->numberofvertices = facetlist_input[i].numberofvertex();
		p->vertexlist = new int[p->numberofvertices];
		for (j = 0; j < p->numberofvertices; j++){
			p->vertexlist[j] = facetlist_input[i].node(j);
			in.facetmarkerlist[i] = facetlist_input[i].markbsurface();
		}
	}
}

void REGION::car2tetgen(){
	in.firstnumber = 0;
	in.numberofpoints = nodelist.size();
	in.pointlist = new REAL[in.numberofpoints * 3];
	//in.pointmarkerlist = new int[in.numberofpoints];
	//std::cout << in.numberofpoints;
	for (int i = 0; i < in.numberofpoints; i++){ //loop over all points
		in.pointlist[i * 3] = nodelist[i].x();
		in.pointlist[i * 3 + 1] = nodelist[i].y();
		in.pointlist[i * 3 + 2] = nodelist[i].z();
		//in.pointmarkerlist[i] = surfacenodelist[i].markbsurface();
	}
}

void REGION::surfacemesh2tetgen(){
	int i, j, inode,jnode, jnode1, jnode2, ii, jj, ii1, mark_, itri, iv1, iv2, iv3, inn1, inn2, kk, knode, kkk, markfix, markb_;
	double dd1, dd2, dd;
	NODE node1, node2, node3;
	SEGMENT segment_;
	FACET facet_, facet2_;
	std::vector<int> countlist, marknodelist, targetnodes, marksunode;


	marksunode.resize(surfacenodelist.size());
	//nodesunode.resize(surfacenodelist.size());
	//segsunode.resize(surfacenodelist.size());
	//firstly check and fix nodes on edges (now only work for one node on a segment because sutriangle not updated; segmentlist not updated
	for (i = 0; i < facetlist.size(); i++){
		for (j = 0; j < 3; j++){
			inode = facetlist[i].node(j);
			surfacenodelist[inode].addsutriangle(i);
		}
	}

	for (inode = 0; inode < surfacenodelist.size(); inode++){
		marksunode[inode] = -1;
	}

	for (inode = 0; inode < surfacenodelist.size(); inode++){
		for (i = 0; i < surfacenodelist[inode].numberofsutriangle(); i++){
			itri = surfacenodelist[inode].sutriangle(i);
			for (j = 0; j < 3; j++){
				jnode = facetlist[itri].node(j);
				if (jnode != inode && marksunode[jnode] != inode){
					surfacenodelist[inode].addsunode(jnode);
					surfacenodelist[inode].addmarksunode(0);
					marksunode[jnode] = inode;
				}
			}
		}
	}
	for (i = 0; i < segmentlist_all.size(); i++){
		surfacenodelist[segmentlist_all[i].node(0)].addedgein(i);
		surfacenodelist[segmentlist_all[i].node(1)].addedgein(i);
	}

	std::cout << "check nodes on edges" << std::endl;
	for (i = 0; i < surfacenodelist.size(); i++){
		node3 = surfacenodelist[i];
		markfix = 0;
		for (kk = 0; kk < node3.numberofsunode(); kk++){
			knode = node3.sunode(kk);
			for (kkk = 0; kkk < surfacenodelist[knode].numberofedgein(); kkk++){
				j = surfacenodelist[knode].edgein(kkk);
				jnode1 = segmentlist_all[j].node(0);
				jnode2 = segmentlist_all[j].node(1);
				node1 = surfacenodelist[jnode1];
				node2 = surfacenodelist[jnode2];
				if (i != jnode1 && i != jnode2){
					dd = mathzz_.twonodedistance(node1, node2);
					dd1 = mathzz_.twonodedistance(node3, node1);
					dd2 = mathzz_.twonodedistance(node3, node2);
					if (std::abs(dd - dd1 - dd2) < 1e-5){
						std::cout << "node " << i << " on segment " << j << " end nodes " << jnode1 << " " << jnode2 << std::endl;
						for (ii = 0; ii < node1.numberofsutriangle(); ii++){//node1 is jnode1; sufficient to loop jnode1
							jj = node1.sutriangle(ii);
							facet2_ = facetlist[jj];
							for (ii1 = 0; ii1 < 3; ii1++){
								if (facetlist[jj].node(ii1) == jnode2){
									std::cout << "fixed by splitting triangle " << jj << "nodes " << facetlist[jj].node(0) << " " << facetlist[jj].node(1) << " " << facetlist[jj].node(2) << std::endl;
									iv1 = facetlist[jj].node(0);
									iv2 = facetlist[jj].node(1);
									iv3 = facetlist[jj].node(2);

									facetlist[jj].node(0, iv1 + iv2 + iv3 - jnode1 - jnode2);
									facetlist[jj].node(1, i);
									facetlist[jj].node(2, jnode1);
									std::cout << "new triangle 1" << jj << "nodes " << facetlist[jj].node(0) << " " << facetlist[jj].node(1) << " " << facetlist[jj].node(2) << std::endl;
									facet_.node(0, iv1 + iv2 + iv3 - jnode1 - jnode2);
									facet_.node(1, i);
									facet_.node(2, jnode2);
									facet_.markbsurface(facetlist[jj].markbsurface());
									facet_.numberofvertex(3);
									std::cout << "new triangle 2" << jj << "nodes " << facet_.node(0) << " " << facet_.node(1) << " " << facet_.node(2) << std::endl;
									facetlist.push_back(facet_);
									markfix = 1;
									break;
								}
							}
						}
						break;
					}
				}
			}
			if (markfix == 1){
				break;
			}
		}
	}
	/*for (i = 0; i < surfacenodelist.size(); i++){
		std::cout << i << std::endl;
		countlist.clear();
		marknodelist.clear();
		mark_ = 0;
		targetnodes.clear();
		countlist.resize(surfacenodelist.size());
		marknodelist.resize(surfacenodelist.size());
		for (j = 0; j < surfacenodelist[i].numberofsutriangle(); j++){
			jj = surfacenodelist[i].sutriangle(j);
			for (ii = 0; ii < 3; ii++){
				countlist[facetlist[jj].node(ii)]++;
			}
		}

		for (j = 0; j < surfacenodelist[i].numberofsutriangle(); j++){
			jj = surfacenodelist[i].sutriangle(j);
			for (ii = 0; ii < 3; ii++){
				if (countlist[facetlist[jj].node(ii)] == 1 && facetlist[jj].node(ii) != i){
					marknodelist[facetlist[jj].node(ii)] = jj;
					mark_ = 1;//nodes on edges exist
					targetnodes.push_back(facetlist[jj].node(ii));
				}
			}
		}
		if (mark_ == 1 && targetnodes.size() != 2){
			std::cout << "targetnodes size=" << targetnodes.size() << std::endl;
			std::exit(EXIT_FAILURE);
		}
		if (mark_ == 1 && targetnodes.size() == 2){
			inn1 = targetnodes[0];
			inn2 = targetnodes[1];
			node1 = surfacenodelist[i];
			node2 = surfacenodelist[inn1];
			node3 = surfacenodelist[inn2];
			dd = mathzz_.twonodedistance(node2, node3);
			dd1 = mathzz_.twonodedistance(node1, node2);
			dd2 = mathzz_.twonodedistance(node1, node3);
			if (std::abs(dd1 - dd - dd2) < 1e-5){//node2 long inn1
				std::cout << "fix node " << inn2 << " split triangle " << marknodelist[inn1] << std::endl;
				itri = marknodelist[inn1];
				iv1 = facetlist[itri].node(0);
				iv2 = facetlist[itri].node(1);
				iv3 = facetlist[itri].node(2);
				facetlist[itri].node(0, i);
				facetlist[itri].node(1, iv1 + iv2 + iv3 - i - inn1);
				facetlist[itri].node(2, inn2);
				facet_.node(0, inn2);
				facet_.node(1, iv1 + iv2 + iv3 - i - inn1);
				facet_.node(2, inn1);
				facetlist.push_back(facet_);
			}
			else if (std::abs(dd2 - dd - dd1) < 1e-5){//node3 long inn2
				std::cout << "fix node " << inn1 << " split triangle " << marknodelist[inn2] << std::endl;
				itri = marknodelist[inn2];
				iv1 = facetlist[itri].node(0);
				iv2 = facetlist[itri].node(1);
				iv3 = facetlist[itri].node(2);
				facetlist[itri].node(0, i);
				facetlist[itri].node(1, iv1 + iv2 + iv3 - i - inn2);
				facetlist[itri].node(2, inn1);
				facet_.node(0, inn1);
				facet_.node(1, iv1 + iv2 + iv3 - i - inn2);
				facet_.node(2, inn2);
				facetlist.push_back(facet_);
			}
		}
	}


	std::cout << "total triagle number:" << facetlist.size() << std::endl;
	for (i = 0; i < facetlist.size(); i++){
		for (j = 0; j < 3; j++){
			if (facetlist[i].node(j) == 4719){
				std::cout << "triangle " << i << "nodes " << facetlist[i].node(0) << " " << facetlist[i].node(1) << " " << facetlist[i].node(2) << std::endl;
			}
		}
	}*/

	std::cout << "check nodes on edges end" << std::endl;

	nfacet = facetlist.size();

	in.firstnumber = 0;
	in.numberofpoints = nsurfacenode;
	in.pointlist = new REAL[in.numberofpoints * 3];
	in.pointmarkerlist = new int[in.numberofpoints];
	std::cout << in.numberofpoints;
	for (i = 0; i < in.numberofpoints; i++){ //loop over all points
		in.pointlist[i * 3] = surfacenodelist[i].x();
		in.pointlist[i * 3 + 1] = surfacenodelist[i].y();
		in.pointlist[i * 3 + 2] = surfacenodelist[i].z();
		in.pointmarkerlist[i] = surfacenodelist[i].markbsurface();
	}
	in.numberoffacets = facetlist.size();
	in.facetlist = new tetgenio::facet[in.numberoffacets];
	in.facetmarkerlist = new int[in.numberoffacets];
	for (i = 0; i < in.numberoffacets; i++){ //loop over all facets on  the surface
		ff = &in.facetlist[i];
		ff->numberofpolygons = 1;
		ff->polygonlist = new tetgenio::polygon[ff->numberofpolygons];
		ff->numberofholes = 0;
		ff->holelist = NULL;
		p = &ff->polygonlist[0];
		p->numberofvertices = facetlist[i].numberofvertex();
		p->vertexlist = new int[p->numberofvertices];
		for (j = 0; j < p->numberofvertices; j++){
			p->vertexlist[j] = facetlist[i].node(j);
			in.facetmarkerlist[i] = facetlist[i].markbsurface() + 1; //let start from 1 then when tetgen2region back to 0
		}
	}
	//just for the 2nd RRM surface coons-patch
	in.numberofregions = rocklist.size();
	in.regionlist = new REAL[in.numberofregions * 5];
	int iii = 0;
	for (i = 0; i < rocklist.size(); i++){
		in.regionlist[i * 5] = rocklist[i].x();
		in.regionlist[i * 5 + 1] = rocklist[i].y();
		in.regionlist[i * 5 + 2] = rocklist[i].z();
		in.regionlist[i * 5 + 3] = i;
		in.regionlist[i * 5 + 4] = -1.0;
	}
}
void REGION::well2tetgen() {
	int iwell, inode, nwellnode, nodec_;
	NODE node_;

	nwellnode = 0;
	for (iwell = 0; iwell < welllist.size(); iwell++){
		nwellnode += welllist[iwell].nodelist().size();
	}

	addin.firstnumber = 0;
	addin.numberofpoints = nwellnode;
	addin.pointlist = new REAL[addin.numberofpoints * 3];
	//addin.pointmarkerlist = new int[addin.numberofpoints];
	nodec_ = -1;
	for (iwell = 0; iwell < welllist.size(); iwell++){
		std::vector<NODE> nodelist_ = welllist[iwell].nodelist();
		for (inode = 0; inode < nodelist_.size(); inode++){
			nodec_++;
			node_ = nodelist_[inode];
			addin.pointlist[nodec_ * 3] = node_.x();
			addin.pointlist[nodec_ * 3 + 1] = node_.y();
			addin.pointlist[nodec_ * 3 + 2] = node_.z();
			//addin.pointmarkerlist[nodec_] = -10 - iwell;//mark well ---useless because cannot be passed to out
		}
	}

}

void REGION::writevolumemesh(std::string filein){
	//wellcondition(); //for visualise well marks
	int i,j, nnode, nelement, markw_;
	std::ofstream outputmesh, out;
	outputmesh.open(filein);
	if (trifacesbutton_ == 1){
		out.open("trifacies.txt");
	}
	nnode = nodelist.size();
	nelement = elementlist.size();

	outputmesh << "# vtk DataFile Version 2.0" << std::endl;
	outputmesh << "Unstructured Grid" << std::endl;
	outputmesh << "ASCII" << std::endl;
	outputmesh << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputmesh << "POINTS " << nnode << " double" << std::endl;
	for (i = 0; i < nnode; i++){
		outputmesh << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	outputmesh << std::endl;
	outputmesh << "CELLS " << nelement << " " << nelement * 5 << std::endl;
	for (i = 0; i < nelement; i++){
		outputmesh << 4 << " " << elementlist[i].node(0) << " " << elementlist[i].node(1) << " " << elementlist[i].node(2) << " " << elementlist[i].node(3)<< std::endl;
	}

	outputmesh << "CELL_TYPES " << nelement << std::endl;
	for (i = 0; i < nelement; i++){
		outputmesh << 10 << std::endl; //for tetrahedron
	}
	/*if (wellvisbutton_ == 1){
		outputmesh << "POINT_DATA" << " " << nnode << std::endl;
		outputmesh << "SCALARS WellID double" << std::endl;
		outputmesh << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputmesh << nodelist[i].markwell() + 1 << std::endl;
		}
	}*/

	outputmesh << "CELL_DATA" << " " << nelement << std::endl;
	outputmesh << "SCALARS Region double" << std::endl;
	outputmesh << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nelement; i++){
		outputmesh << elementlist[i].markregion()+1 << std::endl;
	}
	/*if (wellvisbutton_ == 1){
		outputmesh << "SCALARS WellID double" << std::endl;
		outputmesh << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			markw_ = 0;
			for (j = 0; j < 4; j++){
				if (nodelist[elementlist[i].node(j)].markwell() >= 0){
					markw_ = nodelist[elementlist[i].node(j)].markwell()+1;
					break;
				}
			}
			outputmesh << markw_ << std::endl;
		}
	}*/
	outputmesh.close();
	if (trifacesbutton_ == 1){
		out << bfacetlist.size() << std::endl;
		for (i = 0; i < bfacetlist.size(); i++){
			out << bfacetlist[i].markbsurface() << " " << bfacetlist[i].node(0) << " " << bfacetlist[i].node(1) << " " << bfacetlist[i].node(2) << std::endl;
		}
		out.close();
	}
}

void REGION::writecornerpointgridVTK(const char* file){
	int i;
	std::ofstream outputmesh;
	outputmesh.open(file);
	outputmesh << "# vtk DataFile Version 2.0" << std::endl;
	outputmesh << "Unstructured Grid" << std::endl;
	outputmesh << "ASCII" << std::endl;
	outputmesh << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputmesh << "POINTS " << nodelist.size() << " double" << std::endl;
	for (i = 0; i < nodelist.size(); i++){
		outputmesh << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	outputmesh << std::endl;
	outputmesh << "CELLS " << cpgelementlist.size() << " " << cpgelementlist.size() * 9 << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		outputmesh << 8 << " " << cpgelementlist[i].node(0) << " " << cpgelementlist[i].node(1) << " " << cpgelementlist[i].node(3) << " " << cpgelementlist[i].node(2) <<" "<< cpgelementlist[i].node(4) << " " << cpgelementlist[i].node(5) << " " << cpgelementlist[i].node(7) << " " << cpgelementlist[i].node(6) << std::endl;
	}
	outputmesh << std::endl;
	outputmesh << "CELL_TYPES " << cpgelementlist.size() << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		outputmesh << 12 << std::endl;
	}
	outputmesh.close();
}

void REGION::writecornerpointgridGRDECL(char* file){
	int i, ngx, ngy, ngz, j, ix, iy, iz, nn;
	double dd1, dd2;
	std::ofstream output;
	output.open(file);
	ngx = meshinfo.cpgdimension(0);
	ngy = meshinfo.cpgdimension(1);
	ngz = pillarlist[0].numberofnodes()-1;
	output << "-- Exported by RRM for Petrel or Eclipse (GRDECL format)" << std::endl;
	output << "DIMENS" << std::endl;
	output << ngx << " " << ngy << " " << ngz << std::endl;
	output << "COORD" << std::endl;

	for (iy = 0; iy < ngy + 1; iy++){
		for (ix = 0; ix < ngx + 1; ix++){
			output << nodelist[cpgnodematrix[ix][iy][0]].x() << " " << nodelist[cpgnodematrix[ix][iy][0]].y() << " " << nodelist[cpgnodematrix[ix][iy][0]].z() << " ";
			output << nodelist[cpgnodematrix[ix][iy][ngz]].x() << " " << nodelist[cpgnodematrix[ix][iy][ngz]].y() << " " << nodelist[cpgnodematrix[ix][iy][ngz]].z() << std::endl;
		}
	}
	output << std::endl;
	output << "ZCORN" << std::endl;

	for (iz = 0; iz < ngz; iz++){
		for (iy = 0; iy < ngy; iy++){
			for (ix = 0; ix < ngx; ix++){
				output << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(0)].z() << " " << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(1)].z() << std::endl;

			}
			for (ix = 0; ix < ngx; ix++){
				output << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(2)].z() << " " << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(3)].z() << std::endl;
			}
		}
		for (iy = 0; iy < ngy; iy++){
			for (ix = 0; ix < ngx; ix++){
				output << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(4)].z() << " " << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(5)].z() << std::endl;
			}
			for (ix = 0; ix < ngx; ix++){
				output << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(6)].z() << " " << nodelist[cpgelementlist[ix + iy*ngx + iz*ngx*ngy].node(7)].z() << std::endl;
			}
		}
	}

	output << std::endl;
	output << "ACTNUM" << std::endl;
	for (i = 0; i < ngx*ngy*ngz; i++){
		output << 1 << std::endl;
	}
	output << std::endl;
	output << "Layers" << std::endl;
	for (i = 0; i < ngx*ngy*ngz; i++){
		output << cpgelementlist[i].layer() << std::endl;
	}
	output << std::endl;
	output << "ECHO";
}

void REGION::writeresult(char* file){
	if (meshinfo.type() == 1 && numberofphases_==1){
		writeresult_cvfe_sf_log2t(file);
	}
	else if (meshinfo.type() == 1 && numberofphases_ == 2){
		writeresult_cvfe_mf(file);
	}
	else{
		writeresult_cpgfv(file);
	}
}

void REGION::writeresult_cvfe_sf_log2t(char* fileout){
	int i, nnode, nelement;
	std::vector<int> cycle_;
	std::ofstream outputresult;
	double aa;

	nnode = nodelist.size();
	nelement = elementlist.size();

	std::cout << "outputting results" << std::endl;
	outputresult.open(fileout);
	outputresult << "# vtk DataFile Version 2.0" << std::endl;
	outputresult << "Unstructured Grid" << std::endl;
	outputresult << "ASCII" << std::endl;
	outputresult << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputresult << "POINTS " << nnode << " double" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	outputresult << std::endl;
	outputresult << "CELLS " << nelement << " " << nelement * 5 << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 4 << " " << elementlist[i].node(0) << " " << elementlist[i].node(1) << " " << elementlist[i].node(2) << " " << elementlist[i].node(3) << std::endl;
	}
	outputresult << std::endl;
	outputresult << "CELL_TYPES " << nelement << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 10 << std::endl; //for tetrahedron
	}

	outputresult << "POINT_DATA" << " " << nnode << std::endl;


	outputresult << "SCALARS Corrected_Pressure double" << std::endl; //sf corrected P mf absolute P
	outputresult << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nnode; i++){
		//outputresult << nodelist[i].Po() << std::endl;
		outputresult << nodelist[i].Po() / 100000 << std::endl; //for unit bar
	}
	//outputresult << "SCALARS NodeEdgeMark double" << std::endl; //sf corrected P mf absolute P
	//outputresult << "LOOKUP_TABLE default" << std::endl;
	//for (i = 0; i < nnode; i++){
	//	outputresult <<nodeedgemark[i] << std::endl;
	//}
	//outputresult << "SCALARS NodeCvalue double" << std::endl; //sf corrected P mf absolute P
	//outputresult << "LOOKUP_TABLE default" << std::endl;
	//for (i = 0; i < nnode; i++){
	//	outputresult <<nodecvalue[i] << std::endl;
	//}
	if (ftofbutton_ == 1){
		outputresult << "SCALARS Forward_TOF_days double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			//outputresult << nodelist[i].tof() / 86400 << std::endl; //sec to day
			aa = log2(nodelist[i].tof() / 86400.0);
			if (aa < -4.0){ //includes when tof=0; tof<0.0625
				aa = -4.0;
			}
			outputresult << aa << std::endl; //sec to day
		}
	}
	if (btofbutton_ == 1){
		outputresult << "SCALARS Backward_TOF_days double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			//outputresult << nodelist[i].tof_back() / 86400 << std::endl; //sec to day
			aa = log2(nodelist[i].tof_back() / 86400.0);
			if (aa < -4.0){
				aa = -4.0;
			}
			outputresult << aa << std::endl; //sec to day
		}
		outputresult << "SCALARS Total_TOF_days double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			//outputresult << nodelist[i].tof_total() / 86400 << std::endl; //sec to day
			aa = log2(nodelist[i].tof_total() / 86400);
			if (aa < -4){
				aa = -4;
			}
			outputresult << aa << std::endl; //sec to day
		}
	}

	if (ftracerbutton_ == 1){
		outputresult << "SCALARS Max_Forward_Tracer double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].markmaxtracer() << std::endl;
		}
		outputresult << "SCALARS Tracer0 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer(0) << std::endl;
		}
		outputresult << "SCALARS Tracer1 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer(1) << std::endl;
		}
		outputresult << "SCALARS Tracer2 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer(2) << std::endl;
		}
		outputresult << "SCALARS Tracer3 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer(3) << std::endl;
		}
	}
	if (btracerbutton_ == 1){
		outputresult << "SCALARS Max_Backward_Tracer double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].markmaxtracer_back() << std::endl;
		}

		/*outputresult << "SCALARS Tracerbk0 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer_back(0) << std::endl;
		}
		outputresult << "SCALARS Tracerbk1 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer_back(1) << std::endl;
		}
		outputresult << "SCALARS Tracerbk2 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer_back(2) << std::endl;
		}
		outputresult << "SCALARS Tracerbk3 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer_back(3) << std::endl;
		}	*/
	}
	if (ftracerbutton_ == 1){
		outputresult << "SCALARS Tracer0 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].tracer(0) << std::endl;
		}
	}

	outputresult << "CELL_DATA" << " " << nelement << std::endl;

	outputresult << "VECTORS Velocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_t() << " " << elementlist[i].Uy_t() << " " << elementlist[i].Uz_t() << std::endl;
	}


	if (vis_regionidbutton_ == 1){
		outputresult << "SCALARS regionID int" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].markregion() + 1 << std::endl;
		}
	}
	if (vis_permbutton_ == 1){
		outputresult << "SCALARS Permeability double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].k(0, 0) / 0.987e-15 << std::endl;
		}
	}
	if (vis_porobutton_ == 1){
		outputresult << "SCALARS Porosity double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].porosity() << std::endl;
		}
	}
}

void REGION::writemeshandproperty_tet(char* fileout){
	int i, nnode, nelement;
	std::vector<int> cycle_;
	std::ofstream outputresult;

	nnode = nodelist.size();
	nelement = elementlist.size();

	std::cout << "outputting results" << std::endl;
	outputresult.open(fileout);
	outputresult << "# vtk DataFile Version 2.0" << std::endl;
	outputresult << "Unstructured Grid" << std::endl;
	outputresult << "ASCII" << std::endl;
	outputresult << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputresult << "POINTS " << nnode << " double" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	outputresult << std::endl;
	outputresult << "CELLS " << nelement << " " << nelement * 5 << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 4 << " " << elementlist[i].node(0) << " " << elementlist[i].node(1) << " " << elementlist[i].node(2) << " " << elementlist[i].node(3) << std::endl;
	}
	outputresult << std::endl;
	outputresult << "CELL_TYPES " << nelement << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 10 << std::endl; //for tetrahedron
	}


	outputresult << "CELL_DATA" << " " << nelement << std::endl;

	if (vis_regionidbutton_ == 1){
		outputresult << "SCALARS regionID double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].markregion() + 1 << std::endl;
		}
	}
	if (vis_permbutton_ == 1){
		outputresult << "SCALARS Permeability double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].k(0, 0) / 0.987e-15 << std::endl;
		}
	}
	if (vis_porobutton_ == 1){
		outputresult << "SCALARS Porosity double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].porosity() << std::endl;
		}
	}
}

void REGION::writeresult_cvfe_sf(char* fileout){
	int i, nnode, nelement;
	std::vector<int> cycle_;
	std::ofstream outputresult;

	nnode = nodelist.size();
	nelement = elementlist.size();

	std::cout << "outputting results" << std::endl;
	outputresult.open(fileout);
	outputresult << "# vtk DataFile Version 2.0" << std::endl;
	outputresult << "Unstructured Grid" << std::endl;
	outputresult << "ASCII" << std::endl;
	outputresult << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputresult << "POINTS " << nnode << " double" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	outputresult << std::endl;
	outputresult << "CELLS " << nelement << " " << nelement * 5 << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 4 << " " << elementlist[i].node(0) << " " << elementlist[i].node(1) << " " << elementlist[i].node(2) << " " << elementlist[i].node(3) << std::endl;
	}
	outputresult << std::endl;
	outputresult << "CELL_TYPES " << nelement << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 10 << std::endl; //for tetrahedron
	}

	outputresult << "POINT_DATA" << " " << nnode << std::endl;


	outputresult << "SCALARS Corrected_PressureBar double" << std::endl; //sf corrected P mf absolute P
	outputresult << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nnode; i++){
		//outputresult << nodelist[i].Po() << std::endl;
		outputresult << nodelist[i].Po() / 100000 << std::endl; //for unit bar
	}


	if (ftofbutton_ == 1){
		outputresult << "SCALARS Forward_TOF_days double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			outputresult << nodelist[i].tof() / 86400 << std::endl; //sec to day
		}
	}
	if (btofbutton_ == 1){
		outputresult << "SCALARS Backward_TOF_days double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			outputresult << nodelist[i].tof_back() / 86400 << std::endl; //sec to day
		}
		outputresult << "SCALARS Total_TOF_days double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			outputresult << nodelist[i].tof_total() / 86400 << std::endl; //sec to day
		}
	}

	if (ftracerbutton_ == 1){
		outputresult << "SCALARS Max_Forward_Tracer double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].markmaxtracer() << std::endl;
		}
	}
	if (btracerbutton_ == 1){
		outputresult << "SCALARS Max_Backward_Tracer double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].markmaxtracer_back() << std::endl;
		}
		/*
		outputresult << "SCALARS Tracer0 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(0) << std::endl;
		}
		outputresult << "SCALARS Tracer1 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(1) << std::endl;
		}
		outputresult << "SCALARS Tracer2 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(2) << std::endl;
		}
		outputresult << "SCALARS Tracer3 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(3) << std::endl;
		}
		*/
	}
	/*if (wellvisbutton_ == 1){
	outputresult << "SCALARS WellID int" << std::endl;
	outputresult << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nnode; i++) {
	outputresult << nodelist[i].markwell()+1 << std::endl;
	}
	}*/


	outputresult << "CELL_DATA" << " " << nelement << std::endl;



	outputresult << "VECTORS Velocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_t() << " " << elementlist[i].Uy_t() << " " << elementlist[i].Uz_t() << std::endl;
	}


	if (vis_regionidbutton_ == 1){
		outputresult << "SCALARS regionID double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].markregion() + 1 << std::endl;
		}
	}
	if (vis_permbutton_ == 1){
		outputresult << "SCALARS Permeability double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].k(0, 0) / 0.987e-15 << std::endl;
		}
	}
	if (vis_porobutton_ == 1){
		outputresult << "SCALARS Porosity double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].porosity() << std::endl;
		}
	}
}

void REGION::computemaxtof(){
	double m = 0;
	for (int i = 0; i < nodelist.size(); i++){
		if (nodelist[i].tof()>m){
			m = nodelist[i].tof();
		}
	}
	std::cout << "max tof is " << m << std::endl;
}

void REGION::writeresult_fmm_tet(char* fileout){
	int i, nnode, nelement;
	std::vector<int> cycle_;
	std::ofstream outputresult;

	nnode = nodelist.size();
	nelement = elementlist.size();

	std::cout << "outputting results" << std::endl;
	outputresult.open(fileout);
	outputresult << "# vtk DataFile Version 2.0" << std::endl;
	outputresult << "Unstructured Grid" << std::endl;
	outputresult << "ASCII" << std::endl;
	outputresult << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputresult << "POINTS " << nnode << " double" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	outputresult << std::endl;
	outputresult << "CELLS " << nelement << " " << nelement * 5 << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 4 << " " << elementlist[i].node(0) << " " << elementlist[i].node(1) << " " << elementlist[i].node(2) << " " << elementlist[i].node(3) << std::endl;
	}
	outputresult << std::endl;
	outputresult << "CELL_TYPES " << nelement << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 10 << std::endl; //for tetrahedron
	}

	outputresult << "POINT_DATA" << " " << nnode << std::endl;
	if (dtofbutton_ == 1){
		outputresult << "SCALARS DiffusiveTOF double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			outputresult << nodelist[i].tof() << std::endl;
		}
		if (welltestbutton_ == 1){
			outputresult << "SCALARS Pressure double" << std::endl;
			outputresult << "LOOKUP_TABLE default" << std::endl;
			for (i = 0; i < nnode; i++){
				outputresult << nodelist[i].Po() << std::endl;
			}
		}
	}
	if (dtofbutton_ == 1){
		outputresult << "CELL_DATA" << " " << nelement << std::endl;
		outputresult << "SCALARS FMMVX double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].fmmv(0)<< std::endl;
		}
	}
	if (vis_permbutton_ == 1){
		outputresult << "SCALARS Permeability double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].k(0, 0) / 0.987e-15 << std::endl;
		}
	}
	if (vis_porobutton_ == 1){
		outputresult << "SCALARS Porosity double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].porosity() << std::endl;
		}
	}
}

void REGION::writeresult_cvfe_mf(char* fileout){
	int i, nnode, nelement;
	std::vector<int> cycle_;
	std::ofstream outputresult;

	nnode = nodelist.size();
	nelement = elementlist.size();

	std::cout << "outputting results" << std::endl;
	outputresult.open(fileout);
	outputresult << "# vtk DataFile Version 2.0" << std::endl;
	outputresult << "Unstructured Grid" << std::endl;
	outputresult << "ASCII" << std::endl;
	outputresult << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputresult << "POINTS " << nnode << " double" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	outputresult << std::endl;
	outputresult << "CELLS " << nelement << " " << nelement * 5 << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 4 << " " << elementlist[i].node(0) << " " << elementlist[i].node(1) << " " << elementlist[i].node(2) << " " << elementlist[i].node(3) << std::endl;
	}
	outputresult << std::endl;
	outputresult << "CELL_TYPES " << nelement << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 10 << std::endl; //for tetrahedron
	}

	outputresult << "POINT_DATA" << " " << nnode << std::endl;


	outputresult << "SCALARS Oil_Pressure double" << std::endl;
	outputresult << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nnode; i++){
		//outputresult << nodelist[i].Po() << std::endl;
		outputresult << nodelist[i].Po() / 100000 << std::endl; //for unit bar
	}


	if (ftofbutton_ == 1){
		outputresult << "SCALARS Forward_TOF_log2 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			//double aa = nodelist[i].tof() / 86400;
			double aa = log2(nodelist[i].tof() / 86400);
			if (aa < 0){
				aa = 0;
			}
			outputresult << aa << std::endl; //sec to day
		}
	}
	if (btofbutton_ == 1){
		outputresult << "SCALARS Backward_TOF_log2 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			//double aa = nodelist[i].tof_back() / 86400;
			double aa = log2(nodelist[i].tof_back() / 86400);
			if (aa < 0){
				aa = 0;
			}
			outputresult << aa << std::endl; //sec to day
		}
		outputresult << "SCALARS Total_TOF_log2 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++){
			//double aa = nodelist[i].tof_total() / 86400;
			double aa = log2(nodelist[i].tof_total() / 86400);
			if (aa < 0){
				aa = 0;
			}
			outputresult << aa << std::endl; //sec to day
		}
	}

	if (ftracerbutton_ == 1){
		outputresult << "SCALARS Max_Forward_Tracer double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].markmaxtracer() << std::endl;
		}
	}
	if (btracerbutton_ == 1){
		outputresult << "SCALARS Max_Backward_Tracer double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
			outputresult << nodelist[i].markmaxtracer_back() << std::endl;
		}
		/*
		outputresult << "SCALARS Tracer0 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(0) << std::endl;
		}
		outputresult << "SCALARS Tracer1 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(1) << std::endl;
		}
		outputresult << "SCALARS Tracer2 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(2) << std::endl;
		}
		outputresult << "SCALARS Tracer3 double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nnode; i++) {
		outputresult << nodelist[i].tracer(3) << std::endl;
		}
		*/
	}


	outputresult << "CELL_DATA" << " " << nelement << std::endl;



	outputresult << "VECTORS TotalVelocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_t() << " " << elementlist[i].Uy_t() << " " << elementlist[i].Uz_t() << std::endl;
	}
	outputresult << "VECTORS Oil_Velocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_t() << " " << elementlist[i].Uy_t() << " " << elementlist[i].Uz_t() << std::endl;
	}
	outputresult << "VECTORS Water_Velocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_w() << " " << elementlist[i].Uy_w() << " " << elementlist[i].Uz_w() << std::endl;
	}


	if (vis_regionidbutton_ == 1){
		outputresult << "SCALARS regionID double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].markregion() + 1 << std::endl;
		}
	}
	if (vis_permbutton_ == 1){
		outputresult << "SCALARS Perm_Absolute double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].k(0, 0) / 0.987e-15 << std::endl;
		}
	}
	/*if (vis_saturationbutton_ == 1){
		outputresult << "SCALARS Saturation_Water double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].Sw() << std::endl;
		}
		outputresult << "SCALARS Saturation_oil double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << 1.0 - elementlist[i].Sw() << std::endl;
		}
		outputresult << "SCALARS relativeperm_Water double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].krw() << std::endl;
		}
		outputresult << "SCALARS relativeperm_Oil double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].kro() << std::endl;
		}
	}*/
}

void REGION::simresult_cvfe_mf(char* fileout){
	int i, nnode, nelement;
	std::ofstream outputresult;

	nnode = nodelist.size();
	nelement = elementlist.size();

	std::cout << "outputting results" << std::endl;
	outputresult.open(fileout);
	outputresult << "# vtk DataFile Version 2.0" << std::endl;
	outputresult << "Unstructured Grid" << std::endl;
	outputresult << "ASCII" << std::endl;
	outputresult << "DATASET UNSTRUCTURED_GRID" << std::endl;
	outputresult << "POINTS " << nnode << " double" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z()/zscale_ << std::endl;
	};
	outputresult << std::endl;
	outputresult << "CELLS " << nelement << " " << nelement * 5 << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 4 << " " << elementlist[i].node(0) << " " << elementlist[i].node(1) << " " << elementlist[i].node(2) << " " << elementlist[i].node(3) << std::endl;
	}
	outputresult << std::endl;
	outputresult << "CELL_TYPES " << nelement << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << 10 << std::endl; //for tetrahedron
	}

	outputresult << "POINT_DATA" << " " << nnode << std::endl;

	outputresult << "SCALARS Oil_Pressure double" << std::endl;
	outputresult << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].Po() / 100000 << std::endl; //for unit bar
	}

	outputresult << "SCALARS Sw double" << std::endl;
	outputresult << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nnode; i++){
		outputresult << nodelist[i].Sw() << std::endl; //for unit bar
	}

	//outputresult << "SCALARS NodeEdgeMark double" << std::endl; //sf corrected P mf absolute P
	//outputresult << "LOOKUP_TABLE default" << std::endl;
	//for (i = 0; i < nnode; i++){
	//	outputresult <<nodeedgemark[i] << std::endl;
	//}

	outputresult << "CELL_DATA" << " " << nelement << std::endl;

	outputresult << "VECTORS TotalVelocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_t() << " " << elementlist[i].Uy_t() << " " << elementlist[i].Uz_t() << std::endl;
	}
	/*outputresult << "VECTORS Oil_Velocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_o() << " " << elementlist[i].Uy_o() << " " << elementlist[i].Uz_o() << std::endl;
	}*/
	/*outputresult << "VECTORS Water_Velocity double" << std::endl;
	for (i = 0; i < nelement; i++){
		outputresult << elementlist[i].Ux_w() << " " << elementlist[i].Uy_w() << " " << elementlist[i].Uz_w() << std::endl;
	}*/


	if (vis_regionidbutton_ == 1){
		outputresult << "SCALARS regionID double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].markregion() + 1 << std::endl;
		}
	}
	if (vis_permbutton_ == 1){
		outputresult << "SCALARS Perm_Absolute double" << std::endl;
		outputresult << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			outputresult << elementlist[i].k(0, 0) / 0.987e-15 << std::endl;
		}
	}

}

void REGION::writeSwwithx(char* fileout){

	std::ofstream outputresult;

	outputresult.open(fileout);

	for (int i = 0; i < nodelist.size(); i++){
		outputresult << nodelist[i].x() << " " << nodelist[i].Sw() << std::endl;
	};

}

void REGION::rescalecpgftof(double dd){
	double tt;
	for (int i = 0; i < cpgelementlist.size(); i++){
		tt = cpgelementlist[i].tof()*dd;
		cpgelementlist[i].tof(tt);
	}
}

void REGION::rescalecpgbtof(double dd){
	double tt;
	for (int i = 0; i < cpgelementlist.size(); i++){
		tt = cpgelementlist[i].tof_back()*dd;
		cpgelementlist[i].tof_back(tt);
	}
}

void REGION::writeresult_cpgfv(char* file){
	int i, nelement;
	std::ofstream output;
	nelement = cpgelementlist.size();
	std::cout << "outputting results for CPG" << std::endl;
	output.open(file);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Unstructured Grid" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET UNSTRUCTURED_GRID" << std::endl;
	output << "POINTS " << nodelist.size() << " double" << std::endl;
	for (i = 0; i < nodelist.size(); i++){
		output << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	output << std::endl;
	output << "CELLS " << cpgelementlist.size() << " " << cpgelementlist.size() * 9 << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 8 << " " << cpgelementlist[i].node(0) << " " << cpgelementlist[i].node(1) << " " << cpgelementlist[i].node(3) << " " << cpgelementlist[i].node(2) << " " << cpgelementlist[i].node(4) << " " << cpgelementlist[i].node(5) << " " << cpgelementlist[i].node(7) << " " << cpgelementlist[i].node(6) << std::endl;
	}
	output << std::endl;
	output << "CELL_TYPES " << cpgelementlist.size() << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 12 << std::endl;
	}

	output << "CELL_DATA" << " " << nelement << std::endl;


	output << "SCALARS Corrected_Pressure double" << std::endl;
	output << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nelement; i++){
		output << cpgelementlist[i].Po() / 1000000 << std::endl; //for unit mPa
	}
	if (temperaturebutton_ == 1){
		output << "SCALARS Temperature double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].temperature << std::endl;
		}
	}
	if (ftofbutton_ == 1){
		output << "SCALARS TOF double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].tof() << std::endl;
		}
	}
	if (btofbutton_ == 1){
		output << "SCALARS TOF_back double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].tof_back() << std::endl;
		}
		output << "SCALARS TOF_total double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].tof_total() << std::endl;
		}
	}
	if (ftracerbutton_ == 1){
		output << "SCALARS Tracer int" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].markmaxtracer() + 1 << std::endl; //in code starts from 0 so +1 then starts from 1
		}
	}
	if (btracerbutton_ == 1){
		output << "SCALARS Tracer_back int" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].markmaxtracer_back() + 1 << std::endl; //in code starts from 0 so +1 then starts from 1
		}
	}
	if (vis_permbutton_ == 1){
		output << "SCALARS Permeability double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].k(0, 0) / 1e-15 << std::endl;
		}
	}
	if (vis_porobutton_ == 1){
		output << "SCALARS Porosity double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].porosity() << std::endl;
		}
	}
	output.close();
	std::cout << "DONE" << std::endl;
}

void REGION::writeresult_cpgfv_log2t(char* file){
	int i, nelement;
	std::ofstream output;
	nelement = cpgelementlist.size();
	std::cout << "outputting results for CPG" << std::endl;
	output.open(file);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Unstructured Grid" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET UNSTRUCTURED_GRID" << std::endl;
	output << "POINTS " << nodelist.size() << " double" << std::endl;
	for (i = 0; i < nodelist.size(); i++){
		output << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	output << std::endl;
	output << "CELLS " << cpgelementlist.size() << " " << cpgelementlist.size() * 9 << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 8 << " " << cpgelementlist[i].node(0) << " " << cpgelementlist[i].node(1) << " " << cpgelementlist[i].node(3) << " " << cpgelementlist[i].node(2) << " " << cpgelementlist[i].node(4) << " " << cpgelementlist[i].node(5) << " " << cpgelementlist[i].node(7) << " " << cpgelementlist[i].node(6) << std::endl;
	}
	output << std::endl;
	output << "CELL_TYPES " << cpgelementlist.size() << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 12 << std::endl;
	}

	output << "CELL_DATA" << " " << nelement << std::endl;


	output << "SCALARS Corrected_Pressure double" << std::endl;
	output << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nelement; i++){
		output << cpgelementlist[i].Po() / 100000 << std::endl; //for unit bar
	}

	if (ftofbutton_ == 1){
		output << "SCALARS TOF double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			double aa = log2(cpgelementlist[i].tof() / 86400.0);
			if (aa < -4.0){ //includes when tof=0; tof<0.0625
				aa = -4.0;
			}
			output << aa << std::endl;
		}
	}
	if (btofbutton_ == 1){
		output << "SCALARS TOF_back double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			double aa = log2(cpgelementlist[i].tof_back() / 86400.0);
			if (aa < -4.0){ //includes when tof=0; tof<0.0625
				aa = -4.0;
			}
			output << aa << std::endl; //for unit bar
		}
		output << "SCALARS TOF_total double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			double aa = log2(cpgelementlist[i].tof_total() / 86400.0);
			if (aa < -4.0){ //includes when tof=0; tof<0.0625
				aa = -4.0;
			}
			output << aa << std::endl; //for unit bar
		}
	}
	if (ftracerbutton_ == 1){
		output << "SCALARS Tracer int" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].markmaxtracer() + 1 << std::endl; //in code starts from 0 so +1 then starts from 1
		}
	}
	if (btracerbutton_ == 1){
		output << "SCALARS Tracer_back int" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].markmaxtracer_back() + 1 << std::endl; //in code starts from 0 so +1 then starts from 1
		}
		/*
		output << "SCALARS Tracer0 double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
		output << elementlist[i].tracer(0) << std::endl;
		}
		output << "SCALARS Tracer1 double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
		output << elementlist[i].tracer(1) << std::endl;
		}
		output << "SCALARS Tracer2 double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
		output << elementlist[i].tracer(2) << std::endl;
		}
		output << "SCALARS Tracer3 double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
		output << elementlist[i].tracer(3) << std::endl;
		}
		*/
	}
	if (vis_permbutton_ == 1){
		output << "SCALARS Permeability double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].k(0, 0) / 1e-15 << std::endl;
		}
	}
	if (vis_porobutton_ == 1){
		output << "SCALARS Porosity double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].porosity() << std::endl;
		}
	}
	output.close();
	std::cout << "DONE" << std::endl;
}

void REGION::simresult_cpgfv_mf(char* file){
	int i, nelement;
	std::ofstream output;
	nelement = cpgelementlist.size();
	std::cout << "outputting results for CPG" << std::endl;
	output.open(file);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Unstructured Grid" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET UNSTRUCTURED_GRID" << std::endl;
	output << "POINTS " << nodelist.size() << " double" << std::endl;
	for (i = 0; i < nodelist.size(); i++){
		output << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	output << std::endl;
	output << "CELLS " << cpgelementlist.size() << " " << cpgelementlist.size() * 9 << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 8 << " " << cpgelementlist[i].node(0) << " " << cpgelementlist[i].node(1) << " " << cpgelementlist[i].node(3) << " " << cpgelementlist[i].node(2) << " " << cpgelementlist[i].node(4) << " " << cpgelementlist[i].node(5) << " " << cpgelementlist[i].node(7) << " " << cpgelementlist[i].node(6) << std::endl;
	}
	output << std::endl;
	output << "CELL_TYPES " << cpgelementlist.size() << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 12 << std::endl;
	}

	output << "CELL_DATA" << " " << nelement << std::endl;

	output << "SCALARS Corrected_Pressure double" << std::endl;
	output << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nelement; i++){
		output << cpgelementlist[i].Po() / 100000 << std::endl; //for unit bar
	}

	output << "SCALARS Sw double" << std::endl;
	output << "LOOKUP_TABLE default" << std::endl;
	for (i = 0; i < nelement; i++){
		output << cpgelementlist[i].Sw() << std::endl; //for unit bar
	}

	if (vis_permbutton_ == 1){
		output << "SCALARS Perm_Absolute double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].k(0, 0) / 0.987e-15 << std::endl; //for unit bar
		}
	}

	output.close();
}


void REGION::writesurfacemeshVTK(const char* file){
	int i, j;
	std::ofstream meshout;
	meshout.open(file);
	meshout << "# vtk DataFile Version 2.0" << std::endl;
	meshout << "Unstructured Grid" << std::endl;
	meshout << "ASCII" << std::endl;
	meshout << "DATASET UNSTRUCTURED_GRID" << std::endl;
	meshout << "POINTS " << nsurfacenode << " double" << std::endl;
	for (i = 0; i < nsurfacenode; i++){
		meshout << surfacenodelist[i].x() << " " << surfacenodelist[i].y() << " " << surfacenodelist[i].z() << std::endl;
	};
	meshout << std::endl;
	if (facetlist[0].numberofvertex() == 3){
		meshout << "CELLS " << nfacet << " " << nfacet * 4 << std::endl;
		for (i = 0; i < nfacet; i++){
			meshout << 3 << " " << facetlist[i].node(0) << " " << facetlist[i].node(1) << " " << facetlist[i].node(2) << std::endl;
		}
		meshout << std::endl;
		meshout << "CELL_TYPES " << nfacet << std::endl;
		for (i = 0; i < nfacet; i++){
			meshout << 5 << std::endl; //for triangle
		}

	}

	if (facetlist[0].numberofvertex() == 4){
		meshout << "CELLS " << nfacet << " " << nfacet * 5 << std::endl;
		for (i = 0; i < nfacet; i++){
			meshout << 4 << " " << facetlist[i].node(0) << " " << facetlist[i].node(1) << " " << facetlist[i].node(2) << " " << facetlist[i].node(3) << std::endl;
		}
		meshout << std::endl;
		meshout << "CELL_TYPES " << nfacet << std::endl;
		for (i = 0; i < nfacet; i++){
			meshout << 9 << std::endl; //for quad
		}
	}
	/*
	meshout << "CELL_DATA" << " " << nfacet << std::endl;
	meshout << "SCALARS MARK double" << std::endl;
	meshout << "LOOKUP_TABLE default" << std::endl;
	int mark;
	for (i = 0; i < nfacet; i++){
		mark = 0;
		for (j = 0; j < 3; j++){
			if (facetlist[i].node(j) == 24 || facetlist[i].node(j) == 25 || facetlist[i].node(j) == 2094 || facetlist[i].node(j) == 2095){
				mark = 1;
			}
			if (facetlist[i].node(j) == 387){
				mark = 2;
			}
		}
		meshout << mark << std::endl;
	}
	*/
}

void REGION::triangle_outputVTK(const char* file){
	int i, j;
	std::ofstream meshout;
	meshout.open(file);
	meshout << "# vtk DataFile Version 2.0" << std::endl;
	meshout << "Unstructured Grid" << std::endl;
	meshout << "ASCII" << std::endl;
	meshout << "DATASET UNSTRUCTURED_GRID" << std::endl;
	meshout << "POINTS " << triangleout.numberofpoints << " double" << std::endl;
	for (i = 0; i < triangleout.numberofpoints; i++){
		meshout << triangleout.pointlist[i * 2] << " " << triangleout.pointlist[i * 2 + 1] << " " << 0 << std::endl;
	};
	meshout << std::endl;

	meshout << "CELLS " << triangleout.numberoftriangles << " " << triangleout.numberoftriangles * 4 << std::endl;
	for (i = 0; i < triangleout.numberoftriangles; i++){
		meshout << 3 << " " << triangleout.trianglelist[i * 3] << " " << triangleout.trianglelist[i * 3 + 1] << " " << triangleout.trianglelist[i * 3+2] << std::endl;
	}
	meshout << std::endl;
	meshout << "CELL_TYPES " << triangleout.numberoftriangles << std::endl;
	for (i = 0; i < triangleout.numberoftriangles; i++){
		meshout << 5 << std::endl; //for triangle
	}

}

void REGION::writesurfacemeshPOLY(char* file){
	std::ofstream meshout;
	int i;
	meshout.open(file);
	meshout << "# Part 1 - node list" << std::endl;
	meshout << "# node count, 3 dim, no arribute, with boundary marker" << std::endl;
	meshout << nsurfacenode << " " << 3 << " " << 0 << " " << 1 << std::endl;
	meshout << "# Node index, node coordinates" << std::endl;
	for (i = 0; i < nsurfacenode; i++){
		meshout << i << " " << surfacenodelist[i].x() << " " << surfacenodelist[i].y() << " " << surfacenodelist[i].z() << std::endl;
	}
	meshout << "# Part 2 - facet list" << std::endl;
	meshout << "#facet count, with boundary marker" << std::endl;
	meshout << nfacet << " " << 1 << std::endl;
	meshout << "# facets" << std::endl;
	for (i = 0; i < nfacet; i++){
		meshout << 1 << " " << 0 << " " << facetlist[i].markbsurface()+1 << std::endl; //tetgen (.poly) boundary mark starts from 1
		if (facetlist[i].numberofvertex() == 4){
			meshout << 4 << " " << facetlist[i].node(0) << " " << facetlist[i].node(1) << " " << facetlist[i].node(2) << " " << facetlist[i].node(3) << std::endl;
		}
		if (facetlist[i].numberofvertex() == 3){
			meshout << 3 << " " << facetlist[i].node(0) << " " << facetlist[i].node(1) << " " << facetlist[i].node(2)  << std::endl;
		}
	}
	meshout << "# Part 3 - hole list" << std::endl;
	meshout << 0 << std::endl;
	meshout << "# Part 4 - region list" << std::endl;
	nregion = 0;
	meshout << nregion << std::endl; //no region now
	for (i = 0; i < nregion; i++){
		meshout << i << " " << regionnodelist[i].x() << " " << regionnodelist[i].y() << " " << regionnodelist[i].z() << " " << i << std::endl;
	}

}

void REGION::writepslgPOLY(char* file){
	std::ofstream meshout;
	int i;
	meshout.open(file);
	meshout << "# Part 1 - node list" << std::endl;
	meshout << "# node count, 2 dim, no arribute, no boundary marker" << std::endl;
	meshout << uvnodelist.size() << " " << 2 << " " << 0 << " " << 0 << std::endl;
	meshout << "# Node index, node coordinates" << std::endl;
	for (i = 0; i < uvnodelist.size() ; i++){
		meshout << i+1 << " " << uvnodelist[i].u_current() << " " << uvnodelist[i].v_current() << std::endl;
	}
	meshout << "# Part 2 - segment list" << std::endl;
	meshout << "#facet count, no boundary marker" << std::endl;
	meshout << segmentlist.size() << " " << 0 << std::endl;
	meshout << "# segments" << std::endl;
	for (i = 0; i < segmentlist.size() ; i++){
		meshout << i+1 << " " << segmentlist[i].node(0)+1 << " " << segmentlist[i].node(1)+1 << std::endl;
	}
	meshout << "# Part 3 - hole list" << std::endl;
	meshout << 0 << std::endl;
	meshout << "# Part 4 - region list" << std::endl;
	meshout << 0 << std::endl;
}

void REGION::writepslgvtk(char* file){
	std::ofstream meshout;
	int i;
	meshout.open(file);
	meshout << "# vtk DataFile Version 2.0" << std::endl;
	meshout << "Unstructured Grid" << std::endl;
	meshout << "ASCII" << std::endl;
	meshout << "DATASET UNSTRUCTURED_GRID" << std::endl;
	meshout << "POINTS " << uvnodelist.size() << " double" << std::endl;
	for (i = 0; i < uvnodelist.size(); i++){
		meshout << uvnodelist[i].u_flat() << " " << uvnodelist[i].v_flat()<<" "<<0<< std::endl;
	};
	meshout << std::endl;

	meshout << "CELLS " << segmentlist.size() << " " << segmentlist.size() * 3 << std::endl;
	for (i = 0; i < segmentlist.size(); i++){
		meshout << 2 << " " << segmentlist[i].node(0) << " " << segmentlist[i].node(1) << std::endl;
	}
	meshout << std::endl;
	meshout << "CELL_TYPES " << segmentlist.size() << std::endl;
	for (i = 0; i < segmentlist.size(); i++){
		meshout << 3 << std::endl; //for lines
	}
}

void REGION::writepslgvtkxyz(char* file){
	std::ofstream meshout;
	int i, isurface;
	double x, y, z, u, v;
	meshout.open(file);
	meshout << "# vtk DataFile Version 2.0" << std::endl;
	meshout << "Unstructured Grid" << std::endl;
	meshout << "ASCII" << std::endl;
	meshout << "DATASET UNSTRUCTURED_GRID" << std::endl;
	meshout << "POINTS " << uvnodelist.size() << " double" << std::endl;
	for (i = 0; i < uvnodelist.size(); i++){
		isurface = uvnodelist[i].markoriginsurface();
		u = uvnodelist[i].u_origin();
		v = uvnodelist[i].v_origin();
		x = parasurfacelist[isurface].node3d(u, v).x();
		y = parasurfacelist[isurface].node3d(u, v).y();
		z = parasurfacelist[isurface].node3d(u, v).z();

		meshout << x << " " << y << " " << z << std::endl;
	};
	meshout << std::endl;

	meshout << "CELLS " << segmentlist.size() << " " << segmentlist.size() * 3 << std::endl;
	for (i = 0; i < segmentlist.size(); i++){
		meshout << 2 << " " << segmentlist[i].node(0) << " " << segmentlist[i].node(1) << std::endl;
	}
	meshout << std::endl;
	meshout << "CELL_TYPES " << segmentlist.size() << std::endl;
	for (i = 0; i < segmentlist.size(); i++){
		meshout << 3 << std::endl; //for lines
	}
}

void REGION::readskeleton(const char* inputfile){
	int i, j, k, nparasurface, nu, nv, ninfo, n[4], isurface;
	double x_, y_, z_;
	PARASURFACE parasurface_;
	UVNODE uvnode_;
	std::ifstream input;
	input.open(inputfile);
	input >> nparasurface;
	for (k = 0; k < nparasurface; k++){
		input >> nu >> nv;
		parasurface_.nu(nu);
		parasurface_.nv(nv);
		parasurface_.buildskeleton(); //skeleton of a parasurface is the initial vertex structure
		for (j = 0; j < nv; j++){
			for (i = 0; i < nu; i++){
				input >> x_ >> y_ >> z_;
				uvnode_.x(x_);
				uvnode_.y(y_);
				uvnode_.z(z_);
				parasurface_.adduvnode(uvnode_);
				parasurface_.uvnodematrix(i, j, parasurface_.numberofuvnodes() - 1);
			}
		}
		parasurfacelist.push_back(parasurface_);
		parasurface_.clear();
	}

	//all horizontal surfaces
	for (isurface = 0; isurface < parasurfacelist.size(); isurface++){
		hsurfaceid.push_back(isurface);
	}

	std::cout << "All Sketched Surfaces Imported" << std::endl;

	//compute guideedgelength if resolutiontype==1
	if (meshinfo.resolutiontype() == 1){
		int nu_ = parasurfacelist[0].nu();
		double xmin_ = parasurfacelist[0].uvnode(0).x();
		double xmax_ = parasurfacelist[0].uvnode(nu_ - 1).x();
		double guidelength_ = (xmax_ - xmin_) / meshinfo.resolutionxnumber();
		meshinfo.guideedgelength(guidelength_);
		std::cout << "Guide Edge Length computed for resultion type 1: " << guidelength_<< std::endl;
	}
}

void REGION::writeskeletonVTK_triangle(char* outputfile){ //triangulated skeleton
	int i, j, nn, nn2;
	std::ofstream output;
	output.open(outputfile);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Unstructured Grid" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET UNSTRUCTURED_GRID" << std::endl;

	nn = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		nn = nn + parasurfacelist[i].numberofuvnodes();
	}

	output << "POINTS " << nn << " double" << std::endl;
	for (i = 0; i < parasurfacelist.size(); i++){
		for (j = 0; j < parasurfacelist[i].numberofuvnodes(); j++){
			output << parasurfacelist[i].uvnode(j).x() << " " << parasurfacelist[i].uvnode(j).y() << " " << parasurfacelist[i].uvnode(j).z() << std::endl;
		}
	};
	output << std::endl;

	nn2 = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		nn2 = nn2 + parasurfacelist[i].numberoffacets();
	}

	output << "CELLS " << nn2 << " " << nn2 * 4 << std::endl;
	nn = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		for (j = 0; j < parasurfacelist[i].numberoffacets(); j++){
			output << 3 << " " << parasurfacelist[i].facet(j).node(0)+nn << " " << parasurfacelist[i].facet(j).node(1)+nn << " " << parasurfacelist[i].facet(j).node(2)+nn << std::endl;
		}
		nn = nn + parasurfacelist[i].numberofuvnodes();
	}
	output << std::endl;
	output << "CELL_TYPES " << nn2 << std::endl;
	for (i = 0; i < nn2; i++){
		output << 5 << std::endl;
	}

}

void REGION::writeskeletonVTK_triangle_para(char* outputfile){ //triangulated skeleton in para space
	int i, j, nn, nn2;
	std::ofstream output;
	output.open(outputfile);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Unstructured Grid" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET UNSTRUCTURED_GRID" << std::endl;

	nn = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		nn = nn + parasurfacelist[i].numberofuvnodes();
	}

	output << "POINTS " << nn << " double" << std::endl;
	for (i = 0; i < parasurfacelist.size(); i++){
		for (j = 0; j < parasurfacelist[i].numberofuvnodes(); j++){
			output << parasurfacelist[i].uvnode(j).u_origin() << " " << parasurfacelist[i].uvnode(j).v_origin() << " " << 0 << std::endl;
		}
	};
	output << std::endl;

	nn2 = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		nn2 = nn2 + parasurfacelist[i].numberoffacets();
	}

	output << "CELLS " << nn2 << " " << nn2 * 4 << std::endl;
	nn = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		for (j = 0; j < parasurfacelist[i].numberoffacets(); j++){
			output << 3 << " " << parasurfacelist[i].facet(j).node(0) + nn << " " << parasurfacelist[i].facet(j).node(1) + nn << " " << parasurfacelist[i].facet(j).node(2) + nn << std::endl;
		}
		nn = nn + parasurfacelist[i].numberofuvnodes();
	}
	output << std::endl;
	output << "CELL_TYPES " << nn2 << std::endl;
	for (i = 0; i < nn2; i++){
		output << 5 << std::endl;
	}

}



void REGION::writeskeletonVTK_structured(char* outputfile){
	int i, j, nn, nn2;
	std::ofstream output;
	output.open(outputfile);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Structured Skeleton in 3D" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET STRUCTURED_GRID" << std::endl;
	output << "DIMENSIONS " << parasurfacelist[0].nu() << " " << parasurfacelist[0].nv() << " " << parasurfacelist.size() << std::endl;
	nn = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		nn = nn + parasurfacelist[i].numberofuvnodes();
	}

	output << "POINTS " << nn << " double" << std::endl;
	for (i = 0; i < parasurfacelist.size(); i++){
		for (j = 0; j < parasurfacelist[i].numberofuvnodes(); j++){
			output << parasurfacelist[i].uvnode(j).x() << " " << parasurfacelist[i].uvnode(j).y() << " " << parasurfacelist[i].uvnode(j).z() << std::endl;
		}
	};
	output << std::endl;
}

void REGION::writeskeletonVTK_parametric(char* outputfile){
	int i, j, nn, nn2;
	std::ofstream output;
	output.open(outputfile);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Structured Skeleton in Parametric 2D" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET STRUCTURED_GRID" << std::endl;
	output << "DIMENSIONS " << parasurfacelist[0].nu() << " " << parasurfacelist[0].nv() << " " << parasurfacelist.size() << std::endl;
	nn = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		nn = nn + parasurfacelist[i].numberofuvnodes();
	}

	output << "POINTS " << nn << " double" << std::endl;
	for (i = 0; i < parasurfacelist.size(); i++){
		for (j = 0; j < parasurfacelist[i].numberofuvnodes(); j++){
			output << parasurfacelist[i].uvnode(j).u_origin() << " " << parasurfacelist[i].uvnode(j).v_origin() <<" "<<0<< std::endl;
		}
	};
	output << std::endl;
}

void REGION::writeskeletonVTK_flat(char* outputfile){
	int i, j, nn, nn2;
	std::ofstream output;
	output.open(outputfile);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Structured Skeleton in Parametric 2D" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET STRUCTURED_GRID" << std::endl;
	output << "DIMENSIONS " << parasurfacelist[0].nu() << " " << parasurfacelist[0].nv() << " " << parasurfacelist.size() << std::endl;
	nn = 0;
	for (i = 0; i < parasurfacelist.size(); i++){
		nn = nn + parasurfacelist[i].numberofuvnodes();
	}

	output << "POINTS " << nn << " double" << std::endl;
	for (i = 0; i < parasurfacelist.size(); i++){
		for (j = 0; j < parasurfacelist[i].numberofuvnodes(); j++){
			output << parasurfacelist[i].uvnode(j).u_flat() << " " << parasurfacelist[i].uvnode(j).v_flat()<<" "<<0 << std::endl;
		}
	};
	output << std::endl;
}

void REGION::buildparasurfacefacets_all(){
	int i;
	for (i = 0; i < parasurfacelist.size(); i++){
		parasurfacelist[i].buildfacets();
	}
}

void REGION::para2internalandnoflowbsurface(){
	BSURFACE bsurface_;
	int nhpara = hsurfaceid.size();
	bsurfaceid.resize(6);

	bsurfaceid[0] = hsurfaceid[0]; //lowest
	bsurfaceid[1] = hsurfaceid[nhpara - 1]; //highest
	bsurfaceid[2] = parasurfacelist.size() - 4;
	bsurfaceid[3] = parasurfacelist.size() - 3;
	bsurfaceid[4] = parasurfacelist.size() - 2;
	bsurfaceid[5] = parasurfacelist.size() - 1;
	for (int i = 0; i < parasurfacelist.size(); i++){
		parasurfacelist[i].markbc(0); //all boundary parasurfaces (all boundary surfaces have no-flow condition)
	}
	for (int i = 1; i < hsurfaceid.size() - 1; i++){ //internal horizontal surfaces marked -2
		int ib = hsurfaceid[i];
		parasurfacelist[ib].markbc(-2);
	}

	for (int i = 0; i < parasurfacelist.size(); i++){ //parasurfaces marks to bsurface types (0 or -2)
		bsurface_.type(parasurfacelist[i].markbc()); //default bsurfaces either internal or no-flow
		bsurfacelist.push_back(bsurface_);
	}


}

void REGION::buildverticalskeleton(){//front-back-left-right
	//4 verticals; currently in RRM sketching is in a box window, so the vertical surfaces are bounded by the top and bottom horizontal surfaces
	int i, j, k, nhpara, nu1, nv1, nu, nv, nu2, nv2, isurface, itmp, i1, i2, imin, imax, ib, mark_;
	double x_, y_, z_, uu, vv;
	PARASURFACE parasurface_;
	BSURFACE bsurface_;
	UVNODE uvnode_;
	nhpara = parasurfacelist.size();  //number of horizontal parasurfaces


	i1 = hsurfaceid[0];//lowerst
	i2 = hsurfaceid[nhpara - 1];//highest

	if (parasurfacelist[i1].nu() != parasurfacelist[i2].nu() || parasurfacelist[i1].nv() != parasurfacelist[i2].nv()) {
		std::cout << "bottom and top hsurface different dimension; one with smaller nu recreated" << std::endl;
		if (parasurfacelist[i1].nu() > parasurfacelist[i2].nu()) {
			imin = i2;
			imax = i1;
		}
		else {
			imin = i1;
			imax = i2;
		}
		//recreate imin parasurface
		parasurface_.clear();
		nu1 = parasurfacelist[imax].nu();
		nv1 = parasurfacelist[imax].nv();
		parasurface_.nu(nu1);
		parasurface_.nv(nv1);
		parasurface_.buildskeleton(); //skeleton of a parasurface is the initial vertex structure
		parasurfacelist[imin].skeleton2parametric();//new parasurface based on imin
		parasurfacelist[imin].buildfacets();
		double ddu = 1.0 / (nu1-1);
		double ddv = 1.0 / (nv1-1);
		for (j = 0; j < nv1; j++) {
			for (i = 0; i < nu1; i++) {
				uu = i*ddu;
				vv = j*ddv;
				x_ = parasurfacelist[imin].node3d(uu, vv).x();
				y_ = parasurfacelist[imin].node3d(uu, vv).y();
				z_ = parasurfacelist[imin].node3d(uu, vv).z();
				uvnode_.x(x_);
				uvnode_.y(y_);
				uvnode_.z(z_);
				parasurface_.adduvnode(uvnode_);
				parasurface_.uvnodematrix(i, j, parasurface_.numberofuvnodes() - 1);
			}
		}
		parasurfacelist[imin] = parasurface_; //replace imin with new parasurface
		parasurface_.clear();
	}

	nu1 = parasurfacelist[i1].nu();
	nu2 = parasurfacelist[i1].nv();

	//for front
	parasurface_.nu(nu1);
	parasurface_.nv(2);
	parasurface_.buildskeleton();
	for (i = 0; i < nu1; i++){
		uvnode_ = parasurfacelist[i1].uvnode(i, 0);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 0, parasurface_.numberofuvnodes() - 1);

		uvnode_ = parasurfacelist[i2].uvnode(i, 0);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 1, parasurface_.numberofuvnodes() - 1);
	}
	parasurfacelist.push_back(parasurface_);
	parasurface_.clear();
	//for back
	parasurface_.nu(nu1);
	parasurface_.nv(2);
	parasurface_.buildskeleton();
	for (i = 0; i < nu1; i++){
		uvnode_ = parasurfacelist[i1].uvnode(i, nu2 - 1);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 0, parasurface_.numberofuvnodes() - 1);

		uvnode_ = parasurfacelist[i2].uvnode(i, nu2 - 1);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 1, parasurface_.numberofuvnodes() - 1);
	}
	parasurfacelist.push_back(parasurface_);
	parasurface_.clear();
	//for left
	parasurface_.nu(nu2);
	parasurface_.nv(2);
	parasurface_.buildskeleton();
	for (i = 0; i < nu2; i++){
		uvnode_ = parasurfacelist[i1].uvnode(0, i);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 0, parasurface_.numberofuvnodes() - 1);

		uvnode_ = parasurfacelist[i2].uvnode(0, i);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 1, parasurface_.numberofuvnodes() - 1);
	}
	parasurfacelist.push_back(parasurface_);
	parasurface_.clear();
	//for right
	parasurface_.nu(nu2);
	parasurface_.nv(2);
	parasurface_.buildskeleton();
	for (i = 0; i < nu2; i++){
		uvnode_ = parasurfacelist[i1].uvnode(nu1 - 1, i);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 0, parasurface_.numberofuvnodes() - 1);

		uvnode_ = parasurfacelist[i2].uvnode(nu1 - 1, i);
		parasurface_.adduvnode(uvnode_);
		parasurface_.uvnodematrix(i, 1, parasurface_.numberofuvnodes() - 1);
	}
	parasurfacelist.push_back(parasurface_);
	parasurface_.clear();

	std::cout << "Vertical Bounding Surfaces Built" << std::endl;
}
void REGION::flowb2bsurface(){
	//implement flowbsurfaces
	int i, mark_;
	for (i = 0; i < flowbsurfacelist.size(); i++){//for outer boundary bot-top-front-back-left-right is 0 1 2 3 4 5
		if (flowbsurfacemarklist[i] == 0){ //bot
			mark_ = hsurfaceid[0];
		}
		else if (flowbsurfacemarklist[i] == 1){//top
			mark_ = hsurfaceid[hsurfaceid.size() - 1];
		}
		else if (flowbsurfacemarklist[i] == 2){//front
			mark_ = parasurfacelist.size() - 4;
		}
		else if (flowbsurfacemarklist[i] == 3){//back
			mark_ = parasurfacelist.size() - 3;
		}
		else if (flowbsurfacemarklist[i] == 4){//left
			mark_ = parasurfacelist.size() - 2;
		}
		else if (flowbsurfacemarklist[i] == 5){//right
			mark_ = parasurfacelist.size() - 1;
		}
		bsurfacelist[mark_] = flowbsurfacelist[i];
	}
}
void REGION::skeleton2parametric_all(){//when converting, length scale is preserved so that mesh resolution can be preserved
	int k;
	for (k = 0; k < parasurfacelist.size(); k++){
		parasurfacelist[k].skeleton2parametric();
		parasurfacelist[k].skeleton2flat();
	}
	std::cout << "Parametric Surfaces Built" << std::endl;
}

void REGION::buildtrisurfacemesh(){//no multiple intersecting surfaces
	int isurface, ineighbor, iline, jline, i, j, k, jsurface, ksurface, jdir, iu, iv, icurve, nele1, i1, i2, i3, i4, nsegment, npslguvnode, npslgbuvnode, nmodelsurface;
	double dd, ddm, ddr, u, v, u1, u2, v1, v2, nn, x1, y1, z1, x2, y2, z2, x, y, z, length, minlength, minlength2, weight1, weight2, area, area1, area2, area3, area4, aa;
	int kdir[2], asurface, minlengthiu, minlengthiv, minlengthiu2, minlengthiv2, iend, adir, nnm, nnr, itemp, msurface, idir, inn;
	double garea, x3, y3, z3, weight3, weight4, ustart, vstart, uend, vend, length_, length2d_, dsec;
	int nc1, nc2, ncornernodes_local, jnode, inode;
	double paralength_;
	NODE node_;
	FACET facet_;
	UVNODE point3d_, point3d1_, point3d2_;
	SEGMENT segment_;
	UVNODE uvnode_;
	UVLINE uvline_;
	std::vector<UVLINE> uvlinelist;
	std::vector<MODELSURFACE> modelsurfacelist;
	std::vector<double> vec1, vec2, uvvec;
	std::vector<int> linenodes, nodelistlocal, cornernodes, cornernodes_local, markreplace, markreplacenumber, endnodelist; //ordered nodes
	std::vector<std::vector<int>> facetsunode;
	char* trianglecommand2;

	vec1.resize(3);
	vec2.resize(3);
	uvvec.resize(2);
	surfacenodelist.clear();
	facetlist.clear();
	nsurfacenode = 0;
	nmodelsurface = parasurfacelist.size(); //total number of surfaces in the geometry
	modelsurfacelist.resize(nmodelsurface);

	//firstly identify whether each surface has internal neighbor and record them
	for (i = 1; i < hsurfaceid.size() - 1; i++){ // the last 4 surfaces are vertical; the first and last hsurfaces are not internal***
		isurface = hsurfaceid[i];
		jsurface = parasurfacelist[isurface].neighbor(0);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(1);
		}

		jsurface = parasurfacelist[isurface].neighbor(1);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(2);
		}

		jsurface = parasurfacelist[isurface].neighbor(2);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(3);
		}

		jsurface = parasurfacelist[isurface].neighbor(3);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(4);
		}
	}

	garea = 1.0*meshinfo.guideedgelength()*meshinfo.guideedgelength();
	tol_ = meshinfo.guideedgelength() / 20;
	std::string c1(trianglecommand);
	std::string c2 = std::to_string(garea);
	c1 = c1 + c2;
	const char* cc = c1.c_str();
	trianglecommand2 = const_cast<char*>(cc);

	//then for each surface generate triangular mesh
	for (msurface = 0; msurface < nmodelsurface; msurface++){
		std::cout << "msurface " << msurface << std::endl;
		uvlinelist.clear();
		uvnodelist.clear();
		segmentlist.clear();
		//update trianglecommand locally for each surface for meshing on parametric surface
		//if (resolutiontype_ == 2) {
		//guidensec_ = parasurfacelist[msurface].lengthu(0) / guideedgelength_;
		//}

		//paralength_ = 1.0 / guidensec_;
		//garea = 2.0*paralength_*paralength_;
		//update trianglecommand locally for each surface for meshing on flat surface
		//if (resolutiontype_ == 1) {
		//	guideedgelength_ = parasurfacelist[msurface].lengthu(0) / guidensec_;
		//}
		//guideedgelength_ = 1.0 / guidensec_; //for test one surface para

		//find boundary uv lines first south, north, west, east; so uvlinelist[0-3] are boundary
		jsurface = parasurfacelist[msurface].neighbor(0);//south neighbor
		uvline_.neighborsurface(jsurface); //for finding neighbor lines
		uvline_.direction(1); //south; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthu(0));
		uvlinelist.push_back(uvline_);

		jsurface = parasurfacelist[msurface].neighbor(1);
		uvline_.neighborsurface(jsurface);
		uvline_.direction(2); //north; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthu(parasurfacelist[msurface].nv() - 1));
		uvlinelist.push_back(uvline_);

		jsurface = parasurfacelist[msurface].neighbor(2);
		uvline_.neighborsurface(jsurface);
		uvline_.direction(3); //west; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthv(0));
		uvlinelist.push_back(uvline_);

		jsurface = parasurfacelist[msurface].neighbor(3);
		uvline_.neighborsurface(jsurface);
		uvline_.direction(4); //east; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthv(parasurfacelist[msurface].nu() - 1));
		uvlinelist.push_back(uvline_);

		//find internal uvlines uvlinelist[4--]
		for (ineighbor = 0; ineighbor < modelsurfacelist[msurface].numberofinternalneighbor(); ineighbor++){
			uvline_.neighborsurface(modelsurfacelist[msurface].internalneighbor(ineighbor));
			uvline_.direction(modelsurfacelist[msurface].internalneighbordir(ineighbor)); //of the neighbor surface
			uvline_.markb(0);
			inn = uvline_.neighborsurface();
			if (uvline_.direction() == 1){
				length_ = parasurfacelist[inn].lengthu(0);
			}
			else if (uvline_.direction() == 2){
				length_ = parasurfacelist[inn].lengthu(parasurfacelist[inn].nv() - 1);
			}
			else if (uvline_.direction() == 3){
				length_ = parasurfacelist[inn].lengthv(0);
			}
			else if (uvline_.direction() == 4){
				length_ = parasurfacelist[inn].lengthv(parasurfacelist[inn].nu() - 1);
			}
			uvline_.length(length_);
			uvlinelist.push_back(uvline_);
		}

		//find internal uvlines' neighboring info
		for (iline = 4; iline < uvlinelist.size(); iline++){
			isurface = uvlinelist[iline].neighborsurface();
			idir = uvlinelist[iline].direction();
			if (idir == 1 || idir == 2){
				kdir[0] = 3;
				kdir[1] = 4;
			}
			else{
				kdir[0] = 1;
				kdir[1] = 2;
			}
			for (j = 0; j < 2; j++){
				if (kdir[j] == 1){
					ksurface = parasurfacelist[isurface].neighbor(0);
				}
				if (kdir[j] == 2){
					ksurface = parasurfacelist[isurface].neighbor(1);
				}
				if (kdir[j] == 3){
					ksurface = parasurfacelist[isurface].neighbor(2);
				}
				if (kdir[j] == 4){
					ksurface = parasurfacelist[isurface].neighbor(3);
				}
				//identify neighbor
				for (jline = 0; jline < uvlinelist.size(); jline++){
					jsurface = uvlinelist[jline].neighborsurface();
					if (jsurface == ksurface){
						uvlinelist[iline].neighborline(j, jline);
					}
				}
			}
		}
		endnodelist.clear();
		//create endnodes for boundary lines
		u = 0;
		v = 0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		u = 1.0;
		v = 0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		u = 0;
		v = 1.0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		u = 1.0;
		v = 1.0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		uvlinelist[0].endnode(0, 0);
		uvlinelist[0].endnode(1, 1);
		uvlinelist[1].endnode(0, 2);
		uvlinelist[1].endnode(1, 3);
		uvlinelist[2].endnode(0, 0);
		uvlinelist[2].endnode(1, 2);
		uvlinelist[3].endnode(0, 1);
		uvlinelist[3].endnode(1, 3);
		for (i = 0; i < 4; i++){
			endnodelist.push_back(i);
		}
		//create endnodes for internal lines && create internal nodes for lines
		for (iline = 4; iline < uvlinelist.size(); iline++){//internal lines start from 4
			isurface = uvlinelist[iline].neighborsurface();
			idir = uvlinelist[iline].direction();

			for (iend = 0; iend < 2; iend++){
				if (iend == 0 && idir == 1){
					u = 0;
					v = 0;
				}
				if (iend == 0 && idir == 2){
					u = 0;
					v = 1.0;
				}
				if (iend == 0 && idir == 3){
					u = 0;
					v = 0;
				}
				if (iend == 0 && idir == 4){
					u = 1.0;
					v = 0;
				}

				if (iend == 1 && idir == 1){
					u = 1.0;
					v = 0;
				}
				if (iend == 1 && idir == 2){
					u = 1.0;
					v = 1.0;
				}
				if (iend == 1 && idir == 3){
					u = 0;
					v = 1.0;
				}
				if (iend == 1 && idir == 4){
					u = 1.0;
					v = 1.0;
				}
				uvnode_.u_origin(u);
				uvnode_.v_origin(v);
				uvnode_.markoriginsurface(isurface);
				//find out current u v and inter u v; each uvnode has just one inter if surface has just one neighbor
				point3d_ = parasurfacelist[isurface].node3d(u, v);
				x = point3d_.x();
				y = point3d_.y();
				z = point3d_.z();
				uvnode_.x(x);
				uvnode_.y(y);
				uvnode_.z(z);
				jline = uvlinelist[iline].neighborline(iend);
				if (jline < 4 && jline >= 0){ //if cross boundary line. Boundary line's originsurface is msurface so inter u v is current
					jdir = jline + 1; //boundary direction
					uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);
					uvnode_.u_current(uvvec[0]);
					uvnode_.u_inter(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					uvnode_.v_inter(uvvec[1]);
					uvnodelist.push_back(uvnode_);
					uvlinelist[iline].endnode(iend, uvnodelist.size() - 1);
					endnodelist.push_back(uvnodelist.size() - 1);
					uvlinelist[jline].addinternalnode(uvnodelist.size() - 1);
				}
				else if (jline >= 4){ //cross internal line
					uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);//current u v are on msurface internal
					uvnode_.u_current(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					//find inter u v
					jsurface = uvlinelist[jline].neighborsurface();
					jdir = uvlinelist[jline].direction();
					uvvec = parasurfacelist[jsurface].uvforxyz(x, y, z);//x y z's position on jsurface boundary
					uvnode_.u_inter(uvvec[0]);
					uvnode_.v_inter(uvvec[1]);
					uvnodelist.push_back(uvnode_);
					uvlinelist[iline].endnode(iend, uvnodelist.size() - 1);
					endnodelist.push_back(uvnodelist.size() - 1);
					uvlinelist[jline].addinternalnode(uvnodelist.size() - 1);
				}
				else{//jline<0 means no neighbor line == connecting line vertex; don't allow multiple lines connect to same inter point at vertical surface
					uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);
					uvnode_.u_current(uvvec[0]);
					uvnode_.u_inter(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					uvnode_.v_inter(uvvec[1]);
					int checking = 0;
					for (i = 0; i < endnodelist.size(); i++){ //checking overlapping endnodes
						int iinode = endnodelist[i];
						x2 = uvnodelist[iinode].x();
						y2 = uvnodelist[iinode].y();
						z2 = uvnodelist[iinode].z();
						if (abs(x - x2) < 0.001 && abs(y - y2) < 0.001 && abs(z - z2) < 0.001){
							uvlinelist[iline].endnode(iend, iinode);
							checking = 1;
							break;
						}
					}
					if (checking == 0){
						uvnodelist.push_back(uvnode_);
						uvlinelist[iline].endnode(iend, uvnodelist.size() - 1);
						endnodelist.push_back(uvnodelist.size() - 1);
					}
				}
			}
		} //end of create endnodes for internal lines && internal nodes

		//build segments on lines based on endnodes'uv_current and internal nodes'uv_inter
		for (iline = 0; iline < uvlinelist.size(); iline++){
			//compute line length in 2D
			if (iline < 4){
				isurface = msurface;
			}
			else{
				isurface = uvlinelist[iline].neighborsurface();
			}
			i1 = uvlinelist[iline].endnode(0);
			i2 = uvlinelist[iline].endnode(1);
			x1 = uvnodelist[i1].x();
			y1 = uvnodelist[i1].y();
			z1 = uvnodelist[i1].z();
			x2 = uvnodelist[i2].x();
			y2 = uvnodelist[i2].y();
			z2 = uvnodelist[i2].z();
			ustart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[0];
			vstart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[1];
			uend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[0];
			vend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[1];
			length2d_ = mathzz_.twonodedistance2D(ustart, vstart, uend, vend);
			ddm = meshinfo.guideedgelength() / uvlinelist[iline].length(); // for paralength = 1.0 which is correct
			idir = uvlinelist[iline].direction();
			linenodes.clear();
			linenodes.push_back(uvlinelist[iline].endnode(0));
			for (i = 0; i < uvlinelist[iline].numberofinternalnode(); i++){
				linenodes.push_back(uvlinelist[iline].internalnode(i));
			}
			linenodes.push_back(uvlinelist[iline].endnode(1));
			//order internal nodes (no enbdnodes) (straight insertion sort) for any dir of iline's neighborsurface (u or v constant)
			for (i = 2; i < linenodes.size() - 1; i++){
				itemp = linenodes[i];
				for (j = i - 1; j > 0 && (uvnodelist[linenodes[j]].u_inter() + uvnodelist[linenodes[j]].v_inter())>(uvnodelist[itemp].u_inter() + uvnodelist[itemp].v_inter()); j--){
					linenodes[j + 1] = linenodes[j];
				}
				linenodes[j + 1] = itemp;
			}
			//build segments in intervals
			for (i = 0; i < linenodes.size() - 1; i++){
				//std::cout << "i " << i << std::endl;
				nodelistlocal.clear();
				i1 = linenodes[i];
				i2 = linenodes[i + 1];
				x1 = uvnodelist[i1].x();
				y1 = uvnodelist[i1].y();
				z1 = uvnodelist[i1].z();
				x2 = uvnodelist[i2].x();
				y2 = uvnodelist[i2].y();
				z2 = uvnodelist[i2].z();
				ustart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[0];
				vstart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[1];
				uend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[0];
				vend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[1];
				dsec = mathzz_.twonodedistance2D(ustart, vstart, uend, vend);
				nnr = int(dsec / ddm + 0.5);//number of segments in this interval Round
				if (nnr == 0){
					nnr = 1;
				}
				else if (nnr < 0){
					std::cout << "nnr<0" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				ddr = dsec / nnr;//local segment length
				nodelistlocal.resize(nnr + 1);
				nodelistlocal[0] = i1;
				nodelistlocal[nodelistlocal.size() - 1] = i2;
				//build interval's internal segments' nodes
				for (j = 1; j < nnr; j++){//number of internal nodes = number of segments - 1
					//std::cout << "j " << j << std::endl;
					uvnode_.markoriginsurface(isurface);
					if (idir == 1){
						u = j*ddr + ustart;
						v = 0;
					}
					if (idir == 2){
						u = j*ddr + ustart;
						v = 1.0;
					}
					if (idir == 3){
						u = 0;
						v = j*ddr + vstart;
					}
					if (idir == 4){
						v = j*ddr + vstart;
						u = 1.0;
					}
					uvnode_.u_origin(u);
					uvnode_.v_origin(v);
					point3d_ = parasurfacelist[isurface].node3d(u, v);
					x = point3d_.x();
					y = point3d_.y();
					z = point3d_.z();
					uvnode_.x(x);
					uvnode_.y(y);
					uvnode_.z(z);
					//find out current u v for internal nodes
					if (iline < 4){
						uvvec[0] = u;
						uvvec[1] = v;
					}
					else{
						uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);
					}
					uvnode_.u_current(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					uvnodelist.push_back(uvnode_);
					nodelistlocal[j] = uvnodelist.size() - 1;
				}
				//build segments
				for (j = 0; j < nnr; j++){
					segment_.node(0, nodelistlocal[j]);
					segment_.node(1, nodelistlocal[j + 1]);
					segmentlist.push_back(segment_);
				}
			}
		}

		//use triangle to triangulate uv plane msurface
		trianglein.numberofpoints = uvnodelist.size();
		trianglein.pointlist = new REAL[trianglein.numberofpoints * 2];
		trianglein.pointmarkerlist = NULL;
		trianglein.numberofpointattributes = 0;
		trianglein.numberofholes = 0;
		trianglein.numberofregions = 0;
		trianglein.numberofsegments = segmentlist.size();
		trianglein.segmentlist = new int[trianglein.numberofsegments * 2];
		trianglein.segmentmarkerlist = NULL;
		for (i = 0; i < uvnodelist.size(); i++){
			//trianglein.pointlist[i * 2] = uvnodelist[i].u_current();
			//trianglein.pointlist[i * 2 + 1] = uvnodelist[i].v_current();
			x1 = uvnodelist[i].x();
			y1 = uvnodelist[i].y();
			z1 = uvnodelist[i].z();
			u1 = parasurfacelist[msurface].flatuvforxyz(x1, y1, z1)[0];//for RRM
			v1 = parasurfacelist[msurface].flatuvforxyz(x1, y1, z1)[1];
			//u1 = parasurfacelist[msurface].uvforxyz(x1, y1, z1)[0];//for test one surface para
			//v1 = parasurfacelist[msurface].uvforxyz(x1, y1, z1)[1];
			trianglein.pointlist[i * 2] = u1;
			trianglein.pointlist[i * 2 + 1] = v1;
		}
		for (i = 0; i < segmentlist.size(); i++){
			trianglein.segmentlist[i * 2] = segmentlist[i].node(0);
			trianglein.segmentlist[i * 2 + 1] = segmentlist[i].node(1);
		}
		//initialising output array
		triangleout.pointlist = (REAL *)NULL;            //Not needed if -N switch used.
		//Not needed if -N switch used or number of point attributes is zero:
		triangleout.pointattributelist = (REAL *)NULL;
		triangleout.pointmarkerlist = (int *)NULL; //Not needed if -N or -B switch used.
		triangleout.trianglelist = (int *)NULL;          //Not needed if -E switch used.
		//Not needed if -E switch used or number of triangle attributes is zero:
		triangleout.triangleattributelist = (REAL *)NULL;
		triangleout.neighborlist = (int *)NULL;         //Needed only if -n switch used.
		// Needed only if segments are output (-p or -c) and -P not used:
		triangleout.segmentlist = (int *)NULL;
		// Needed only if segments are output (-p or -c) and -P and -B not used:
		triangleout.segmentmarkerlist = (int *)NULL;
		triangleout.edgelist = (int *)NULL;             //Needed only if -e switch used.
		triangleout.edgemarkerlist = (int *)NULL;   // Needed if -e used and -B not used.
		//if (msurface == 5){
		//writepslgvtk("pslg5.vtk");
		//writepslgvtkxyz("pslgxyz5.vtk");
		//}
		//triangulate("pqa0.002", &trianglein, &triangleout, &trianglevor);
		triangulate(trianglecommand2, &trianglein, &triangleout, &trianglevor);

		//pass triangleout to local node and facet lists
		surfacenodelist_local.clear();
		facetlist_local.clear();
		for (i = 0; i < triangleout.numberofpoints; i++){
			//std::cout << msurface << " " << i << std::endl;
			if (i < uvnodelist.size()){
				//u = uvnodelist[i].u_origin(); //more accurate
				//v = uvnodelist[i].v_origin();
				//isurface = uvnodelist[i].markoriginsurface();
				x = uvnodelist[i].x(); //more accurate
				y = uvnodelist[i].y();
				z = uvnodelist[i].z();
			}
			else{
				u = triangleout.pointlist[i * 2];
				v = triangleout.pointlist[i * 2 + 1];
				point3d_ = parasurfacelist[msurface].node3d_fromflat(u, v);
				//point3d_ = parasurfacelist[msurface].node3d(u, v); //for test one surface para
				x = point3d_.x();
				y = point3d_.y();
				z = point3d_.z();
			}
			node_.markbsurface(msurface);
			node_.x(x);
			node_.y(y);
			node_.z(z);
			surfacenodelist_local.push_back(node_);
		}
		for (i = 0; i < triangleout.numberoftriangles; i++){
			facet_.markbsurface(msurface);
			facet_.numberofvertex(3);
			facet_.node(0, triangleout.trianglelist[i * 3]);
			facet_.node(1, triangleout.trianglelist[i * 3 + 1]);
			facet_.node(2, triangleout.trianglelist[i * 3 + 2]);
			facetlist_local.push_back(facet_);
		}
		//check overlapping nodes
		ncornernodes_local = uvnodelist.size();
		cornernodes_local.clear();
		markreplace.clear();
		markreplacenumber.clear();
		markreplace.resize(surfacenodelist_local.size());
		for (i = 0; i < markreplace.size(); i++){
			markreplace[i] = -1;
		}
		markreplacenumber.resize(surfacenodelist_local.size());
		if (msurface == 0){
			for (i = 0; i < ncornernodes_local; i++){
				cornernodes.push_back(i); //for msurface=0, local and global numbering are the same
			}
		}

		if (msurface > 0){
			for (i = 0; i < ncornernodes_local; i++){
				cornernodes_local.push_back(i);
			}
			//create facets surrounding nodes list locally for all nodes on this surface
			facetsunode.clear();
			facetsunode.resize(triangleout.numberofpoints);
			for (i = 0; i < facetlist_local.size(); i++){
				for (j = 0; j < 3; j++){
					facetsunode[facetlist_local[i].node(j)].push_back(i);
				}
			}
			//first check then update cornercodes by comparing local cornernodes with all other cornernodes
			for (i = 0; i < ncornernodes_local; i++){
				for (j = 0; j < cornernodes.size(); j++){
					jnode = cornernodes[j];
					double tol = 1.0;
					if (abs(surfacenodelist_local[i].x() - surfacenodelist[jnode].x()) < tol && abs(surfacenodelist_local[i].y() - surfacenodelist[jnode].y()) < tol && abs(surfacenodelist_local[i].z() - surfacenodelist[jnode].z()) < tol){
						markreplace[i] = jnode;
						for (k = i + 1; k < surfacenodelist_local.size(); k++){
							markreplacenumber[k]++;
						}
					}
				}
			}
			//update cornernodes here so don't compare with self-surface
			for (i = 0; i < ncornernodes_local; i++){
				if (markreplace[i] < 0){
					cornernodes.push_back(i - markreplacenumber[i] + nsurfacenode);
				}
			}
		}
		//compare new triangle generated nodes with local corner points
		for (i = ncornernodes_local; i < surfacenodelist_local.size(); i++){
			x1 = surfacenodelist_local[i].x();
			y1 = surfacenodelist_local[i].y();
			z1 = surfacenodelist_local[i].z();
			for (j = 0; j < i; j++){//compare all nodes; low to high
				x2 = surfacenodelist_local[j].x();
				y2 = surfacenodelist_local[j].y();
				z2 = surfacenodelist_local[j].z();
				double tol = tol_; //for test
				if (std::abs(x1 - x2) < tol && std::abs(y1 - y2) < tol && std::abs(z1 - z2) < tol){
					if (markreplace[j] >= 0){
						markreplace[i] = markreplace[j];
					}
					else{
						markreplace[i] = j + nsurfacenode - markreplacenumber[j];
					}
					for (k = i + 1; k < surfacenodelist_local.size(); k++){
						markreplacenumber[k]++; //no update for k<=ncornernodes_local
					}
					break;
				}
			}
		}

		//pass local list to region "triangle2surfacemesh"
		for (i = 0; i < surfacenodelist_local.size(); i++){
			if (markreplace[i] == -1){ //new nodes
				/*if (i < uvnodelist.size()){
				u = uvnodelist[i].u_origin();
				v = uvnodelist[i].v_origin();
				isurface = uvnodelist[i].markoriginsurface();
				}
				else{
				u = triangleout.pointlist[i * 2];
				v = triangleout.pointlist[i * 2 + 1];
				isurface = msurface;
				}
				point3d_ = parasurfacelist[isurface].node3d_fromflat(u, v);
				x = point3d_.x();
				y = point3d_.y();
				z = point3d_.z();
				node_.markbsurface(msurface);
				node_.x(x);
				node_.y(y);
				node_.z(z);*/
				surfacenodelist.push_back(surfacenodelist_local[i]);
			}
		}
		for (i = 0; i < facetlist_local.size(); i++){ //connectivity update
			facet_.markbsurface(msurface);
			facet_.numberofvertex(3);
			for (j = 0; j < 3; j++){
				inode = facetlist_local[i].node(j);
				if (markreplace[inode] >= 0){
					jnode = markreplace[inode];
				}
				else{
					jnode = inode - markreplacenumber[inode] + nsurfacenode;
				}
				facet_.node(j, jnode);
			}
			facetlist.push_back(facet_);
		}
		nfacet = facetlist.size();

		//record segment (
		for (i = 0; i < segmentlist.size(); i++){
			for (j = 0; j < 2; j++){
				inode = segmentlist[i].node(j);
				if (markreplace[inode] >= 0){
					jnode = markreplace[inode];
				}
				else{
					jnode = inode - markreplacenumber[inode] + nsurfacenode;
				}
				segment_.node(j, jnode);
			}
			segmentlist_all.push_back(segment_);
		}

		nsurfacenode = surfacenodelist.size();
		std::cout << "for msurface " << msurface << " nsurfacenode is " << nsurfacenode << std::endl;
	}
}


void REGION::buildtriangularsurfacemesh(){//no multiple intersecting surfaces
	int isurface, ineighbor, iline, jline, i, j, k, jsurface, ksurface, jdir, iu, iv, icurve, nele1, i1, i2, i3, i4, nsegment, npslguvnode, npslgbuvnode, nmodelsurface;
	double dd, ddm, ddr, u, v, u1, u2, v1, v2, nn, x1, y1, z1, x2, y2, z2, x, y, z, length, minlength, minlength2, weight1, weight2, area, area1, area2, area3, area4, aa;
	int kdir[2], asurface, minlengthiu, minlengthiv, minlengthiu2, minlengthiv2, iend, adir, nnm, nnr, itemp, msurface, idir, inn;
	double garea, x3, y3, z3, weight3, weight4, ustart, vstart, uend, vend, length_, length2d_, dsec;
	int nc1, nc2, ncornernodes_local, jnode, inode;
	double paralength_;
	NODE node_;
	FACET facet_;
	UVNODE point3d_, point3d1_, point3d2_;
	SEGMENT segment_;
	UVNODE uvnode_;
	UVLINE uvline_;
	std::vector<UVLINE> uvlinelist;
	std::vector<MODELSURFACE> modelsurfacelist;
	std::vector<double> vec1, vec2, uvvec;
	std::vector<int> linenodes, nodelistlocal, cornernodes, cornernodes_local, markreplace, markreplacenumber, endnodelist; //ordered nodes
	std::vector<std::vector<int>> facetsunode;
	char* trianglecommand2;

	vec1.resize(3);
	vec2.resize(3);
	uvvec.resize(2);
	surfacenodelist.clear();
	facetlist.clear();
	nsurfacenode = 0;
	nmodelsurface = parasurfacelist.size(); //total number of surfaces in the geometry
	modelsurfacelist.resize(nmodelsurface);

	//firstly identify whether each surface has internal neighbor and record them
	for (i = 1; i < hsurfaceid.size() - 1; i++){ // the last 4 surfaces are vertical; the first and last hsurfaces are not internal***
		isurface = hsurfaceid[i];
		jsurface = parasurfacelist[isurface].neighbor(0);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(1);
		}

		jsurface = parasurfacelist[isurface].neighbor(1);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(2);
		}

		jsurface = parasurfacelist[isurface].neighbor(2);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(3);
		}

		jsurface = parasurfacelist[isurface].neighbor(3);
		if (jsurface >= 0){
			modelsurfacelist[jsurface].addinternalneighbor(isurface);
			modelsurfacelist[jsurface].addinternalneighbordir(4);
		}
	}

	garea = 1.0*meshinfo.guideedgelength()*meshinfo.guideedgelength();
	tol_ = meshinfo.guideedgelength() / 20;
	std::string c1(trianglecommand);
	std::string c2 = std::to_string(garea);
	c1 = c1 + c2;
	const char* cc = c1.c_str();
	trianglecommand2 = const_cast<char*>(cc);

	//then for each surface generate triangular mesh
	for (msurface = 0; msurface < nmodelsurface; msurface++){
		std::cout << "msurface " << msurface << std::endl;
		uvlinelist.clear();
		uvnodelist.clear();
		segmentlist.clear();
		//update trianglecommand locally for each surface for meshing on parametric surface
		//if (resolutiontype_ == 2) {
			//guidensec_ = parasurfacelist[msurface].lengthu(0) / guideedgelength_;
		//}

		//paralength_ = 1.0 / guidensec_;
		//garea = 2.0*paralength_*paralength_;
		//update trianglecommand locally for each surface for meshing on flat surface
		//if (resolutiontype_ == 1) {
		//	guideedgelength_ = parasurfacelist[msurface].lengthu(0) / guidensec_;
		//}
		//guideedgelength_ = 1.0 / guidensec_; //for test one surface para

		//find boundary uv lines first south, north, west, east; so uvlinelist[0-3] are boundary
		jsurface = parasurfacelist[msurface].neighbor(0);//south neighbor
		uvline_.neighborsurface(jsurface); //for finding neighbor lines
		uvline_.direction(1); //south; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthu(0));
		uvlinelist.push_back(uvline_);

		jsurface = parasurfacelist[msurface].neighbor(1);
		uvline_.neighborsurface(jsurface);
		uvline_.direction(2); //north; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthu(parasurfacelist[msurface].nv() - 1));
		uvlinelist.push_back(uvline_);

		jsurface = parasurfacelist[msurface].neighbor(2);
		uvline_.neighborsurface(jsurface);
		uvline_.direction(3); //west; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthv(0));
		uvlinelist.push_back(uvline_);

		jsurface = parasurfacelist[msurface].neighbor(3);
		uvline_.neighborsurface(jsurface);
		uvline_.direction(4); //east; this direction is of the current surface
		uvline_.markb(1);
		uvline_.length(parasurfacelist[msurface].lengthv(parasurfacelist[msurface].nu() - 1));
		uvlinelist.push_back(uvline_);

		//find internal uvlines uvlinelist[4--]
		for (ineighbor = 0; ineighbor < modelsurfacelist[msurface].numberofinternalneighbor(); ineighbor++){
			uvline_.neighborsurface(modelsurfacelist[msurface].internalneighbor(ineighbor));
			uvline_.direction(modelsurfacelist[msurface].internalneighbordir(ineighbor)); //of the neighbor surface
			uvline_.markb(0);
			inn = uvline_.neighborsurface();
			if (uvline_.direction() == 1){
				length_ = parasurfacelist[inn].lengthu(0);
			}
			else if (uvline_.direction() == 2){
				length_ = parasurfacelist[inn].lengthu(parasurfacelist[inn].nv() - 1);
			}
			else if (uvline_.direction() == 3){
				length_ = parasurfacelist[inn].lengthv(0);
			}
			else if (uvline_.direction() == 4){
				length_ = parasurfacelist[inn].lengthv(parasurfacelist[inn].nu() - 1);
			}
			uvline_.length(length_);
			uvlinelist.push_back(uvline_);
		}

		//find internal uvlines' neighboring info
		for (iline = 4; iline < uvlinelist.size(); iline++){
			isurface = uvlinelist[iline].neighborsurface();
			idir = uvlinelist[iline].direction();
			if (idir == 1 || idir == 2){
				kdir[0] = 3;
				kdir[1] = 4;
			}
			else{
				kdir[0] = 1;
				kdir[1] = 2;
			}
			for (j = 0; j < 2; j++){
				if (kdir[j] == 1){
					ksurface = parasurfacelist[isurface].neighbor(0);
				}
				if (kdir[j] == 2){
					ksurface = parasurfacelist[isurface].neighbor(1);
				}
				if (kdir[j] == 3){
					ksurface = parasurfacelist[isurface].neighbor(2);
				}
				if (kdir[j] == 4){
					ksurface = parasurfacelist[isurface].neighbor(3);
				}
				//identify neighbor
				for (jline = 0; jline < uvlinelist.size(); jline++){
					jsurface = uvlinelist[jline].neighborsurface();
					if (jsurface == ksurface){
						uvlinelist[iline].neighborline(j, jline);
					}
				}
			}
		}
		endnodelist.clear();
		//create endnodes for boundary lines
		u = 0;
		v = 0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		u = 1.0;
		v = 0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		u = 0;
		v = 1.0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		u = 1.0;
		v = 1.0;
		uvnode_.markoriginsurface(msurface);
		uvnode_.u_current(u);
		uvnode_.v_current(v);
		uvnode_.u_origin(u);
		uvnode_.v_origin(v);
		uvnode_.u_inter(u);
		uvnode_.v_inter(v);
		point3d_ = parasurfacelist[msurface].node3d(u, v);
		x = point3d_.x();
		y = point3d_.y();
		z = point3d_.z();
		uvnode_.x(x);
		uvnode_.y(y);
		uvnode_.z(z);
		uvnodelist.push_back(uvnode_);
		uvlinelist[0].endnode(0, 0);
		uvlinelist[0].endnode(1, 1);
		uvlinelist[1].endnode(0, 2);
		uvlinelist[1].endnode(1, 3);
		uvlinelist[2].endnode(0, 0);
		uvlinelist[2].endnode(1, 2);
		uvlinelist[3].endnode(0, 1);
		uvlinelist[3].endnode(1, 3);
		for (i = 0; i < 4; i++){
			endnodelist.push_back(i);
		}
		//create endnodes for internal lines && create internal nodes for lines
		for (iline = 4; iline < uvlinelist.size(); iline++){//internal lines start from 4
			isurface = uvlinelist[iline].neighborsurface();
			idir = uvlinelist[iline].direction();

			for (iend = 0; iend < 2; iend++){
				if (iend == 0 && idir == 1){
					u = 0;
					v = 0;
				}
				if (iend == 0 && idir == 2){
					u = 0;
					v = 1.0;
				}
				if (iend == 0 && idir == 3){
					u = 0;
					v = 0;
				}
				if (iend == 0 && idir == 4){
					u = 1.0;
					v = 0;
				}

				if (iend == 1 && idir == 1){
					u = 1.0;
					v = 0;
				}
				if (iend == 1 && idir == 2){
					u = 1.0;
					v = 1.0;
				}
				if (iend == 1 && idir == 3){
					u = 0;
					v = 1.0;
				}
				if (iend == 1 && idir == 4){
					u = 1.0;
					v = 1.0;
				}
				uvnode_.u_origin(u);
				uvnode_.v_origin(v);
				uvnode_.markoriginsurface(isurface);
				//find out current u v and inter u v; each uvnode has just one inter if surface has just one neighbor
				point3d_ = parasurfacelist[isurface].node3d(u, v);
				x = point3d_.x();
				y = point3d_.y();
				z = point3d_.z();
				uvnode_.x(x);
				uvnode_.y(y);
				uvnode_.z(z);
				jline = uvlinelist[iline].neighborline(iend);
				if (jline < 4 && jline >= 0){ //if cross boundary line. Boundary line's originsurface is msurface so inter u v is current
					jdir = jline + 1; //boundary direction
					uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);
					uvnode_.u_current(uvvec[0]);
					uvnode_.u_inter(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					uvnode_.v_inter(uvvec[1]);
					uvnodelist.push_back(uvnode_);
					uvlinelist[iline].endnode(iend, uvnodelist.size() - 1);
					endnodelist.push_back(uvnodelist.size() - 1);
					uvlinelist[jline].addinternalnode(uvnodelist.size() - 1);
				}
				else if (jline >= 4){ //cross internal line
					uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);//current u v are on msurface internal
					uvnode_.u_current(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					//find inter u v
					jsurface = uvlinelist[jline].neighborsurface();
					jdir = uvlinelist[jline].direction();
					uvvec = parasurfacelist[jsurface].uvforxyz(x, y, z);//x y z's position on jsurface boundary
					uvnode_.u_inter(uvvec[0]);
					uvnode_.v_inter(uvvec[1]);
					uvnodelist.push_back(uvnode_);
					uvlinelist[iline].endnode(iend, uvnodelist.size() - 1);
					endnodelist.push_back(uvnodelist.size() - 1);
					uvlinelist[jline].addinternalnode(uvnodelist.size() - 1);
				}
				else{//jline<0 means no neighbor line == connecting line vertex; don't allow multiple lines connect to same inter point at vertical surface
					uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);
					uvnode_.u_current(uvvec[0]);
					uvnode_.u_inter(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					uvnode_.v_inter(uvvec[1]);
					int checking = 0;
					for (i = 0; i < endnodelist.size(); i++){ //checking overlapping endnodes
						int iinode = endnodelist[i];
						x2 = uvnodelist[iinode].x();
						y2 = uvnodelist[iinode].y();
						z2 = uvnodelist[iinode].z();
						if (abs(x - x2) < 0.001 && abs(y - y2) < 0.001 && abs(z - z2) < 0.001){
							uvlinelist[iline].endnode(iend, iinode);
							checking = 1;
							break;
						}
					}
					if (checking == 0){
						uvnodelist.push_back(uvnode_);
						uvlinelist[iline].endnode(iend, uvnodelist.size() - 1);
						endnodelist.push_back(uvnodelist.size() - 1);
					}
				}
			}
		} //end of create endnodes for internal lines && internal nodes

		//build segments on lines based on endnodes'uv_current and internal nodes'uv_inter
		for (iline = 0; iline < uvlinelist.size(); iline++){
			//compute line length in 2D
			if (iline < 4){
				isurface = msurface;
			}
			else{
				isurface = uvlinelist[iline].neighborsurface();
			}
			i1 = uvlinelist[iline].endnode(0);
			i2 = uvlinelist[iline].endnode(1);
			x1 = uvnodelist[i1].x();
			y1 = uvnodelist[i1].y();
			z1 = uvnodelist[i1].z();
			x2 = uvnodelist[i2].x();
			y2 = uvnodelist[i2].y();
			z2 = uvnodelist[i2].z();
			ustart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[0];
			vstart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[1];
			uend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[0];
			vend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[1];
			length2d_ = mathzz_.twonodedistance2D(ustart, vstart, uend, vend);
			ddm = meshinfo.guideedgelength()/uvlinelist[iline].length(); // for paralength = 1.0 which is correct
			idir = uvlinelist[iline].direction();
			linenodes.clear();
			linenodes.push_back(uvlinelist[iline].endnode(0));
			for (i = 0; i < uvlinelist[iline].numberofinternalnode(); i++){
				linenodes.push_back(uvlinelist[iline].internalnode(i));
			}
			linenodes.push_back(uvlinelist[iline].endnode(1));
			//order internal nodes (no endnodes) (straight insertion sort) for any dir of iline's neighborsurface (u or v constant)
			for (i = 2; i < linenodes.size() - 1; i++){
				itemp = linenodes[i];
				for (j = i - 1; j > 0 && (uvnodelist[linenodes[j]].u_inter() + uvnodelist[linenodes[j]].v_inter())>(uvnodelist[itemp].u_inter() + uvnodelist[itemp].v_inter()); j--){
					linenodes[j + 1] = linenodes[j];
				}
				linenodes[j + 1] = itemp;
			}
			//build segments in intervals
			for (i = 0; i < linenodes.size() - 1; i++){
				//std::cout << "i " << i << std::endl;
				nodelistlocal.clear();
				i1 = linenodes[i];
				i2 = linenodes[i + 1];
				x1 = uvnodelist[i1].x();
				y1 = uvnodelist[i1].y();
				z1 = uvnodelist[i1].z();
				x2 = uvnodelist[i2].x();
				y2 = uvnodelist[i2].y();
				z2 = uvnodelist[i2].z();
				ustart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[0];
				vstart = parasurfacelist[isurface].uvforxyz(x1, y1, z1)[1];
				uend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[0];
				vend = parasurfacelist[isurface].uvforxyz(x2, y2, z2)[1];
				dsec = mathzz_.twonodedistance2D(ustart, vstart, uend, vend);
				nnr = int(dsec / ddm + 0.5);//number of segments in this interval Round
				if (nnr == 0){
					nnr = 1;
				}
				else if (nnr < 0){
					std::cout << "nnr<0" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				ddr = dsec / nnr;//local segment length
				nodelistlocal.resize(nnr + 1);
				nodelistlocal[0] = i1;
				nodelistlocal[nodelistlocal.size() - 1] = i2;
				//build interval's internal segments' nodes
				for (j = 1; j < nnr; j++){//number of internal nodes = number of segments - 1
					//std::cout << "j " << j << std::endl;
					uvnode_.markoriginsurface(isurface);
					if (idir == 1){
						u = j*ddr + ustart;
						v = 0;
					}
					if (idir == 2){
						u = j*ddr + ustart;
						v = 1.0;
					}
					if (idir == 3){
						u = 0;
						v = j*ddr + vstart;
					}
					if (idir == 4){
						v = j*ddr + vstart;
						u = 1.0;
					}
					uvnode_.u_origin(u);
					uvnode_.v_origin(v);
					point3d_ = parasurfacelist[isurface].node3d(u, v);
					x = point3d_.x();
					y = point3d_.y();
					z = point3d_.z();
					uvnode_.x(x);
					uvnode_.y(y);
					uvnode_.z(z);
					//find out current u v for internal nodes
					if (iline < 4){
						uvvec[0] = u;
						uvvec[1] = v;
					}
					else{
						uvvec = parasurfacelist[msurface].uvforxyz(x, y, z);
					}
					uvnode_.u_current(uvvec[0]);
					uvnode_.v_current(uvvec[1]);
					uvnodelist.push_back(uvnode_);
					nodelistlocal[j] = uvnodelist.size() - 1;
				}
				//build segments
				for (j = 0; j < nnr; j++){
					segment_.node(0, nodelistlocal[j]);
					segment_.node(1, nodelistlocal[j + 1]);
					segmentlist.push_back(segment_);
				}
			}
		}

		//use triangle to triangulate uv plane msurface
		trianglein.numberofpoints = uvnodelist.size();
		trianglein.pointlist = new REAL[trianglein.numberofpoints * 2];
		trianglein.pointmarkerlist = NULL;
		trianglein.numberofpointattributes = 0;
		trianglein.numberofholes = 0;
		trianglein.numberofregions = 0;
		trianglein.numberofsegments = segmentlist.size();
		trianglein.segmentlist = new int[trianglein.numberofsegments * 2];
		trianglein.segmentmarkerlist = NULL;
		for (i = 0; i < uvnodelist.size(); i++){
			//trianglein.pointlist[i * 2] = uvnodelist[i].u_current();
			//trianglein.pointlist[i * 2 + 1] = uvnodelist[i].v_current();
			x1 = uvnodelist[i].x();
			y1 = uvnodelist[i].y();
			z1 = uvnodelist[i].z();
			u1 = parasurfacelist[msurface].flatuvforxyz(x1, y1, z1)[0];//for RRM
			v1 = parasurfacelist[msurface].flatuvforxyz(x1, y1, z1)[1];
			//u1 = parasurfacelist[msurface].uvforxyz(x1, y1, z1)[0];//for test one surface para
			//v1 = parasurfacelist[msurface].uvforxyz(x1, y1, z1)[1];
			trianglein.pointlist[i * 2] = u1;
			trianglein.pointlist[i * 2 + 1] = v1;
		}
		for (i = 0; i < segmentlist.size(); i++){
			trianglein.segmentlist[i * 2] = segmentlist[i].node(0);
			trianglein.segmentlist[i * 2 + 1] = segmentlist[i].node(1);
		}
		//initialising output array
		triangleout.pointlist = (REAL *)NULL;            //Not needed if -N switch used.
		//Not needed if -N switch used or number of point attributes is zero:
		triangleout.pointattributelist = (REAL *)NULL;
		triangleout.pointmarkerlist = (int *)NULL; //Not needed if -N or -B switch used.
		triangleout.trianglelist = (int *)NULL;          //Not needed if -E switch used.
		//Not needed if -E switch used or number of triangle attributes is zero:
		triangleout.triangleattributelist = (REAL *)NULL;
		triangleout.neighborlist = (int *)NULL;         //Needed only if -n switch used.
		// Needed only if segments are output (-p or -c) and -P not used:
		triangleout.segmentlist = (int *)NULL;
		// Needed only if segments are output (-p or -c) and -P and -B not used:
		triangleout.segmentmarkerlist = (int *)NULL;
		triangleout.edgelist = (int *)NULL;             //Needed only if -e switch used.
		triangleout.edgemarkerlist = (int *)NULL;   // Needed if -e used and -B not used.
		//if (msurface == 5){
			//writepslgvtk("pslg5.vtk");
			//writepslgvtkxyz("pslgxyz5.vtk");
		//}
		//triangulate("pqa0.002", &trianglein, &triangleout, &trianglevor);
		triangulate(trianglecommand2, &trianglein, &triangleout, &trianglevor);

		//pass triangleout to local node and facet lists
		surfacenodelist_local.clear();
		facetlist_local.clear();
		for (i = 0; i < triangleout.numberofpoints; i++){
			//std::cout << msurface << " " << i << std::endl;
			if (i < uvnodelist.size()){
				//u = uvnodelist[i].u_origin(); //more accurate
				//v = uvnodelist[i].v_origin();
				//isurface = uvnodelist[i].markoriginsurface();
				x = uvnodelist[i].x(); //more accurate
				y = uvnodelist[i].y();
				z = uvnodelist[i].z();
			}
			else{
				u = triangleout.pointlist[i * 2];
				v = triangleout.pointlist[i * 2 + 1];
				point3d_ = parasurfacelist[msurface].node3d_fromflat(u, v);
				//point3d_ = parasurfacelist[msurface].node3d(u, v); //for test one surface para
				x = point3d_.x();
				y = point3d_.y();
				z = point3d_.z();
			}
			node_.markbsurface(msurface);
			node_.x(x);
			node_.y(y);
			node_.z(z);
			surfacenodelist_local.push_back(node_);
		}
		for (i = 0; i < triangleout.numberoftriangles; i++){
			facet_.markbsurface(msurface);
			facet_.numberofvertex(3);
			facet_.node(0, triangleout.trianglelist[i * 3]);
			facet_.node(1, triangleout.trianglelist[i * 3 + 1]);
			facet_.node(2, triangleout.trianglelist[i * 3 + 2]);
			facetlist_local.push_back(facet_);
		}
		//check overlapping nodes
		ncornernodes_local = uvnodelist.size();
		cornernodes_local.clear();
		markreplace.clear();
		markreplacenumber.clear();
		markreplace.resize(surfacenodelist_local.size());
		for (i = 0; i < markreplace.size(); i++){
			markreplace[i] = -1;
		}
		markreplacenumber.resize(surfacenodelist_local.size());
		if (msurface == 0){
			for (i = 0; i < ncornernodes_local; i++){
				cornernodes.push_back(i); //for msurface=0, local and global numbering are the same
			}
		}

		if (msurface > 0){
			for (i = 0; i < ncornernodes_local; i++){
				cornernodes_local.push_back(i);
			}
			//create facets surrounding nodes list locally for all nodes on this surface
			facetsunode.clear();
			facetsunode.resize(triangleout.numberofpoints);
			for (i = 0; i < facetlist_local.size(); i++){
				for (j = 0; j < 3; j++){
					facetsunode[facetlist_local[i].node(j)].push_back(i);
				}
			}
			//first check then update cornercodes by comparing local cornernodes with all other cornernodes
			for (i = 0; i < ncornernodes_local; i++){
				for (j = 0; j < cornernodes.size(); j++){
					jnode = cornernodes[j];
					double tol = 1.0;
					if (abs(surfacenodelist_local[i].x() - surfacenodelist[jnode].x()) < tol && abs(surfacenodelist_local[i].y() - surfacenodelist[jnode].y()) < tol && abs(surfacenodelist_local[i].z() - surfacenodelist[jnode].z()) < tol){
						markreplace[i] = jnode;
						for (k = i + 1; k < surfacenodelist_local.size(); k++){
							markreplacenumber[k]++;
						}
					}
				}
			}
			//update cornernodes here so don't compare with self-surface
			for (i = 0; i < ncornernodes_local; i++){
				if (markreplace[i] < 0){
					cornernodes.push_back(i - markreplacenumber[i] + nsurfacenode);
				}
			}
		}
		//compare new triangle generated nodes with local corner points
		for (i = ncornernodes_local; i < surfacenodelist_local.size(); i++){
			x1 = surfacenodelist_local[i].x();
			y1 = surfacenodelist_local[i].y();
			z1 = surfacenodelist_local[i].z();
			for (j = 0; j < i; j++){//compare all nodes; low to high
				x2 = surfacenodelist_local[j].x();
				y2 = surfacenodelist_local[j].y();
				z2 = surfacenodelist_local[j].z();
				double tol = tol_; //for test
				if (std::abs(x1 - x2) < tol && std::abs(y1 - y2) < tol && std::abs(z1 - z2) < tol){
					if (markreplace[j] >= 0){
						markreplace[i] = markreplace[j];
					}
					else{
						markreplace[i] = j + nsurfacenode - markreplacenumber[j];
					}
					for (k = i + 1; k < surfacenodelist_local.size(); k++){
						markreplacenumber[k]++; //no update for k<=ncornernodes_local
					}
					break;
				}
			}
		}

		//pass local list to region "triangle2surfacemesh"
		for (i = 0; i < surfacenodelist_local.size(); i++){
			if (markreplace[i] == -1){ //new nodes
				/*if (i < uvnodelist.size()){
					u = uvnodelist[i].u_origin();
					v = uvnodelist[i].v_origin();
					isurface = uvnodelist[i].markoriginsurface();
				}
				else{
					u = triangleout.pointlist[i * 2];
					v = triangleout.pointlist[i * 2 + 1];
					isurface = msurface;
				}
				point3d_ = parasurfacelist[isurface].node3d_fromflat(u, v);
				x = point3d_.x();
				y = point3d_.y();
				z = point3d_.z();
				node_.markbsurface(msurface);
				node_.x(x);
				node_.y(y);
				node_.z(z);*/
				surfacenodelist.push_back(surfacenodelist_local[i]);
			}
		}
		for (i = 0; i < facetlist_local.size(); i++){ //connectivity update
			facet_.markbsurface(msurface);
			facet_.numberofvertex(3);
			for (j = 0; j < 3; j++){
				inode = facetlist_local[i].node(j);
				if (markreplace[inode] >= 0){
					jnode = markreplace[inode];
				}
				else{
					jnode = inode - markreplacenumber[inode] + nsurfacenode;
				}
				facet_.node(j, jnode);
			}
			facetlist.push_back(facet_);
		}
		nfacet = facetlist.size();

		//record segment (
		for (i = 0; i < segmentlist.size(); i++){
			for (j = 0; j < 2; j++){
				inode = segmentlist[i].node(j);
				if (markreplace[inode] >= 0){
					jnode = markreplace[inode];
				}
				else{
					jnode = inode - markreplacenumber[inode] + nsurfacenode;
				}
				segment_.node(j, jnode);
			}
			segmentlist_all.push_back(segment_);
		}

		nsurfacenode = surfacenodelist.size();
		std::cout << "for msurface " << msurface << " nsurfacenode is " << nsurfacenode << std::endl;
	}
}

void REGION::parasurfaceneighbours_b(){ //only consider neighbouring 4 vertical boundary
	int i, j, imark_;
	double x_, y_, z_, x2_, y2_, z2_;
	UVNODE uvnode_, uvnode2_;
	std::vector<double> vec_, vec2_;
	for (i = 0; i < parasurfacelist.size(); i++){
		imark_ = parasurfacelist[i].markbc(); //-2 internal 0 boundary
		//for south
		uvnode_ = parasurfacelist[i].node3d(0.5, 0);
		uvnode2_ = parasurfacelist[i].node3d(0.25, 0); //in case some non-neighbor surface is on 0.5, 0 by coincident
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				int jmark_ = parasurfacelist[j].markbc();
				if (jmark_ >= 0){
					vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
					vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
					if (imark_ == -2 && vec_[0]>0 && vec_[0]<1 && vec_[1]>0 && vec_[1] < 1){
						parasurfacelist[i].neighbor(0, j);
						break;
					}
					if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
						parasurfacelist[i].neighbor(0, j);
						break;
					}
				}
			}
		}
		//for north
		uvnode_ = parasurfacelist[i].node3d(0.5, 1.0);
		uvnode2_ = parasurfacelist[i].node3d(0.25, 1.0);
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				int jmark_ = parasurfacelist[j].markbc();
				if (jmark_ >= 0){
					vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
					vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
					if (imark_ == -2 && vec_[0]>0 && vec_[0]<1.0 && vec_[1]>0 && vec_[1] < 1.0){
						parasurfacelist[i].neighbor(1, j);
						break;
					}
					if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
						parasurfacelist[i].neighbor(1, j);
						break;
					}
				}
			}
		}
		//for west
		uvnode_ = parasurfacelist[i].node3d(0, 0.5);
		uvnode2_ = parasurfacelist[i].node3d(0, 0.25);
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				int jmark_ = parasurfacelist[j].markbc();
				if (jmark_ >= 0){
					vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
					vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
					if (imark_ == -2 && vec_[0]>0 && vec_[0]<1.0 && vec_[1]>0 && vec_[1] < 1.0){
						parasurfacelist[i].neighbor(2, j);
						break;
					}
					if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
						parasurfacelist[i].neighbor(2, j);
						break;
					}
				}
			}
		}
		//for east
		uvnode_ = parasurfacelist[i].node3d(1.0, 0.5);
		uvnode2_ = parasurfacelist[i].node3d(1.0, 0.25);
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				int jmark_ = parasurfacelist[j].markbc();
				if (jmark_ >= 0){
					vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
					vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
					if (imark_ == -2 && vec_[0]>0 && vec_[0]<1.0 && vec_[1]>0 && vec_[1] < 1.0){
						parasurfacelist[i].neighbor(3, j);
						break;
					}
					if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
						parasurfacelist[i].neighbor(3, j);
						break;
					}
				}
			}
		}
	}
	std::cout << "Finding Surfaces' Neighbours Done" << std::endl;
}


void REGION::parasurfaceneighbours(){
	int i, j, imark_;
	double x_, y_, z_, x2_, y2_, z2_;
	UVNODE uvnode_, uvnode2_;
	std::vector<double> vec_, vec2_;
	for (i = 0; i < parasurfacelist.size(); i++){
		imark_ = parasurfacelist[i].markbc(); //-2 internal 0 boundary
		//for south
		uvnode_ = parasurfacelist[i].node3d(0.5, 0);
		uvnode2_ = parasurfacelist[i].node3d(0.25, 0); //in case some non-neighbor surface is on 0.5, 0 by coincident
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
				vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
				if (imark_ == -2 && vec_[0]>0 && vec_[0]<1 && vec_[1]>0 && vec_[1]<1){
					parasurfacelist[i].neighbor(0, j);
					break;
				}
				if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
					parasurfacelist[i].neighbor(0, j);
					break;
				}
			}
		}
		//for north
		uvnode_ = parasurfacelist[i].node3d(0.5, 1.0);
		uvnode2_ = parasurfacelist[i].node3d(0.25, 1.0);
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
				vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
				if (imark_ == -2 && vec_[0]>0 && vec_[0]<1.0 && vec_[1]>0 && vec_[1]<1.0){
					parasurfacelist[i].neighbor(1, j);
					break;
				}
				if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
					parasurfacelist[i].neighbor(1, j);
					break;
				}
			}
		}
		//for west
		uvnode_ = parasurfacelist[i].node3d(0, 0.5);
		uvnode2_ = parasurfacelist[i].node3d(0, 0.25);
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
				vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
				if (imark_ == -2 && vec_[0]>0 && vec_[0]<1.0 && vec_[1]>0 && vec_[1]<1.0){
					parasurfacelist[i].neighbor(2, j);
					break;
				}
				if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
					parasurfacelist[i].neighbor(2, j);
					break;
				}
			}
		}
		//for east
		uvnode_ = parasurfacelist[i].node3d(1.0, 0.5);
		uvnode2_ = parasurfacelist[i].node3d(1.0, 0.25);
		x_ = uvnode_.x();
		y_ = uvnode_.y();
		z_ = uvnode_.z();
		x2_ = uvnode2_.x();
		y2_ = uvnode2_.y();
		z2_ = uvnode2_.z();
		for (j = 0; j < parasurfacelist.size(); j++){
			if (j != i){
				vec_ = parasurfacelist[j].uvforxyz(x_, y_, z_);
				vec2_ = parasurfacelist[j].uvforxyz(x2_, y2_, z2_);
				if (imark_ == -2 && vec_[0]>0 && vec_[0]<1.0 && vec_[1]>0 && vec_[1]<1.0){
					parasurfacelist[i].neighbor(3, j);
					break;
				}
				if (imark_ == 0 && vec_[0] >= 0 && vec2_[0] >= 0){
					parasurfacelist[i].neighbor(3, j);
					break;
				}
			}
		}
	}
	std::cout << "Finding Surfaces' Neighbours Done" << std::endl;
}

void REGION::buildcpg_ordering(){
	int ip, i, j, jp, k, nx, ny, nz, baseid;
	double dx, dy, xx, yy, zz;
	NODE node_, node2_;
	UVNODE uvnode_, uvnode2_;
	std::vector<std::vector<int>> table, tablemark, mainuptree, maindowntree;
	std::vector<int> hordering, horderingmarkbt;
	GRAPHEDGE graphedge_;
	cpgfacelist.clear();
	graphnodelist.clear();
	graphedgelist.clear();
	int nhsurface = hsurfaceid.size();
	table.resize(nhsurface);
	horderingmarkbt.resize(nhsurface);
	graphnodelist.resize(nhsurface);
	for (i = 0; i < nhsurface; i++){
		table[i].resize(nhsurface);
	}
	tablemark.resize(nhsurface);
	for (i = 0; i < nhsurface; i++){
		tablemark[i].resize(nhsurface);
	}
	std::vector<int> markl, markr;//left or right touching
	markl.resize(nhsurface);
	markr.resize(nhsurface);
	for (i = 0; i < nhsurface; i++){ //last 4 parasurfaces are vertical front-back-left-right order not important
		int inei[4];
		for (j = 0; j < 4; j++){
			inei[j] = parasurfacelist[i].neighbor(j);
			if (inei[j] == nhsurface + 2){//left
				markl[i] = 1; //directly parasurface
			}
			else if (inei[j] == nhsurface + 3){//right
				markr[i] = 1;
			}
			else if (inei[j] < nhsurface){//horizons
				if (j == 0){ //south v=0
					uvnode_ = parasurfacelist[i].node3d(0.5, 0.01);
					xx = uvnode_.x();
					yy = uvnode_.y();
					zz = uvnode_.z();
					//get node from xy from neighbor surface and compare
					uvnode2_ = parasurfacelist[inei[j]].node3d_fromxy(xx, yy);
					if (uvnode2_.flag() == 0){
						std::cout << "error in compare two surfaces xy return 0 3d node" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					if (uvnode2_.z() > zz){ //inei[j] higher than i. although table computed twice but doesn't matter
						table[inei[j]][i] = 1;
						table[i][inei[j]] = -1;
						graphedge_.graphnode(0, i); //build edges
						graphedge_.graphnode(1, inei[j]);
						graphedgelist.push_back(graphedge_);
						graphnodelist[i].addedgeout(graphedgelist.size() - 1);
						graphnodelist[inei[j]].addedgein(graphedgelist.size() - 1);
					}
					else{ //inei[j] lower than i
						table[inei[j]][i] = -1;
						table[i][inei[j]] = 1;
						graphedge_.graphnode(0, inei[j]);
						graphedge_.graphnode(1, i);
						graphedgelist.push_back(graphedge_);
						graphnodelist[inei[j]].addedgeout(graphedgelist.size() - 1);
						graphnodelist[i].addedgein(graphedgelist.size() - 1);
					}
				}
				else if (j == 1){ //north v=1
					uvnode_ = parasurfacelist[i].node3d(0.5, 0.99);
					xx = uvnode_.x();
					yy = uvnode_.y();
					zz = uvnode_.z();
					//get node from xy from neighbor surface and compare
					uvnode2_ = parasurfacelist[inei[j]].node3d_fromxy(xx, yy);
					if (uvnode2_.flag() == 0){
						std::cout << "error in compare two surfaces xy return 0 3d node" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					if (uvnode2_.z() > zz){ //inei[j] higher than i. although table computed twice but doesn't matter
						table[inei[j]][i] = 1;
						table[i][inei[j]] = -1;
						graphedge_.graphnode(0, i); //build edges
						graphedge_.graphnode(1, inei[j]);
						graphedgelist.push_back(graphedge_);
						graphnodelist[i].addedgeout(graphedgelist.size() - 1);
						graphnodelist[inei[j]].addedgein(graphedgelist.size() - 1);
					}
					else{ //inei[j] lower than i
						table[inei[j]][i] = -1;
						table[i][inei[j]] = 1;
						graphedge_.graphnode(0, inei[j]);
						graphedge_.graphnode(1, i);
						graphedgelist.push_back(graphedge_);
						graphnodelist[inei[j]].addedgeout(graphedgelist.size() - 1);
						graphnodelist[i].addedgein(graphedgelist.size() - 1);
					}
				}
				else if (j == 2){ //west u=0
					uvnode_ = parasurfacelist[i].node3d(0.01, 0.5);
					xx = uvnode_.x();
					yy = uvnode_.y();
					zz = uvnode_.z();
					//get node from xy from neighbor surface and compare
					uvnode2_ = parasurfacelist[inei[j]].node3d_fromxy(xx, yy);
					if (uvnode2_.flag() == 0){
						std::cout << "error in compare two surfaces xy return 0 3d node" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					if (uvnode2_.z() > zz){ //inei[j] higher than i. although table computed twice but doesn't matter
						table[inei[j]][i] = 1;
						table[i][inei[j]] = -1;
						graphedge_.graphnode(0, i); //build edges
						graphedge_.graphnode(1, inei[j]);
						graphedgelist.push_back(graphedge_);
						graphnodelist[i].addedgeout(graphedgelist.size() - 1);
						graphnodelist[inei[j]].addedgein(graphedgelist.size() - 1);
					}
					else{ //inei[j] lower than i
						table[inei[j]][i] = -1;
						table[i][inei[j]] = 1;
						graphedge_.graphnode(0, inei[j]);
						graphedge_.graphnode(1, i);
						graphedgelist.push_back(graphedge_);
						graphnodelist[inei[j]].addedgeout(graphedgelist.size() - 1);
						graphnodelist[i].addedgein(graphedgelist.size() - 1);
					}
				}
				else if (j == 3){ //east u=1
					uvnode_ = parasurfacelist[i].node3d(0.99, 0.5);
					xx = uvnode_.x();
					yy = uvnode_.y();
					zz = uvnode_.z();
					//get node from xy from neighbor surface and compare
					uvnode2_ = parasurfacelist[inei[j]].node3d_fromxy(xx, yy);
					if (uvnode2_.flag() == 0){
						std::cout << "error in compare two surfaces xy return 0 3d node" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					if (uvnode2_.z() > zz){ //inei[j] higher than i. although table computed twice but doesn't matter
						table[inei[j]][i] = 1;
						table[i][inei[j]] = -1;
						graphedge_.graphnode(0, i); //build edges
						graphedge_.graphnode(1, inei[j]);
						graphedgelist.push_back(graphedge_);
						graphnodelist[i].addedgeout(graphedgelist.size() - 1);
						graphnodelist[inei[j]].addedgein(graphedgelist.size() - 1);
					}
					else{ //inei[j] lower than i
						table[inei[j]][i] = -1;
						table[i][inei[j]] = 1;
						graphedge_.graphnode(0, inei[j]);
						graphedge_.graphnode(1, i);
						graphedgelist.push_back(graphedge_);
						graphnodelist[inei[j]].addedgeout(graphedgelist.size() - 1);
						graphnodelist[i].addedgein(graphedgelist.size() - 1);
					}
				}
			}
		}
	}

	//second step: build trees
	std::vector<std::vector<int>> group;
	std::vector<int> mainhorizon;
	int count = 0;
	for (i = 0; i < nhsurface; i++){
		if (markl[i] == 1 && markr[i] == 1){
			count++;
			mainhorizon.push_back(i);
		}
	}
	mainuptree.resize(mainhorizon.size());
	maindowntree.resize(mainhorizon.size());
	//reorder mainhorizon bottom to top
	for (i = 1; i < mainhorizon.size(); i++){ //straight insertion sort
		int itmp = mainhorizon[i];
		for (j = i - 1; j >= 0 && parasurfacelist[mainhorizon[j]].uvnode(int(parasurfacelist[mainhorizon[j]].nu() / 2), int(parasurfacelist[mainhorizon[j]].nv() / 2)).z() > parasurfacelist[itmp].uvnode(int(parasurfacelist[itmp].nu() / 2), int(parasurfacelist[itmp].nv() / 2)).z(); j--){
			mainhorizon[j + 1] = mainhorizon[j];
		}
		mainhorizon[j + 1] = itmp;
	}
	std::vector<int> horizon_position;
	horizon_position.resize(nhsurface);

	std::cout << "reorder horizons using DFS" << std::endl;
	for (int imain = 0; imain < mainhorizon.size(); imain++){ //relations must allow DFS to work
		int ih = mainhorizon[imain];
		markb_ = ih;
		DFSgraphnode_rh(ih); //from below
		ngnode = postordering.size();
		reversepostordering.resize(ngnode);
		for (i = 0; i < ngnode; i++){
			int ie = postordering[ngnode - 1 - i];
			reversepostordering[i] = ie;
		}
		maindowntree[imain] = reversepostordering;
		for (i = 0; i < ngnode; i++){
			hordering.push_back(postordering[i]);
			//horderingmarkbt[i + hsize] = -1;
		}
		postordering.clear();

		DFSgraphnode(ih); // to above
		ngnode = postordering.size();
		reversepostordering.resize(ngnode);
		for (i = 0; i < ngnode; i++){
			int ie = postordering[ngnode - 1 - i];
			reversepostordering[i] = ie;
		}
		mainuptree[imain] = reversepostordering;
		for (i = 1; i < ngnode; i++){
			hordering.push_back(reversepostordering[i]); //ignore the first which is mainhsurface
		}
		postordering.clear();
		reversepostordering.clear();
	}
	if (hordering.size() != nhsurface){
		std::cout << "hordering size incorrect" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	for (i = 0; i < hordering.size(); i++){
		int isur = hordering[i];
		horizon_position[isur] = i; //horizons' position in a pillar if only one layer between two horizons
	}

	//build pillars
	nx = meshinfo.cpgdimension(0) + 1;
	ny = meshinfo.cpgdimension(1) + 1;
	nz = meshinfo.cpgdimension(2); //number of layers(between two horizons)
	baseid = hsurfaceid[0]; //use bottom surface as base
	double xstart_ = parasurfacelist[baseid].node3d(0, 0).x();
	double xend_ = parasurfacelist[baseid].node3d(1, 0).x();
	double ystart_ = parasurfacelist[baseid].node3d(0, 0).y();
	double yend_ = parasurfacelist[baseid].node3d(0, 1).y();
	double lx = xend_ - xstart_;
	double ly = yend_ - ystart_;
	dx = lx / (nx - 1);
	dy = ly / (ny - 1);
	std::vector<std::vector<int>> pillarmatrix;
	pillarmatrix.resize(nx);
	for (int i = 0; i < nx; i++){
		pillarmatrix[i].resize(ny);
	}
	for (j = 0; j < ny; j++){
		for (i = 0; i < nx; i++){
			PILLAR pillar_;
			pillar_.initialise(hsurfaceid.size() + (hsurfaceid.size() - 1)*(nz - 1));
			xx = i*dx + xstart_;
			yy = j*dy + ystart_;
			node_.x(xx);
			node_.y(yy);
			for (int imain = 0; imain < mainhorizon.size(); imain++){
				double zzold, zzmain;
				std::vector<int> ordering_ = maindowntree[imain];
				int isur = ordering_[0];
				int ipph = horizon_position[isur]; //main horizon order from 0
				int ipp = ipph * nz; //all nodes order position including internal layers between horizons from 0
				uvnode_ = parasurfacelist[isur].node3d_fromxy(xx, yy);
				if (uvnode_.flag() == 0){
					std::cout << "mainhorizon not contacted by pillar" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				zz = uvnode_.z();
				node_.z(zz);
				nodelist.push_back(node_);
				pillar_.pillarnode(ipp, nodelist.size() - 1);
				pillar_.pillarmarker(ipp, 1);
				zzmain = zz;
				zzold = zz;
				for (int ii = 1; ii < ordering_.size(); ii++){
					isur = ordering_[ii];
					ipp = horizon_position[isur] * nz;
					uvnode_ = parasurfacelist[isur].node3d_fromxy(xx, yy);
					if (uvnode_.flag() == 1){
						zz = uvnode_.z();
						zzold = zz;
					}
					else{
						zz = zzold;
					}
					node_.z(zz);
					nodelist.push_back(node_);
					pillar_.pillarnode(ipp, nodelist.size() - 1);//pillar node only account horizon nodes
					pillar_.pillarmarker(ipp, 1); //computed
				}
				ordering_ = mainuptree[imain];
				zzold = zzmain;
				for (int ii = 1; ii < ordering_.size(); ii++){
					isur = ordering_[ii];
					ipp = horizon_position[isur] * nz;
					uvnode_ = parasurfacelist[isur].node3d_fromxy(xx, yy);
					if (uvnode_.flag() == 1){
						zz = uvnode_.z();
						zzold = zz;
					}
					else{
						zz = zzold;
					}
					node_.z(zz);
					nodelist.push_back(node_);
					pillar_.pillarnode(ipp, nodelist.size() - 1);
					pillar_.pillarmarker(ipp, 1); //computed
				}
			}
			pillarlist.push_back(pillar_);
			pillarmatrix[i][j] = pillarlist.size() - 1;
		}
	}

	//insert nodes between pillar horizon nodes
	if (nz > 1){
		for (i = 0; i < pillarlist.size(); i++){
			for (j = 0; j < nhsurface - 1; j++){
				int inode = pillarlist[i].pillarnode(j*nz);
				int inode2 = pillarlist[i].pillarnode((j + 1)*nz);
				node_ = nodelist[inode];//two horizon nodes
				node2_ = nodelist[inode2];
				for (k = 0; k < nz - 1; k++){
					double xx = node_.x() + (node2_.x() - node_.x())*(k + 1) / nz;
					double yy = node_.y() + (node2_.y() - node_.y())*(k + 1) / nz;
					double zz = node_.z() + (node2_.z() - node_.z())*(k + 1) / nz;
					NODE node3_;
					node3_.x(xx);
					node3_.y(yy);
					node3_.z(zz);
					nodelist.push_back(node3_);
					int ipp = j*nz + k + 1;
					pillarlist[i].pillarnode(ipp, nodelist.size() - 1);
				}
			}
		}
	}


	//build cpg from pillars;
	int nex = meshinfo.cpgdimension(0);
	int ney = meshinfo.cpgdimension(1);
	int nez = (nhsurface - 1)*nz;

	cpgnodematrix.clear();
	cpgelementmatrix.clear();

	cpgnodematrix.resize(nex + 1);
	for (i = 0; i < nex + 1; i++){
		cpgnodematrix[i].resize(ney + 1);
	}
	for (i = 0; i < nex + 1; i++){
		for (j = 0; j < ney + 1; j++){
			cpgnodematrix[i][j].resize(nez + 1);
		}
	}

	for (k = 0; k < nez + 1; k++){
		for (j = 0; j < ney + 1; j++){
			for (i = 0; i < nex + 1; i++){
				cpgnodematrix[i][j][k] = pillarlist[pillarmatrix[i][j]].pillarnode(k);
			}
		}
	}

	cpgelementmatrix.resize(nex);
	for (i = 0; i < nex; i++){
		cpgelementmatrix[i].resize(ney);
	}
	for (i = 0; i < nex; i++){
		for (j = 0; j < ney; j++){
			cpgelementmatrix[i][j].resize(nez);
		}
	}


	for (k = 0; k < nez; k++){
		for (j = 0; j < ney; j++){
			for (i = 0; i < nex; i++){
				cpgelementmatrix[i][j][k] = k*nex*ney + j*nex + i;
			}
		}
	}

	//build connectivity and neighbors for corner-point grid x, y ,z direction in order
	CPGELEMENT cpgelement;
	int i0, i1, i2, i3, i4, i5, i6, i7;
	int nn[4];
	double xx4[4], yy4[4], zz4[4];
	double x0, x1, x2, x3, y0, y1, y2, y3, z0, z1, z2, z3;
	FACET bfacet_;
	//CPGFACE cpgface_;

	for (k = 0; k < nez; k++){
		for (j = 0; j < ney; j++){
			for (i = 0; i < nex; i++){
				cpgelement.layer(k);
				//neighbors
				int nc = cpgelementlist.size();
				if (i > 0){
					cpgelement.neighbor(0, nc - 1);
				}
				if (i < nex - 1){
					cpgelement.neighbor(1, nc + 1);
				}
				if (j>0){
					cpgelement.neighbor(2, nc - nex);
				}
				if (j < ney - 1){
					cpgelement.neighbor(3, nc + nex);
				}
				if (k>0){
					cpgelement.neighbor(4, nc - nex*ney);
				}
				if (k < nez - 1){
					cpgelement.neighbor(5, nc + nex*ney);
				}

				i0 = cpgnodematrix[i][j][k];
				i1 = cpgnodematrix[i + 1][j][k];
				i2 = cpgnodematrix[i][j + 1][k];
				i3 = cpgnodematrix[i + 1][j + 1][k];
				i4 = cpgnodematrix[i][j][k + 1];
				i5 = cpgnodematrix[i + 1][j][k + 1];
				i6 = cpgnodematrix[i][j + 1][k + 1];
				i7 = cpgnodematrix[i + 1][j + 1][k + 1];
				cpgelement.node(0, i0);//order:: lb--rb--lt--rt then k+1 layer
				cpgelement.node(1, i1);
				cpgelement.node(2, i2);
				cpgelement.node(3, i3);
				cpgelement.node(4, i4);
				cpgelement.node(5, i5);
				cpgelement.node(6, i6);
				cpgelement.node(7, i7);
				//cpgelement.layer(flag[k*(nex + 1)*(ney + 1) + j*(nex + 1) + i]);//use bottom points to flag layers (from 0 bot2top)
				double x_ = (nodelist[i0].x() + nodelist[i1].x() + nodelist[i2].x() + nodelist[i3].x() + nodelist[i4].x() + nodelist[i5].x() + nodelist[i6].x() + nodelist[i7].x()) / 8.0;
				double y_ = (nodelist[i0].y() + nodelist[i1].y() + nodelist[i2].y() + nodelist[i3].y() + nodelist[i4].y() + nodelist[i5].y() + nodelist[i6].y() + nodelist[i7].y()) / 8.0;
				double z_ = (nodelist[i0].z() + nodelist[i1].z() + nodelist[i2].z() + nodelist[i3].z() + nodelist[i4].z() + nodelist[i5].z() + nodelist[i6].z() + nodelist[i7].z()) / 8.0;
				node_.x(x_);
				node_.y(y_);
				node_.z(z_);
				cpgelement.centnode(node_);
				//volume = 6 tetrahedrons
				double vol_ = 0;
				for (int ic = 0; ic < 6; ic++){
					if (ic == 0){
						nn[0] = i0;
						nn[1] = i1;
						nn[2] = i2;
						nn[3] = i4;
					}
					else if (ic == 1){
						nn[0] = i1;
						nn[1] = i2;
						nn[2] = i4;
						nn[3] = i6;
					}
					else if (ic == 2){
						nn[0] = i1;
						nn[1] = i4;
						nn[2] = i5;
						nn[3] = i6;
					}
					else if (ic == 3){
						nn[0] = i1;
						nn[1] = i2;
						nn[2] = i3;
						nn[3] = i7;
					}
					else if (ic == 4){
						nn[0] = i1;
						nn[1] = i2;
						nn[2] = i5;
						nn[3] = i7;
					}
					else if (ic == 5){
						nn[0] = i2;
						nn[1] = i5;
						nn[2] = i7;
						nn[3] = i6;
					}
					for (int ii = 0; ii < 4; ii++){
						xx4[ii] = nodelist[nn[ii]].x();
						yy4[ii] = nodelist[nn[ii]].y();
						zz4[ii] = nodelist[nn[ii]].z();
					}
					vol_ = vol_ + mathzz_.volume_tetra_positive(xx4, yy4, zz4);
				}
				cpgelement.volume(vol_);
				////6 faces //need change to adapt to cpgfacelist
				//for (int ic = 0; ic < 6; ic++){
				//	if (ic == 0){
				//		x0 = nodelist[i0].x(); y0 = nodelist[i0].y(); z0 = nodelist[i0].z();
				//		x1 = nodelist[i2].x(); y1 = nodelist[i2].y(); z1 = nodelist[i2].z();
				//		x2 = nodelist[i4].x(); y2 = nodelist[i4].y(); z2 = nodelist[i4].z();
				//		x3 = nodelist[i6].x(); y3 = nodelist[i6].y(); z3 = nodelist[i6].z();
				//	}
				//	else if (ic == 1){
				//		x0 = nodelist[i1].x(); y0 = nodelist[i1].y(); z0 = nodelist[i1].z();
				//		x1 = nodelist[i3].x(); y1 = nodelist[i3].y(); z1 = nodelist[i3].z();
				//		x2 = nodelist[i5].x(); y2 = nodelist[i5].y(); z2 = nodelist[i5].z();
				//		x3 = nodelist[i7].x(); y3 = nodelist[i7].y(); z3 = nodelist[i7].z();
				//	}
				//	else if (ic == 2){
				//		x0 = nodelist[i0].x(); y0 = nodelist[i0].y(); z0 = nodelist[i0].z();
				//		x1 = nodelist[i1].x(); y1 = nodelist[i1].y(); z1 = nodelist[i1].z();
				//		x2 = nodelist[i4].x(); y2 = nodelist[i4].y(); z2 = nodelist[i4].z();
				//		x3 = nodelist[i5].x(); y3 = nodelist[i5].y(); z3 = nodelist[i5].z();
				//	}
				//	else if (ic == 3){
				//		x0 = nodelist[i2].x(); y0 = nodelist[i2].y(); z0 = nodelist[i2].z();
				//		x1 = nodelist[i3].x(); y1 = nodelist[i3].y(); z1 = nodelist[i3].z();
				//		x2 = nodelist[i6].x(); y2 = nodelist[i6].y(); z2 = nodelist[i6].z();
				//		x3 = nodelist[i7].x(); y3 = nodelist[i7].y(); z3 = nodelist[i7].z();
				//	}
				//	else if (ic == 4){
				//		x0 = nodelist[i0].x(); y0 = nodelist[i0].y(); z0 = nodelist[i0].z();
				//		x1 = nodelist[i1].x(); y1 = nodelist[i1].y(); z1 = nodelist[i1].z();
				//		x2 = nodelist[i2].x(); y2 = nodelist[i2].y(); z2 = nodelist[i2].z();
				//		x3 = nodelist[i3].x(); y3 = nodelist[i3].y(); z3 = nodelist[i3].z();
				//	}
				//	else if (ic == 5){
				//		x0 = nodelist[i4].x(); y0 = nodelist[i4].y(); z0 = nodelist[i4].z();
				//		x1 = nodelist[i5].x(); y1 = nodelist[i5].y(); z1 = nodelist[i5].z();
				//		x2 = nodelist[i6].x(); y2 = nodelist[i6].y(); z2 = nodelist[i6].z();
				//		x3 = nodelist[i7].x(); y3 = nodelist[i7].y(); z3 = nodelist[i7].z();
				//	}
				//	x_ = (x0 + x1 + x2 + x3) / 4.0;
				//	y_ = (y0 + y1 + y2 + y3) / 4.0;
				//	z_ = (z0 + z1 + z2 + z3) / 4.0;
				//	node_.x(x_);
				//	node_.y(y_);
				//	node_.z(z_);
				//	cpgface_.centnode(node_);
				//	double xx[3], yy[3], zz[3], edgevec1[3], edgevec2[3];

				//	xx[0] = x0;
				//	yy[0] = y0;
				//	zz[0] = z0;

				//	xx[1] = x1;
				//	yy[1] = y1;
				//	zz[1] = z1;

				//	xx[2] = x2;
				//	yy[2] = y2;
				//	zz[2] = z2;

				//	double area1 = mathzz_.area_triangle(xx, yy, zz);

				//	xx[0] = x3;
				//	yy[0] = y3;
				//	zz[0] = z3;

				//	double area2 = mathzz_.area_triangle(xx, yy, zz);
				//	std::vector<double> unitnormalvec_, xikvec_;
				//	xikvec_.resize(3);
				//	cpgface_.area(area1 + area2);
				//	edgevec1[0] = xx[1] - xx[0];
				//	edgevec1[1] = yy[1] - yy[0];
				//	edgevec1[2] = zz[1] - zz[0];
				//	edgevec2[0] = xx[2] - xx[0];
				//	edgevec2[1] = yy[2] - yy[0];
				//	edgevec2[2] = zz[2] - zz[0];
				//	unitnormalvec_ = mathzz_.unitnormal(edgevec1, edgevec2); //not nessisarily point to outside of element need abs
				//	xikvec_[0] = cpgface_.centnode().x() - cpgelement.centnode().x();
				//	xikvec_[1] = cpgface_.centnode().y() - cpgelement.centnode().y();
				//	xikvec_[2] = cpgface_.centnode().z() - cpgelement.centnode().z();
				//	if (mathzz_.dotproduct(xikvec_, unitnormalvec_) < 0){
				//		unitnormalvec_[0] = -unitnormalvec_[0];
				//		unitnormalvec_[1] = -unitnormalvec_[1];
				//		unitnormalvec_[2] = -unitnormalvec_[2];
				//	}
				//	cpgface_.unitnormalvec(unitnormalvec_);
				//	cpgfacelist.push_back(cpgface_);
				//	cpgelement.cpgface(ic, cpgfacelist.size() - 1);
				//}
				cpgelementlist.push_back(cpgelement);
				cpgelement.clear();
			}
		}
	}
	graphnodelist.clear();
	graphedgelist.clear();
	std::cout << "CPG generated from pillars" << std::endl;
}

void REGION::buildcpg_cut(){
	//find out left&right touching
	std::vector<int> markl, markr;//left or right touching
	markl.resize(hsurfaceid.size());
	markr.resize(hsurfaceid.size());
	for (int i = 0; i < hsurfaceid.size(); i++){ //last 4 parasurfaces are vertical front-back-left-right
		int inei[4];
		for (int j = 0; j < 4; j++){
			inei[j] = parasurfacelist[hsurfaceid[i]].neighbor(j);
			if (inei[j] == hsurfaceid.size() + 2){//left
				markl[i] = 1;
			}
			else if (inei[j] == hsurfaceid.size() + 3){//right
				markr[i] = 1;
			}
		}
	}
	std::vector<int> mainhorizon, otherhorizon;
	for (int i = 0; i < hsurfaceid.size(); i++){
		if (markl[i] == 1 && markr[i] == 1){
			mainhorizon.push_back(hsurfaceid[i]); //mainsurface directly point to parasurface
		}
		else{
			otherhorizon.push_back(hsurfaceid[i]);
		}
	}
	//reorder mainhorizon bottom to top
	for (int i = 1; i < mainhorizon.size(); i++){ //straight insertion sort
		int itmp = mainhorizon[i];
		int j;
		for (j = i - 1; j >= 0 && parasurfacelist[mainhorizon[j]].uvnode(int(parasurfacelist[mainhorizon[j]].nu() / 2), int(parasurfacelist[mainhorizon[j]].nv() / 2)).z() > parasurfacelist[itmp].uvnode(int(parasurfacelist[itmp].nu() / 2), int(parasurfacelist[itmp].nv() / 2)).z(); j--){
			mainhorizon[j + 1] = mainhorizon[j];
		}
		mainhorizon[j + 1] = itmp;
	}

	int nx = meshinfo.cpgdimension(0) + 1;
	int ny = meshinfo.cpgdimension(1) + 1;
	int nz = meshinfo.cpgdimension(2); //number of layers(between two horizons)
	int baseid = hsurfaceid[0]; //use bottom surface as base
	double xstart_ = parasurfacelist[baseid].node3d(0, 0).x();
	double xend_ = parasurfacelist[baseid].node3d(1, 0).x();
	double ystart_ = parasurfacelist[baseid].node3d(0, 0).y();
	double yend_ = parasurfacelist[baseid].node3d(0, 1).y();
	double lx = xend_ - xstart_;
	double ly = yend_ - ystart_;
	double dx = lx / (nx - 1);
	double dy = ly / (ny - 1);
	std::vector<std::vector<int>> pillarmatrix;
	pillarmatrix.resize(nx);
	for (int i = 0; i < nx; i++){
		pillarmatrix[i].resize(ny);
	}
	for (int j = 0; j < ny; j++){
		for (int i = 0; i < nx; i++){
			PILLAR pillar_;
			pillar_.initialise(mainhorizon.size());
			double xx = i*dx + xstart_;
			double yy = j*dy + ystart_;
			NODE node_;
			node_.x(xx);
			node_.y(yy);
			for (int imain = 0; imain < mainhorizon.size(); imain++){
				int isur = mainhorizon[imain];
				UVNODE uvnode_ = parasurfacelist[isur].node3d_fromxy(xx, yy);
				if (uvnode_.flag() == 0){
					std::cout << "mainhorizon not contacted by pillar" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				double zz = uvnode_.z();
				node_.z(zz);
				nodelist.push_back(node_);
				pillar_.pillarnode(imain, nodelist.size() - 1);
			}
			pillarlist.push_back(pillar_);
			pillarmatrix[i][j] = pillarlist.size() - 1;
		}
	}

	//build cpg from pillars;
	int nex = meshinfo.cpgdimension(0);
	int ney = meshinfo.cpgdimension(1);
	int nez = mainhorizon.size() - 1;

	cpgnodematrix.clear();
	cpgelementmatrix.clear();
	cpgnodematrix.resize(nex + 1);
	for (int i = 0; i < nex + 1; i++){
		cpgnodematrix[i].resize(ney + 1);
	}
	for (int i = 0; i < nex + 1; i++){
		for (int j = 0; j < ney + 1; j++){
			cpgnodematrix[i][j].resize(nez + 1);
		}
	}

	for (int k = 0; k < nez + 1; k++){
		for (int j = 0; j < ney + 1; j++){
			for (int i = 0; i < nex + 1; i++){
				cpgnodematrix[i][j][k] = pillarlist[pillarmatrix[i][j]].pillarnode(k);
			}
		}
	}

	cpgelementmatrix.resize(nex);
	for (int i = 0; i < nex; i++){
		cpgelementmatrix[i].resize(ney);
	}
	for (int i = 0; i < nex; i++){
		for (int j = 0; j < ney; j++){
			cpgelementmatrix[i][j].resize(nez);
		}
	}


	for (int k = 0; k < nez; k++){
		for (int j = 0; j < ney; j++){
			for (int i = 0; i < nex; i++){
				cpgelementmatrix[i][j][k] = k*nex*ney + j*nex + i;
			}
		}
	}

	//build connectivity and neighbors for corner-point grid x, y ,z direction in order
	CPGELEMENT cpgelement;
	int i0, i1, i2, i3, i4, i5, i6, i7;
	int nn[4];
	double xx4[4], yy4[4], zz4[4];
	double x0, x1, x2, x3, y0, y1, y2, y3, z0, z1, z2, z3;
	FACET bfacet_;
	//CPGFACE cpgface_;

	for (int k = 0; k < nez; k++){
		for (int j = 0; j < ney; j++){
			for (int i = 0; i < nex; i++){
				//neighbors
				int nc = cpgelementlist.size();
				if (i > 0){
					cpgelement.neighbor(0, nc - 1);
				}
				if (i < nex - 1){
					cpgelement.neighbor(1, nc + 1);
				}
				if (j>0){
					cpgelement.neighbor(2, nc - nex);
				}
				if (j < ney - 1){
					cpgelement.neighbor(3, nc + nex);
				}
				if (k>0){
					cpgelement.neighbor(4, nc - nex*ney);
				}
				if (k < nez - 1){
					cpgelement.neighbor(5, nc + nex*ney);
				}

				i0 = cpgnodematrix[i][j][k];
				i1 = cpgnodematrix[i + 1][j][k];
				i2 = cpgnodematrix[i][j + 1][k];
				i3 = cpgnodematrix[i + 1][j + 1][k];
				i4 = cpgnodematrix[i][j][k + 1];
				i5 = cpgnodematrix[i + 1][j][k + 1];
				i6 = cpgnodematrix[i][j + 1][k + 1];
				i7 = cpgnodematrix[i + 1][j + 1][k + 1];
				cpgelement.node(0, i0);//order:: lb--rb--lt--rt then k+1 layer
				cpgelement.node(1, i1);
				cpgelement.node(2, i2);
				cpgelement.node(3, i3);
				cpgelement.node(4, i4);
				cpgelement.node(5, i5);
				cpgelement.node(6, i6);
				cpgelement.node(7, i7);
				cpgelementlist.push_back(cpgelement);
				cpgelement.clear();
			}
		}
	}

	//cut cpgelement using otherhorizon (need to be implemented)

	std::cout << "CPG generated by cutting elements" << std::endl;
}

void REGION::buildcpg_parallel(){ //only needs horizontal surfaces; includes bfacetlist and neighboring element
	int isurface, jsurface, itmp, i, j, k, nnx, nny, nnv, nnz_2layer, nnvc, ic, jc, jv, nlayer, nex, ney, nez, msurface, nc;
	//int surfaceflag[7]; //mark horizontal or vertical
	double ddx, ddy, ddv, ddvc, zdistance, x_, y_, z_, u, v, x1, x2, y1, y2, z1, z2, xx[3], yy[3], zz[3], area1, area2;
	double x0, y0, z0, x3, y3, z3, edgevec1[3], edgevec2[3], xx4[4], yy4[4], zz4[4], vol_;
	int i0, i1, i2, i3, i4, i5, i6, i7, nn[4], ii;
	std::vector<double> unitnormalvec_, xikvec_;
	std::vector<int> flag;
	NODE node_;
	UVNODE uvnode1_;
	CPGELEMENT cpgelement;
	FACET bfacet_;
	//CPGFACE cpgface_;
	cpgfacelist.clear();
	xikvec_.resize(3);

	nnx = 21; //number of nodes per line along x
	nny = 5; //number of nodes per line along y
	ddx = 1.0 / (nnx - 1); //distance between two uv nodes
	ddy = 1.0 / (nny - 1);
	nnv = 2; //number of elements in z direction per layer
	meshinfo.cpgdimension(0, nnx - 1);
	meshinfo.cpgdimension(1, nny - 1);

	//re-order horizontal surfaces bottom to top
	for (i = 1; i < hsurfaceid.size(); i++){ //straight insertion sort
		itmp = hsurfaceid[i];
		for (j = i - 1; j >= 0 && parasurfacelist[hsurfaceid[j]].node3d(0.5, 0.5).z()> parasurfacelist[itmp].node3d(0.5, 0.5).z(); j--){
			hsurfaceid[j + 1] = hsurfaceid[j];
		}
		hsurfaceid[j + 1] = itmp;
	}
	nlayer = 0;
	//build nodes on surfaces and between surfaces for corner-point grid along u is +x along v is +y
	msurface = hsurfaceid[0];
	for (i = 0; i < hsurfaceid.size() - 1; i++){
		isurface = hsurfaceid[i];
		jsurface = hsurfaceid[i + 1];
		zdistance = parasurfacelist[jsurface].node3d(0.5, 0.5).z() - parasurfacelist[isurface].node3d(0.5, 0.5).z();
		nlayer = nlayer + nnv;
		for (jv = 0; jv < nnv; jv++){ //vertical internal bottom to top (not including top)
			for (jc = 0; jc < nny; jc++){ //along y direction
				for (ic = 0; ic < nnx; ic++){ //along x direction
					u = ic*ddx;
					v = jc*ddy;
					//straight pillar
					x1 = parasurfacelist[msurface].node3d(u, v).x();
					y1 = parasurfacelist[msurface].node3d(u, v).y();
					z1 = parasurfacelist[isurface].node3d(u, v).z();
					x2 = parasurfacelist[msurface].node3d(u, v).x();
					y2 = parasurfacelist[msurface].node3d(u, v).y();
					z2 = parasurfacelist[jsurface].node3d(u, v).z();
					x_ = x1 + (x2 - x1)*jv / nnv;
					y_ = y1 + (y2 - y1)*jv / nnv;
					z_ = z1 + (z2 - z1)*jv / nnv;
					node_.x(x_);
					node_.y(y_);
					node_.z(z_);
					/*if (i == 0 && jv==0){ //bsurface mark bot, top, left, right, front, back
						node_.markbsurface(0);
					}
					if (jc == 0){
						node_.markbsurface(4);
					}
					else if (jc == nny - 1){
						node_.markbsurface(5);
					}
					if (ic == 0){
						node_.markbsurface(2);
					}
					else if (ic == nnx - 1){
						node_.markbsurface(3);
					}*/
					nodelist.push_back(node_); //order is direction x, y, z and surfaces bottom to top
					flag.push_back(i); //flag horizontal layers of the input parametric surfaces; layer i from 0
				}
			}
		}
	}
	//top surface
	i = hsurfaceid.size() - 1;
	isurface = hsurfaceid[i];
	for (jc = 0; jc < nny; jc++){
		for (ic = 0; ic < nnx; ic++){
			u = ic*ddx;
			v = jc*ddy;
			uvnode1_ = parasurfacelist[isurface].node3d(u, v);
			x_ = uvnode1_.x();
			y_ = uvnode1_.y();
			z_ = uvnode1_.z();
			node_.x(x_);
			node_.y(y_);
			node_.z(z_);
			//bsurface mark bot, top, left, right, front, back
			node_.markbsurface(1);
			if (jc == 0){
				node_.markbsurface(4);
			}
			else if (jc == nny - 1){
				node_.markbsurface(5);
			}
			if (ic == 0){
				node_.markbsurface(2);
			}
			else if (ic == nnx - 1){
				node_.markbsurface(3);
			}
			flag.push_back(i);
			nodelist.push_back(node_);
		}
	}
	meshinfo.cpgdimension(2, nlayer);
	nex = meshinfo.cpgdimension(0);
	ney = meshinfo.cpgdimension(1);
	nez = meshinfo.cpgdimension(2);

	cpgnodematrix.resize(nex + 1);
	for (i = 0; i < nex + 1; i++){
		cpgnodematrix[i].resize(ney + 1);
	}
	for (i = 0; i < nex + 1; i++){
		for (j = 0; j < ney + 1; j++){
			cpgnodematrix[i][j].resize(nez + 1);
		}
	}

	for (k = 0; k < nez+1; k++){
		for (j = 0; j < ney+1; j++){
			for (i = 0; i < nex+1; i++){
				cpgnodematrix[i][j][k] = k*(nex + 1)*(ney + 1) + j*(nex + 1) + i;
			}
		}
	}

	cpgelementmatrix.resize(nex);
	for (i = 0; i < nex; i++){
		cpgelementmatrix[i].resize(ney);
	}
	for (i = 0; i < nex; i++){
		for (j = 0; j < ney; j++){
			cpgelementmatrix[i][j].resize(nez);
		}
	}


	for (k = 0; k < nez; k++){
		for (j = 0; j < ney; j++){
			for (i = 0; i < nex; i++){
				cpgelementmatrix[i][j][k] = k*nex*ney + j*nex + i;
			}
		}
	}

	//build connectivity and neighbors for corner-point grid x, y ,z direction in order

	for (k = 0; k < nez; k++){
		for (j = 0; j < ney; j++){
			for (i = 0; i < nex; i++){
				//neighbors
				nc = cpgelementlist.size();
				if (i > 0){
					cpgelement.neighbor(0, nc - 1);
				}
				if (i < nex - 1){
					cpgelement.neighbor(1, nc + 1);
				}
				if (j>0){
					cpgelement.neighbor(2, nc - nex);
				}
				if (j < ney - 1){
					cpgelement.neighbor(3, nc + nex);
				}
				if (k>0){
					cpgelement.neighbor(4, nc - nex*ney);
				}
				if (k < nez - 1){
					cpgelement.neighbor(5, nc + nex*ney);
				}

				i0 = cpgnodematrix[i][j][k];
				i1 = cpgnodematrix[i + 1][j][k];
				i2 = cpgnodematrix[i][j + 1][k];
				i3 = cpgnodematrix[i + 1][j + 1][k];
				i4 = cpgnodematrix[i][j][k + 1];
				i5 = cpgnodematrix[i + 1][j][k + 1];
				i6 = cpgnodematrix[i][j + 1][k + 1];
				i7 = cpgnodematrix[i + 1][j + 1][k + 1];
				cpgelement.node(0, i0);//order:: lb--rb--lt--rt then k+1 layer
				cpgelement.node(1, i1);
				cpgelement.node(2, i2);
				cpgelement.node(3, i3);
				cpgelement.node(4, i4);
				cpgelement.node(5, i5);
				cpgelement.node(6, i6);
				cpgelement.node(7, i7);
				cpgelement.layer(flag[k*(nex + 1)*(ney + 1) + j*(nex + 1) + i]);//use bottom points to flag layers (from 0 bot2top)
				x_ = (nodelist[i0].x() + nodelist[i1].x() + nodelist[i2].x() + nodelist[i3].x() + nodelist[i4].x() + nodelist[i5].x() + nodelist[i6].x() + nodelist[i7].x()) / 8.0;
				y_ = (nodelist[i0].y() + nodelist[i1].y() + nodelist[i2].y() + nodelist[i3].y() + nodelist[i4].y() + nodelist[i5].y() + nodelist[i6].y() + nodelist[i7].y()) / 8.0;
				z_ = (nodelist[i0].z() + nodelist[i1].z() + nodelist[i2].z() + nodelist[i3].z() + nodelist[i4].z() + nodelist[i5].z() + nodelist[i6].z() + nodelist[i7].z()) / 8.0;
				node_.x(x_);
				node_.y(y_);
				node_.z(z_);
				cpgelement.centnode(node_);
				//volume = 6 tetrahedrons
				vol_ = 0;
				for (ic = 0; ic < 6; ic++){
					if (ic == 0){
						nn[0] = i0;
						nn[1] = i1;
						nn[2] = i2;
						nn[3] = i4;
					}
					else if (ic == 1){
						nn[0] = i1;
						nn[1] = i2;
						nn[2] = i4;
						nn[3] = i6;
					}
					else if (ic == 2){
						nn[0] = i1;
						nn[1] = i4;
						nn[2] = i5;
						nn[3] = i6;
					}
					else if (ic == 3){
						nn[0] = i1;
						nn[1] = i2;
						nn[2] = i3;
						nn[3] = i7;
					}
					else if (ic == 4){
						nn[0] = i1;
						nn[1] = i2;
						nn[2] = i5;
						nn[3] = i7;
					}
					else if (ic == 5){
						nn[0] = i2;
						nn[1] = i5;
						nn[2] = i7;
						nn[3] = i6;
					}
					for (ii = 0; ii < 4; ii++){
						xx4[ii] = nodelist[nn[ii]].x();
						yy4[ii] = nodelist[nn[ii]].y();
						zz4[ii] = nodelist[nn[ii]].z();
					}
					vol_ = vol_ + mathzz_.volume_tetra_positive(xx4, yy4, zz4);
				}
				cpgelement.volume(vol_);
				////6 faces
				//for (ic = 0; ic < 6; ic++){ //need change to adapt to cpgfacelist
				//	if (ic == 0){
				//		x0 = nodelist[i0].x(); y0 = nodelist[i0].y(); z0 = nodelist[i0].z();
				//		x1 = nodelist[i2].x(); y1 = nodelist[i2].y(); z1 = nodelist[i2].z();
				//		x2 = nodelist[i4].x(); y2 = nodelist[i4].y(); z2 = nodelist[i4].z();
				//		x3 = nodelist[i6].x(); y3 = nodelist[i6].y(); z3 = nodelist[i6].z();
				//	}
				//	else if (ic == 1){
				//		x0 = nodelist[i1].x(); y0 = nodelist[i1].y(); z0 = nodelist[i1].z();
				//		x1 = nodelist[i3].x(); y1 = nodelist[i3].y(); z1 = nodelist[i3].z();
				//		x2 = nodelist[i5].x(); y2 = nodelist[i5].y(); z2 = nodelist[i5].z();
				//		x3 = nodelist[i7].x(); y3 = nodelist[i7].y(); z3 = nodelist[i7].z();
				//	}
				//	else if (ic == 2){
				//		x0 = nodelist[i0].x(); y0 = nodelist[i0].y(); z0 = nodelist[i0].z();
				//		x1 = nodelist[i1].x(); y1 = nodelist[i1].y(); z1 = nodelist[i1].z();
				//		x2 = nodelist[i4].x(); y2 = nodelist[i4].y(); z2 = nodelist[i4].z();
				//		x3 = nodelist[i5].x(); y3 = nodelist[i5].y(); z3 = nodelist[i5].z();
				//	}
				//	else if (ic == 3){
				//		x0 = nodelist[i2].x(); y0 = nodelist[i2].y(); z0 = nodelist[i2].z();
				//		x1 = nodelist[i3].x(); y1 = nodelist[i3].y(); z1 = nodelist[i3].z();
				//		x2 = nodelist[i6].x(); y2 = nodelist[i6].y(); z2 = nodelist[i6].z();
				//		x3 = nodelist[i7].x(); y3 = nodelist[i7].y(); z3 = nodelist[i7].z();
				//	}
				//	else if (ic == 4){
				//		x0 = nodelist[i0].x(); y0 = nodelist[i0].y(); z0 = nodelist[i0].z();
				//		x1 = nodelist[i1].x(); y1 = nodelist[i1].y(); z1 = nodelist[i1].z();
				//		x2 = nodelist[i2].x(); y2 = nodelist[i2].y(); z2 = nodelist[i2].z();
				//		x3 = nodelist[i3].x(); y3 = nodelist[i3].y(); z3 = nodelist[i3].z();
				//	}
				//	else if (ic == 5){
				//		x0 = nodelist[i4].x(); y0 = nodelist[i4].y(); z0 = nodelist[i4].z();
				//		x1 = nodelist[i5].x(); y1 = nodelist[i5].y(); z1 = nodelist[i5].z();
				//		x2 = nodelist[i6].x(); y2 = nodelist[i6].y(); z2 = nodelist[i6].z();
				//		x3 = nodelist[i7].x(); y3 = nodelist[i7].y(); z3 = nodelist[i7].z();
				//	}
				//	x_ = (x0 + x1 + x2 + x3) / 4.0;
				//	y_ = (y0 + y1 + y2 + y3) / 4.0;
				//	z_ = (z0 + z1 + z2 + z3) / 4.0;
				//	node_.x(x_);
				//	node_.y(y_);
				//	node_.z(z_);
				//	cpgface_.centnode(node_);

				//	xx[0] = x0;
				//	yy[0] = y0;
				//	zz[0] = z0;

				//	xx[1] = x1;
				//	yy[1] = y1;
				//	zz[1] = z1;

				//	xx[2] = x2;
				//	yy[2] = y2;
				//	zz[2] = z2;

				//	area1 = mathzz_.area_triangle(xx, yy, zz);

				//	xx[0] = x3;
				//	yy[0] = y3;
				//	zz[0] = z3;

				//	area2 = mathzz_.area_triangle(xx, yy, zz);

				//	cpgface_.area(area1 + area2);

				//	edgevec1[0] = xx[1] - xx[0];
				//	edgevec1[1] = yy[1] - yy[0];
				//	edgevec1[2] = zz[1] - zz[0];
				//	edgevec2[0] = xx[2] - xx[0];
				//	edgevec2[1] = yy[2] - yy[0];
				//	edgevec2[2] = zz[2] - zz[0];
				//	unitnormalvec_ = mathzz_.unitnormal(edgevec1, edgevec2); //not nessisarily point to outside of element need abs
				//	xikvec_[0] = cpgface_.centnode().x() - cpgelement.centnode().x();
				//	xikvec_[1] = cpgface_.centnode().y() - cpgelement.centnode().y();
				//	xikvec_[2] = cpgface_.centnode().z() - cpgelement.centnode().z();
				//	if (mathzz_.dotproduct(xikvec_, unitnormalvec_) < 0){
				//		unitnormalvec_[0] = -unitnormalvec_[0];
				//		unitnormalvec_[1] = -unitnormalvec_[1];
				//		unitnormalvec_[2] = -unitnormalvec_[2];
				//	}
				//	cpgface_.unitnormalvec(unitnormalvec_);
				//	cpgfacelist.push_back(cpgface_);
				//	cpgelement.cpgface(ic, cpgfacelist.size() - 1); //boundary cpgfaces are included
				//}
				cpgelementlist.push_back(cpgelement);
				cpgelement.clear();
			}
		}
	}

	//for boundary facets
	bfacet_.numberofvertex(4);
	k = 0; //bottom
	bfacet_.markbsurface(0);
	for (j = 0; j < ney; j++){
		for (i = 0; i < nex; i++){
			bfacet_.node(0, cpgnodematrix[i][j][k]);
			bfacet_.node(1, cpgnodematrix[i+1][j][k]);
			bfacet_.node(2, cpgnodematrix[i][j+1][k]);
			bfacet_.node(3, cpgnodematrix[i+1][j+1][k]);
			bfacet_.element(cpgelementmatrix[i][j][k]);
			bfacetlist.push_back(bfacet_);
		}
	}
	k = nez;//top
	bfacet_.markbsurface(1);
	for (j = 0; j < ney; j++){
		for (i = 0; i < nex; i++){
			bfacet_.node(0, cpgnodematrix[i][j][k]);
			bfacet_.node(1, cpgnodematrix[i + 1][j][k]);
			bfacet_.node(2, cpgnodematrix[i][j + 1][k]);
			bfacet_.node(3, cpgnodematrix[i + 1][j + 1][k]);
			bfacet_.element(cpgelementmatrix[i][j][k-1]);
			bfacetlist.push_back(bfacet_);
		}
	}
	i = 0;//left
	bfacet_.markbsurface(2);
	for (k = 0; k < nez; k++){
		for (j = 0; j < ney; j++){
			bfacet_.node(0, cpgnodematrix[i][j][k]);
			bfacet_.node(1, cpgnodematrix[i][j+1][k]);
			bfacet_.node(2, cpgnodematrix[i][j][k+1]);
			bfacet_.node(3, cpgnodematrix[i][j+1][k+1]);
			bfacet_.element(cpgelementmatrix[i][j][k]);
			bfacetlist.push_back(bfacet_);
		}
	}
	i = nex;//right
	bfacet_.markbsurface(3);
	for (k = 0; k < nez; k++){
		for (j = 0; j < ney; j++){
			bfacet_.node(0, cpgnodematrix[i][j][k]);
			bfacet_.node(1, cpgnodematrix[i][j + 1][k]);
			bfacet_.node(2, cpgnodematrix[i][j][k + 1]);
			bfacet_.node(3, cpgnodematrix[i][j + 1][k + 1]);
			bfacet_.element(cpgelementmatrix[i-1][j][k]);
			bfacetlist.push_back(bfacet_);
		}
	}
	j = 0;//front
	bfacet_.markbsurface(4);
	for (k = 0; k < nez; k++){
		for (i = 0; i < nex; i++){
			bfacet_.node(0, cpgnodematrix[i][j][k]);
			bfacet_.node(1, cpgnodematrix[i+1][j][k]);
			bfacet_.node(2, cpgnodematrix[i][j][k + 1]);
			bfacet_.node(3, cpgnodematrix[i+1][j][k + 1]);
			bfacet_.element(cpgelementmatrix[i][j][k]);
			bfacetlist.push_back(bfacet_);
		}
	}
	j = ney;//back
	bfacet_.markbsurface(5);
	for (k = 0; k < nez; k++){
		for (i = 0; i < nex; i++){
			bfacet_.node(0, cpgnodematrix[i][j][k]);
			bfacet_.node(1, cpgnodematrix[i + 1][j][k]);
			bfacet_.node(2, cpgnodematrix[i][j][k + 1]);
			bfacet_.node(3, cpgnodematrix[i + 1][j][k + 1]);
			bfacet_.element(cpgelementmatrix[i][j-1][k]);
			bfacetlist.push_back(bfacet_);
		}
	}
	nbfacet = bfacetlist.size();


	/*
	for (k = 0; k < nez; k++){
	for (j = 0; j < ney; j++){
	for (i = 0; i < nex; i++){
	cpgelement.node(0, k*(nex + 1)*(ney + 1) + j*(nex + 1) + i);
	cpgelement.node(1, k*(nex + 1)*(ney + 1) + j*(nex + 1) + i + 1);
	cpgelement.node(2, k*(nex + 1)*(ney + 1) + (j + 1)*(nex + 1) + i);
	cpgelement.node(3, k*(nex + 1)*(ney + 1) + (j + 1)*(nex + 1) + i + 1);

	cpgelement.node(4, (k + 1)*(nex + 1)*(ney + 1) + j*(nex + 1) + i);
	cpgelement.node(5, (k + 1)*(nex + 1)*(ney + 1) + j*(nex + 1) + i + 1);
	cpgelement.node(6, (k + 1)*(nex + 1)*(ney + 1) + (j + 1)*(nex + 1) + i);
	cpgelement.node(7, (k + 1)*(nex + 1)*(ney + 1) + (j + 1)*(nex + 1) + i + 1);
	cpgelement.layer(flag[k*(nex + 1)*(ney + 1) + j*(nex + 1) + i]);
	cpgelementlist.push_back(cpgelement);
	}
	}
	}
	*/
}

void REGION::calltetgen_in2out(){
	std::cout << "TetGen starts..." << std::endl;
	double garea = 2.0*meshinfo.guideedgelength()*meshinfo.guideedgelength()*meshinfo.guideedgelength();
	char* tetgencommandb;
	std::string c1(tetgencommand);
	std::string c2 = std::to_string(garea);
	c1 = c1 + c2;
	const char* cc = c1.c_str();
	tetgencommandb = const_cast<char*>(cc);
	tetrahedralize(tetgencommand, &in, &out);
	//tetrahedralize(tetgencommandb, &in, &out);
	std::cout << "TetGen Ends" << std::endl;
}



void REGION::calltetgenrefine_mid2out(tetgenio mid){
	if (addin.numberofpoints > 0){
		char* tetgencommand2 = "rq1.5/10iAnn";

		tetrahedralize(tetgencommand2, &mid, &out, &addin);
	}

}

void REGION::calltetgenrefine_mid2out(){
	if (addin.numberofpoints > 0){
		/*char* tetgencommand2 = "riAnnVa";
		double garea = 1.0*meshinfo.guideedgelength()*meshinfo.guideedgelength()*meshinfo.guideedgelength();
		char* tetgencommandb;
		std::string c1(tetgencommand2);
		std::string c2 = std::to_string(garea);
		c1 = c1 + c2;
		const char* cc = c1.c_str();
		tetgencommandb = const_cast<char*>(cc);*/

		char* tetgencommand2 = "rq1.5/10iAnnV";

		tetrahedralize(tetgencommand2, &mid, &out, &addin);
	}
	/*char* tetgencommand2 = "rq2.0/10mAnn";
	tetrahedralize(tetgencommand2, &out, &out);*/
}
void REGION::calltetgen_in2mid(){
	double garea = 1.0*meshinfo.guideedgelength()*meshinfo.guideedgelength()*meshinfo.guideedgelength();
	char* tetgencommandb;
	std::string c1(tetgencommand);
	std::string c2 = std::to_string(garea);
	c1 = c1 + c2;
	const char* cc = c1.c_str();
	tetgencommandb = const_cast<char*>(cc);
	tetrahedralize(tetgencommandb, &in, &mid);
}
void REGION::zscale(double d){
	zscale_ = d;
}
void REGION::tetgen2region(){
	int i, j, ie, count, nnode, nelement;
	NODE node_;
	TETRAHEDRON tetrahedron_;
	FACET bfacet_;
	nnode = out.numberofpoints;
	nelement = out.numberoftetrahedra;
	for (i = 0; i < nnode; i++){
		node_.x(out.pointlist[i * 3]);
		node_.y(out.pointlist[i * 3 + 1]);
		node_.z(out.pointlist[i * 3 + 2]*zscale_);
		//node_.markbsurface(out.pointmarkerlist[i]);//not complete since nodes on edges have mark=-1
		//if (out.pointmarkerlist[i] <= -10){ //boundary marker of well nodes
			//node_.markwell(-out.pointmarkerlist[i] - 10);
		//}
		nodelist.push_back(node_);
	}

	for (i = 0; i < nelement; i++){
		for (j = 0; j < 4; j++){
			tetrahedron_.node(j, out.tetrahedronlist[i * 4 + j]);
			tetrahedron_.neighbor(j, out.neighborlist[i * 4 + j]); //neighboring element for each element
		}
		tetrahedron_.markregion(out.tetrahedronattributelist[i]); //elemental attribute
		elementlist.push_back(tetrahedron_);
	}
	elementneighbormark_ = 1; //remember elemental neighbors have been recorded
	bfacetelementmark_ = 1;
	bfacet_.numberofvertex(3); //3 for triangular facets

	//std::ofstream outf;
	//outf.open("bfacemark.txt");
	/*outf << "parasurfacelistsize " << parasurfacelist.size() << std::endl;
	for (i = 0; i < out.numberoftrifaces; i++){
		outf << out.trifacemarkerlist[i] << std::endl;
	}*/

	if (inputsurfacetype_ == 1){
		for (i = 0; i < out.numberoftrifaces; i++){ //out.numberoftrifaces also includes trifaces on internal input surfaces
			if ((inputsurfacetype_ != 1 && out.trifacemarkerlist[i] >= 0) || (inputsurfacetype_ == 1 && out.trifacemarkerlist[i] >= 0 && out.trifacemarkerlist[i] < (parasurfacelist.size() + 1))){ //if not internal surface;
				bfacet_.markbsurface(out.trifacemarkerlist[i] - 1);//trifacetmarkerlist starts from 1
				for (j = 0; j < 3; j++){
					bfacet_.node(j, out.trifacelist[3 * i + j]);
				}

				//face2tetlist here only contains boundary faces. In tetgen, don't output internal faces.
				count = 0;
				ie = out.face2tetlist[2 * i];
				if (ie >= 0){
					count++;
					bfacet_.element(ie);
				}
				ie = out.face2tetlist[2 * i + 1];
				if (ie >= 0){
					count++;
					bfacet_.element(ie);
				}
				if (count < 1){
					std::cout << "error output faces not on boundary examine out.face2tetlist" << std::endl;
					std::cout << i << " " << out.trifacemarkerlist[i] << std::endl;
					std::cout << count << " " << out.face2tetlist[2 * i] << " " << out.face2tetlist[2 * i + 1] << std::endl;
					std::cout << bfacet_.node(0) << " " << bfacet_.node(1) << " " << bfacet_.node(2) << std::endl;
					std::exit(EXIT_FAILURE);
				}
				bfacetlist.push_back(bfacet_);
			}
		}
	}
	else if (inputsurfacetype_ == 2){
		for (i = 0; i < out.numberoftrifaces; i++){ //out.numberoftrifaces also includes trifaces on internal input surfaces
			if ((inputsurfacetype_ != 1 && out.trifacemarkerlist[i] >= 0) || (inputsurfacetype_ == 1 && out.trifacemarkerlist[i] >= 0 && out.trifacemarkerlist[i] < (parasurfacelist.size() + 1))){ //if not internal surface;
				bfacet_.markbsurface(out.trifacemarkerlist[i]);//trifacetmarkerlist starts from 0
				for (j = 0; j < 3; j++){
					bfacet_.node(j, out.trifacelist[3 * i + j]);
				}

				//face2tetlist here only contains boundary faces. In tetgen, don't output internal faces.
				count = 0;
				ie = out.face2tetlist[2 * i];
				if (ie >= 0){
					count++;
					bfacet_.element(ie);
				}
				ie = out.face2tetlist[2 * i + 1];
				if (ie >= 0){
					count++;
					bfacet_.element(ie);
				}
				if (count < 1){
					std::cout << "error output faces not on boundary examine out.face2tetlist" << std::endl;
					std::cout << i << " " << out.trifacemarkerlist[i] << std::endl;
					std::cout << count << " " << out.face2tetlist[2 * i] << " " << out.face2tetlist[2 * i + 1] << std::endl;
					std::cout << bfacet_.node(0) << " " << bfacet_.node(1) << " " << bfacet_.node(2) << std::endl;
					std::exit(EXIT_FAILURE);
				}
				bfacetlist.push_back(bfacet_);
			}
		}
	}
	nbfacet = bfacetlist.size();

}

void REGION::tetgen2region_car(){
	int i, j, ie, count, nnode, nelement;
	NODE node_;
	TETRAHEDRON tetrahedron_;
	FACET bfacet_;
	nnode = out.numberofpoints;
	nelement = out.numberoftetrahedra;
	nodelist.clear();
	for (i = 0; i < nnode; i++){
		node_.x(out.pointlist[i * 3]);
		node_.y(out.pointlist[i * 3 + 1]);
		node_.z(out.pointlist[i * 3 + 2] * zscale_);
		nodelist.push_back(node_);
	}

	for (i = 0; i < nelement; i++){
		for (j = 0; j < 4; j++){
			tetrahedron_.node(j, out.tetrahedronlist[i * 4 + j]);
			tetrahedron_.neighbor(j, out.neighborlist[i * 4 + j]); //neighboring element for each element
		}
		//tetrahedron_.markregion(out.tetrahedronattributelist[i]); //elemental attribute
		elementlist.push_back(tetrahedron_);
	}
	elementneighbormark_ = 1; //remember elemental neighbors have been recorded


}

void REGION::tetrahedralmesh_postprocessCVFE(){
	std::cout << "tetrahedral mesh post-processing CVFE" << std::endl;
	bsurfacemarkface2node();
	computeelementalvolume();
	//postprocessing for edge-based data structure start
	nodalcontrolvolumeandelementsunode();
	if (bfacetelementmark_ == 0){
		bfacetelement();
	}
	nodesunode();
	buildedgesandedgesunode();
	computeedgelength();
	//postprocessing for edge-based data structure end
	bfacetareaandnormal();
	std::cout << "tetrahedral mesh post-processing CVFE end" << std::endl;
}

void REGION::CPG_postprocessCVFE(){
	std::cout << "Corner Point Grid post-processing CVFE" << std::endl;
	buildedgesandedgesunodeCPG();
	bfacetareaandnormalCPG();
	std::cout << "Corner Point Grid post-processing CVFE end" << std::endl;
}

void REGION::bfacetareaandnormal(){
	int i, j, ie, jnode;
	double area, edgevec1[3], edgevec2[3], xx3[3], yy3[3], zz3[3], testvec[3];
	std::vector<double> unitnormalvec_;
	std::cout << "Computing boundary faces and normals" << std::endl;
	for (i = 0; i < nbfacet; i++){
		for (j = 0; j < 3; j++){
			xx3[j] = nodelist[bfacetlist[i].node(j)].x();
			yy3[j] = nodelist[bfacetlist[i].node(j)].y();
			zz3[j] = nodelist[bfacetlist[i].node(j)].z();
		}
		area = mathzz_.area_triangle(xx3, yy3, zz3); //always positive (from one side it is clockwise, the other side anti-clockwise
		bfacetlist[i].area(area); //signed area
		//calculate bfacet unit normal vectors that not necessarilly point to the inside of the geometry
		edgevec1[0] = xx3[1] - xx3[0];
		edgevec1[1] = yy3[1] - yy3[0];
		edgevec1[2] = zz3[1] - zz3[0];
		edgevec2[0] = xx3[2] - xx3[0];
		edgevec2[1] = yy3[2] - yy3[0];
		edgevec2[2] = zz3[2] - zz3[0];
		unitnormalvec_ = mathzz_.unitnormal(edgevec1, edgevec2);
		ie = bfacetlist[i].element();
		for (j = 0; j < 4; j++){
			jnode = elementlist[ie].node(j);
			if (jnode != bfacetlist[i].node(0) && jnode != bfacetlist[i].node(1) && jnode != bfacetlist[i].node(2)){
				testvec[0] = nodelist[jnode].x() - xx3[2];
				testvec[1] = nodelist[jnode].y() - yy3[2];
				testvec[2] = nodelist[jnode].z() - zz3[2];
				if ((unitnormalvec_[0] * testvec[0] + unitnormalvec_[1] * testvec[1] + unitnormalvec_[2] * testvec[2]) < 0){
					unitnormalvec_[0] = -unitnormalvec_[0];
					unitnormalvec_[1] = -unitnormalvec_[1];
					unitnormalvec_[2] = -unitnormalvec_[2];
				}
			}
		}
		bfacetlist[i].unitnormalvec(unitnormalvec_); //assign unit normal vector to the bfacet
	}
	std::cout << "DONE" << std::endl;
}

void REGION::bfacetareaandnormalCPG(){
	int i, j, ie, jnode, imark, inward;
	double area, edgevec1[3], edgevec2[3], xx3[3], yy3[3], zz3[3], testvec[3];
	std::vector<double> unitnormalvec_;
	for (i = 0; i < nbfacet; i++){
		imark = bfacetlist[i].markbsurface();
		for (j = 0; j < 3; j++){
			xx3[j] = nodelist[bfacetlist[i].node(j)].x();
			yy3[j] = nodelist[bfacetlist[i].node(j)].y();
			zz3[j] = nodelist[bfacetlist[i].node(j)].z();
		}
		//calculate bfacet unit normal vector based on half of the bfacet that is a triangle
		edgevec1[0] = xx3[1] - xx3[0];
		edgevec1[1] = yy3[1] - yy3[0];
		edgevec1[2] = zz3[1] - zz3[0];
		edgevec2[0] = xx3[2] - xx3[0];
		edgevec2[1] = yy3[2] - yy3[0];
		edgevec2[2] = zz3[2] - zz3[0];
		unitnormalvec_ = mathzz_.unitnormal(edgevec1, edgevec2);
		if (imark == 1 || imark == 3 || imark == 4){ //reason is in blue notebook
			unitnormalvec_[0] = -unitnormalvec_[0];
			unitnormalvec_[1] = -unitnormalvec_[1];
			unitnormalvec_[2] = -unitnormalvec_[2];
		}
		bfacetlist[i].unitnormalvec(unitnormalvec_); //assign unit normal vector to the bfacet

		area = mathzz_.area_triangle(xx3, yy3, zz3); //always positive (from one side it is clockwise, the other side anti-clockwise
		//the other half bfacet
		xx3[0] = nodelist[bfacetlist[i].node(3)].x();
		yy3[0] = nodelist[bfacetlist[i].node(3)].y();
		zz3[0] = nodelist[bfacetlist[i].node(3)].z();
		area = area + mathzz_.area_triangle(xx3, yy3, zz3);
		bfacetlist[i].area(area); //positive area
	}
}

void REGION::bsurfacemarkface2node(){
	int i, j, mark_, nnode;
	/*
	allocate points' marks from facets' marks; one BC at a time so that geometrical boundary edge has only 1 BC
	at the moment 2 BC in total
	firstly, all points on edges (where input surfaces intersect) has mark -1. Then after boundary points' marks are
	re-allocated according to bfacets' marks, the only points with mark -1 are on the intertal edge (the source well)
	we have chosen points' with boundary mark, then because the end points of the well are with mark 0 if no marks are input. So
	we input -1 for the end points' marks.
	if more than 1 wells, then they can be identified by their locations
	*/
	int type_;
	nnode = nodelist.size();
	bfacetBC.resize(4);
	std::cout << "assign faces and nodes boundary marks" << std::endl;
	//find and store all Dirichelt, Neumann and no-flow trifaces
	for (i = 0; i < nbfacet; i++){
		mark_ = bfacetlist[i].markbsurface();
		type_ = bsurfacelist[mark_].type();
		if (type_ == 0){
			bfacetBC[0].push_back(i); //store noflow
		}
		if (type_ == 1){
			bfacetBC[1].push_back(i); //store Dirichlet
		}
		if (type_ == 2){
			bfacetBC[2].push_back(i); //store Neumann
		}
		if (type_ == 3){
			bfacetBC[3].push_back(i);
		}
	}

	//use trifaces marks to redefine nodes' marks to avoid negative marks on edges

	//firstly no-flow boundary
	for (i = 0; i < bfacetBC[0].size(); i++){
		j = bfacetBC[0][i]; //number of bfacet
		mark_ = bfacetlist[j].markbsurface(); //which boundary
		nodelist[bfacetlist[j].node(0)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(1)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(2)].markbsurface(mark_);
	}

	//then Neumann boundary
	for (i = 0; i < bfacetBC[2].size(); i++){
		j = bfacetBC[2][i]; //number of bfacet
		mark_ = bfacetlist[j].markbsurface();
		nodelist[bfacetlist[j].node(0)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(1)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(2)].markbsurface(mark_);
	}

	//Dirichlet
	for (i = 0; i < bfacetBC[1].size(); i++){
		j = bfacetBC[1][i]; //number of bfacet
		mark_ = bfacetlist[j].markbsurface();
		nodelist[bfacetlist[j].node(0)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(1)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(2)].markbsurface(mark_);
	}
	for (i = 0; i < bfacetBC[3].size(); i++){
		j = bfacetBC[3][i]; //number of bfacet
		mark_ = bfacetlist[j].markbsurface();
		nodelist[bfacetlist[j].node(0)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(1)].markbsurface(mark_);
		nodelist[bfacetlist[j].node(2)].markbsurface(mark_);
	}
	//the remaining nodes with mark -1 are definitely not on outside edges but inside edges

	//boundary node list
	for (i = 0; i < nnode; i++){
		if (nodelist[i].markbsurface() >= 0){
			bnodelist.push_back(i);
		}
	}
	nbnode = bnodelist.size();
	std::cout << "DONE" << std::endl;

}

void REGION::computeelementalvolume(){
	int i, j, nelement;
	double xx4[4], yy4[4], zz4[4], vol;
	double totalvolume;
	nelement = elementlist.size();
	if (meshinfo.type() == 1){ //unstructured tetrahedral case
		std::cout << "computing elements' volume (CPG or unstructured tetrahedral)" << std::endl;
		totalvolume = 0;
		for (i = 0; i < nelement; i++){
			for (j = 0; j < 4; j++){
				xx4[j] = nodelist[elementlist[i].node(j)].x();
				yy4[j] = nodelist[elementlist[i].node(j)].y();
				zz4[j] = nodelist[elementlist[i].node(j)].z();
			};
			vol = mathzz_.volume_tetra_positive(xx4, yy4, zz4); //same as abs((r12xr13)*r14)
			elementlist[i].volume(vol);

			totalvolume = totalvolume + elementlist[i].volume();
		}
	}
	else{
		std::cout << "error: computetetelementalvolume is only for tetrahedral mesh" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::cout << "DONE totalvolume= " << totalvolume << std::endl;
}

void REGION::computeelementalfacenormalandarea(){
	int i, n0, n1, n2, n3, ie, nelement;
	std::cout << "calculate and store normal vectors and areas for each face of every element" << std::endl;
	double area, edgevec1[3], edgevec2[3];
	std::vector<double> vec_, vec2_;
	//calculate and store normal vectors and areas for each face of every element
	vec2_.resize(3);
	nelement = elementlist.size();
	for (ie = 0; ie < nelement; ie++){//element faces are organised to make sure normals are outwards
		for (i = 0; i < 4; i++){
			n0 = elementlist[ie].face(i, 0);
			n1 = elementlist[ie].face(i, 1);
			n2 = elementlist[ie].face(i, 2);
			edgevec1[0] = nodelist[n1].x() - nodelist[n0].x();
			edgevec1[1] = nodelist[n1].y() - nodelist[n0].y();
			edgevec1[2] = nodelist[n1].z() - nodelist[n0].z();
			edgevec2[0] = nodelist[n2].x() - nodelist[n0].x();
			edgevec2[1] = nodelist[n2].y() - nodelist[n0].y();
			edgevec2[2] = nodelist[n2].z() - nodelist[n0].z();
			vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
			elementlist[ie].face_normalx(i, vec_[0]);
			elementlist[ie].face_normaly(i, vec_[1]);
			elementlist[ie].face_normalz(i, vec_[2]);
			//checking
			n3 = elementlist[ie].node(i);
			vec2_[0] = nodelist[n3].x() - nodelist[n0].x();
			vec2_[1] = nodelist[n3].y() - nodelist[n0].y();
			vec2_[2] = nodelist[n3].z() - nodelist[n0].z();
			if (mathzz_.dotproduct(vec_, vec2_) > 0){
				std::cout << "wrong normal vector direction" << std::endl;
			}
			area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
			elementlist[ie].face_area(i, area);
		}
	}
}

void REGION::nodalcontrolvolumeandelementsunode(){
	int j, ie, jnode, nelement;
	double volume_, dualvolume_;
	nelement = elementlist.size();
	if (meshinfo.type() == 1){
		std::cout << "calculate dual volume and find elements surrounding nodes for tetrahedral mesh" << std::endl;
		for (ie = 0; ie < nelement; ie++){
			volume_ = elementlist[ie].volume();
			for (j = 0; j < 4; j++){
				jnode = elementlist[ie].node(j);
				if (jnode == 243){
					izz = 1;
				}
				dualvolume_ = nodelist[jnode].dualvolume();
				nodelist[jnode].dualvolume(dualvolume_ + 0.25*volume_);
				nodelist[jnode].addelement(ie);
			}
		}
		std::cout << "DONE" << std::endl;
	}
}

void REGION::bfacetelement(){
	int i, j, ie, jnode, ii, nelement;
	std::vector<int> mark;
	nelement = elementlist.size();
	mark.resize(nelement);
	for (i = 0; i < nbfacet; i++){
		mark.clear();
		mark.resize(nelement);
		for (j = 0; j < 3; j++){
			jnode = bfacetlist[i].node(j);
			for (ii = 0; ii < nodelist[jnode].numberofelement(); ii++){
				ie = nodelist[jnode].element(ii);
				mark[ie]++;
				if (mark[ie] == 3){
					bfacetlist[i].element(ie);
					break;
				}
			}
		}
	}
}

void REGION::elementsuelement(){
	int ie, iface, i, j, inode, je, jface, ii, iinode, icount, nlocalface, nlocalnode, nnode, nelement;
	std::vector<int> lpoin;
	nnode = nodelist.size();
	nelement = elementlist.size();
	lpoin.resize(nnode);
	nlocalface = 4;
	nlocalnode = 3;

	for (ie = 0; ie < nelement; ie++){
		for (iface = 0; iface < nlocalface; iface++){
			for (i = 0; i < nlocalnode; i++){
				inode = elementlist[ie].face(iface, i);
				lpoin[inode] = 1;
			}
			inode = elementlist[ie].face(iface, 0);
			for (j = 0; j < nodelist[inode].numberofelement(); j++){
				je = nodelist[inode].element(j);
				if (je != ie){
					for (jface = 0; jface < nlocalface; jface++){
						icount = 0;
						for (ii = 0; ii < nlocalnode; ii++){
							iinode = elementlist[je].face(jface, ii);
							icount = icount + lpoin[iinode];
						}
						if (icount == nlocalnode){
							elementlist[ie].neighbor(iface, je);
							for (ii = 0; ii < nlocalnode; ii++){
								iinode = elementlist[je].face(jface, ii);
								lpoin[iinode] = 0;
							}
							break; //when satisfy, stop running the rest of the loop
						}
					}
					if (icount == nlocalnode){
						break;
					}
				}
			}
		}
	}
}

void REGION::nodesunode(){
	int inode, jnode, i, j, ie, nnode;
	std::vector<int> marksunode;
	nnode = nodelist.size();
	marksunode.resize(nnode);
	std::cout << "Computing nodes surrounding nodes" << std::endl;
	for (inode = 0; inode < nnode; inode++){
		marksunode[inode] = -1;
	}

	for (inode = 0; inode < nnode; inode++){
		for (i = 0; i < nodelist[inode].numberofelement(); i++){
			ie = nodelist[inode].element(i);
			for (j = 0; j < 4; j++){
				jnode = elementlist[ie].node(j);
				if (jnode != inode && marksunode[jnode] != inode){
					nodelist[inode].addsunode(jnode);
					nodelist[inode].addmarksunode(0);
					marksunode[jnode] = inode;
				}
			}
		}
	}
	std::cout << "DONE" << std::endl;
}

void REGION::buildedgesandedgesunode(){
	int inode, isunode, imin, imax, iedge, i, j, jnode, nnode;
	EDGE edge_;
	std::cout << "Building edges and edges surrounding nodes" << std::endl;
	nnode = nodelist.size();
	for (inode = 0; inode < nnode; inode++){
		for (i = 0; i < nodelist[inode].numberofsunode(); i++){ //all neighbor nodes
			if (nodelist[inode].marksunode(i) == 0){ //if not calculated
				isunode = nodelist[inode].sunode(i);
				for (j = 0; j < nodelist[isunode].numberofsunode(); j++){ //nodes surrounding neighbors
					jnode = nodelist[isunode].sunode(j);
					if (jnode == inode){ //this is an edge
						nodelist[isunode].marksunode(j, 1); //mark jnode's neighbor
						imin = min(inode, isunode);
						imax = inode + isunode - imin;
						edge_.node(0, imin);
						edge_.node(1, imax);
						edgelist.push_back(edge_); //record edge
						iedge = edgelist.size() - 1;
						nodelist[imin].addedgeout(iedge); //record edge from node imin
						nodelist[imax].addedgein(iedge);
					}
				}
			}
		}
	}
	nedge = edgelist.size();
	std::cout << "DONE" << std::endl;
}

void REGION::buildedgesandedgesunodeCPG(){
	int nex, ney, nez, i, j, k, n[8], inode, iedge, imin, imax;
	EDGE edge_;

	nex = meshinfo.cpgdimension(0);
	ney = meshinfo.cpgdimension(1);
	nez = meshinfo.cpgdimension(2);
	//build edges bot to top, front to back, left to right from nodes so that min node is first node of edge
	for (k = 0; k < nez+1; k++){
		for (j = 0; j < ney+1; j++){
			for (i = 0; i < nex+1; i++){
				if (i < nex){ //not right boundary
					imin = cpgnodematrix[i][j][k];
					imax = cpgnodematrix[i + 1][j][k];
					edge_.node(0, imin);
					edge_.node(1, imax);
					edgelist.push_back(edge_); //record edge
					iedge = edgelist.size() - 1;
					nodelist[imin].addedgeout(iedge); //record edge from node imin
					nodelist[imax].addedgein(iedge);
				}
				if (j < ney){ //not back boundary
					imin = cpgnodematrix[i][j][k];
					imax = cpgnodematrix[i][j + 1][k];
					edge_.node(0, imin);
					edge_.node(1, imax);
					edgelist.push_back(edge_); //record edge
					iedge = edgelist.size() - 1;
					nodelist[imin].addedgeout(iedge); //record edge from node imin
					nodelist[imax].addedgein(iedge);
				}
				if (k < nez){ //not top boundary
					imin = cpgnodematrix[i][j][k];
					imax = cpgnodematrix[i][j][k + 1];
					edge_.node(0, imin);
					edge_.node(1, imax);
					edgelist.push_back(edge_); //record edge
					iedge = edgelist.size() - 1;
					nodelist[imin].addedgeout(iedge); //record edge from node imin
					nodelist[imax].addedgein(iedge);
				}
			}
		}
	}
	nedge = edgelist.size();
}

void REGION::properties_cvfe_sf(){ //for tetmesh
	int ie, inode, ii, i, j, mark_, nelement;
	double k_, kmin_, kmax_, porosity_, zmid_;
	nelement = elementlist.size();

	//set k and mu and porosity for elements and nodes (nodal k and mu only useful for visualisation; deactivated;)
	for (ie = 0; ie < nelement; ie++){
		mark_ = elementlist[ie].markregion(); //mark_ is from 0
		double kx_ = rocklist[mark_].k(0, 0);
		double ky_ = rocklist[mark_].k(1, 1);
		double kz_ = rocklist[mark_].k(2, 2);
		//use nonisotropic k tensor  initial value being zero defined in tetrahedron and node.cpp

		elementlist[ie].k(0, 0, kx_);
		elementlist[ie].k(1, 1, ky_);
		elementlist[ie].k(2, 2, kz_);

		porosity_ = rocklist[mark_].porosity();
		elementlist[ie].porosity(porosity_);
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			nodelist[inode].porosity(nodelist[inode].porosity() + porosity_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			nodelist[inode].k(0, 0, nodelist[inode].k(0, 0) + kx_* 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			nodelist[inode].k(1, 1, nodelist[inode].k(1, 1) + ky_* 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			nodelist[inode].k(2, 2, nodelist[inode].k(2, 2) + kz_* 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}

}

void REGION::properties_cvfe_sf_khscaledpaper(){ //for tetmesh
	int ie, inode, ii, i, j, mark_, nelement;
	double k_, kmin_, kmax_, porosity_, mu_o_, zmid_;
	nelement = elementlist.size();

	//region zmin, zmax and gradiant
	std::vector<double> zmin, zmax, zgrad;
	zmin.resize(rocklist.size());
	zmax.resize(rocklist.size());
	zgrad.resize(rocklist.size());

	for (i = 0; i < rocklist.size(); i++){
		zmin[i] = 1.0e+10;
		zmax[i] = -1.0e+10;
	}

	for (ie = 0; ie < nelement; ie++){
		int ir = elementlist[ie].markregion();
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			if (nodelist[inode].z()<zmin[ir]){
				zmin[ir] = nodelist[inode].z();
			}
			if (nodelist[inode].z()>zmax[ir]){
				zmax[ir] = nodelist[inode].z();
			}
		}
	}
	/*for (i = 0; i < rocklist.size(); i++){
		zgrad[i] = (rocklist[i].permhigh() - rocklist[i].permlow()) / (zmax[i] - zmin[i]);
	}*/

	//set k and mu and porosity for elements and nodes (nodal k and mu only useful for visualisation; deactivated;)
	for (ie = 0; ie < nelement; ie++){
		zmid_ = (nodelist[elementlist[ie].node(0)].z() + nodelist[elementlist[ie].node(1)].z() + nodelist[elementlist[ie].node(2)].z() + nodelist[elementlist[ie].node(3)].z()) / 4.0;
		mark_ = elementlist[ie].markregion(); //mark_ is from 0
		//k_ = rocklist[mark_].permlow() + (zmid_ - zmin[mark_])*zgrad[mark_];
		//use nonisotropic k tensor  initial value being zero defined in tetrahedron and node.cpp
		for (i = 0; i < 3; i++){ //isotropic case
			for (j = 0; j < 3; j++){
				if (i == j){
					elementlist[ie].k(i, j, k_);
					//for (ii = 0; ii < 4; ii++){
					//	inode = elementlist[ie].node(ii);
					//	nodelist[inode].k(i, j, nodelist[inode].k(i, j) + k_*0.25*elementlist[ie].volume() / nodelist[inode].dualvolume()); //k for node is defined
					//}
					if (i == 2 && j == 2){ //kv/kh!=1
						double kscalez_ = 0.1;
						elementlist[ie].k(i, j, k_*kscalez_);
					}
				}
			}
		}

		porosity_ = rocklist[mark_].porosity();
		elementlist[ie].porosity(porosity_);
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			nodelist[inode].porosity(nodelist[inode].porosity() + porosity_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			//nodelist[inode].mu_o(nodelist[inode].mu_o() + mu_o_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}

}

void REGION::properties_cvfe_sf_randompaper(){ //for tetmesh random log normal distribution
	int ie, inode, ii, i, j, mark_, nelement;
	double k_, k2_, kmin_, kmax_, porosity_, mu_o_, zmid_;

	std::default_random_engine generator;
	std::normal_distribution<double> perm_distribution(log(500), 1.1);
	std::normal_distribution<double> perm2_distribution(log(50), 0.3);
	std::normal_distribution<double> poro_distribution(0.2, 0.01);
	nelement = elementlist.size();

	//set k and mu and porosity for elements and nodes (nodal k and mu only useful for visualisation; deactivated;)
	for (ie = 0; ie < nelement; ie++){
		mark_ = elementlist[ie].markregion(); //mark_ is from 0
		k_ = perm_distribution(generator);
		k2_ = perm2_distribution(generator);
		porosity_ = poro_distribution(generator);
		k_ = exp(k_);
		k2_ = exp(k2_);
		if (k_>2000){
			k_ = 2000;
		}
		if (k_ < 20){
			k_ = 20;
		}
		if (k2_>100){
			k2_ = 100;
		}
		if (k2_ < 5){
			k2_ = 5;
		}
		if (porosity_>0.5){
			porosity_ = 0.5;
		}
		if (porosity_ < 0.05){
			porosity_ = 0.05;
		}
		//use nonisotropic k tensor  initial value being zero defined in tetrahedron and node.cpp
		for (i = 0; i < 3; i++){ //isotropic case
			for (j = 0; j < 3; j++){
				if (i == j){
					if (i == 2){
						elementlist[ie].k(i, j, k2_*0.987e-15);
					}
					else{
						elementlist[ie].k(i, j, k_*0.987e-15);
					}
					//for (ii = 0; ii < 4; ii++){
					//	inode = elementlist[ie].node(ii);
					//	nodelist[inode].k(i, j, nodelist[inode].k(i, j) + k_*0.25*elementlist[ie].volume() / nodelist[inode].dualvolume()); //k for node is defined
					//}
				}
			}
		}

		elementlist[ie].porosity(porosity_);
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			nodelist[inode].porosity(nodelist[inode].porosity() + porosity_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			//nodelist[inode].mu_o(nodelist[inode].mu_o() + mu_o_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}

}


void REGION::properties_cvfe_sf_paper_cylinder_full(){ //for tetmesh random log normal distribution
	int ie, inode, ii, i, j, mark_, nelement;
	double k_, k2_, kmin_, kmax_, porosity_, mu_o_, zmid_;
	k_ = 500;
	k2_ = 50;
	nelement = elementlist.size();

	std::vector<double> cyclex, cycley;
	for (i = 0; i < 60; i++){
		double rndx = rand() % 1000 + 0;
		double rndy = rand() % 1000 + 0;
		cyclex.push_back(rndx);
		cycley.push_back(rndy);
	}

	//set k and mu and porosity for elements and nodes (nodal k and mu only useful for visualisation; deactivated;)
	for (ie = 0; ie < nelement; ie++){
		mark_ = elementlist[ie].markregion(); //mark_ is from 0
		int nn1 = elementlist[ie].node(0);
		int nn2 = elementlist[ie].node(1);
		int nn3 = elementlist[ie].node(2);
		int nn4 = elementlist[ie].node(3);
		double cx = 0.25*(nodelist[nn1].x() + nodelist[nn2].x() + nodelist[nn3].x() + nodelist[nn4].x());
		double cy = 0.25*(nodelist[nn1].y() + nodelist[nn2].y() + nodelist[nn3].y() + nodelist[nn4].y());
		for (i = 0; i < 3; i++){ //isotropic case
			for (j = 0; j < 3; j++){
				if (i == j){
					if (i == 2){
						elementlist[ie].k(i, j, k2_*0.987e-15);
					}
					else{
						elementlist[ie].k(i, j, k_*0.987e-15);
					}
					//for (ii = 0; ii < 4; ii++){
					//	inode = elementlist[ie].node(ii);
					//	nodelist[inode].k(i, j, nodelist[inode].k(i, j) + k_*0.25*elementlist[ie].volume() / nodelist[inode].dualvolume()); //k for node is defined
					//}
				}
			}
		}
		for (int ic = 0; ic < cyclex.size(); ic++){
			if (std::sqrt((cx - cyclex[ic])*(cx - cyclex[ic]) + (cy - cycley[ic])*(cy - cycley[ic])) <= 50){
				elementlist[ie].k(0, 0, k2_*0.987e-15);
				elementlist[ie].k(1, 1, k2_*0.987e-15);
				break;
			}
		}

		porosity_ = rocklist[mark_].porosity();
		elementlist[ie].porosity(porosity_);
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			nodelist[inode].porosity(nodelist[inode].porosity() + porosity_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
			//nodelist[inode].mu_o(nodelist[inode].mu_o() + mu_o_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}

}

void REGION::properties_cvfe_sf_paper_cylinder(){
	int ie, inode, ii, i, j, mark_, nelement;
	double k_, kv_, k2_, k2v_, kmin_, kmax_, porosity_, mu_o_, zmid_, zzz;
	k_ = 100;
	k2_ = 1;
	kv_ = 1;
	k2v_ = 0.2;
	nelement = elementlist.size();

	std::vector<double> cyclex, cycley, lowcyclex, lowcycley;

	/*for (i = 0; i < 30; i++){
		double rndx = rand() % 1000 + 0;
		double rndy = rand() % 1000 + 0;
	}*/


	/*for (i = 0; i < 50; i++){
		double rndx = rand() % 1000 + 0;
		double rndy = rand() % 1000 + 0;
		cyclex.push_back(rndx);
		cycley.push_back(rndy);
	}*/
	std::ifstream randin;
	randin.open("randomout.txt");
	double rndx, rndy;
	for (int i = 0; i < 50; i++){
		randin >> rndx >> rndy;
		cyclex.push_back(rndx);
		cycley.push_back(rndy);
	}

	//set k and mu and porosity for elements and nodes (nodal k and mu only useful for visualisation; deactivated;)
	for (ie = 0; ie < nelement; ie++){
		mark_ = elementlist[ie].markregion(); //mark_ is from 0
		int nn1 = elementlist[ie].node(0);
		int nn2 = elementlist[ie].node(1);
		int nn3 = elementlist[ie].node(2);
		int nn4 = elementlist[ie].node(3);
		double cx = 0.25*(nodelist[nn1].x() + nodelist[nn2].x() + nodelist[nn3].x() + nodelist[nn4].x());
		double cy = 0.25*(nodelist[nn1].y() + nodelist[nn2].y() + nodelist[nn3].y() + nodelist[nn4].y());
		double cz = 0.25*(nodelist[nn1].z() + nodelist[nn2].z() + nodelist[nn3].z() + nodelist[nn4].z());
		elementlist[ie].k(0, 0, k_*0.987e-15);
		elementlist[ie].k(1, 1, k_*0.987e-15);
		elementlist[ie].k(2, 2, kv_*0.987e-15);

		for (int ic = 0; ic < 50; ic++){
			if (ic<30 && cz >= 100*zscale_ && std::sqrt((cx - cyclex[ic])*(cx - cyclex[ic]) + (cy - cycley[ic])*(cy - cycley[ic])) <= 70){
				elementlist[ie].k(0, 0, k2_*0.987e-15);
				elementlist[ie].k(1, 1, k2_*0.987e-15);
				elementlist[ie].k(2, 2, k2v_*0.987e-15);
				break;
			}
			else if (ic>=30 && cz <= 100*zscale_ && std::sqrt((cx - cyclex[ic])*(cx - cyclex[ic]) + (cy - cycley[ic])*(cy - cycley[ic])) <= 70){
				elementlist[ie].k(0, 0, k2_*0.987e-15);
				elementlist[ie].k(1, 1, k2_*0.987e-15);
				elementlist[ie].k(2, 2, k2v_*0.987e-15);
				break;
			}
		}

		porosity_ = rocklist[mark_].porosity();
		elementlist[ie].porosity(porosity_);
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			nodelist[inode].porosity(nodelist[inode].porosity() + porosity_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}

}

void REGION::setcar_perm_rand_paperdfmm(){

	double k_ = 100e-15;
	double k2_ = 1e-15;
	double kv_ = 1e-15;
	double k2v_ = 0.2e-15;
	int nelement = cpgelementlist.size();

	std::vector<double> cyclex, cycley;

	/*for (int i = 0; i < 30; i++){
	double rndx = rand() % 100 + 0;
	double rndy = rand() % 100 + 0;
	}
	std::ofstream randout;
	randout.open("randomout.txt");*/

	for (int i = 0; i < 50; i++){
		double rndx = rand() % 100 + 0;
		double rndy = rand() % 100 + 0;
		//randout << rndx << " " << rndy << std::endl;
		cyclex.push_back(rndx);
		cycley.push_back(rndy);
	}
	/*std::ifstream randin;
	randin.open("randomout.txt");
	double rndx, rndy;
	for (int i = 0; i < 50; i++){
		randin >> rndx >> rndy;
		cyclex.push_back(rndx);
		cycley.push_back(rndy);
	}*/

	//set k and mu and porosity for elements and nodes (nodal k and mu only useful for visualisation; deactivated;)
	for (int ie = 0; ie < nelement; ie++){

		double cx = cpgelementlist[ie].centnode().x();
		double cy = cpgelementlist[ie].centnode().y();
		double cz = cpgelementlist[ie].centnode().z();
		cpgelementlist[ie].k(0, 0, k_);
		cpgelementlist[ie].k(1, 1, k_);
		cpgelementlist[ie].k(2, 2, kv_);

		for (int ic = 0; ic < 50; ic++){
			if (ic<25 && cz >= 10 && std::sqrt((cx - cyclex[ic])*(cx - cyclex[ic]) + (cy - cycley[ic])*(cy - cycley[ic])) <= 6){
				cpgelementlist[ie].k(0, 0, k2_);
				cpgelementlist[ie].k(1, 1, k2_);
				cpgelementlist[ie].k(2, 2, k2v_);
				break;
			}
			else if (ic >= 25 && cz <= 10 && std::sqrt((cx - cyclex[ic])*(cx - cyclex[ic]) + (cy - cycley[ic])*(cy - cycley[ic])) <= 6){
				cpgelementlist[ie].k(0, 0, k2_);
				cpgelementlist[ie].k(1, 1, k2_);
				cpgelementlist[ie].k(2, 2, k2v_);
				break;
			}
		}

	}

}


void REGION::setcar_perm_rand(){

	double k_ = 100;
	double k2_ = 1;
	double kv_ = 1;
	double k2v_ = 0.2;
	int nelement = cpgelementlist.size();

	std::vector<double> cyclex, cycley, lowcyclex, lowcycley;

	/*for (int i = 0; i < 30; i++){
		double rndx = rand() % 1000 + 0;
		double rndy = rand() % 1000 + 0;
	}*/
	/*std::ofstream randout;
	randout.open("randomout.txt");*/

	/*for (int i = 0; i < 50; i++){
		double rndx = rand() % 1000 + 0;
		double rndy = rand() % 1000 + 0;
		randout << rndx << " " << rndy << std::endl;
		cyclex.push_back(rndx);
		cycley.push_back(rndy);
	}*/
	std::ifstream randin;
	randin.open("randomout.txt");
	double rndx, rndy;
	for (int i = 0; i < 50; i++){
		randin >> rndx >> rndy;
		cyclex.push_back(rndx);
		cycley.push_back(rndy);
	}

	//set k and mu and porosity for elements and nodes (nodal k and mu only useful for visualisation; deactivated;)
	for (int ie = 0; ie < nelement; ie++){

		double cx = cpgelementlist[ie].centnode().x();
		double cy = cpgelementlist[ie].centnode().y();
		double cz = cpgelementlist[ie].centnode().z();
		cpgelementlist[ie].k(0, 0, k_*0.987e-15);
		cpgelementlist[ie].k(1, 1, k_*0.987e-15);
		cpgelementlist[ie].k(2, 2, kv_*0.987e-15);

		for (int ic = 0; ic < 50; ic++){
			if (ic<30 && cz >= 20 && std::sqrt((cx - cyclex[ic])*(cx - cyclex[ic]) + (cy - cycley[ic])*(cy - cycley[ic])) <= 70){
				cpgelementlist[ie].k(0, 0, k2_*0.987e-15);
				cpgelementlist[ie].k(1, 1, k2_*0.987e-15);
				cpgelementlist[ie].k(2, 2, k2v_*0.987e-15);
				break;
			}
			else if (ic >= 30 && cz <= 20 && std::sqrt((cx - cyclex[ic])*(cx - cyclex[ic]) + (cy - cycley[ic])*(cy - cycley[ic])) <= 70){
				cpgelementlist[ie].k(0, 0, k2_*0.987e-15);
				cpgelementlist[ie].k(1, 1, k2_*0.987e-15);
				cpgelementlist[ie].k(2, 2, k2v_*0.987e-15);
				break;
			}
		}

	}

}

void REGION::properties_cvfe_mf(){ //for tetmesh multiphase
	int ie, inode, ii, i, j, mark_, nelement;
	double k_, kmin_, kmax_, porosity_, mu_o_, mu_w_, zmid_, rho_o_;
	//constant value in a rock may not be assigned to elements to save storage
	nelement = elementlist.size();

	//region zmin, zmax and gradiant
	std::vector<double> zmin, zmax, zgrad;
	zmin.resize(rocklist.size());
	zmax.resize(rocklist.size());
	zgrad.resize(rocklist.size());

	for (i = 0; i < rocklist.size(); i++){
		zmin[i] = 1.0e+10;
		zmax[i] = -1.0e+10;
	}

	for (ie = 0; ie < nelement; ie++){
		int ir = elementlist[ie].markregion();
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			if (nodelist[inode].z()<zmin[ir]){
				zmin[ir] = nodelist[inode].z();
			}
			if (nodelist[inode].z()>zmax[ir]){
				zmax[ir] = nodelist[inode].z();
			}
		}
	}
	/*for (i = 0; i < rocklist.size(); i++){
		zgrad[i] = (rocklist[i].permhigh() - rocklist[i].permlow()) / (zmax[i] - zmin[i]);
	}*/

	//set k and mu rho and porosity for elements and nodes
	for (ie = 0; ie < nelement; ie++){
		zmid_ = (nodelist[elementlist[ie].node(0)].z() + nodelist[elementlist[ie].node(1)].z() + nodelist[elementlist[ie].node(2)].z() + nodelist[elementlist[ie].node(3)].z()) / 4.0;
		mark_ = elementlist[ie].markregion(); //mark_ is from 0
		//k_ = rocklist[mark_].permlow() + (zmid_ - zmin[mark_])*zgrad[mark_];
		//use nonisotropic k tensor  initial value being zero defined in tetrahedron and node.cpp
		for (i = 0; i < 3; i++){ //isotropic case
			for (j = 0; j < 3; j++){
				if (i == j){
					elementlist[ie].k(i, j, k_);
					//for (ii = 0; ii < 4; ii++){ //nodal permeability
					//	inode = elementlist[ie].node(ii);
					//	nodelist[inode].k(i, j, nodelist[inode].k(i, j) + k_*0.25*elementlist[ie].volume() / nodelist[inode].dualvolume()); //k for node is defined
					//}
				}
			}
		}
		//mu_o_ = rocklist[mark_].mu_o(); //node-based mu and k and rho only useful for visualisation
		//mu_w_ = rocklist[mark_].mu_w(); //only nodal porosity useful for flow diagnostics
		//rho_o_ = rocklist[mark_].rho_o();
		//double rho_w_ = rocklist[mark_].rho_w();
		//double Siw_ = rocklist[mark_].Siw();
		double Pct_ = rocklist[mark_].Pct();
		double lamda_ = rocklist[mark_].lamda();
		//elementlist[ie].mu_w(mu_w_);
		//elementlist[ie].mu_o(mu_o_);
		//elementlist[ie].rho_o(rho_o_);
		////elementlist[ie].rho_w(rho_w_);
		//elementlist[ie].Siw(Siw_);
		//if (Pct_ >= 0){
		//	elementlist[ie].Pct(Pct_);
		//}
		//else{
		//	double Pct_est = 7.37*pow(elementlist[ie].k(0, 0)/0.987e-15, -0.43)*6894.76; //threshold pressure estimation (Thomas) mD to psi
		//	elementlist[ie].Pct(Pct_est);
		//}
		//elementlist[ie].lamda(lamda_);
		porosity_ = rocklist[mark_].porosity();
		elementlist[ie].porosity(porosity_);
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			nodelist[inode].porosity(nodelist[inode].porosity() + porosity_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		//	nodelist[inode].mu_w(nodelist[inode].mu_w() + mu_w_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume()); //nodal viscosity
		//	nodelist[inode].mu_o(nodelist[inode].mu_o() + mu_o_ * 0.25*elementlist[ie].volume() / nodelist[inode].dualvolume());
		}
	}

}

void REGION::elementalmobit_sf(){
	//compute mobility single-phase
	int ie;
	for (ie = 0; ie < elementlist.size(); ie++){
		elementlist[ie].mobit(1.0 / mu_o);
	}
}


void REGION::properties_cpgfv(){
	int ie, i, j, mark_, ilayer, inode;
	double k_, poro_, mu_o_, zmid_;

	//region zmin, zmax and gradiant
	std::vector<double> zmin, zmax, zgrad;
	zmin.resize(rocklist.size());
	zmax.resize(rocklist.size());
	zgrad.resize(rocklist.size());

	for (i = 0; i < rocklist.size(); i++){
		zmin[i] = 1.0e+10;
		zmax[i] = -1.0e+10;
	}

	for (ie = 0; ie < cpgelementlist.size(); ie++){
		int ir = cpgelementlist[ie].layer();
		for (i = 0; i < 8; i++){
			inode = cpgelementlist[ie].node(i);
			if (nodelist[inode].z()<zmin[ir]){
				zmin[ir] = nodelist[inode].z();
			}
			if (nodelist[inode].z()>zmax[ir]){
				zmax[ir] = nodelist[inode].z();
			}
		}
	}
	/*for (i = 0; i < rocklist.size(); i++){
		zgrad[i] = (rocklist[i].permhigh() - rocklist[i].permlow()) / (zmax[i] - zmin[i]);
	}*/

	for (ie = 0; ie < cpgelementlist.size(); ie++){
		zmid_ = 0;
		for (i = 0; i < 8; i++){
			inode = cpgelementlist[ie].node(i);
			zmid_ = zmid_ + nodelist[inode].z();
		}
		zmid_ = zmid_ / 8.0;
		ilayer = cpgelementlist[ie].layer();
		//k_ = rocklist[ilayer].permlow() + (zmid_ - zmin[ilayer])*zgrad[ilayer];
		//mu_o_ = rocklist[ilayer].mu_o();
		poro_ = rocklist[ilayer].porosity();
		cpgelementlist[ie].mu(mu_o_);
		cpgelementlist[ie].porosity(poro_);
		for (i = 0; i < 3; i++){ //isotropic case
			for (j = 0; j < 3; j++){
				if (i == j){
					cpgelementlist[ie].k(i, j, k_);
				}
			}
		}
	}

}


void REGION::clearnodesbc(){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].markbc(-2);
		nodelist[i].markbc(0);
	}
}
void REGION::boundarycondition2node(){ //for nodes
	int mark_, btype_, i, nnode;
	double influx_, P_, dimx;
	BSURFACE bsurface_;
	/*
	currently all boundary conditions constant on each boundary
	-2: internal (by default)
	1: Dirichlet: pressure(P=p-rho*g*z)=constant;
	2: Neumann: influx = constant; Neumann bc inposed via boundary trifacets; (volume flow rate)
	0: no-flow (free-slip; mark=bnmark_noflow): n*u=n*grad P=0; Neumann being 0: doesn't need implementation for pressure calculation
	   but when calculating nodal velocity, need implementation;
       surface boundary conditions are set in Calltetgen()
	   obtain P from u and boundary conditions
	3: Gradient Dirichlet boundary condition;
	*/
	nnode = nodelist.size();
	dimx = meshinfo.xmax() - meshinfo.xmin();

	for (i = 0; i < nnode; i++){
		mark_ = nodelist[i].markbsurface();
		if (mark_ >= 0){
			btype_ = bsurfacelist[mark_].type();
			int sign_ = bsurfacelist[mark_].sign();
			if (btype_ == 1){ //for fixed pressure (Dirichlet) boundary surface
				P_ = bsurfacelist[mark_].P();
				nodelist[i].Po(P_);
				nodelist[i].markbc(1);
			}
			//if btype==2, Neumann bc inposed via boundary trifacets
			if (btype_ == 2){
				nodelist[i].markbc(2);
			}
			else if (btype_ == 0){
				nodelist[i].markbc(0);
			}
			else if (btype_ == 3){
				//P_ = 50000000.0 - (50000000.0 - 200000) / dimx*(nodelist[i].x() - meshinfo.xmin());
				if (mark_==0){
					P_ = (500 - 0.1*(nodelist[i].x() - meshinfo.xmin()))*100000.0;
				}
				else if (mark_ == 3){
					P_ = (2 + 0.1*(meshinfo.xmax() - nodelist[i].x()))*100000.0;
				}
				nodelist[i].Po(P_);
				nodelist[i].markbc(1);
			}
			else if (btype_ == 4){//linear pressure bc
				double dp_ = flowbsurfacelist[0].P() - flowbsurfacelist[1].P();
				if (upscaledirection_ == 1){
					P_ = dp_*(nodelist[i].x() - meshinfo.xmin())/(meshinfo.xmax() - meshinfo.xmin());
				}
				else if (upscaledirection_ == 2){
					P_ = dp_*(nodelist[i].y() - meshinfo.ymin()) / (meshinfo.ymax() - meshinfo.ymin());
				}
				else if (upscaledirection_ == 3){
					P_ = dp_*(nodelist[i].z() - meshinfo.zmin()) / (meshinfo.zmax() - meshinfo.zmin());
				}
				nodelist[i].Po(P_);
				nodelist[i].markbc(1);
			}
			if (sign_ == 1){//injecting water
				nodelist[i].Sw(1);
				nodelist[i].markbc_Sw(1);
			}
			if (sign_ == -1){
				producernodes.push_back(i);
			}
		}
	}
}

void REGION::wellcondition_cpgfv(int nx, int ny, int nz){
	//local variables
	int iwell, ie, i, j, ne, n0, n1, n2, n3, ii, jj, iflag, bcmark;
	double x_, y_, z_, wellx, welly, wellz, dd;
	double xx[3], yy[3], zz[3], area_, area[3];

	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}

	for (iwell = 0; iwell < welllist.size(); iwell++){
		bcmark = welllist[iwell].type();
		wellx = welllist[iwell].geonodelist()[0].x();
		welly = welllist[iwell].geonodelist()[0].y();
		iflag = 0;
		for (j = 0; j < ny; j++){
			for (i = 0; i < nx; i++){
				ne = j*nx + i;
				n0 = cpgelementlist[ne].node(0);
				n1 = cpgelementlist[ne].node(1);
				n2 = cpgelementlist[ne].node(2);
				n3 = cpgelementlist[ne].node(3);
				//first triangle
				xx[0] = nodelist[n0].x();
				xx[1] = nodelist[n1].x();
				xx[2] = nodelist[n2].x();
				yy[0] = nodelist[n0].y();
				yy[1] = nodelist[n1].y();
				yy[2] = nodelist[n2].y();
				zz[0] = 0;
				zz[1] = 0;
				zz[2] = 0;
				area_ = mathzz_.area_triangle(xx, yy, zz);
				//3 sub triangles
				xx[0] = wellx;
				yy[0] = welly;
				xx[1] = nodelist[n0].x();
				yy[1] = nodelist[n0].y();
				xx[2] = nodelist[n1].x();
				yy[2] = nodelist[n1].y();
				area[0] = mathzz_.area_triangle(xx, yy, zz);
				xx[1] = nodelist[n1].x();
				yy[1] = nodelist[n1].y();
				xx[2] = nodelist[n2].x();
				yy[2] = nodelist[n2].y();
				area[1] = mathzz_.area_triangle(xx, yy, zz);
				xx[1] = nodelist[n0].x();
				yy[1] = nodelist[n0].y();
				xx[2] = nodelist[n2].x();
				yy[2] = nodelist[n2].y();
				area[2] = mathzz_.area_triangle(xx, yy, zz);
				if (std::abs(area[0] + area[1] + area[2] - area_)<0.0001*std::sqrt(area_)) {
					ii = i;
					jj = j;
					iflag = 1;
					break;
				}
				//second triangle
				xx[0] = nodelist[n1].x();
				xx[1] = nodelist[n2].x();
				xx[2] = nodelist[n3].x();
				yy[0] = nodelist[n1].y();
				yy[1] = nodelist[n2].y();
				yy[2] = nodelist[n3].y();
				zz[0] = 0;
				zz[1] = 0;
				zz[2] = 0;
				area_ = mathzz_.area_triangle(xx, yy, zz);
				//3 sub triangles
				xx[0] = wellx;
				yy[0] = welly;
				xx[1] = nodelist[n1].x();
				yy[1] = nodelist[n1].y();
				xx[2] = nodelist[n2].x();
				yy[2] = nodelist[n2].y();
				area[0] = mathzz_.area_triangle(xx, yy, zz);
				xx[1] = nodelist[n2].x();
				yy[1] = nodelist[n2].y();
				xx[2] = nodelist[n3].x();
				yy[2] = nodelist[n3].y();
				area[1] = mathzz_.area_triangle(xx, yy, zz);
				xx[1] = nodelist[n1].x();
				yy[1] = nodelist[n1].y();
				xx[2] = nodelist[n3].x();
				yy[2] = nodelist[n3].y();
				area[2] = mathzz_.area_triangle(xx, yy, zz);
				if (std::abs(area[0] + area[1] + area[2] - area_)<0.0001*std::sqrt(area_)) {
					ii = i;
					jj = j;
					iflag = 1;
					break;
				}
			}
			if (iflag == 1){
				break;
			}
		}
		if (iflag == 1){
			for (i = 0; i < nz; i++){
				//ne = cpgelementmatrix[ii][jj][i];
				ne = ii + jj*nx + i*nx*ny;
				cpgelementlist[ne].markwell(iwell);

				if (bcmark == 1){
					cpgelementlist[ne].markbc(1);
					cpgelementlist[ne].Po(welllist[iwell].value());
				}
				//else if (bcmark == 2){
				//	cpgelementlist[ne].markbc(-1);
				//	NODE node1 = cpgfacelist[cpgelementlist[ne].cpgface(4)].centnode();//bot mid node
				//	NODE node2 = cpgfacelist[cpgelementlist[ne].cpgface(5)].centnode();//top mid node
				//	dd = mathzz_.twonodedistance(node1, node2);
				//	cpgelementlist[ne].source(dd / welllist[iwell].length()*welllist[iwell].value()); //source
				//}
				//else if (bcmark == 3){
				//	cpgelementlist[ne].markbc(3);
				//	cpgelementlist[ne].pbh(welllist[iwell].value());
				//}
			}
		}
		else{
			std::cout << "well location outside of model domain" << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}
	std::cout << "wells done" << std::endl;
}

void REGION::tracingboundarycondition_cpgfv(){
	int i, mark_, ie, nelement;
	nelement = cpgelementlist.size();
	tracingblist.clear();
	if (graph.tracingsign() == 1){//forward Tracer
		tracingblist = inflowblist;
	}
	else if (graph.tracingsign() == -1){
		tracingblist = outflowblist;
	}
	graph.ntracingb(tracingblist.size());

	for (i = 0; i < nelement; i++){
		cpgelementlist[i].marktracingb(-1); //clear marktracingb first
	}

	//mark tracer boundary cpg elements
	for (i = 0; i < graph.ntracingb(); i++){
		mark_ = tracingblist[i].mark();
		if (tracingblist[i].type() != 1){
			std::cout << "cpg element tracer boundary type not 1 (from well)" << std::endl;
			std::exit(EXIT_FAILURE);
		}
		for (ie = 0; ie < cpgelementlist.size(); ie++){
			if (cpgelementlist[ie].markwell() == mark_){
				cpgelementlist[ie].marktracingb(i);
			}
		}
	}
}

/*void REGION::wellcondition(){
	//set pressure-controled and rate-controled wells
	//allocate source on nodes
	//points on source edges has mark being -1, the same as on edges (in out.pointmarkerlist)
	//So we need to identify wells using locations

	//local variables
	int iwell, i;
	double x, y, z, u, v, wellx, welly, wellz;

	if (model.numberofwells() > 0){
		std::cout << "There are " << model.numberofwells() << " wells in the reservoir" << std::endl;
	}

	//change well's position to xyz from uv
	for (iwell = 0; iwell < model.numberofwells(); iwell++) {
		u = model.well(iwell).x();
		v = model.well(iwell).y();
		x = parasurfacelist[hsurfaceid[0]].node3d(u, v).x();
		y = parasurfacelist[hsurfaceid[0]].node3d(u, v).y();

	}

	for (iwell = 0; iwell < model.numberofwells(); iwell++){
		for (i = 0; i < nnode; i++){
			x = nodelist[i].x();
			y = nodelist[i].y();
			z = nodelist[i].z();
			u = model.well(iwell).x();
			v = model.well(iwell).y();
			wellx = parasurfacelist[hsurfaceid[0]].node3d(u, v).x();
			welly = parasurfacelist[hsurfaceid[0]].node3d(u, v).y();
			wellz = model.well(iwell).z();
			if (abs(x - wellx) < 0.001 && abs(y - welly) < 0.001 && z >= wellz){
				nodelist[i].markwell(iwell);
				if (model.well(iwell).type() == 1){//pressure-controled well
					nodelist[i].P(model.well(iwell).value());
					nodelist[i].markbc(1);
				}
				else if (model.well(iwell).type() == 2){ //rate-controled well
					nodelist[i].source(model.well(iwell).value());
					nodelist[i].markbc(-1);
				}
			}
		}
	}
}*/

void REGION::markwellnode_papermesh(){
	NODE node1_, node2_;
	int nnode = nodelist.size();
	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}
	//mark nodes that are well 0
	int iwell = 0;
	for (int i = 0; i < nnode; i++){//wellmethod=1: at least 2 nodes
		int flag_ = 0;
		std::vector<NODE> geonodelist_ = welllist[iwell].geonodelist();
		for (int j = 0; j < geonodelist_.size()-1; j++){
			node1_.x(geonodelist_[j].x());
			node1_.y(geonodelist_[j].y());
			node1_.z(geonodelist_[j].z()*zscale_);
			node2_.x(geonodelist_[j+1].x());
			node2_.y(geonodelist_[j+1].y());
			node2_.z(geonodelist_[j+1].z()*zscale_);

			if (std::abs(mathzz_.twonodedistance(nodelist[i], node1_) + mathzz_.twonodedistance(nodelist[i], node2_) - mathzz_.twonodedistance(node1_, node2_)) < 0.00001){
				nodelist[i].markwell(iwell);
				flag_ = 1;
				break;
			}
		}


	}


	//for wells 1 to 4
	for (iwell = 1; iwell < welllist.size(); iwell++){
		for (int i = 0; i < nnode; i++){
			if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
				nodelist[i].markwell(iwell); //mark well
			}
		}
	}

}



void REGION::markwellnode(){
	int nnode = nodelist.size();
	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}
	//mark nodes that are wells
	if (wellmethod_ == 1){
		for (int i = 0; i < nnode; i++){//wellmethod=1: at least 2 nodes
			int flag_ = 0;
			for (int iwell = 0; iwell < welllist.size(); iwell++){
				std::vector<NODE> geonodelist_ = welllist[iwell].geonodelist();
				for (int j = 0; j < geonodelist_.size() - 1; j++){
					NODE node1_ = geonodelist_[j];
					NODE node2_ = geonodelist_[j + 1];
					if (std::abs(mathzz_.twonodedistance(nodelist[i], node1_) + mathzz_.twonodedistance(nodelist[i], node2_) - mathzz_.twonodedistance(node1_, node2_)) < 0.00001){
						nodelist[i].markwell(iwell);
						flag_ = 1;
						break;
					}
				}
				if (flag_ == 1){
					break;
				}
			}
		}
	}
	else if (wellmethod_ == 2){
		//find min x,y and max x, y
		double xmin = 100000000;
		double ymin = 100000000;
		double xmax = -100000000;
		double ymax = -100000000;
		for (int i = 0; i < nnode; i++){
			if (nodelist[i].x() < xmin){
				xmin = nodelist[i].x();
			}
			if (nodelist[i].y() < ymin){
				ymin = nodelist[i].y();
			}
			if (nodelist[i].x() > xmax){
				xmax = nodelist[i].x();
			}
			if (nodelist[i].y() > ymax){
				ymax = nodelist[i].y();
			}
		}
		for (int i = 0; i < nnode; i++){
			if (std::abs(nodelist[i].x() - xmin) < 1e-5 && std::abs(nodelist[i].y() - ymin) < 1e-5){
				nodelist[i].markwell(0); //fist well
			}
			else if (std::abs(nodelist[i].x() - xmax) < 1e-5 && std::abs(nodelist[i].y() - ymax) < 1e-5){
				nodelist[i].markwell(1); //second well
			}
		}
	}
	else if (wellmethod_ == 3){
		for (int iwell = 0; iwell < welllist.size(); iwell++){
			for (int i = 0; i < nnode; i++){
				if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
					nodelist[i].markwell(iwell); //mark well
				}
			}
		}
	}
}


void REGION::markwellnode_paper2(){
	int nnode = nodelist.size();
	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}
	int iwell = 0;
	for (int i = 0; i < nnode; i++){
		if (nodelist[i].z() >= 100 * zscale_ && std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
			nodelist[i].markwell(iwell); //mark well
		}
	}

	for (iwell = 1; iwell < welllist.size(); iwell++){
		for (int i = 0; i < nnode; i++){
			if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
				nodelist[i].markwell(iwell); //mark well
			}
		}
	}

}


void REGION::markwellnode_paper2b(){
	int nnode = nodelist.size();
	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}
	int iwell = 0;
	for (int i = 0; i < nnode; i++){
		if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
			nodelist[i].markwell(iwell); //mark well
		}
	}
	iwell = 1;
	for (int i = 0; i < nnode; i++){
		if (nodelist[i].z() >= 250 * zscale_ &&std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
			nodelist[i].markwell(iwell); //mark well
		}
	}
}

void REGION::markwellnode_paper2c(){
	int nnode = nodelist.size();
	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}
	int iwell = 0;
	for (int i = 0; i < nnode; i++){
		if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
			nodelist[i].markwell(iwell); //mark well
		}
	}
	iwell = 1;
	for (int i = 0; i < nnode; i++){
		if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
			nodelist[i].markwell(iwell); //mark well
		}
	}
}

void REGION::wellcondition2node(){ //for CVFE
	//set pressure-controled and volume flow rate-controled wells
	//allocate source on nodes
	//points on source edges has mark being -1, the same as on edges (in out.pointmarkerlist)
	//So we need to identify wells using locations

	//local variables
	int iwell, i, nnode, j, flag_, jnode;
	double x_, y_, z_, wellx, welly, wellz, dd, xmin, ymin, xmax, ymax;
	WELL well_;
	NODE node1_, node2_;
	nnode = nodelist.size();

	for (i = 0; i < nnode; i++){
		iwell = nodelist[i].markwell();
		if (iwell >= 0){
			if (welllist[iwell].type() == 1){//pressure-controled well
				nodelist[i].Po(welllist[iwell].value());
				nodelist[i].markbc(1); //press
			}
			if (welllist[iwell].type() == 2){ //volume rate-controled well Neumann bc
				//nodelist[i].markbc(-1);//source
				//dd = 0;
				//for (j = 0; j < nodelist[i].numberofsunode(); j++){
				//	jnode = nodelist[i].sunode(j);
				//	if (nodelist[jnode].markwell() == iwell){
				//		dd = dd + 0.5*mathzz_.twonodedistance(nodelist[i], nodelist[jnode]);
				//	}
				//}
				//nodelist[i].source(welllist[iwell].value()*dd / welllist[iwell].length()); //allocate well's volume rate source
			}
			if (welllist[iwell].sign() == 1){//injecting water
				nodelist[i].Sw(1);
				nodelist[i].markbc_Sw(1);
			}
			if (welllist[iwell].sign() == -1){
				producernodes.push_back(i);
			}
		}
	}

	std::cout << "wells done" << std::endl;
}

int REGION::checkwaterbreak(){
	int flag = 0;
	for (int i = 0; i < producernodes.size(); i++){
		int inode = producernodes[i];
		double sw_ = nodelist[inode].Sw();
		if ((sw_ - Siw_)>0.0001){
			flag = 1;
		}
	}
	return flag;
}

int REGION::checkwaterbreak_cpg(){
	int flag = 0;
	for (int i = 0; i < producercpgelelist.size(); i++){
		int ie = producercpgelelist[i];
		double sw_ = cpgelementlist[ie].Sw();
		if ((sw_ - Siw_)>0.0001){
			flag = 1;
		}
	}
	return flag;
}


void REGION::nodalSwbc(){
	for (int i = 0; i < nodelist.size(); i++){
		int iwell = nodelist[i].markwell();
		if (iwell >= 0){
			if (welllist[iwell].sign() == 1){//injecting water
				nodelist[i].Sw(1);
			}
		}
	}
}

void REGION::wellcondition_fmm(){ //for CVFE
	//set pressure-controled and volume flow rate-controled wells
	//allocate source on nodes
	//points on source edges has mark being -1, the same as on edges (in out.pointmarkerlist)
	//So we need to identify wells using locations

	//local variables
	int iwell, i, nnode, j, flag_, jnode;
	double x_, y_, z_, wellx, welly, wellz, dd, xmin, ymin, xmax, ymax;
	WELL well_;
	NODE node1_, node2_;
	nnode = nodelist.size();
	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}
	//mark nodes that are wells
	if (wellmethod_ == 1){
		for (i = 0; i < nnode; i++){// reason:more than one well & maybe more added nodes than in TETGENIO::addin
			flag_ = 0;
			for (iwell = 0; iwell < welllist.size(); iwell++){
				std::vector<NODE> geonodelist_ = welllist[iwell].geonodelist();
				if (geonodelist_.size() >1){
					for (j = 0; j < geonodelist_.size() - 1; j++){
						node1_ = geonodelist_[j];
						node2_ = geonodelist_[j + 1];
						if (std::abs(mathzz_.twonodedistance(nodelist[i], node1_) + mathzz_.twonodedistance(nodelist[i], node2_) - mathzz_.twonodedistance(node1_, node2_)) < 0.00001){
							nodelist[i].markwell(iwell);
							flag_ = 1;
							break;
						}
					}
					if (flag_ == 1){
						break;
					}
				}
				else if (geonodelist_.size() == 1){//corner wells
					if (std::abs(nodelist[i].x() - geonodelist_[0].x()) < 1e-5 && std::abs(nodelist[i].y() - geonodelist_[0].y()) < 1e-5){
						nodelist[i].markwell(iwell);
					}
				}
			}
		}
	}
	else if (wellmethod_ == 2){
		//find min x,y and max x, y
		xmin = 100000000;
		ymin = 100000000;
		xmax = -100000000;
		ymax = -100000000;
		for (i = 0; i < nnode; i++){
			if (nodelist[i].x() < xmin){
				xmin = nodelist[i].x();
			}
			if (nodelist[i].y() < ymin){
				ymin = nodelist[i].y();
			}
			if (nodelist[i].x() > xmax){
				xmax = nodelist[i].x();
			}
			if (nodelist[i].y() > ymax){
				ymax = nodelist[i].y();
			}
		}
		for (i = 0; i < nnode; i++){
			if (std::abs(nodelist[i].x() - xmin) < 1e-5 && std::abs(nodelist[i].y() - ymin) < 1e-5){
				nodelist[i].markwell(0); //fist well
			}
			else if (std::abs(nodelist[i].x() - xmax) < 1e-5 && std::abs(nodelist[i].y() - ymax) < 1e-5){
				nodelist[i].markwell(1); //second well
			}
		}
	}
	else if (wellmethod_ == 3){
		for (iwell = 0; iwell < welllist.size(); iwell++){
			for (i = 0; i < nnode; i++){
				if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
					nodelist[i].markwell(iwell); //mark well
				}
			}
		}
	}

	for (i = 0; i < nnode; i++){
		iwell = nodelist[i].markwell();
		if (iwell >= 0){
			if (welllist[iwell].type() == 1){//pressure-controled well
				nodelist[i].Po(welllist[iwell].value());
				nodelist[i].markbc(1); //press
			}
			else if (welllist[iwell].type() == 2){ //volume rate-controled well
				nodelist[i].markbc(-1);//source
				dd = 0;
				for (j = 0; j < nodelist[i].numberofsunode(); j++){
					jnode = nodelist[i].sunode(j);
					if (nodelist[jnode].markwell() == iwell){
						dd = dd + 0.5*mathzz_.twonodedistance(nodelist[i], nodelist[jnode]);
					}
				}
				nodelist[i].source(welllist[iwell].value()*dd / welllist[iwell].length()); //allocate well's volume rate source
			}
		}
	}
	for (i = 0; i < nnode; i++){
		iwell = nodelist[i].markwell();
		if (iwell == startwellnumber_){
			startnodeset_.push_back(i);
		}
	}
	std::cout << "wells done" << std::endl;
}

void REGION::wellcondition_paper1(){ //only for wellmethod3
	//set pressure-controled and volume flow rate-controled wells
	//allocate source on nodes
	//points on source edges has mark being -1, the same as on edges (in out.pointmarkerlist)
	//So we need to identify wells using locations

	//local variables
	int iwell, i, nnode, j, flag_, jnode;
	double x_, y_, z_, wellx, welly, wellz, dd, xmin, ymin, xmax, ymax;
	WELL well_;
	NODE node1_, node2_;
	nnode = nodelist.size();
	if (welllist.size() > 0){
		std::cout << "There are " << welllist.size() << " wells in the reservoir" << std::endl;
	}
	//mark nodes that are wells


		iwell = 0;
		for (i = 0; i < nnode; i++){
			if (std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
				nodelist[i].markwell(iwell); //mark well
			}
		}
		for (iwell = 1; iwell < welllist.size(); iwell++){
			for (i = 0; i < nnode; i++){
				if (nodelist[i].z()>=100*zscale_ && std::abs(nodelist[i].x() - welllist[iwell].geonodelist()[0].x()) < 1e-5 && std::abs(nodelist[i].y() - welllist[iwell].geonodelist()[0].y()) < 1e-5){
					nodelist[i].markwell(iwell); //mark well
				}
			}
		}


	for (i = 0; i < nnode; i++){
		iwell = nodelist[i].markwell();
		if (iwell >= 0){
			if (welllist[iwell].type() == 1){//pressure-controled well
				nodelist[i].Po(welllist[iwell].value());
				nodelist[i].markbc(1); //press
			}
			else if (welllist[iwell].type() == 2){ //volume rate-controled well
				nodelist[i].markbc(-1);//source
				dd = 0;
				for (j = 0; j < nodelist[i].numberofsunode(); j++){
					jnode = nodelist[i].sunode(j);
					if (nodelist[jnode].markwell() == iwell){
						dd = dd + 0.5*mathzz_.twonodedistance(nodelist[i], nodelist[jnode]);
					}
				}
				nodelist[i].source(welllist[iwell].value()*dd / welllist[iwell].length()); //allocate well's volume rate source
			}
		}
	}
	std::cout << "wells done" << std::endl;
}

void REGION::fmm_tet_sedfv(){
	int id_min;

	std::vector<int> band, fbset, fbset0;
	//here initial set is a group of nodes
	for (int i = 0; i < nodelist.size(); i++){
		if (nodelist[i].markwell() == startwellnumber_){
			if (std::abs(nodelist[i].z() - 200) < 0.0001){
				nodelist[i].status(2);//frozen
				nodelist[i].tof(0); //diffusive tof
				fbset0.push_back(i);
			}
		}
	}

	do{
		fbset = fbset0;
		//find and compute narrow band
		for (int i = 0; i < fbset.size(); i++){
			int inode = fbset[i];
			std::vector<int> localedgelist;
			for (int j = 0; j < nodelist[inode].numberofedgein(); j++){
				int ied = nodelist[inode].edgein(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < nodelist[inode].numberofedgeout(); j++){
				int ied = nodelist[inode].edgeout(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < localedgelist.size(); j++){
				int ied = localedgelist[j];
				int jnode = edgelist[ied].node(0) + edgelist[ied].node(1) - inode;
				if (nodelist[jnode].status() == 0){
					band.push_back(jnode);//narrow band
					nodelist[jnode].status(1); //in band
				}
				if (nodelist[jnode].status() != 2){//previously added-to-band cells are recomputed
					std::vector<int> localedgelist2;
					double tmin;

					for (int k = 0; k < nodelist[jnode].numberofedgein(); k++){
						int jed = nodelist[jnode].edgein(k);
						localedgelist2.push_back(jed);
					}
					for (int k = 0; k < nodelist[jnode].numberofedgeout(); k++){
						int jed = nodelist[jnode].edgeout(k);
						localedgelist2.push_back(jed);
					}
					double ta = 0;
					double maxt = 0;
					double avt = 0;
					int count = 0;
					for (int k = 0; k < localedgelist2.size(); k++){
						int jed = localedgelist2[k];
						int knode = edgelist[jed].node(0) + edgelist[jed].node(1) - jnode;
						if (nodelist[knode].status() == 2){
							ta = ta + edgelist[jed].area();
							avt = avt + nodelist[knode].tof();
							count++;
							if (nodelist[knode].tof()>maxt){
								maxt = nodelist[knode].tof();
							}
						}

					}
					if (ta == 0){
						std::cout << "error no frozen neighbor" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					avt = avt / count;
					double ff = 1;
					double tt = avt + nodelist[jnode].dualvolume() / ta / std::sqrt(ff);
					if (tt < maxt){
						tt = maxt + nodelist[jnode].dualvolume() / ta / std::sqrt(ff);
					}
					nodelist[jnode].tof(tt);
				}
			}
		}

		//choose smallest and proceed

		double tof_min;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int inode = band[i];
			double tt = nodelist[inode].tof();
			if (i == 0){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
			else if (tt < tof_min){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
		}
		nodelist[id_min].status(2); //frozen
		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);
}

void REGION::interpolatecpgdtof2point(){
	std::vector<int> count;
	count.resize(nodelist.size());
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		for (int i = 0; i < 8; i++){
			int inode = cpgelementlist[ie].node(i);
			nodelist[inode].tof(nodelist[inode].tof() + cpgelementlist[ie].tof());
			count[inode] = count[inode] + 1;
		}
	}
	for (int inode = 0; inode < nodelist.size(); inode++){
		nodelist[inode].tof(nodelist[inode].tof() / count[inode]);
	}
}

void REGION::nodaldtof2t(){
	for (int i = 0; i < nodelist.size(); i++){
		double tau = nodelist[i].tof();
		double tt = tau*tau / 4;
		nodelist[i].tof(tt);
	}
}

void REGION::nodaldtofana2t(){
	for (int i = 0; i < nodelist.size(); i++){
		double tau = nodelist[i].tof_back();
		double tt = tau*tau / 4;
		nodelist[i].tof(tt);
	}
}

void REGION::computenodalc_fmm(){
	double c;
	nodalc.clear();
	nodalc.resize(nodelist.size());
	for (int i = 0; i < nodelist.size(); i++){
		if (nodelist[i].tof() != 0){
			c = nodelist[i].tof_back() / nodelist[i].tof();
			nodalc[i] = c;
		}
		else{
			nodalc[i] = 1;
		}
	}
}

void REGION::analyticalt_homo_fmmpaperex1(){
	double x0 = 0;
	double y0 = 0;
	double z0 = 0;
	double fx = fmm_avvx;
	double fy = fmm_avvy;
	double fz = fmm_avvz;
	for (int inode = 0; inode < nodelist.size(); inode++){
		double edx = nodelist[inode].x() - x0;
		double edy = nodelist[inode].y() - y0;
		double edz = nodelist[inode].z() - z0;
		double ll_ = std::sqrt(edx*edx + edy*edy + edz*edz);
		double ff;
		if (edx != 0){
			double m = edy / edx; double n = edz / edx;
			double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
			ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
		}
		else if (edz == 0){
			ff = fy;
		}
		else{
			double m = edy / edz;
			double left = m*m / fy / fy + 1 / fz / fz;
			ff = std::sqrt((1 + m*m) / left);
		}
		double tt = ll_ / ff;
		nodelist[inode].tof_back(tt);
	}

}



void REGION::analyticalt_homo_fmmpaperex2(){
	double x0 = 500;
	double y0 = 500;
	double fx = std::sqrt(elementlist[0].k(0, 0) / mu_o / elementlist[0].porosity() / ct_);
	double fy = std::sqrt(elementlist[0].k(1, 1) / mu_o / elementlist[0].porosity() / ct_);
	double fz = std::sqrt(elementlist[0].k(2, 2) / mu_o / elementlist[0].porosity() / ct_);
	for (int inode = 0; inode < nodelist.size(); inode++){
		double edx = nodelist[inode].x() - x0;
		double edy = nodelist[inode].y() - y0;
		double edz = 0;
		double ll_ = std::sqrt(edx*edx + edy*edy);

		double ff;
		if (edx != 0){
			double m = edy / edx; double n = edz / edx;
			double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
			ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
		}
		else if (edz == 0){
			ff = fy;
		}
		else{
			double m = edy / edz;
			double left = m*m / fy / fy + 1 / fz / fz;
			ff = std::sqrt((1 + m*m) / left);
		}
		double tt = ll_ / ff;
		nodelist[inode].tof_back(tt);
	}

}

void REGION::analyticaldtof_homo_fmmpaper2(){
	double x0 = 500;
	double y0 = 500;
	double fx = fmm_avvx;
	double fy = fmm_avvy;
	double fz = fmm_avvz;
	for (int inode = 0; inode < nodelist.size(); inode++){
		double edx = nodelist[inode].x() - x0;
		double edy = nodelist[inode].y() - y0;
		double edz = 0;
		double ll_ = std::sqrt(edx*edx + edy*edy);

		double ff;
		if (edx != 0){
			double m = edy / edx; double n = edz / edx;
			double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
			ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
		}
		else if (edz == 0){
			ff = fy;
		}
		else{
			double m = edy / edz;
			double left = m*m / fy / fy + 1 / fz / fz;
			ff = std::sqrt((1 + m*m) / left);
		}
		double tt = ll_ / ff;
		nodelist[inode].tof_back(tt);
	}

}

void REGION::analyticaldtof_homo_fmmpaper2car(){
	double x0 = 500;
	double y0 = 500;
	double fx = fmm_avvx;
	double fy = fmm_avvy;
	double fz = fmm_avvz;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		double edx = cpgelementlist[ie].centnode().x() - x0;
		double edy = cpgelementlist[ie].centnode().y() - y0;
		double edz = 0;
		double ll_ = std::sqrt(edx*edx + edy*edy);

		double ff;
		if (edx != 0){
			double m = edy / edx; double n = edz / edx;
			double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
			ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
		}
		else if (edz == 0){
			ff = fy;
		}
		else{
			double m = edy / edz;
			double left = m*m / fy / fy + 1 / fz / fz;
			ff = std::sqrt((1 + m*m) / left);
		}
		double tt = ll_ / ff;
		cpgelementlist[ie].tof(tt);
	}

}

void REGION::welltestanat_isohomo_fmmpaper2(){ //convert into isotropic homogeneous for analytical solution
	double k_ = 0;
	double poro_ = 0;
	for (int ie = 0; ie < elementlist.size(); ie++){
		k_ = k_ + elementlist[ie].k(0, 0) + elementlist[ie].k(1, 1) + elementlist[ie].k(2, 2);
		poro_ = poro_ + elementlist[ie].porosity();
	}
	k_ = k_ / elementlist.size() / 3.0;
	poro_ = poro_ / elementlist.size();
	for (int i = 0; i < nodelist.size(); i++){
		double x = nodelist[i].x();
		double y = nodelist[i].y();
		double x0 = 500; double y0 = 500;
		double rr = (x - x0)*(x - x0) + (y - y0)*(y - y0);
		double t = rr*poro_*mu_o*ct_ / 4 / k_;
		nodelist[i].tof_back(t);
	}
}

void REGION::welltestanat_isohomo_fmmpaper2car(){ //convert into isotropic homogeneous for analytical solution
	double k_ = 0;
	double poro_ = 0;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		k_ = k_ + cpgelementlist[ie].k(0, 0) + cpgelementlist[ie].k(1, 1) + cpgelementlist[ie].k(2, 2);
		poro_ = poro_ + cpgelementlist[ie].porosity();
	}
	k_ = k_ / cpgelementlist.size() / 3.0;
	poro_ = poro_ / cpgelementlist.size();
	for (int i = 0; i < cpgelementlist.size(); i++){
		double x = cpgelementlist[i].centnode().x();
		double y = cpgelementlist[i].centnode().y();
		double x0 = 500; double y0 = 500;
		double rr = (x - x0)*(x - x0) + (y - y0)*(y - y0);
		double t = rr*poro_*mu_o*ct_ / 4 / k_;
		cpgelementlist[i].tof_back(t);
	}
}

void REGION::computefmmdvt(double dt, int nt){

	dvt.clear();//numerical
	dvt2.clear();//semi-ana
	dvt3.clear();//infinite acting analytical
	dvt.resize(nt+1);
	dvt2.resize(nt+1);
	dvt3.resize(nt + 1);
	for (int it = 0; it <= nt; it++){
		double maxt = it*dt;
		for (int i = 0; i < nodelist.size(); i++){
			if (nodelist[i].tof() <= maxt){
				dvt[it] = dvt[it] + nodelist[i].dualvolume()*nodelist[i].porosity();
			}
			if (nodelist[i].tof_back() <= maxt){
				dvt2[it] = dvt2[it] + nodelist[i].dualvolume()*nodelist[i].porosity();
			}
		}
		dvt3[it] = 3.1415926*4.0*elementlist[0].k(0, 0) * 40 * maxt / mu_o / ct_;
	}
}

void REGION::computefmmdvt_cpg(double dt, int nt){

	dvt.clear();//numerical

	dvt.resize(nt + 1);
	//dvt3.resize(nt+1);
	//double kk = 1e-15;
	for (int it = 0; it <= nt; it++){
		dvt[it] = 0;
		double maxt = it*dt;
		//dvt3[it] = 3.1415926*4.0 *kk* 40 * maxt / mu_o / ct_;
		for (int i = 0; i < cpgelementlist.size(); i++){
			if (cpgelementlist[i].tof() <= maxt){
				//if (cpgelementlist[i].tof() <= maxt && cpgelementlist[i].tof() != 0){
				dvt[it] = dvt[it] + cpgelementlist[i].volume()*cpgelementlist[i].porosity();
				//dvt[it] = dvt[it] + cpgelementlist[i].volume();
			}
		}
	}
}

void REGION::initialnodalPo(double p){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].Po(p);
	}
}

void REGION::reordernodebyt(){
	nodeorder.clear();
	nodeorder.resize(nodelist.size());
	for (int i = 0; i < nodeorder.size(); i++){
		nodeorder[i] = i;
	}

	//re-order nodes by t
	int itmp, i, j;
	for (i = 1; i < nodeorder.size(); i++){ //straight insertion sort
		itmp = nodeorder[i];
		for (j = i - 1; j >= 0 && nodelist[nodeorder[j]].tof()>nodelist[nodeorder[itmp]].tof(); j--){
			nodeorder[j + 1] = nodeorder[j];
		}
		nodeorder[j + 1] = itmp;
	}
}

void REGION::updatenodaldp_fmm(double x, double y, double qw, double dt, int nt){

	double totalt;
	dpc.clear(); //numerical
	dpc2.clear();//semi-ana
	dpc3.clear();
	dpc.resize(nt);
	dpc2.resize(nt);
	dpc3.resize(nt);
	for (int it = 0; it < 5*12*30; it++){
		totalt = it*dt;
		for (int inode = 0; inode < nodelist.size(); inode++){
			if (nodelist[inode].tof() <= totalt){
				double dp_ = qw*dt / ct_ / dvt[it];
				//double dp_ = qw*dt / ct_ / dvt2[it]; //dvt3[it] at t=0 is 0
				/*double dp_ = 0;
				if (it>0){
					dp_ = qw*dt*mu_o / 4 / 3.1415926 / elementlist[0].k(0, 0) / 40 / totalt;
				}*/
				nodelist[inode].Po(nodelist[inode].Po() - dp_);
			}
		}
	}

	int ic=0;
	for (int inode = 0; inode < nodelist.size(); inode++){
		double xx = nodelist[inode].x();
		double yy = nodelist[inode].y();
		if (std::abs(xx - x) < 0.00000001 && std::abs(yy - y) < 0.00000001){
			ic = inode;
			break;
		}
	}
	for (int it = 1; it < nt; it++){
		double dp_ = qw*dt / ct_ / dvt[it] / 6894.76; //psi
		double dp2_ = qw*dt / ct_ / dvt2[it] / 6894.76; //psi
		double dp3_ = qw*mu_o / 4.0 / 3.1415926 / elementlist[0].k(0, 0) / 40.0 / 6894.76/it/dt*dt;
		dpc[it] = dpc[it - 1] + dp_;
		dpc2[it] = dpc2[it - 1] + dp2_;
		dpc3[it] = dpc3[it - 1] + dp3_;
	}


}

void REGION::computewelltestderivative_fmmt(double dt, int nt, double qw){


	ddp.clear();
	ddp2.clear();
	ddp3.clear();
	ddp.resize(nt);
	ddp2.resize(nt);
	ddp3.resize(nt);
	double ana = qw*mu_o / 4 / 3.14 / nodelist[0].k(0, 0) / 40 / 6895;
	for (int i = 0; i < nt; i++){
		ddp[i] = qw*dt*i / ct_ / dvt[i] / 6895; // pascal to psi
		ddp2[i] = qw*dt*i / ct_ / dvt2[i] / 6895; // pascal to psi
		ddp3[i] = ana;
	}

}

void REGION::computewelltestderivative_fmmcpg(double dt, int nt, double qw){


	ddp.clear();
	ddp.resize(nt);

	for (int i = 0; i < nt; i++){
		ddp[i] = qw*dt*i / ct_ / dvt[i];
	}

}

void REGION::writefmmdvt(char* file){
	std::ofstream out;
	out.open(file);
	for (int i = 0; i < dvt.size(); i++){
		out << i << " " << dvt[i]*6.29 <<" "<<dvt2[i]*6.29<<" "<<dvt3[i]*6.29<< std::endl; //bbl
	}
}
void REGION::writefmmdvt_cpg(char* file){
	std::ofstream out;
	out.open(file);
	for (int i = 0; i < dvt.size(); i++){
		out << (i+1)*dt/86400 << " " << dvt[i]<< std::endl; //bbl
	}
}

void REGION::writefmmddp(char* file){

	std::ofstream out;
	out.open(file);
	for (int i = 0; i < ddp.size(); i++){
		out << i << " " << ddp[i] << " " << ddp2[i] <<" "<<ddp3[i]<< std::endl; //bbl
	}
}
void REGION::writefmmddp_cpg(char* file){

	std::ofstream out;
	out.open(file);
	for (int i = 0; i < ddp.size(); i++){
		out << (i+1)*dt/86400 << " " << ddp[i]/6895.0 << std::endl; // pascal to psi
	}
}
void REGION::writefmmddpwf_cpg(char* file){

	std::ofstream out;
	out.open(file);
	for (int i = 0; i < ddpwf.size(); i++){
		out << (i+1)*dt / 86400 << " " << ddpwf[i] / 6895.0 << std::endl; // pascal to psi
	}
}

void REGION::writefmmdpc(char* file){

	std::ofstream out;
	out.open(file);
	for (int i = 0; i < dpc.size(); i++){
		out << i << " " << dpc[i] <<" "<<dpc2[i]<<" "<<dpc3[i]<< std::endl;
	}

}
void REGION::writefmmdpc_cpg(char* file){
	std::ofstream out;
	out.open(file);
	for (int i = 0; i < dpc.size(); i++){
		out << (i + 1)*dt / 86400 << " " << dpc[i]/6895.0 << std::endl;
	}

}

void REGION::writefmmwellp_homo_semiana(char* file){

	std::ofstream out;
	out.open(file);
	for (int i = 0; i < dpc.size(); i++){
		out << i << " " << 3000-dpc[i] << std::endl;
	}

}

void REGION::clearnodalfmminfo(){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].status(0);
		nodelist[i].tof(0);
		nodelist[i].tof_back(0);
	}
}
std::vector<int> REGION::fbset0_node(double x, double y, double z){
	std::vector<int> fbset0;
	for (int i = 0; i < nodelist.size(); i++){
		double xx = nodelist[i].x();
		double yy = nodelist[i].y();
		double zz = nodelist[i].z();
		if (std::abs(xx - x) < 0.00000001 && std::abs(yy - y) < 0.00000001 && std::abs(zz - z) < 0.00000001){
			nodelist[i].status(2);
			nodelist[i].tof(0);
			fbset0.push_back(i);
		}
	}
	return fbset0;

}

std::vector<int> REGION::fbset0_ele(int ie){
	std::vector<int> fbset0;
	fbset0.push_back(ie);
	cpgelementlist[ie].status(2);
	cpgelementlist[ie].tof(0);
	return fbset0;
}

std::vector<int> REGION::fbset0_verticalwell(double x, double y){
	std::vector<int> fbset0;
	for (int i = 0; i < nodelist.size(); i++){
		double xx = nodelist[i].x();
		double yy = nodelist[i].y();
		if (std::abs(xx - x) < 0.00000001 && std::abs(yy - y) < 0.00000001 ){
			nodelist[i].status(2);
			nodelist[i].tof(0);
			fbset0.push_back(i);
		}
	}
	return fbset0;

}

std::vector<int> REGION::fbset0_wells_cpg(std::vector<int> wellvec){
	std::vector<int> fbset0;

	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		//cpgelementlist[ie].tracersize(wellvec.size());
		for (int i = 0; i< wellvec.size(); i++){
			int iwell = wellvec[i];
			if (cpgelementlist[ie].markwell() == iwell){
				cpgelementlist[ie].status(2);
				cpgelementlist[ie].tof(0);
				cpgelementlist[ie].tracer(i, 1);
				fbset0.push_back(ie);
			}
		}
	}
	return fbset0;

}

std::vector<int> REGION::fbset0_wells_cpg_back(std::vector<int> wellvec){
	std::vector<int> fbset0;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		//cpgelementlist[ie].tracersize(wellvec.size());
		for (int i = 0; i< wellvec.size(); i++){
			int iwell = wellvec[i];
			if (cpgelementlist[ie].markwell() == iwell){
				cpgelementlist[ie].status(2);
				cpgelementlist[ie].tof_back(0);
				cpgelementlist[ie].tracer_back(i, 1);
				fbset0.push_back(ie);
			}
		}
	}
	return fbset0;

}

void REGION::cpgtracersize(int nc){
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].tracersize(nc);
	}
}

void REGION::cpgtracersize_back(int nc){
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].tracersize_back(nc);
	}
}

std::vector<int> REGION::fbset0_well_cpg(int iwell_, std::vector<int> wellvec){
	std::vector<int> fbset0;
	int itracer_;
	for (int i = 0; i < wellvec.size(); i++){
		if (iwell_ == wellvec[i]){
			itracer_ = i;
		}
	}
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		if (cpgelementlist[ie].markwell() == iwell_){
			cpgelementlist[ie].status(2);
			cpgelementlist[ie].tof(0);
			cpgelementlist[ie].tracer(itracer_, 1);
			fbset0.push_back(ie);
		}
	}
	return fbset0;

}

std::vector<int> REGION::fbset0_verticalwell_car(double x, double y){ //not good
	std::vector<int> fbset0;
	for (int i = 0; i < cpgelementlist.size(); i++){
		for (int j = 0; j < 8; j++){
			int inode = cpgelementlist[i].node(j);
			double xx = nodelist[inode].x();
			double yy = nodelist[inode].y();
			if (std::abs(xx - x) < 0.00000001 && std::abs(yy - y) < 0.00000001){
				double edx = cpgelementlist[i].centnode().x()-x;
				double edy = cpgelementlist[i].centnode().y()-y;
				double edz = 0;
				double fx = cpgelementlist[i].fmmv(0);
				double fy = cpgelementlist[i].fmmv(1);
				double fz = cpgelementlist[i].fmmv(2);
				double ll_ = std::sqrt(edx*edx + edy*edy + edz*edz);
				double ff;
				if (edx != 0){
					double m = edy / edx; double n = edz / edx;
					double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
					ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
				}
				else if (edz == 0){
					ff = fy;
				}
				else{
					double m = edy / edz;
					double left = m*m / fy / fy + 1 / fz / fz;
					ff = std::sqrt((1 + m*m) / left);
				}
				double tt = ll_ / ff;
				cpgelementlist[i].tof(tt);
				cpgelementlist[i].status(2);

				fbset0.push_back(i);
				break;
			}
		}
	}
	return fbset0;

}

void REGION::nodeproperty2edge(){
	for (int ie = 0; ie < edgelist.size(); ie++){
		int i1 = edgelist[ie].node(0);
		int i2 = edgelist[ie].node(1);
		/*double k0 = 0.5*(nodelist[i1].k(0, 0) + nodelist[i2].k(0, 0));
		double k1 = 0.5*(nodelist[i1].k(1, 1) + nodelist[i2].k(1, 1));
		double k2 = 0.5*(nodelist[i1].k(2, 2) + nodelist[i2].k(2, 2));
		double poro_ = 0.5*(nodelist[i1].porosity() + nodelist[i2].porosity());
		double kk0 = k0 / mu_o / ct_ / poro_;
		double kk1 = k1 / mu_o / ct_ / poro_;
		double kk2 = k2 / mu_o / ct_ / poro_;*/
		double k0 = nodelist[i1].k(0, 0);
		double k1 = nodelist[i1].k(0, 0);
		double k2 = nodelist[i1].k(0, 0);
		double poro_ = nodelist[i1].porosity();
		double kk0a = std::sqrt(k0 / mu_o / ct_ / poro_);
		double kk1a = std::sqrt(k1 / mu_o / ct_ / poro_);
		double kk2a = std::sqrt(k2 / mu_o / ct_ / poro_);
		k0 = nodelist[i2].k(0, 0);
		k1 = nodelist[i2].k(0, 0);
		k2 = nodelist[i2].k(0, 0);
		double kk0b = std::sqrt(k0 / mu_o / ct_ / poro_);
		double kk1b = std::sqrt(k1 / mu_o / ct_ / poro_);
		double kk2b = std::sqrt(k2 / mu_o / ct_ / poro_);
		double kk0 = 0.5*(kk0a + kk0b);
		double kk1 = 0.5*(kk1a + kk1b);
        double kk2 = 0.5*(kk2a + kk2b);

		edgelist[ie].k(0, 0, kk0);
		edgelist[ie].k(1, 1, kk1);
		edgelist[ie].k(2, 2, kk2);
	}
}

void REGION::fmm_tet_sed_homov(std::vector<int> fbset0){
	int id_min;

	std::vector<int> band, fbset;


	do{
		fbset = fbset0;
		//find and compute narrow band
		for (int i = 0; i < fbset.size(); i++){
			int inode = fbset[i];
			std::vector<int> localedgelist;
			for (int j = 0; j < nodelist[inode].numberofedgein(); j++){
				int ied = nodelist[inode].edgein(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < nodelist[inode].numberofedgeout(); j++){
				int ied = nodelist[inode].edgeout(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < localedgelist.size(); j++){
				int ied = localedgelist[j];
				int jnode = edgelist[ied].node(0) + edgelist[ied].node(1) - inode;
				if (nodelist[jnode].status() != 2){//previously added-to-band cells are recomputed
					std::vector<int> localedgelist2;
					double tmin;

					for (int k = 0; k < nodelist[jnode].numberofedgein(); k++){
						int jed = nodelist[jnode].edgein(k);
						localedgelist2.push_back(jed);
					}
					for (int k = 0; k < nodelist[jnode].numberofedgeout(); k++){
						int jed = nodelist[jnode].edgeout(k);
						localedgelist2.push_back(jed);
					}
					int flag = 0;
					for (int k = 0; k < localedgelist2.size(); k++){
						int jed = localedgelist2[k];
						int knode = edgelist[jed].node(0) + edgelist[jed].node(1) - jnode;
						if (nodelist[knode].status() == 2){

							double edx = nodelist[jnode].x() - nodelist[knode].x();
							double edy = nodelist[jnode].y() - nodelist[knode].y();
							double edz = nodelist[jnode].z() - nodelist[knode].z();
							double ll_ = edgelist[jed].length();

							double fx = fmm_avvx;
							double fy = fmm_avvy;
							double fz = fmm_avvz;

							flag++;
							double ff;
							if (edx != 0){
								double m = edy / edx; double n = edz / edx;
								double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
								ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
							}
							else if (edz == 0){
								ff = fy;
							}
							else{
								double m = edy / edz;
								double left = m*m / fy / fy + 1 / fz / fz;
								ff = std::sqrt((1 + m*m) / left);
							}
							double tt = nodelist[knode].tof() + ll_ / ff;
							if (flag == 1){
								tmin = tt;
							}
							else if (tt < tmin){
								tmin = tt;
							}
						}
					}
					if (flag>0){
						if (nodelist[jnode].status() == 0){//don't include backtrace nodes
							band.push_back(jnode);//narrow band
							nodelist[jnode].status(1); //in band
						}
						nodelist[jnode].tof(tmin);
					}
				}
			}
		}

		//choose smallest and proceed

		double tof_min;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int inode = band[i];
			double tt = nodelist[inode].tof();
			if (i == 0){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
			else if (tt < tof_min){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
		}
		nodelist[id_min].status(2); //frozen
		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);
}

void REGION::fmm_correction(){
	if (nodalc.size() == 0){
		std::cout << "nodalc value needed for fmm correction" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].tof(nodelist[i].tof()*nodalc[i]);
	}
}

void REGION::fmm_tet_sed(std::vector<int> fbset0){
	int id_min;

	std::vector<int> band, fbset;
	//here initial set is a group of nodes
	//for (int i = 0; i < nodelist.size(); i++){
	//	if (nodelist[i].markwell() == startwellnumber_){
	//		if (std::abs(nodelist[i].z() - 200) < 0.0001){
	//			nodelist[i].status(2);//frozen
	//			nodelist[i].tof(0); //diffusive tof
	//			fbset0.push_back(i);
	//		}
	//	}
	//}

	do{
		fbset = fbset0;
		//find and compute narrow band
		for (int i = 0; i < fbset.size(); i++){
			int inode = fbset[i];
			std::vector<int> localedgelist;
			for (int j = 0; j < nodelist[inode].numberofedgein(); j++){
				int ied = nodelist[inode].edgein(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < nodelist[inode].numberofedgeout(); j++){
				int ied = nodelist[inode].edgeout(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < localedgelist.size(); j++){
				int ied = localedgelist[j];
				int jnode = edgelist[ied].node(0) + edgelist[ied].node(1) - inode;
				if (nodelist[jnode].status() !=2){//previously added-to-band cells are recomputed
					std::vector<int> localedgelist2;
					double tmin;

					for (int k = 0; k < nodelist[jnode].numberofedgein(); k++){
						int jed = nodelist[jnode].edgein(k);
						localedgelist2.push_back(jed);
					}
					for (int k = 0; k < nodelist[jnode].numberofedgeout(); k++){
						int jed = nodelist[jnode].edgeout(k);
						localedgelist2.push_back(jed);
					}
					int flag = 0;
					for (int k = 0; k < localedgelist2.size(); k++){
						int jed = localedgelist2[k];
						int knode = edgelist[jed].node(0) + edgelist[jed].node(1) - jnode;
						if (nodelist[knode].status() == 2){

							double edx = nodelist[jnode].x() - nodelist[knode].x();
							double edy = nodelist[jnode].y() - nodelist[knode].y();
							double edz = nodelist[jnode].z() - nodelist[knode].z();
							double ll_ = edgelist[jed].length();
							double fx = edgelist[jed].fmmv(0);
							double fy = edgelist[jed].fmmv(1);
							double fz = edgelist[jed].fmmv(2);

							flag++;
							double ff;
							if (edx != 0){
								double m = edy / edx; double n = edz / edx;
								double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
								ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
							}
							else if(edz==0){
								ff = fy;
							}
							else{
								double m = edy / edz;
								double left = m*m / fy / fy + 1 / fz / fz;
								ff = std::sqrt((1 + m*m) / left);
							}
							double tt = nodelist[knode].tof() + ll_ / ff;
							if (flag == 1){
								tmin = tt;
							}
							else if (tt < tmin){
								tmin = tt;
							}
						}
					}
					if(flag>0){
						if (nodelist[jnode].status() == 0){//don't include backtrace nodes
							band.push_back(jnode);//narrow band
							nodelist[jnode].status(1); //in band
						}
						nodelist[jnode].tof(tmin);
					}
				}
			}
		}

		//choose smallest and proceed

		double tof_min;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int inode = band[i];
			double tt = nodelist[inode].tof();
			if (i == 0){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
			else if (tt < tof_min){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
		}
		nodelist[id_min].status(2); //frozen
		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);
}

void REGION::fmm_tet_med_elebased(){
	int id_min;
	double b[3], c[3], d[3], bi, ci, di, B, C, D, aa, bb, cc;
	std::vector<std::vector<double>> matrix33;
	std::vector<int> bandele, bandnode, fbset, fbset0;
	std::vector<double> bandnodet;
	matrix33.resize(3);
	for (int i = 0; i < 3; i++){
		matrix33[i].resize(3);
	}
	shapefunction();

	//for (int ie = 0; ie < elementlist.size(); ie++){
	//	int flag = 0;
	//	for (int j = 0; j < 4; j++){
	//		int jnode = elementlist[ie].node(j);
	//		if (nodelist[jnode].markwell() == startwellnumber_){
	//			flag = 1;
	//		}
	//	}
	//	if (flag == 1){
	//		fbset0.push_back(ie);
	//		elementlist[ie].status(2);
	//		for (int j = 0; j < 4; j++){
	//			int jnode = elementlist[ie].node(j);
	//			nodelist[jnode].tof(0); //diffusive tof
	//			nodelist[jnode].status(2);
	//		}
	//	}
	//}
	nodelist[0].tof(0);
	nodelist[0].status(2);
	for (int ie = 0; ie < elementlist.size(); ie++){
		int flag = 0;
		for (int j = 0; j < 4; j++){
			int jnode = elementlist[ie].node(j);
			if (jnode==0){
				flag = 1;
			}
		}
		if (flag == 1){
			fbset0.push_back(ie);
			elementlist[ie].status(2);
			for (int j = 0; j < 4; j++){
				int jnode = elementlist[ie].node(j);
				nodelist[jnode].tof(0); //diffusive tof
				nodelist[jnode].status(2);
			}
		}
	}

	//initial narrow bands of elements and nodes
	for (int ii = 0; ii < fbset0.size(); ii++){
		int iee = fbset0[ii];
		for (int i = 0; i < 4; i++){
			int ie = elementlist[iee].neighbor(i);
			if (ie >= 0){
				if (elementlist[ie].status() == 0){
					int iunk;
					int count = 0;
					for (int jj = 0; jj < 4; jj++){
						int nn = elementlist[ie].node(jj);
						if (nodelist[nn].status() == 0 || nodelist[nn].status() == 1){
							nodelist[nn].status(1);
							iunk = nn;
						}
						else if (nodelist[nn].status() == 2){
							count++;
						}
					}
					if (count == 3){
						//compute new node tof
						double ff = 1;
						std::vector<double> tvec;
						tvec.resize(3);
						int crow = -1;
						for (int ii = 0; ii < 4; ii++){
							int nn = elementlist[ie].node(ii);
							if (nn != iunk){
								crow++;
								double x1 = nodelist[nn].x();
								double y1 = nodelist[nn].y();
								double z1 = nodelist[nn].z();
								double x2 = nodelist[iunk].x();
								double y2 = nodelist[iunk].y();
								double z2 = nodelist[iunk].z();
								double vx = x2 - x1;//ud*l
								double vy = y2 - y1;
								double vz = z2 - z1;
								matrix33[crow][0] = vx;
								matrix33[crow][1] = vy;
								matrix33[crow][2] = vz;
								tvec[crow] = nodelist[nn].tof();
							}
						}
						matrix33 = mathzz_.inverse33matrix(matrix33);
						double a11 = matrix33[0][0]; double a12 = matrix33[0][1]; double a13 = matrix33[0][2];
						double a21 = matrix33[1][0]; double a22 = matrix33[1][1]; double a23 = matrix33[1][2];
						double a31 = matrix33[2][0]; double a32 = matrix33[2][1]; double a33 = matrix33[2][2];
						double b1 = a11 + a21 + a31; double b2 = a12 + a22 + a32; double b3 = a31 + a32 + a33;
						aa = b1*b1 + b2*b2 + b3*b3;
						bb = -2 * (b1*b1*tvec[0] + b2*b2*tvec[1] + b3*b3*tvec[2]);
						cc = b1*b1*tvec[0] * tvec[0] + b2*b2*tvec[1] * tvec[1] + b3*b3*tvec[2] * tvec[2] - 1.0 / ff / ff;
						double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;

						//if (tt >= tvec[0] && tt >= tvec[1] && tt >= tvec[2]){//causality
							bandnodet.push_back(max(tt,0));
							bandele.push_back(ie);
							bandnode.push_back(iunk);
							elementlist[ie].status(1);
						//}
					}
				}
			}
		}
	}
	//inital band finish
	int flag;

	do{

		//choose smallest and proceed
		double tof_min;
		int id_vec, inext;
		id_min = -1;
		int cc = 0;
		for (int i = 0; i < bandnode.size(); i++){
			int inode = bandnode[i];
			if (nodelist[inode].status() == 1){
				double tt = bandnodet[i];
				cc++;
					if (cc == 1){
						tof_min = tt;
						id_min = inode;
						id_vec = i;
					}
					else if (tt < tof_min){
						tof_min = tt;
						id_min = inode;
						id_vec = i;
					}
			}
		}
		nodelist[id_min].status(2); //frozen
		nodelist[id_min].tof(tof_min);
		inext = bandele[id_vec];
		elementlist[inext].status(2);
		bandnode.erase(bandnode.begin() + id_vec);
		bandele.erase(bandele.begin() + id_vec);
		bandnodet.erase(bandnodet.begin() + id_vec);

		for (int i = 0; i < 4; i++){
			int ie = elementlist[inext].neighbor(i);
			if (ie >= 0){
				if (elementlist[ie].status() == 0){//one ele corresponds to one node
					int iunk;
					int count = 0;
					for (int ii = 0; ii < 4; ii++){
						int iinode = elementlist[ie].node(ii);
						if (nodelist[iinode].status() == 0 || nodelist[iinode].status() == 1){
							nodelist[iinode].status(1);
							iunk = iinode;
						}
						else if (nodelist[iinode].status() == 2){
							count++;
						}
					}
					if (count == 3){
						//compute new node tof
						double ff = 1;
						std::vector<double> tvec;
						tvec.resize(3);
						int crow = -1;
						for (int ii = 0; ii < 4; ii++){
							int nn = elementlist[ie].node(ii);
							if (nn != iunk){
								crow++;
								double x1 = nodelist[nn].x();
								double y1 = nodelist[nn].y();
								double z1 = nodelist[nn].z();
								double x2 = nodelist[iunk].x();
								double y2 = nodelist[iunk].y();
								double z2 = nodelist[iunk].z();
								double vx = x2 - x1;//ud*l
								double vy = y2 - y1;
								double vz = z2 - z1;
								matrix33[crow][0] = vx;
								matrix33[crow][1] = vy;
								matrix33[crow][2] = vz;
								tvec[crow] = nodelist[nn].tof();
							}
						}
						matrix33 = mathzz_.inverse33matrix(matrix33);
						double a11 = matrix33[0][0]; double a12 = matrix33[0][1]; double a13 = matrix33[0][2];
						double a21 = matrix33[1][0]; double a22 = matrix33[1][1]; double a23 = matrix33[1][2];
						double a31 = matrix33[2][0]; double a32 = matrix33[2][1]; double a33 = matrix33[2][2];
						double b1 = a11 + a21 + a31; double b2 = a12 + a22 + a32; double b3 = a31 + a32 + a33;
						aa = b1*b1 + b2*b2 + b3*b3;
						bb = -2 * (b1*b1*tvec[0] + b2*b2*tvec[1] + b3*b3*tvec[2]);
						cc = b1*b1*tvec[0] * tvec[0] + b2*b2*tvec[1] * tvec[1] + b3*b3*tvec[2] * tvec[2] - 1.0 / ff / ff;
						double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;

						//if (tt >= tvec[0] && tt >= tvec[1] && tt >= tvec[2]){//causality
							bandnodet.push_back(max(tt,0));
							bandele.push_back(ie);
							bandnode.push_back(iunk);
							elementlist[ie].status(1);
						//}
					}
				}
			}
		}
		flag = 0;
		for (int i = 0; i<bandnode.size(); i++){
			int inode = bandnode[i];
			if (nodelist[inode].status() != 2){
				flag = 1;
			}
		}

	} while (flag == 1);
}

void REGION::fmm_tet_medmixed(){
	int id_min;
	double a11, a12, a13, a21, a22, a23, a31, a32, a33, b1, b2, b3, aa, bb, cc;
	std::vector<std::vector<double>> matrix33;
	std::vector<int> band, fbset, fbset0;
	matrix33.resize(3);
	for (int i = 0; i < 3; i++){
		matrix33[i].resize(3);
	}
	//here initial set is a group of nodes
	for (int i = 0; i < nodelist.size(); i++){
		if (nodelist[i].markwell() == startwellnumber_){
			if (std::abs(nodelist[i].z() - 200) < 0.0001){
				nodelist[i].status(2);//frozen
				nodelist[i].tof(0); //diffusive tof
				fbset0.push_back(i);
			}
		}
	}
	int inode;
	int ist0 = fbset0[0]; //start from any tof=0
	int inext;
	do{
		inode = ist0;
		int csuele = 0;
		//find and compute initial narrow band elements
		for (int i = 0; i < nodelist[inode].numberofelement(); i++){
			int ie = nodelist[inode].element(i);
			int count = 0;
			for (int ii = 0; ii < 4; ii++){
				int nn = elementlist[ie].node(ii);
				if (nodelist[nn].status() == 2){
					count++;
				}
				else{
					inext = nn;
				}
			}
			if (count == 3 && nodelist[inext].status()==0){
				band.push_back(inext);//narrow band
				nodelist[inext].status(1);
			}
			if (count == 3 && nodelist[inext].status() == 1){
				csuele++;
				//recompute already or new band nodes using elements
				double tmin;
				//firstly element based
				int jnode = inext;
				int countele = 0;
				int flag = 0;
				for (int k = 0; k < nodelist[jnode].numberofelement(); k++){
					int ie = nodelist[jnode].element(k);
					int count = 0;
					for (int ii = 0; ii < 4; ii++){
						int nn = elementlist[ie].node(ii);
						if (nodelist[nn].status() == 2){
							count++;
						}
					}
					if (count == 3){//this is a defining element
						double ff = 1;
						std::vector<double> tvec;
						tvec.resize(3);
						int crow = -1;
						for (int ii = 0; ii < 4; ii++){
							int nn = elementlist[ie].node(ii);
							if (nn != jnode){
								crow++;
								double x1 = nodelist[nn].x();
								double y1 = nodelist[nn].y();
								double z1 = nodelist[nn].z();
								double x2 = nodelist[jnode].x();
								double y2 = nodelist[jnode].y();
								double z2 = nodelist[jnode].z();
								double vx = x2 - x1;//ud*l
								double vy = y2 - y1;
								double vz = z2 - z1;
								matrix33[crow][0] = vx;
								matrix33[crow][1] = vy;
								matrix33[crow][2] = vz;
								tvec[crow] = nodelist[nn].tof();
							}
						}
						matrix33 = mathzz_.inverse33matrix(matrix33);
						a11 = matrix33[0][0]; a12 = matrix33[0][1]; a13 = matrix33[0][2];
						a21 = matrix33[1][0]; a22 = matrix33[1][1]; a23 = matrix33[1][2];
						a31 = matrix33[2][0]; a32 = matrix33[2][1]; a33 = matrix33[2][2];
						b1 = a11 + a21 + a31; b2 = a12 + a22 + a32; b3 = a31 + a32 + a33;
						aa = b1*b1 + b2*b2 + b3*b3;
						bb = -2 * (b1*b1*tvec[0] + b2*b2*tvec[1] + b3*b3*tvec[2]);
						cc = b1*b1*tvec[0] * tvec[0] + b2*b2*tvec[1] * tvec[1] + b3*b3*tvec[2] * tvec[2] - 1.0 / ff / ff;
						double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;
						if (tt > tvec[0] && tt > tvec[1] && tt > tvec[2]){//causality
							countele++;
							if (countele == 1){
								tmin = tt;
							}
							else if (countele > 1 && tt < tmin){
								tmin = tt;
							}
						}

					}
				}
				if (countele>0){
					nodelist[jnode].tof(tmin);
				}
				else{
					std::vector<int> localedgelist2;
					//single edge based
					for (int k = 0; k < nodelist[jnode].numberofedgein(); k++){
						int jed = nodelist[jnode].edgein(k);
						localedgelist2.push_back(jed);
					}
					for (int k = 0; k < nodelist[jnode].numberofedgeout(); k++){
						int jed = nodelist[jnode].edgeout(k);
						localedgelist2.push_back(jed);
					}
					int flag = 0;
					for (int k = 0; k < localedgelist2.size(); k++){
						int jed = localedgelist2[k];
						int knode = edgelist[jed].node(0) + edgelist[jed].node(1) - jnode;
						if (nodelist[knode].status() == 2){
							flag++;
							double edx = nodelist[jnode].x() - nodelist[knode].x();
							double edy = nodelist[jnode].y() - nodelist[knode].y();
							double edz = nodelist[jnode].z() - nodelist[knode].z();
							double k11 = edgelist[jed].k(0, 0);
							double k22 = edgelist[jed].k(1, 1);
							double k33 = edgelist[jed].k(2, 2);
							/*double ff = std::sqrt(k11*edx*k11*edx + k22*edy*k22*edy + k33*edz*k33*edz) / edgelist[jed].length();*/
							double ff = 1;
							double tt = nodelist[knode].tof() + edgelist[jed].length() / std::sqrt(ff);
							if (flag == 1){
								tmin = tt;
							}
							else if (tt < tmin){
								tmin = tt;
							}
						}
					}
					if (flag == 0){
						std::cout << "error no frozen neighbor" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					else{
						nodelist[jnode].tof(tmin);
					}
				}
			}
		}
		if (csuele == 0){
			std::vector<int> localedgelist;
			for (int j = 0; j < nodelist[inode].numberofedgein(); j++){
				int ied = nodelist[inode].edgein(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < nodelist[inode].numberofedgeout(); j++){
				int ied = nodelist[inode].edgeout(j);
				localedgelist.push_back(ied);
			}
			for (int j = 0; j < localedgelist.size(); j++){
				int ied = localedgelist[j];
				int jnode = edgelist[ied].node(0) + edgelist[ied].node(1) - inode;
				if (nodelist[jnode].status() == 0){
					band.push_back(jnode);//narrow band
					nodelist[jnode].status(1); //in band
				}
				if (nodelist[jnode].status()==1){//previously added-to-band cells are recomputed
					std::vector<int> localedgelist2;
					double tmin;
					//single edge based
					for (int k = 0; k < nodelist[jnode].numberofedgein(); k++){
						int jed = nodelist[jnode].edgein(k);
						localedgelist2.push_back(jed);
					}
					for (int k = 0; k < nodelist[jnode].numberofedgeout(); k++){
						int jed = nodelist[jnode].edgeout(k);
						localedgelist2.push_back(jed);
					}
					int flag = 0;
					for (int k = 0; k < localedgelist2.size(); k++){
						int jed = localedgelist2[k];
						int knode = edgelist[jed].node(0) + edgelist[jed].node(1) - jnode;
						if (nodelist[knode].status() == 2){
							flag++;
							double edx = nodelist[jnode].x() - nodelist[knode].x();
							double edy = nodelist[jnode].y() - nodelist[knode].y();
							double edz = nodelist[jnode].z() - nodelist[knode].z();
							double k11 = edgelist[jed].k(0, 0);
							double k22 = edgelist[jed].k(1, 1);
							double k33 = edgelist[jed].k(2, 2);
							/*double ff = std::sqrt(k11*edx*k11*edx + k22*edy*k22*edy + k33*edz*k33*edz) / edgelist[jed].length();*/
							double ff = 1;
							double tt = nodelist[knode].tof() + edgelist[jed].length() / std::sqrt(ff);
							if (flag == 1){
								tmin = tt;
							}
							else if (tt < tmin){
								tmin = tt;
							}
						}
					}
					if (flag == 0){
						std::cout << "error no frozen neighbor" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					else{
						nodelist[jnode].tof(tmin);
					}
				}
			}
		}

		//choose smallest and proceed

		double tof_min;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int inode = band[i];
			double tt = nodelist[inode].tof();
			if (i == 0){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
			else if (tt < tof_min){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
		}
		nodelist[id_min].status(2); //frozen
		band.erase(band.begin() + id_vec);
		ist0 = id_min;
	} while (id_min >= 0 && band.size()>0);
}

void REGION::fmm_tet_med_nodebased(){
	int id_min;
	double a11, a12, a13, a21, a22, a23, a31, a32, a33, b1, b2, b3, aa, bb, cc;
	double b[3], c[3], d[3], bi, ci, di, B, C, D;
	std::vector<std::vector<double>> matrix33;
	std::vector<int> band, fbset, fbset0;
	matrix33.resize(3);
	for (int i = 0; i < 3; i++){
		matrix33[i].resize(3);
	}
	shapefunction();
	for (int ie = 0; ie < elementlist.size(); ie++){
		int flag = 0;
		for (int i = 0; i < 4; i++){
			int inode = elementlist[ie].node(i);
			if (inode == 0){
				flag = 1;
			}
		}
		if (flag == 1){
			for (int i = 0; i < 4; i++){
				int inode = elementlist[ie].node(i);
				if (nodelist[inode].status() == 0){
					nodelist[inode].status(2);
					nodelist[inode].tof(0);
					fbset0.push_back(inode);
				}
			}
		}
	}

	do{
		fbset = fbset0;
		for (int ibb = 0; ibb < fbset.size(); ibb++){
			int inode = fbset[ibb];
			std::cout << "fmm node " << inode << std::endl;
			for (int i = 0; i < nodelist[inode].numberofelement(); i++){
				int ie = nodelist[inode].element(i);
				int count = 0;
				int inext;
				for (int ii = 0; ii < 4; ii++){
					int nn = elementlist[ie].node(ii);
					if (nodelist[nn].status() == 2){
						count++;
					}
					else{
						inext = nn;
					}
				}

				if (count == 3 && nodelist[inext].status() != 2){
					//recompute already or new band nodes using elements
					double tmin;
					//firstly element based
					int jnode = inext;
					int countele = 0;
					int flag = 0;
					for (int k = 0; k < nodelist[jnode].numberofelement(); k++){
						int ie = nodelist[jnode].element(k);
						int count = 0;
						for (int ii = 0; ii < 4; ii++){
							int nn = elementlist[ie].node(ii);
							if (nodelist[nn].status() == 2){
								count++;
							}
						}
						if (count == 3){//this is a defining element
							double ff = 1;
							std::vector<double> tvec;
							tvec.resize(3);
							int crow = -1;
							for (int ii = 0; ii < 4; ii++){
								int nn = elementlist[ie].node(ii);
								if (nn != jnode){
									crow++;
									double x1 = nodelist[nn].x();
									double y1 = nodelist[nn].y();
									double z1 = nodelist[nn].z();
									double x2 = nodelist[jnode].x();
									double y2 = nodelist[jnode].y();
									double z2 = nodelist[jnode].z();
									double vx = x2 - x1;//ud*l
									double vy = y2 - y1;
									double vz = z2 - z1;
									matrix33[crow][0] = vx;
									matrix33[crow][1] = vy;
									matrix33[crow][2] = vz;
									tvec[crow] = nodelist[nn].tof();
								}
							}
							matrix33 = mathzz_.inverse33matrix(matrix33);
							a11 = matrix33[0][0]; a12 = matrix33[0][1]; a13 = matrix33[0][2];
							a21 = matrix33[1][0]; a22 = matrix33[1][1]; a23 = matrix33[1][2];
							a31 = matrix33[2][0]; a32 = matrix33[2][1]; a33 = matrix33[2][2];
							b1 = a11 + a21 + a31; b2 = a12 + a22 + a32; b3 = a31 + a32 + a33;
							aa = b1*b1 + b2*b2 + b3*b3;
							bb = -2 * (b1*b1*tvec[0] + b2*b2*tvec[1] + b3*b3*tvec[2]);
							cc = b1*b1*tvec[0] * tvec[0] + b2*b2*tvec[1] * tvec[1] + b3*b3*tvec[2] * tvec[2] - 1.0 / ff / ff;
							double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;
							if (tt > tvec[0] && tt > tvec[1] && tt > tvec[2]){//causality
								countele++;
								if (countele == 1){
									tmin = tt;
								}
								else if (countele > 1 && tt < tmin){
									tmin = tt;
								}
							}
						}
					}
					if (countele>0){
						nodelist[jnode].tof(tmin);
						if (nodelist[jnode].status() == 0){
							band.push_back(jnode);//narrow band
							nodelist[jnode].status(1);
						}
					}
				}
			}
		}

		//choose smallest and proceed

		double tof_min;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int inode = band[i];
			double tt = nodelist[inode].tof();
			if (i == 0){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
			else if (tt < tof_min){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
		}
		nodelist[id_min].status(2); //frozen
		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);
}

void REGION::fmm_tet_fe_nodebased(){
	int id_min;
	double a11, a12, a13, a21, a22, a23, a31, a32, a33, b1, b2, b3, aa, bb, cc;
	double b[3], c[3], d[3], bi, ci, di, B, C, D;
	std::vector<std::vector<double>> matrix33;
	std::vector<int> band, fbset, fbset0;
	matrix33.resize(3);
	for (int i = 0; i < 3; i++){
		matrix33[i].resize(3);
	}
	shapefunction();
	for (int ie = 0; ie < elementlist.size(); ie++){
		int flag = 0;
		for (int i = 0; i < 4; i++){
			int inode = elementlist[ie].node(i);
			if (inode == 0){
				flag = 1;
			}
		}
		if (flag == 1){
			for (int i = 0; i < 4; i++){
				int inode = elementlist[ie].node(i);
				if (nodelist[inode].status() == 0){
					nodelist[inode].status(2);
					nodelist[inode].tof(0);
					fbset0.push_back(inode);
				}
			}
		}
	}

	do{
		fbset = fbset0;
		for (int ibb = 0; ibb < fbset.size(); ibb++){
			int inode = fbset[ibb];
			std::cout << "fmm node " << inode << std::endl;
			for (int i = 0; i < nodelist[inode].numberofelement(); i++){
				int ie = nodelist[inode].element(i);
				int count = 0;
				int inext;
				for (int ii = 0; ii < 4; ii++){
					int nn = elementlist[ie].node(ii);
					if (nodelist[nn].status() == 2){
						count++;
					}
					else{
						inext = nn;
					}
				}

				if (count == 3 && nodelist[inext].status() != 2){
					//recompute already or new band nodes using elements
					double tmin;
					//firstly element based
					int jnode = inext;
					int countele = 0;
					int flag = 0;
					for (int k = 0; k < nodelist[jnode].numberofelement(); k++){
						int ie = nodelist[jnode].element(k);
						int count = 0;
						for (int ii = 0; ii < 4; ii++){
							int nn = elementlist[ie].node(ii);
							if (nodelist[nn].status() == 2){
								count++;
							}
						}
						if (count == 3){//this is a defining element
							double ff = 1;
							std::vector<double> tvec, localn;
							tvec.resize(3);
							localn.resize(3);
							int crow = -1;
							B = 0; C = 0; D = 0;
							for (int jj = 0; jj < 4; jj++){
								int nn = elementlist[ie].node(jj);
								if (nn != jnode){
									crow++;
									b[crow] = elementlist[ie].Nb(jj);
									c[crow] = elementlist[ie].Nc(jj);
									d[crow] = elementlist[ie].Nd(jj);
									tvec[crow] = nodelist[nn].tof();
									B = B + b[crow] * tvec[crow];
									C = C + c[crow] * tvec[crow];
									D = D + d[crow] * tvec[crow];
								}
								else{
									bi = elementlist[ie].Nb(jj);
									ci = elementlist[ie].Nc(jj);
									di = elementlist[ie].Nd(jj);
								}
							}
							aa = bi*bi + ci*ci + di*di;
							bb = 2.0*(B*bi + C*ci + D*di);
							cc = B*B + C*C + D*D - 1.0 / ff / ff;
							double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;
							//if (tt >= tvec[0] && tt >= tvec[1] && tt >= tvec[2]){//causality
								countele++;
								if (countele == 1){
									tmin = tt;
								}
								else if (countele > 1 && tt < tmin){
									tmin = tt;
								}
							//}
						}
					}
					if (countele>0){
						nodelist[jnode].tof(tmin);
						if (nodelist[jnode].status() == 0){
							band.push_back(jnode);//narrow band
							nodelist[jnode].status(1);
						}
					}
				}
			}
		}

		//choose smallest and proceed

		double tof_min;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int inode = band[i];
			double tt = nodelist[inode].tof();
			if (i == 0){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
			else if (tt < tof_min){
				tof_min = tt;
				id_min = inode;
				id_vec = i;
			}
		}
		nodelist[id_min].status(2); //frozen
		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);
}

void REGION::fmm_tet_fe(){
	int id_min;
	double b[3], c[3], d[3], bi, ci, di, B, C, D, aa, bb, cc;
	std::vector<std::vector<double>> matrix33;
	std::vector<int> bandele,bandnode, fbset, fbset0;
	std::vector<double> bandnodet;
	matrix33.resize(3);
	for (int i = 0; i < 3; i++){
		matrix33[i].resize(3);
	}
	shapefunction();
	/*for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].tof(0);
	}*/

	for (int ie = 0; ie < elementlist.size(); ie++){
		int flag = 0;
		for (int j = 0; j < 4; j++){
			int jnode = elementlist[ie].node(j);
			if (nodelist[jnode].markwell() == startwellnumber_){
				flag = 1;
			}
		}
		if (flag == 1){
			fbset0.push_back(ie);
			elementlist[ie].status(2);
			for (int j = 0; j < 4; j++){
				int jnode = elementlist[ie].node(j);
				nodelist[jnode].tof(0); //diffusive tof
				nodelist[jnode].status(2);
			}
		}
	}

	//initial narrow bands of elements and nodes
	for (int ii = 0; ii < fbset0.size(); ii++){
		int iee = fbset0[ii];
		for (int i = 0; i < 4; i++){
			int ie = elementlist[iee].neighbor(i);
			if (ie >= 0){
				if (elementlist[ie].status() == 0){
					int iunk;
					int count = 0;
					for (int jj = 0; jj < 4; jj++){
						int nn = elementlist[ie].node(jj);
						if (nodelist[nn].status() == 0 || nodelist[nn].status() == 1){
							nodelist[nn].status(1);
							iunk = nn;
						}
						else if (nodelist[nn].status() == 2){
							count++;
						}
					}
					if (count == 3){
						//compute new node tof
						double ff = 1;
						std::vector<double> tvec, localn;
						tvec.resize(3);
						localn.resize(3);
						int crow = -1;
						B = 0; C = 0; D = 0;
						for (int jj = 0; jj < 4; jj++){
							int nn = elementlist[ie].node(jj);
							if (nn != iunk){
								crow++;
								b[crow] = elementlist[ie].Nb(jj);
								c[crow] = elementlist[ie].Nc(jj);
								d[crow] = elementlist[ie].Nd(jj);
								tvec[crow] = nodelist[nn].tof();
								B = B + b[crow] * tvec[crow];
								C = C + c[crow] * tvec[crow];
								D = D + d[crow] * tvec[crow];
							}
							else{
								bi = elementlist[ie].Nb(jj);
								ci = elementlist[ie].Nc(jj);
								di = elementlist[ie].Nd(jj);
							}
						}
						aa = bi*bi + ci*ci + di*di;
						bb = 2.0*(B*bi + C*ci + D*di);
						cc = B*B + C*C + D*D - 1.0 / ff / ff;
						double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;
						if (tt >= tvec[0] && tt >= tvec[1] && tt >= tvec[2]){//causality
							bandnodet.push_back(tt);
							bandele.push_back(ie);
							bandnode.push_back(iunk);
							elementlist[ie].status(1);
						}
						/*else{
							bandnodet.push_back(max(tvec[0], tvec[1], tvec[2]));
							bandele.push_back(ie);
							bandnode.push_back(iunk);
						}*/
					}
				}
			}
		}
	}
	//inital band finish
	int flag;
	do{
		//choose smallest and proceed
		double tof_min=1e+20;
		int id_vec, inext;
		id_min = -1;
		for (int i = 0; i < bandnode.size(); i++){
			int inode = bandnode[i];
			if (nodelist[inode].status() == 1){
				double tt = bandnodet[i];
				if (tt < tof_min){
					tof_min = tt;
					id_min = inode;
					id_vec = i;
				}
			}
		}
		nodelist[id_min].status(2); //frozen
		nodelist[id_min].tof(tof_min);
		inext = bandele[id_vec];
		elementlist[inext].status(2);
		bandnode.erase(bandnode.begin() + id_vec);
		bandele.erase(bandele.begin() + id_vec);
		bandnodet.erase(bandnodet.begin() + id_vec);

		for (int i = 0; i < 4; i++){
			int ie = elementlist[inext].neighbor(i);
			if (ie >= 0){
				if (elementlist[ie].status() == 0){//one ele corresponds to one node
					int iunk;
					int count = 0;
					for (int ii = 0; ii < 4; ii++){
						int iinode = elementlist[ie].node(ii);
						if (nodelist[iinode].status() == 0 || nodelist[iinode].status() == 1){
							nodelist[iinode].status(1);
							iunk = iinode;
						}
						else if (nodelist[iinode].status() == 2){
							count++;
						}
					}
					if (count == 3){
						//compute new node tof
						double ff = 1;
						std::vector<double> tvec, localn;
						tvec.resize(3);
						localn.resize(3);
						int crow = -1;
						B = 0; C = 0; D = 0;
						for (int jj = 0; jj < 4; jj++){
							int nn = elementlist[ie].node(jj);
							if (nn != iunk){
								crow++;
								b[crow] = elementlist[ie].Nb(jj);
								c[crow] = elementlist[ie].Nc(jj);
								d[crow] = elementlist[ie].Nd(jj);
								tvec[crow] = nodelist[nn].tof();
								B = B + b[crow] * tvec[crow];
								C = C + c[crow] * tvec[crow];
								D = D + d[crow] * tvec[crow];
							}
							else{
								bi = elementlist[ie].Nb(jj);
								ci = elementlist[ie].Nc(jj);
								di = elementlist[ie].Nd(jj);
							}
						}
						aa = bi*bi + ci*ci + di*di;
						bb = 2.0*(B*bi + C*ci + D*di);
						cc = B*B + C*C + D*D - 1.0 / ff / ff;
						double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;
						if (tt >= tvec[0] && tt >= tvec[1] && tt >= tvec[2]){//causality
							bandnodet.push_back(tt);
							bandele.push_back(ie);
							bandnode.push_back(iunk);
						}
						/*else{
							bandnodet.push_back(max(tvec[0], tvec[1], tvec[2]));
							bandele.push_back(ie);
							bandnode.push_back(iunk);
							elementlist[ie].status(1);
						}*/
					}
				}
			}
		}
		flag = 0;
		for (int i = 0; i<bandnode.size(); i++){
			int inode = bandnode[i];
			if (nodelist[inode].status() != 2){
				flag = 1;
			}
		}

	} while (flag==1);
}

void REGION::fmm_tet_fefirst(){
	int id_min;
	double b[3], c[3], d[3], bi, ci, di, B, C, D, aa, bb, cc;
	std::vector<std::vector<double>> matrix33;
	std::vector<int> bandele, bandnode, fbset, fbset0;
	std::vector<double> bandnodet;
	matrix33.resize(3);
	for (int i = 0; i < 3; i++){
		matrix33[i].resize(3);
	}
	shapefunction();
	/*for (int i = 0; i < nodelist.size(); i++){
	nodelist[i].tof(0);
	}*/

	for (int ie = 0; ie < elementlist.size(); ie++){
		int flag = 0;
		for (int j = 0; j < 4; j++){
			int jnode = elementlist[ie].node(j);
			if (nodelist[jnode].markwell() == startwellnumber_){
				flag = 1;
			}
		}
		if (flag == 1){
			fbset0.push_back(ie);
			elementlist[ie].status(2);
			for (int j = 0; j < 4; j++){
				int jnode = elementlist[ie].node(j);
				nodelist[jnode].tof(0); //diffusive tof
				nodelist[jnode].status(2);
			}
		}
	}


	for (int ii = 0; ii < fbset0.size(); ii++){
		int iee = fbset0[ii];
		for (int i = 0; i < 4; i++){
			int ie = elementlist[iee].neighbor(i);
			if (ie >= 0){
				if (elementlist[ie].status() == 0){
					int iunk;
					int count = 0;
					for (int jj = 0; jj < 4; jj++){
						int nn = elementlist[ie].node(jj);
						if (nodelist[nn].status() == 0 || nodelist[nn].status() == 1){
							nodelist[nn].status(1);
							iunk = nn;
						}
						else if (nodelist[nn].status() == 2){
							count++;
						}
					}
					if (count == 3){
						//compute new node tof
						double ff = 1;
						std::vector<double> tvec, localn;
						tvec.resize(3);
						localn.resize(3);
						int crow = -1;
						B = 0; C = 0; D = 0;
						for (int jj = 0; jj < 4; jj++){
							int nn = elementlist[ie].node(jj);
							if (nn != iunk){
								crow++;
								b[crow] = elementlist[ie].Nb(jj);
								c[crow] = elementlist[ie].Nc(jj);
								d[crow] = elementlist[ie].Nd(jj);
								tvec[crow] = nodelist[nn].tof();
								B = B + b[crow] * tvec[crow];
								C = C + c[crow] * tvec[crow];
								D = D + d[crow] * tvec[crow];
							}
							else{
								bi = elementlist[ie].Nb(jj);
								ci = elementlist[ie].Nc(jj);
								di = elementlist[ie].Nd(jj);
							}
						}
						aa = bi*bi + ci*ci + di*di;
						bb = 2.0*(B*bi + C*ci + D*di);
						cc = B*B + C*C + D*D - 1.0 / ff / ff;
						double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;
						if (tt >= tvec[0] && tt >= tvec[1] && tt >= tvec[2]){//causality
							bandnodet.push_back(tt);
							bandele.push_back(ie);
							bandnode.push_back(iunk);
							elementlist[ie].status(1);
						}
						/*else{
						bandnodet.push_back(max(tvec[0], tvec[1], tvec[2]));
						bandele.push_back(ie);
						bandnode.push_back(iunk);
						}*/
					}
				}
			}
		}
	}
	//inital band finish
	int flag;
	do{
		//choose smallest and proceed
		double tof_min = 1e+20;
		int id_vec, inext;
		id_min = -1;
		for (int i = 0; i < bandnode.size(); i++){
			int inode = bandnode[i];
			if (nodelist[inode].status() == 1){
				double tt = bandnodet[i];
				if (tt < tof_min){
					tof_min = tt;
					id_min = inode;
					id_vec = i;
				}
			}
		}
		nodelist[id_min].status(2); //frozen
		nodelist[id_min].tof(tof_min);
		inext = bandele[id_vec];
		elementlist[inext].status(2);
		bandnode.erase(bandnode.begin() + id_vec);
		bandele.erase(bandele.begin() + id_vec);
		bandnodet.erase(bandnodet.begin() + id_vec);

		for (int i = 0; i < 4; i++){
			int ie = elementlist[inext].neighbor(i);
			if (ie >= 0){
				if (elementlist[ie].status() == 0){//one ele corresponds to one node
					int iunk;
					int count = 0;
					for (int ii = 0; ii < 4; ii++){
						int iinode = elementlist[ie].node(ii);
						if (nodelist[iinode].status() == 0 || nodelist[iinode].status() == 1){
							nodelist[iinode].status(1);
							iunk = iinode;
						}
						else if (nodelist[iinode].status() == 2){
							count++;
						}
					}
					if (count == 3){
						//compute new node tof
						double ff = 1;
						std::vector<double> tvec, localn;
						tvec.resize(3);
						localn.resize(3);
						int crow = -1;
						B = 0; C = 0; D = 0;
						for (int jj = 0; jj < 4; jj++){
							int nn = elementlist[ie].node(jj);
							if (nn != iunk){
								crow++;
								b[crow] = elementlist[ie].Nb(jj);
								c[crow] = elementlist[ie].Nc(jj);
								d[crow] = elementlist[ie].Nd(jj);
								tvec[crow] = nodelist[nn].tof();
								B = B + b[crow] * tvec[crow];
								C = C + c[crow] * tvec[crow];
								D = D + d[crow] * tvec[crow];
							}
							else{
								bi = elementlist[ie].Nb(jj);
								ci = elementlist[ie].Nc(jj);
								di = elementlist[ie].Nd(jj);
							}
						}
						aa = bi*bi + ci*ci + di*di;
						bb = 2.0*(B*bi + C*ci + D*di);
						cc = B*B + C*C + D*D - 1.0 / ff / ff;
						double tt = (-bb + std::sqrt(bb*bb - 4 * aa*cc)) / 2.0 / aa;
						if (tt >= tvec[0] && tt >= tvec[1] && tt >= tvec[2]){//causality
							bandnodet.push_back(tt);
							bandele.push_back(ie);
							bandnode.push_back(iunk);
						}
						/*else{
						bandnodet.push_back(max(tvec[0], tvec[1], tvec[2]));
						bandele.push_back(ie);
						bandnode.push_back(iunk);
						elementlist[ie].status(1);
						}*/
					}
				}
			}
		}
		flag = 0;
		for (int i = 0; i<bandnode.size(); i++){
			int inode = bandnode[i];
			if (nodelist[inode].status() != 2){
				flag = 1;
			}
		}

	} while (flag == 1);
}

void REGION::cpgflux2fmmv(){
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		/*double fx = 0.5*(std::abs(cpgelementlist[ie].flux(0)) + std::abs(cpgelementlist[ie].flux(1)));
		double fy = 0.5*(std::abs(cpgelementlist[ie].flux(2)) + std::abs(cpgelementlist[ie].flux(3)));
		double fz = 0.5*(std::abs(cpgelementlist[ie].flux(4)) + std::abs(cpgelementlist[ie].flux(5)));
		cpgelementlist[ie].fmmv(0, fx);
		cpgelementlist[ie].fmmv(1, fy);
		cpgelementlist[ie].fmmv(2, fz);
		*/
		double fx, fy, fz;
		if (cpgelementlist[ie].flux(0) < 0){
			fx = -cpgelementlist[ie].flux(0) / cpgelementlist[ie].porosity();
			cpgelementlist[ie].fmmv(0, fx);
		}
		else if (cpgelementlist[ie].flux(1) < 0){
			fx = -cpgelementlist[ie].flux(1) / cpgelementlist[ie].porosity();
			cpgelementlist[ie].fmmv(0, fx);
		}
		if (cpgelementlist[ie].flux(2) < 0){
			fy = -cpgelementlist[ie].flux(2) / cpgelementlist[ie].porosity();
			cpgelementlist[ie].fmmv(1, fy);
		}
		else if (cpgelementlist[ie].flux(3) < 0){
			fy = -cpgelementlist[ie].flux(3) / cpgelementlist[ie].porosity();
			cpgelementlist[ie].fmmv(1, fy);
		}
		if (cpgelementlist[ie].flux(4) < 0){
			fz = -cpgelementlist[ie].flux(4) / cpgelementlist[ie].porosity();
			cpgelementlist[ie].fmmv(2, fz);
		}
		else if (cpgelementlist[ie].flux(5) < 0){
			fz = -cpgelementlist[ie].flux(5) / cpgelementlist[ie].porosity();
			cpgelementlist[ie].fmmv(2, fz);
		}



	}
}

void REGION::cpgreverseflux(){
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		for (int i = 0; i < 6; i++){
			double ff = cpgelementlist[ie].flux(i);
			cpgelementlist[ie].flux(i, -ff);
		}
	}
}

void REGION::setfmmv_tet_paperfmm1ex2(){
	double fx, fy, fz;
	for (int ie = 0; ie < elementlist.size(); ie++){
		int n0 = elementlist[ie].node(0);
		int n1 = elementlist[ie].node(1);
		int n2 = elementlist[ie].node(2);
		int n3 = elementlist[ie].node(3);
		double x = 0.25*(nodelist[n0].x() + nodelist[n1].x() + nodelist[n2].x() + nodelist[n3].x());
		double y = 0.25*(nodelist[n0].y() + nodelist[n1].y() + nodelist[n2].y() + nodelist[n3].y());
		double z = 0.25*(nodelist[n0].z() + nodelist[n1].z() + nodelist[n2].z() + nodelist[n3].z());
		if (x >= 20 && x <= 60 && z >= 20 && z <= 60){
			fx = 0.1; fy = 0.1; fz = 0.1;
		}
		else{
			fx = 1; fy = 1; fz = 0.5;
		}
		elementlist[ie].fmmv(0, fx);
		elementlist[ie].fmmv(1, fy);
		elementlist[ie].fmmv(2, fz);

	}
}

void REGION::setpermporo_tet_paperEFMMC(){
	double fx, fy, fz;
	for (int ie = 0; ie < elementlist.size(); ie++){
		int n0 = elementlist[ie].node(0);
		int n1 = elementlist[ie].node(1);
		int n2 = elementlist[ie].node(2);
		int n3 = elementlist[ie].node(3);
		double x = 0.25*(nodelist[n0].x() + nodelist[n1].x() + nodelist[n2].x() + nodelist[n3].x());
		double y = 0.25*(nodelist[n0].y() + nodelist[n1].y() + nodelist[n2].y() + nodelist[n3].y());
		double z = 0.25*(nodelist[n0].z() + nodelist[n1].z() + nodelist[n2].z() + nodelist[n3].z());
		if (x >= 400 && x <= 600){
			fx = 1 * 0.987e-15; fy = 1 * 0.987e-15; fz = 1 * 0.987e-15;
		}
		else{
			fx = 0.01 * 0.987e-15; fy = 0.01 * 0.987e-15; fz = 0.01 * 0.987e-15;
		}
		elementlist[ie].k(0, 0, fx);
		elementlist[ie].k(1, 1, fy);
		elementlist[ie].k(2, 2, fz);
		elementlist[ie].porosity(0.1);

	}
}

void REGION::cpgflux_removefacearea_car(double dx, double dy, double dz){
	//remove face area in cpgelement flux
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		for (int j = 0; j < 6; j++){
			double area_;
			if (j == 0 || j == 1){
				area_ = dy*dz;
			}
			else if (j == 2 || j == 3){
				area_ = dx*dz;
			}
			if (j == 4 || j == 5){
				area_ = dx*dy;
			}
			int je = cpgelementlist[ie].neighbor(j);
			if (je >= 0){
				double flux_ = cpgelementlist[ie].flux(j);
				cpgelementlist[ie].flux(j, flux_/area_);
			}
		}
	}
}

//void REGION::dfmmdiagnostic_cpg(std::vector<int> fbset0){
//	std::cout << "FMM CPG start" << std::endl;
//	std::vector<int> band, fbset;
//	int id_min;
//
//	do{
//		fbset = fbset0;
//		//find and compute narrow band
//		for (int ib = 0; ib < fbset.size(); ib++){
//			int ist = fbset[ib];
//			for (int i = 0; i < 6; i++){
//				int inei = cpgelementlist[ist].neighbor(i);
//				if (inei >= 0){
//					if (cpgelementlist[inei].status() == 0){
//						band.push_back(inei);//narrow band
//						cpgelementlist[inei].status(1); //in band
//					}
//					if (cpgelementlist[inei].status() != 2){//previously added-to-band cells are recomputed
//						double fx, fy, fz;
//						double vol = cpgelementlist[inei].porosity()*cpgelementlist[inei].volume();
//						int marklr = -1;
//						int je = cpgelementlist[inei].neighbor(0);
//						if (je >= 0){
//							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(0)<0){
//								marklr = 0;
//								fx = -cpgelementlist[inei].flux(0);
//							}
//						}
//						je = cpgelementlist[inei].neighbor(1);
//						if (je >= 0){
//							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(1)<0){
//								marklr = 1;//use one direction only
//								fx = -cpgelementlist[inei].flux(1);
//							}
//						}
//						int markfb = -1;
//						je = cpgelementlist[inei].neighbor(2);
//						if (je >= 0){
//							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(2)<0){
//								markfb = 2;
//								fy = -cpgelementlist[inei].flux(2);
//							}
//						}
//						je = cpgelementlist[inei].neighbor(3);
//						if (je >= 0){
//							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(3)<0){
//								markfb = 3;
//								fy = -cpgelementlist[inei].flux(3);
//							}
//						}
//						int markbt = -1;
//						je = cpgelementlist[inei].neighbor(4);
//						if (je >= 0){
//							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(4)<0){
//								markbt = 4;
//								fz = -cpgelementlist[inei].flux(4);
//							}
//						}
//						je = cpgelementlist[inei].neighbor(5);
//						if (je >= 0){
//							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(5)<0){
//								markbt = 5;
//								fz = -cpgelementlist[inei].flux(5);
//							}
//						}
//						if (marklr < 0 && markfb < 0 && markbt < 0){
//							std::cout << "no neighbor is frozen" << std::endl;
//							std::exit(EXIT_FAILURE);
//						}
//						double t;
//						//one frozen
//						if (marklr >= 0 && markfb < 0 && markbt < 0){
//							int ele1 = cpgelementlist[inei].neighbor(marklr);
//							double t1 = cpgelementlist[ele1].tof();
//							t = vol / fx + t1;
//						}
//						else if (marklr < 0 && markfb >= 0 && markbt < 0){
//							int ele1 = cpgelementlist[inei].neighbor(markfb);
//							double t1 = cpgelementlist[ele1].tof();
//
//							t = vol / fy + t1;
//						}
//						else if (marklr < 0 && markfb < 0 && markbt >= 0){
//							int ele1 = cpgelementlist[inei].neighbor(markbt);
//							double t1 = cpgelementlist[ele1].tof();
//
//							t = vol / fz + t1;
//						}
//						//two frozen
//						else if (marklr >= 0 && markfb >= 0 && markbt < 0){
//							int ele1 = cpgelementlist[inei].neighbor(marklr);
//							int ele2 = cpgelementlist[inei].neighbor(markfb);
//							double t1 = cpgelementlist[ele1].tof();
//							double t2 = cpgelementlist[ele2].tof();
//							double a = fx*fx / vol / vol + fy*fy / vol / vol;
//							double b = -2 * t1 *fx*fx / vol / vol - 2 * t2*fy*fy / vol / vol;
//							double c = t1*t1*fx*fx / vol / vol + t2*t2*fy*fy / vol / vol - 1;
//							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
//						}
//						else if (marklr >= 0 && markfb < 0 && markbt >= 0){
//							int ele1 = cpgelementlist[inei].neighbor(marklr);
//							int ele2 = cpgelementlist[inei].neighbor(markbt);
//							double t1 = cpgelementlist[ele1].tof();
//							double t2 = cpgelementlist[ele2].tof();
//							double a = 1 * fx*fx / vol / vol + 1 * fz*fz / vol / vol;
//							double b = -2 * t1*fx*fx / vol / vol - 2 * t2 *fz*fz / vol / vol;
//							double c = t1*t1 *fx*fx / vol / vol + t2*t2 *fz*fz / vol / vol - 1;
//							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
//						}
//						else if (marklr < 0 && markfb >= 0 && markbt >= 0){
//							int ele1 = cpgelementlist[inei].neighbor(markfb);
//							int ele2 = cpgelementlist[inei].neighbor(markbt);
//							double t1 = cpgelementlist[ele1].tof();
//							double t2 = cpgelementlist[ele2].tof();
//							double a = 1 * fy*fy / vol / vol + 1 * fz*fz / vol / vol;
//							double b = -2 * fy*fy* t1 / vol / vol - 2 * fz*fz* t2 / vol / vol;
//							double c = t1*t1 *fy*fy / vol / vol + t2*t2 *fz*fz / vol / vol - 1;
//							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
//						}
//						else if (marklr >= 0 && markfb >= 0 && markbt >= 0){
//							int ele1 = cpgelementlist[inei].neighbor(marklr);
//							int ele2 = cpgelementlist[inei].neighbor(markfb);
//							int ele3 = cpgelementlist[inei].neighbor(markbt);
//							double t1 = cpgelementlist[ele1].tof();
//							double t2 = cpgelementlist[ele2].tof();
//							double t3 = cpgelementlist[ele3].tof();
//							double a = 1 * fx*fx / vol / vol + 1 * fy*fy / vol / vol + 1 * fz*fz / vol / vol;
//							double b = -2 * (t1 *fx*fx / vol / vol + t2 *fy*fy / vol / vol + t3*fz*fz / vol / vol);
//							double c = t1*t1*fx*fx / vol / vol + t2*t2*fy*fy / vol / vol + t3*t3 *fz*fz / vol / vol - 1;
//							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
//						}
//
//						cpgelementlist[inei].tof(t);
//					}
//				}
//			}
//		}
//
//		//choose smallest and proceed
//		double tof_min = 1e+10;
//		int id_vec;
//		id_min = -1;
//		for (int i = 0; i < band.size(); i++){
//			int iele = band[i];
//			if (cpgelementlist[iele].tof() < tof_min){
//				tof_min = cpgelementlist[iele].tof();
//				id_min = iele;
//				id_vec = i;
//			}
//		}
//		cpgelementlist[id_min].status(2); //frozen
//		/*std::cout << "FMM finished " << id_min << std::endl;
//		if (id_min == 1832){
//			izz = 1;
//		}*/
//		band.erase(band.begin() + id_vec);
//		fbset0.clear();
//		fbset0.push_back(id_min);
//	} while (id_min >= 0 && band.size()>0);
//	std::cout << "FMM CPG done" << std::endl;
//}

void REGION::dfmmdiagnostic_cpg_tof_exact(std::vector<int> fbset0){
	//for tracer
	reversepostordering.clear();

	std::cout << "FMM CPG TOF start" << std::endl;
	std::vector<int> band, fbset;
	int id_min, id_vec, countcycle_;
	countcycle_ = 0;
	do{
		countcycle_++;
		if (countcycle_ > 1){
			band.erase(band.begin() + id_vec);
		}
		fbset = fbset0;
		//find and compute narrow band
		for (int ib = 0; ib < fbset.size(); ib++){
			int ist = fbset[ib];
			reversepostordering.push_back(ist);
			for (int i = 0; i < 6; i++){
				int inei = cpgelementlist[ist].neighbor(i);
				if (inei >= 0){
					if (cpgelementlist[inei].status() == 0 && cpgelementlist[ist].flux(i)>0){
						int flag = 0;
						for (int jj = 0; jj < 6; jj++){
							int jjnei = cpgelementlist[inei].neighbor(jj);
							if (jjnei >= 0){
								if (cpgelementlist[inei].flux(jj) < 0 && cpgelementlist[jjnei].status() != 2){
									flag = 1;
								}
							}
						}
						if (flag == 0){//only compute and add to band if all inflow neighbours are frozen for accuracy
							band.push_back(inei);//narrow band
							cpgelementlist[inei].status(1); //in band
							double totalinflux_ = 0;
							double rhs_ = cpgelementlist[inei].volume()*cpgelementlist[inei].porosity();
							for (int jjj = 0; jjj < 6; jjj++){
								int jjje = cpgelementlist[inei].neighbor(jjj);
								if (jjje >= 0){
									if (cpgelementlist[jjje].status() == 2 && cpgelementlist[inei].flux(jjj)<0){
										totalinflux_ = totalinflux_ + cpgelementlist[inei].flux(jjj);
										rhs_ = rhs_ - cpgelementlist[inei].flux(jjj)*cpgelementlist[jjje].tof();
									}
								}
							}
							double t = -rhs_ / totalinflux_;
							cpgelementlist[inei].tof(t);
						}
					}
				}
			}
		}

		//choose smallest and proceed
		double tof_min = 1e+30;

		id_min = -1;
		if (band.size() > 0){
			for (int i = 0; i < band.size(); i++){
				int iele = band[i];
				if (cpgelementlist[iele].tof() < tof_min){
					tof_min = cpgelementlist[iele].tof();
					id_min = iele;
					id_vec = i;
				}
			}
			cpgelementlist[id_min].status(2); //frozen
			fbset0.clear();
			fbset0.push_back(id_min);
		}
	} while (id_min >= 0 && band.size()>0);
	std::cout << "FMM CPG TOF done" << std::endl;
	fbset0.clear();
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::dfmmdiagnostic_cpg_btof_exact(std::vector<int> fbset0){
	//for tracer
	reversepostordering.clear();

	std::cout << "FMM CPG TOF start" << std::endl;
	std::vector<int> band, fbset;
	int id_min, id_vec, countcycle_;
	countcycle_ = 0;
	do{
		countcycle_++;
		if (countcycle_ > 1){
			band.erase(band.begin() + id_vec);
		}
		fbset = fbset0;
		//find and compute narrow band
		for (int ib = 0; ib < fbset.size(); ib++){
			int ist = fbset[ib];
			reversepostordering.push_back(ist);
			for (int i = 0; i < 6; i++){
				int inei = cpgelementlist[ist].neighbor(i);
				if (inei >= 0){
					if (cpgelementlist[inei].status() == 0 && cpgelementlist[ist].flux(i)>0){
						int flag = 0;
						for (int jj = 0; jj < 6; jj++){
							int jjnei = cpgelementlist[inei].neighbor(jj);
							if (jjnei >= 0){
								if (cpgelementlist[inei].flux(jj) < 0 && cpgelementlist[jjnei].status() != 2){
									flag = 1;
								}
							}
						}
						if (flag == 0){//only compute and add to band if all inflow neighbours are frozen for accuracy
							band.push_back(inei);//narrow band
							cpgelementlist[inei].status(1); //in band
							double totalinflux_ = 0;
							double rhs_ = cpgelementlist[inei].volume()*cpgelementlist[inei].porosity();
							for (int jjj = 0; jjj < 6; jjj++){
								int jjje = cpgelementlist[inei].neighbor(jjj);
								if (jjje >= 0){
									if (cpgelementlist[jjje].status() == 2 && cpgelementlist[inei].flux(jjj)<0){
										totalinflux_ = totalinflux_ + cpgelementlist[inei].flux(jjj);
										rhs_ = rhs_ - cpgelementlist[inei].flux(jjj)*cpgelementlist[jjje].tof_back();
									}
								}
							}
							double t = -rhs_ / totalinflux_;
							cpgelementlist[inei].tof_back(t);
						}
					}
				}
			}
		}

		//choose smallest and proceed
		double tof_min = 1e+10;

		id_min = -1;
		if (band.size() > 0){
			for (int i = 0; i < band.size(); i++){
				int iele = band[i];
				if (cpgelementlist[iele].tof_back() < tof_min){
					tof_min = cpgelementlist[iele].tof_back();
					id_min = iele;
					id_vec = i;
				}
			}
			cpgelementlist[id_min].status(2); //frozen
			fbset0.clear();
			fbset0.push_back(id_min);
		}
	} while (id_min >= 0 && band.size()>0);
	std::cout << "FMM CPG TOF done" << std::endl;
	fbset0.clear();
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::dfmmdiagnostic_cpg_tof(std::vector<int> fbset0){
	std::cout << "FMM CPG start" << std::endl;
	std::vector<int> band, fbset;
	int id_min;

	do{
		fbset = fbset0;
		//find and compute narrow band
		for (int ib = 0; ib < fbset.size(); ib++){
			int ist = fbset[ib];
			for (int i = 0; i < 6; i++){
				int inei = cpgelementlist[ist].neighbor(i);
				if (inei >= 0){
					if (cpgelementlist[inei].status() == 0 && cpgelementlist[ist].flux(i)>0){
						band.push_back(inei);//narrow band
						cpgelementlist[inei].status(1); //in band
					}
					if (cpgelementlist[inei].status() == 1){//previously added-to-band cells are recomputed
						double fx, fy, fz;
						double vol = cpgelementlist[inei].porosity()*cpgelementlist[inei].volume();
						int marklr = -1;
						int je = cpgelementlist[inei].neighbor(0);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(0)<0){
								marklr = 0;
								fx = -cpgelementlist[inei].flux(0);
							}
						}
						je = cpgelementlist[inei].neighbor(1);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(1)<0){
								marklr = 1;//use one direction only
								fx = -cpgelementlist[inei].flux(1);
							}
						}
						int markfb = -1;
						je = cpgelementlist[inei].neighbor(2);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(2)<0){
								markfb = 2;
								fy = -cpgelementlist[inei].flux(2);
							}
						}
						je = cpgelementlist[inei].neighbor(3);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(3)<0){
								markfb = 3;
								fy = -cpgelementlist[inei].flux(3);
							}
						}
						int markbt = -1;
						je = cpgelementlist[inei].neighbor(4);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(4)<0){
								markbt = 4;
								fz = -cpgelementlist[inei].flux(4);
							}
						}
						je = cpgelementlist[inei].neighbor(5);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(5)<0){
								markbt = 5;
								fz = -cpgelementlist[inei].flux(5);
							}
						}
						if (marklr < 0 && markfb < 0 && markbt < 0){
							std::cout << "no neighbor is frozen" << std::endl;
							std::exit(EXIT_FAILURE);
						}
						double t;
						//one frozen
						if (marklr >= 0 && markfb < 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							double t1 = cpgelementlist[ele1].tof();
							t = vol / fx + t1;
						}
						else if (marklr < 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							double t1 = cpgelementlist[ele1].tof();

							t = vol / fy + t1;
						}
						else if (marklr < 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();

							t = vol / fz + t1;
						}
						//two frozen
						else if (marklr >= 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							t = (fx*t1 + fy*t2 + vol) / (fx + fy);
						}
						else if (marklr >= 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							t = (fx*t1 + fz*t2 + vol) / (fx + fz);
						}
						else if (marklr < 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							t = (fy*t1 + fz*t2 + vol) / (fy + fz);
						}
						else if (marklr >= 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							int ele3 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							double t3 = cpgelementlist[ele3].tof();
							t = (fx*t1 + fy*t2 + fz*t3 + vol) / (fx + fy + fz);
						}

						cpgelementlist[inei].tof(t);
					}
				}
			}
		}

		//choose smallest and proceed
		double tof_min = 1e+10;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int iele = band[i];
			if (cpgelementlist[iele].tof() < tof_min){
				tof_min = cpgelementlist[iele].tof();
				id_min = iele;
				id_vec = i;
			}
		}
		cpgelementlist[id_min].status(2); //frozen
		/*std::cout << "FMM finished " << id_min << std::endl;
		if (id_min == 1832){
		izz = 1;
		}*/
		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);
	std::cout << "FMM CPG done" << std::endl;
}

void REGION::dfmmdiagnostic_cpg_tracer(std::vector<int> fbset0, int iwell_, std::vector<int> wellvec){
	std::cout << "FMM CPG start" << std::endl;
	std::vector<int> band, fbset;
	int id_min;

	//which tracer
	int itracer_;
	for (int i = 0; i < wellvec.size(); i++){
		if (iwell_ == wellvec[i]){
			itracer_ = i;
		}
	}


	do{
		fbset = fbset0;
		//find and compute narrow band
		for (int ib = 0; ib < fbset.size(); ib++){
			int ist = fbset[ib];
			for (int i = 0; i < 6; i++){
				int inei = cpgelementlist[ist].neighbor(i);
				if (inei >= 0){
					if (cpgelementlist[inei].status() == 0 && cpgelementlist[ist].flux(i)>0){
						/*int flag = 0;
						for (int ii = 0; ii < 6; ii++){
							int je = cpgelementlist[inei].neighbor(ii);
							if (je >= 0){
								if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(ii) < 0){
									flag = 1;
								}
							}
						}*/
						//if (flag == 1){
							band.push_back(inei);//narrow band
							cpgelementlist[inei].status(1); //in band
						//}
					}
					if (cpgelementlist[inei].status() == 1){//previously added-to-band cells are recomputed
						double fx, fy, fz;
						double vol = cpgelementlist[inei].porosity()*cpgelementlist[inei].volume();
						int marklr = -1;
						int je = cpgelementlist[inei].neighbor(0);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(0)<0){
								marklr = 0;
								fx = -cpgelementlist[inei].flux(0);
							}
						}
						je = cpgelementlist[inei].neighbor(1);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(1)<0){
								marklr = 1;//use one direction only
								fx = -cpgelementlist[inei].flux(1);
							}
						}
						int markfb = -1;
						je = cpgelementlist[inei].neighbor(2);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(2)<0){
								markfb = 2;
								fy = -cpgelementlist[inei].flux(2);
							}
						}
						je = cpgelementlist[inei].neighbor(3);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(3)<0){
								markfb = 3;
								fy = -cpgelementlist[inei].flux(3);
							}
						}
						int markbt = -1;
						je = cpgelementlist[inei].neighbor(4);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(4)<0){
								markbt = 4;
								fz = -cpgelementlist[inei].flux(4);
							}
						}
						je = cpgelementlist[inei].neighbor(5);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(5)<0){
								markbt = 5;
								fz = -cpgelementlist[inei].flux(5);
							}
						}
						if (marklr < 0 && markfb < 0 && markbt < 0){
							std::cout << "no neighbor is frozen" << std::endl;
							std::exit(EXIT_FAILURE);
						}
						//std::vector<double> cvec;
						//cvec.resize(wellvec.size());
						double c;
						//one frozen
						if (marklr >= 0 && markfb < 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
								cvec[ii] = cpgelementlist[ele1].tracer(ii);
							}*/
							c = cpgelementlist[ele1].tracer(itracer_);
						}
						else if (marklr < 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
								cvec[ii] = cpgelementlist[ele1].tracer(ii);
							}*/
							c = cpgelementlist[ele1].tracer(itracer_);
						}
						else if (marklr < 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
								cvec[ii] = cpgelementlist[ele1].tracer(ii);
							}*/
							c = cpgelementlist[ele1].tracer(itracer_);
						}
						//two frozen
						else if (marklr >= 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
								cvec[ii] = (fx*cpgelementlist[ele1].tracer(ii) + fy*cpgelementlist[ele2].tracer(ii)) / (fx + fy);
							}*/
							c = (fx*cpgelementlist[ele1].tracer(itracer_) + fy*cpgelementlist[ele2].tracer(itracer_)) / (fx + fy);
						}
						else if (marklr >= 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
								cvec[ii] = (fx*cpgelementlist[ele1].tracer(ii) + fz*cpgelementlist[ele2].tracer(ii)) / (fx + fz);
							}*/
							c = (fx*cpgelementlist[ele1].tracer(itracer_) + fz*cpgelementlist[ele2].tracer(itracer_)) / (fx + fz);
						}
						else if (marklr < 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
								cvec[ii] = (fy*cpgelementlist[ele1].tracer(ii) + fz*cpgelementlist[ele2].tracer(ii)) / (fy + fz);
							}*/
							c = (fy*cpgelementlist[ele1].tracer(itracer_) + fz*cpgelementlist[ele2].tracer(itracer_)) / (fy + fz);
						}
						else if (marklr >= 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							int ele3 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
								cvec[ii] = (fx*cpgelementlist[ele1].tracer(ii) + fy*cpgelementlist[ele2].tracer(ii) + fz*cpgelementlist[ele3].tracer(ii)) / (fx+fy + fz);
							}*/
							c = (fx*cpgelementlist[ele1].tracer(itracer_) + fy*cpgelementlist[ele2].tracer(itracer_) + fz*cpgelementlist[ele3].tracer(itracer_)) / (fx + fy + fz);
						}
						/*for (int ii = 0; ii < wellvec.size(); ii++){
							cpgelementlist[inei].tracer(ii, cvec[ii]);
						}*/
						cpgelementlist[inei].tracer(itracer_, c);
					}
				}
			}
		}

		//choose smallest and proceed
		double c_min = 1e+10;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int iele = band[i];
			if (cpgelementlist[iele].tracer(itracer_) < c_min){
				c_min = cpgelementlist[iele].tracer(itracer_);
				id_min = iele;
				id_vec = i;
			}
		}
		cpgelementlist[id_min].status(2); //frozen
		/*std::cout << "FMM finished " << id_min << std::endl;
		if (id_min == 1832){
		izz = 1;
		}*/
		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);

	std::cout << "FMM CPG done" << std::endl;
	fbset0.clear();
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::dfmmdiagnostic_cpg_tracer_exact(std::vector<int> fbset0, int iwell_, std::vector<int> wellvec){
	std::cout << "FMM CPG start" << std::endl;
	std::vector<int> band, fbset;
	int id_min, id_vec, countcycle_;

	//which tracer
	int itracer_;
	for (int i = 0; i < wellvec.size(); i++){
		if (iwell_ == wellvec[i]){
			itracer_ = i;
		}
	}
	countcycle_ = 0;

	do{
		countcycle_++;
		if (countcycle_ > 1){
			band.erase(band.begin() + id_vec);
		}
		fbset = fbset0;
		//find and compute narrow band
		for (int ib = 0; ib < fbset.size(); ib++){
			int ist = fbset[ib];
			for (int i = 0; i < 6; i++){
				int inei = cpgelementlist[ist].neighbor(i);
				if (inei >= 0){
					if (cpgelementlist[inei].status() == 0 && cpgelementlist[ist].flux(i)>0){
						int flag = 0;
						for (int ii = 0; ii < 6; ii++){
							int je = cpgelementlist[inei].neighbor(ii);
							if (je >= 0){
								if (cpgelementlist[je].status() != 2 && cpgelementlist[inei].flux(ii) < 0){
									flag = 1;
								}
							}
						}
						if (flag == 0){
							band.push_back(inei);//narrow band
							cpgelementlist[inei].status(1); //in band
						}
					}
					if (cpgelementlist[inei].status() == 1){//previously added-to-band cells are recomputed
						double fx, fy, fz;
						double vol = cpgelementlist[inei].porosity()*cpgelementlist[inei].volume();
						int marklr = -1;
						int je = cpgelementlist[inei].neighbor(0);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(0)<0){
								marklr = 0;
								fx = -cpgelementlist[inei].flux(0);
							}
						}
						je = cpgelementlist[inei].neighbor(1);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(1)<0){
								marklr = 1;//use one direction only
								fx = -cpgelementlist[inei].flux(1);
							}
						}
						int markfb = -1;
						je = cpgelementlist[inei].neighbor(2);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(2)<0){
								markfb = 2;
								fy = -cpgelementlist[inei].flux(2);
							}
						}
						je = cpgelementlist[inei].neighbor(3);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(3)<0){
								markfb = 3;
								fy = -cpgelementlist[inei].flux(3);
							}
						}
						int markbt = -1;
						je = cpgelementlist[inei].neighbor(4);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(4)<0){
								markbt = 4;
								fz = -cpgelementlist[inei].flux(4);
							}
						}
						je = cpgelementlist[inei].neighbor(5);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2 && cpgelementlist[inei].flux(5)<0){
								markbt = 5;
								fz = -cpgelementlist[inei].flux(5);
							}
						}
						if (marklr < 0 && markfb < 0 && markbt < 0){
							std::cout << "no neighbor is frozen" << std::endl;
							std::exit(EXIT_FAILURE);
						}
						//std::vector<double> cvec;
						//cvec.resize(wellvec.size());
						double c;
						//one frozen
						if (marklr >= 0 && markfb < 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
							cvec[ii] = cpgelementlist[ele1].tracer(ii);
							}*/
							c = cpgelementlist[ele1].tracer(itracer_);
						}
						else if (marklr < 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
							cvec[ii] = cpgelementlist[ele1].tracer(ii);
							}*/
							c = cpgelementlist[ele1].tracer(itracer_);
						}
						else if (marklr < 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
							cvec[ii] = cpgelementlist[ele1].tracer(ii);
							}*/
							c = cpgelementlist[ele1].tracer(itracer_);
						}
						//two frozen
						else if (marklr >= 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
							cvec[ii] = (fx*cpgelementlist[ele1].tracer(ii) + fy*cpgelementlist[ele2].tracer(ii)) / (fx + fy);
							}*/
							c = (fx*cpgelementlist[ele1].tracer(itracer_) + fy*cpgelementlist[ele2].tracer(itracer_)) / (fx + fy);
						}
						else if (marklr >= 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
							cvec[ii] = (fx*cpgelementlist[ele1].tracer(ii) + fz*cpgelementlist[ele2].tracer(ii)) / (fx + fz);
							}*/
							c = (fx*cpgelementlist[ele1].tracer(itracer_) + fz*cpgelementlist[ele2].tracer(itracer_)) / (fx + fz);
						}
						else if (marklr < 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
							cvec[ii] = (fy*cpgelementlist[ele1].tracer(ii) + fz*cpgelementlist[ele2].tracer(ii)) / (fy + fz);
							}*/
							c = (fy*cpgelementlist[ele1].tracer(itracer_) + fz*cpgelementlist[ele2].tracer(itracer_)) / (fy + fz);
						}
						else if (marklr >= 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							int ele3 = cpgelementlist[inei].neighbor(markbt);
							/*for (int ii = 0; ii < wellvec.size(); ii++){
							cvec[ii] = (fx*cpgelementlist[ele1].tracer(ii) + fy*cpgelementlist[ele2].tracer(ii) + fz*cpgelementlist[ele3].tracer(ii)) / (fx+fy + fz);
							}*/
							c = (fx*cpgelementlist[ele1].tracer(itracer_) + fy*cpgelementlist[ele2].tracer(itracer_) + fz*cpgelementlist[ele3].tracer(itracer_)) / (fx + fy + fz);
						}
						/*for (int ii = 0; ii < wellvec.size(); ii++){
						cpgelementlist[inei].tracer(ii, cvec[ii]);
						}*/
						cpgelementlist[inei].tracer(itracer_, c);
					}
				}
			}
		}

		//choose smallest and proceed
		double c_min = 1e+10;
		id_min = -1;
		if (band.size() > 0){
			for (int i = 0; i < band.size(); i++){
				int iele = band[i];
				if (cpgelementlist[iele].tracer(itracer_) < c_min){
					c_min = cpgelementlist[iele].tracer(itracer_);
					id_min = iele;
					id_vec = i;
				}
			}
			cpgelementlist[id_min].status(2); //frozen
			fbset0.clear();
			fbset0.push_back(id_min);
		}
	} while (id_min >= 0 && band.size()>0);

	std::cout << "FMM CPG done" << std::endl;
	fbset0.clear();
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::dfmmdiagnostic_cpg_tracer_exact_toforder(std::vector<int> fbset0, std::vector<int> wellvec){

	std::vector<double> rhs_;
	std::cout << "DFMM-tof reordered CPGFV for tracer concentrations" << std::endl;
	int nele = cpgelementlist.size();

	rhs_.resize(wellvec.size());


	for (int iorder = 0; iorder < reversepostordering.size(); iorder++){
		int ie = reversepostordering[iorder];

		if (cpgelementlist[ie].status() != 2){

			for (int icc = 0; icc < wellvec.size(); icc++){
				rhs_[icc] = 0;
			}
			double totalinflux_ = 0;
			for (int iface = 0; iface < 6; iface++){ //loop over all 6 faces
				int je = cpgelementlist[ie].neighbor(iface);
				if (je >= 0){
					double flux_ = cpgelementlist[ie].flux(iface);
					if (flux_ < 0 && cpgelementlist[je].status()==2){ //choose neighboring element
						totalinflux_ = totalinflux_ + flux_;
						//if (cpgelement[je].status() == 2){
							for (int icc = 0; icc < wellvec.size(); icc++){
								rhs_[icc] = rhs_[icc] - cpgelementlist[je].tracer(icc) * flux_;
							}
						//}
					}
				}
			}

			double totaloutflux_ = -totalinflux_;

			for (int icc = 0; icc < wellvec.size(); icc++){
				cpgelementlist[ie].tracer(icc, rhs_[icc] / totaloutflux_);
			}
			cpgelementlist[ie].status(2);

		}
	}
	std::cout << "DONE" << std::endl;
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::dfmmdiagnostic_cpg_btracer_exact_btoforder(std::vector<int> fbset0, std::vector<int> wellvec){

	std::vector<double> rhs_;
	std::cout << "DFMM-btof reordered CPGFV for btracer concentrations" << std::endl;
	int nele = cpgelementlist.size();

	rhs_.resize(wellvec.size());


	for (int iorder = 0; iorder < reversepostordering.size(); iorder++){
		int ie = reversepostordering[iorder];

		if (cpgelementlist[ie].status() != 2){

			for (int icc = 0; icc < wellvec.size(); icc++){
				rhs_[icc] = 0;
			}
			double totalinflux_ = 0;
			for (int iface = 0; iface < 6; iface++){ //loop over all 6 faces
				int je = cpgelementlist[ie].neighbor(iface);
				if (je >= 0){
					double flux_ = cpgelementlist[ie].flux(iface);
					if (flux_ < 0 && cpgelementlist[je].status() == 2){ //choose neighboring element
						totalinflux_ = totalinflux_ + flux_;
						//if (cpgelement[je].status() == 2){
						for (int icc = 0; icc < wellvec.size(); icc++){
							rhs_[icc] = rhs_[icc] - cpgelementlist[je].tracer_back(icc) * flux_;
						}
						//}
					}
				}
			}

			double totaloutflux_ = -totalinflux_;

			for (int icc = 0; icc < wellvec.size(); icc++){
				cpgelementlist[ie].tracer_back(icc, rhs_[icc] / totaloutflux_);
			}
			cpgelementlist[ie].status(2);

		}
	}
	std::cout << "DONE" << std::endl;
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::dfmmdiagnostic_cpg_btracer_exact_reversetoforder(std::vector<int> fbset0, std::vector<int> wellvec){

	std::vector<double> rhs_;
	std::cout << "DFMM-tof reordered CPGFV for tracer concentrations" << std::endl;
	int nele = cpgelementlist.size();

	rhs_.resize(wellvec.size());


	for (int iorder = reversepostordering.size()-1; iorder >=0; iorder--){
		int ie = reversepostordering[iorder];

		if (cpgelementlist[ie].status() != 2){

			for (int icc = 0; icc < wellvec.size(); icc++){
				rhs_[icc] = 0;
			}
			double totalinflux_ = 0;
			for (int iface = 0; iface < 6; iface++){ //loop over all 6 faces
				int je = cpgelementlist[ie].neighbor(iface);
				if (je >= 0){
					double flux_ = cpgelementlist[ie].flux(iface);
					if (flux_ < 0 && cpgelementlist[je].status() == 2){ //choose neighboring element
						totalinflux_ = totalinflux_ + flux_;
						for (int icc = 0; icc < wellvec.size(); icc++){
							rhs_[icc] = rhs_[icc] - cpgelementlist[je].tracer_back(icc) * flux_;
						}
					}
				}
			}

			double totaloutflux_ = -totalinflux_;

			for (int icc = 0; icc < wellvec.size(); icc++){
				cpgelementlist[ie].tracer_back(icc, rhs_[icc] / totaloutflux_);
			}
			cpgelementlist[ie].status(2);

		}
	}
	std::cout << "DONE" << std::endl;
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::dfmmdiagnostic_cpg_btof_exact_reversetoforder(std::vector<int> fbset0){

	double rhs_;
	std::cout << "DFMM-tof reordered CPGFV for tracer concentrations" << std::endl;
	int nele = cpgelementlist.size();

	for (int iorder = reversepostordering.size() - 1; iorder >= 0; iorder--){
		int ie = reversepostordering[iorder];

		if (cpgelementlist[ie].status() != 2){
			rhs_ = cpgelementlist[ie].volume()*cpgelementlist[ie].porosity();
			double totalinflux_ = 0;
			for (int iface = 0; iface < 6; iface++){ //loop over all 6 faces
				int je = cpgelementlist[ie].neighbor(iface);
				if (je >= 0){
					double flux_ = cpgelementlist[ie].flux(iface);
					if (flux_ < 0 && cpgelementlist[je].status() == 2){ //choose neighboring element
						totalinflux_ = totalinflux_ + flux_;
						rhs_ = rhs_ - cpgelementlist[je].tof_back() * flux_;
					}
				}
			}
			double totaloutflux_ = -totalinflux_;
			cpgelementlist[ie].tof_back(rhs_ / totaloutflux_);
			cpgelementlist[ie].status(2);
		}
	}
	std::cout << "DONE" << std::endl;
	for (int i = 0; i < cpgelementlist.size(); i++){
		cpgelementlist[i].status(0);
	}
}

void REGION::fmm_cartesian3d(std::vector<int> fbset0){//fast marching method
	std::cout << "FMM CPG start" << std::endl;
	std::vector<int> band, fbset;
	int id_min;
	double dx, dy, dz;
	do{
		fbset = fbset0;
		//find and compute narrow band
		for (int ib = 0; ib < fbset.size(); ib++){
			int ist = fbset[ib];
			//std::cout << "ist" <<ist<< std::endl;
			for (int i = 0; i < 6; i++){
				int inei = cpgelementlist[ist].neighbor(i);
				//std::cout << "inei" << inei << std::endl;
				if (inei >= 0){
					if (cpgelementlist[inei].status() == 0){
						band.push_back(inei);//narrow band
						cpgelementlist[inei].status(1); //in band
					}
					if (cpgelementlist[inei].status() != 2){//previously added-to-band cells are recomputed
						double fx = std::abs(cpgelementlist[inei].fmmv(0));
						double fy = std::abs(cpgelementlist[inei].fmmv(1));
						double fz = std::abs(cpgelementlist[inei].fmmv(2));
						dx = cpgelementlist[inei].dx;
						dy = cpgelementlist[inei].dy;
						dz = cpgelementlist[inei].dz;
						int marklr = -1;
						int je = cpgelementlist[inei].neighbor(0);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2){
								marklr = 0;
							}
						}
						je = cpgelementlist[inei].neighbor(1);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2){
								marklr = 1;//use one direction only
							}
						}
						int markfb = -1;
						je = cpgelementlist[inei].neighbor(2);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2){
								markfb = 2;
							}
						}
						je = cpgelementlist[inei].neighbor(3);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2){
								markfb = 3;
							}
						}
						int markbt = -1;
						je = cpgelementlist[inei].neighbor(4);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2){
								markbt = 4;
							}
						}
						je = cpgelementlist[inei].neighbor(5);
						if (je >= 0){
							if (cpgelementlist[je].status() == 2){
								markbt = 5;
							}
						}
						if (marklr < 0 && markfb < 0 && markbt < 0){
							std::cout << "no neighbor is frozen" << std::endl;
							std::exit(EXIT_FAILURE);
						}
						double t;
						//one frozen
						if (marklr >= 0 && markfb < 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							double t1 = cpgelementlist[ele1].tof();
							t = dx / fx + t1;
						}
						else if (marklr < 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							double t1 = cpgelementlist[ele1].tof();
							t = dy / fy + t1;
						}
						else if (marklr < 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();
							t = dz / fz + t1;
						}
						//two frozen
						else if (marklr >= 0 && markfb >= 0 && markbt < 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							double a = fx*fx / dx / dx + fy*fy / dy / dy;
							double b = -2 * t1 *fx*fx / dx / dx - 2 * t2*fy*fy / dy / dy;
							double c = t1*t1*fx*fx / dx / dx + t2*t2*fy*fy / dy / dy - 1;
							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
						}
						else if (marklr >= 0 && markfb < 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							double a = 1 * fx*fx / dx / dx + 1 * fz*fz / dz / dz;
							double b = -2 * t1*fx*fx / dx / dx - 2 * t2 *fz*fz / dz / dz;
							double c = t1*t1 *fx*fx / dx / dx + t2*t2 *fz*fz / dz / dz - 1;
							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
						}
						else if (marklr < 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markfb);
							int ele2 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							double a = 1 * fy*fy / dy / dy + 1 * fz*fz / dz / dz;
							double b = -2 * fy*fy* t1 / dy / dy - 2 * fz*fz* t2 / dz / dz;
							double c = t1*t1 *fy*fy / dy / dy + t2*t2 *fz*fz / dz / dz - 1;
							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
						}
						else if (marklr >= 0 && markfb >= 0 && markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							int ele2 = cpgelementlist[inei].neighbor(markfb);
							int ele3 = cpgelementlist[inei].neighbor(markbt);
							double t1 = cpgelementlist[ele1].tof();
							double t2 = cpgelementlist[ele2].tof();
							double t3 = cpgelementlist[ele3].tof();
							double a = 1 * fx*fx / dx / dx + 1 * fy*fy / dy / dy + 1 * fz*fz / dz / dz;
							double b = -2 * (t1 *fx*fx / dx / dx + t2 *fy*fy / dy / dy + t3*fz*fz / dz / dz);
							double c = t1*t1*fx*fx / dx / dx + t2*t2*fy*fy / dy / dy + t3*t3 *fz*fz / dz / dz - 1;
							t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
						}
						cpgelementlist[inei].tof(t);
					}
				}
			}
		}

		//choose smallest and proceed
		double tof_min = 1e+20;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int iele = band[i];
			if (cpgelementlist[iele].tof() < tof_min){
				tof_min = cpgelementlist[iele].tof();
				id_min = iele;
				id_vec = i;
			}
		}
		cpgelementlist[id_min].status(2); //frozen

		band.erase(band.begin() + id_vec);
		fbset0.clear();
		fbset0.push_back(id_min);
	} while (id_min >= 0 && band.size()>0);
	std::cout << "FMM CPG done" << std::endl;
}

void REGION::computeaveragetof_cpg(){
	double avt = 0;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		avt = avt + cpgelementlist[ie].tof();
	}
	avt = avt / cpgelementlist.size();
	std::cout << "average numerical tof " << avt << std::endl;
}

void REGION::computeaveragetofback_cpg(){
	double avt = 0;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		avt = avt + cpgelementlist[ie].tof_back();
	}
	avt = avt / cpgelementlist.size();
	std::cout << "average analytical tof " << avt << std::endl;
}

void REGION::computeaveragetof_cpg_analytical(){
	double avt = 0;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		double edx = cpgelementlist[ie].centnode().x();
		double edy = cpgelementlist[ie].centnode().y();
		double edz = cpgelementlist[ie].centnode().z();
		double fx = 1; double fy = 1; double fz = 0.5;
		double ll_ = std::sqrt(edx*edx + edy*edy + edz*edz);
		double ff;
		if (edx != 0){
			double m = edy / edx; double n = edz / edx;
			double left = (1 / fx / fx + m*m / fy / fy + n*n / fz / fz);
			ff = std::sqrt((1 + m*m + n*n) / left); //directional derivative from ellipse
		}
		else if (edz == 0){
			ff = fy;
		}
		else{
			double m = edy / edz;
			double left = m*m / fy / fy + 1 / fz / fz;
			ff = std::sqrt((1 + m*m) / left);
		}
		double tt = ll_ / ff;
		cpgelementlist[ie].tof_back(tt);
		avt = avt + tt;
	}
	avt = avt / cpgelementlist.size();
	std::cout << "average analytical tof " << avt << std::endl;
}

void REGION::computeaveragetof_tet(){
	double avt = 0;
	for (int i = 0; i < nodelist.size(); i++){
		avt = avt + nodelist[i].tof();
	}
	avt = avt / nodelist.size();
	std::cout << "average numerical tof " << avt << std::endl;
}

void REGION::computeaveragePo_tet(){
	double avt = 0;
	for (int i = 0; i < nodelist.size(); i++){
		avt = avt + nodelist[i].Po();
	}
	avt = avt / nodelist.size();
	std::cout << "average numerical Po " << avt/6894.76 << std::endl;
}

void REGION::computeaveragetofback_tet(){
	double avt = 0;
	for (int i = 0; i < nodelist.size(); i++){
		avt = avt + nodelist[i].tof_back();
	}
	avt = avt / nodelist.size();
	std::cout << "average numerical tofback " << avt << std::endl;
}


void REGION::fmm_cartesian2d_iso(){
	//firstly construct a Cartesian mesh x 0 to 100 y 0 to 100
	double lx = 100.0;
	double ly = 100.0;
	double dx = 1.0;
	double dy = 1.0;
	int nx = int(lx / dx);
	int ny = int(ly / dy);
	int nz = 1;
	std::vector<std::vector<int>> cpgelementmatrix;
	std::vector<std::vector<std::vector<int>>> cpgnodematrix;
	cpgelementmatrix.resize(nx);
	for (int i = 0; i < nx; i++){
		cpgelementmatrix[i].resize(ny);
	}

	for (int j = 0; j < ny; j++){
		for (int i = 0; i < nx; i++){
			cpgelementmatrix[i][j] = j*nx + i;
		}
	}
	cpgnodematrix.resize(nx + 1);
	for (int i= 0; i < nx + 1; i++){
		cpgnodematrix[i].resize(ny+1);
	}
	for (int i = 0; i < nx + 1; i++){
		for (int j = 0; j < ny + 1; j++){
			cpgnodematrix[i][j].resize(2);
		}
	}
	NODE node_;
	for (int k = 0; k < nz + 1; k++){
		for (int j = 0; j < ny + 1; j++){
			for (int i = 0; i < nx + 1; i++){
				cpgnodematrix[i][j][k] =k*(nx+1)*(ny+1)+j*(nx + 1) + i;
				node_.x(dx*i);
				node_.y(dy*j);
				node_.z(0);
				nodelist.push_back(node_);
			}
		}
	}

	//build connectivity and neighbors
	for (int j = 0; j < ny; j++){
		for (int i = 0; i < nx; i++){
			CPGELEMENT cpgelement;
			//neighbors
			cpgelement.layer(0);//layer 0 means not reached
			cpgelement.mu(1.0); //mu to denote F
			int nc = cpgelementlist.size();
			if (i > 0){
				cpgelement.neighbor(0, nc - 1);//left
			}
			if (i < nx - 1){
				cpgelement.neighbor(1, nc + 1);//right
			}
			if (j>0){
				cpgelement.neighbor(2, nc - nx);//down
			}
			if (j < ny - 1){
				cpgelement.neighbor(3, nc + nx);//up
			}
			cpgelement.node(0, cpgnodematrix[i][j][0]);
			cpgelement.node(1, cpgnodematrix[i+1][j][0]);
			cpgelement.node(2, cpgnodematrix[i][j + 1][0]);
			cpgelement.node(3, cpgnodematrix[i + 1][j + 1][0]);
			cpgelement.node(4, cpgnodematrix[i][j][1]);
			cpgelement.node(5, cpgnodematrix[i + 1][j][1]);
			cpgelement.node(6, cpgnodematrix[i][j + 1][1]);
			cpgelement.node(7, cpgnodematrix[i + 1][j + 1][1]);
			cpgelement.mu(1);//velocity for fmm
			cpgelementlist.push_back(cpgelement);
		}
	}
	int ist = cpgelementmatrix[nx / 2][ny / 2];
	cpgelementlist[ist].layer(2); //frozen
	cpgelementlist[ist].tof(0); //this is diffusive tof
	std::vector<int> band;
	int id_min=ist;
	do{
		ist = id_min;
		//find and compute narrow band
		for (int i = 0; i < 4; i++){
			int inei = cpgelementlist[ist].neighbor(i);
			if (inei >= 0){
				if (cpgelementlist[inei].layer() == 0){
					band.push_back(inei);//narrow band
					cpgelementlist[inei].layer(1); //in band
				}
				if (cpgelementlist[inei].layer() != 2){//previously added-to-band cells are recomputed
					double ff = cpgelementlist[inei].mu();
					int marklr = -1;
					int je = cpgelementlist[inei].neighbor(0);
					if (je >= 0){
						if (cpgelementlist[je].layer() == 2){
							marklr = 0;
						}
					}
					je = cpgelementlist[inei].neighbor(1);
					if (je >= 0){
						if (cpgelementlist[je].layer() == 2){
							marklr = 1;//use one direction only
						}
					}
					int markbt = -1;
					je = cpgelementlist[inei].neighbor(2);
					if (je >= 0){
						if (cpgelementlist[je].layer() == 2){
							markbt = 2;
						}
					}
					je = cpgelementlist[inei].neighbor(3);
					if (je >= 0){
						if (cpgelementlist[je].layer() == 2){
							markbt = 3;
						}
					}
					if (marklr < 0 && markbt < 0){
						std::cout << "no neighbor is frozen" << std::endl;
						std::exit(EXIT_FAILURE);
					}
					double t;
					if (marklr < 0 || markbt < 0){ //one frozen
						double t1, d1;
						if (marklr >= 0){ //left-right frozen
							int ele1 = cpgelementlist[inei].neighbor(marklr);
							t1 = cpgelementlist[ele1].tof();
							d1 = dx;
						}
						else if (markbt >= 0){
							int ele1 = cpgelementlist[inei].neighbor(markbt);
							t1 = cpgelementlist[ele1].tof();
							d1 = dy;
						}
						t = d1 / ff + t1;
					}
					else if (marklr >= 0 && markbt >= 0){
						int ele1 = cpgelementlist[inei].neighbor(marklr);
						int ele2 = cpgelementlist[inei].neighbor(markbt);
						double t1 = cpgelementlist[ele1].tof();
						double t2 = cpgelementlist[ele2].tof();
						double a = 1 / dx / dx + 1 / dy / dy;
						double b = -2 * t1 / dx / dx - 2 * t2 / dy / dy;
						double c = t1*t1 / dx / dx + t2*t2 / dy / dy - 1 / ff / ff;
						t = (-b + std::sqrt(b*b - 4 * a*c)) / 2.0 / a;
					}
					cpgelementlist[inei].tof(t);
				}
			}
		}

		//choose smallest and proceed

		double tof_min = 1e+10;
		int id_vec;
		id_min = -1;
		for (int i = 0; i < band.size(); i++){
			int iele = band[i];
			if (cpgelementlist[iele].tof() < tof_min){
				tof_min = cpgelementlist[iele].tof();
				id_min = iele;
				id_vec = i;
			}
		}
		cpgelementlist[id_min].layer(2); //frozen
		band.erase(band.begin() + id_vec);
	} while (id_min>=0 && band.size()>0);
}

void REGION::setcar_well_nomodel(int wellid_, int ix, int iy, int sign, double pw){
	int nz = car_nz;
	int nx = car_nx;
	int ny = car_ny;
	for (int k = 0; k < nz; k++){
		int ie = k*nx*ny+iy*nx+ix;
		cpgelementlist[ie].Po(pw);
		cpgelementlist[ie].markbc(1);
		cpgelementlist[ie].markwell(wellid_);
		if (sign == 1){
			cpgelementlist[ie].Sw(1.0);
			cpgelementlist[ie].markbc_Sw(1);
		}
		else if (sign == -1){
			producercpgelelist.push_back(ie);
		}
	}
}

void REGION::setcar_well_nomodel(int wellid_, int ix, int iy, double dep, int sign, double pw){
	int nz = car_nz;
	int nx = car_nx;
	int ny = car_ny;
	for (int k = 0; k < nz; k++){
		int ie = k*nx*ny + iy*nx + ix;
		if (cpgelementlist[ie].centnode().z() >= dep){
			cpgelementlist[ie].Po(pw * 1e+5);
			cpgelementlist[ie].markbc(1);
			cpgelementlist[ie].markwell(wellid_);
			if (sign == 1){
				cpgelementlist[ie].Sw(1.0);
				cpgelementlist[ie].markbc_Sw(1);
			}
			else if (sign == -1){
				producercpgelelist.push_back(ie);
			}
		}
	}
}

void REGION::setcar_pbc(int id, double p_, int sign){
	double xmin = 0;
	double xmax = 100;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		if (id == 2){
			double x = cpgelementlist[ie].centnode().x();
			if (std::abs(x - xmin) < 0.00001){
				if (sign == 1){
					cpgelementlist[ie].Sw(1);
					cpgelementlist[ie].markbc_Sw(1);
				}
				else if (sign == -1){
					producercpgelelist.push_back(ie);
				}

				cpgelementlist[ie].Po(p_ * 1e+5);
				cpgelementlist[ie].markbc(1);
			}
		}
		else if (id == 3){
			double x = cpgelementlist[ie].centnode().x();
			if (std::abs(x - xmax) < 0.00001){
				if (sign == 1){
					cpgelementlist[ie].Sw(1);
					cpgelementlist[ie].markbc_Sw(1);
				}
				else if (sign == -1){
					producercpgelelist.push_back(ie);
				}

				cpgelementlist[ie].Po(p_ * 1e+5);
				cpgelementlist[ie].markbc(1);
			}
		}
	}
}

void REGION::buildcarmesh(double x0, double y0, double z0, double lx, double ly, double lz, int nx, int ny, int nz){
	std::cout << "build CPG start" << std::endl;
	double dx = (lx-x0) / nx;
	double dy = (ly-y0) / ny;
	double dz = (lz-z0) / nz;
	double vol_ = dx*dy*dz;
	double car_dx = dx;
	double car_dy = dy;
	double car_dz = dz;
	car_nx = nx;
	car_ny = ny;
	car_nz = nz;

	int nex = nx;
	int ney = ny;
	int nez = nz;
	//no need cpgmatrix

	NODE node_;
	for (int k = 0; k < nz + 1; k++){
		for (int j = 0; j < ny + 1; j++){
			for (int i = 0; i < nx + 1; i++){
				node_.x(dx*i+x0);
				node_.y(dy*j+y0);
				node_.z(dz*k+z0);
				nodelist.push_back(node_);
			}
		}
	}

	//build connectivity and neighbors
	for (int k = 0; k < nz; k++){
		for (int j = 0; j < ny; j++){
			for (int i = 0; i < nx; i++){
				CPGELEMENT cpgelement;
				//neighbors
				cpgelement.layer(0);//layer 0 means not reached
				int nc = cpgelementlist.size();
				if (i > 0){
					cpgelement.neighbor(0, nc - 1);//left -x
				}
				if (i < nx - 1){
					cpgelement.neighbor(1, nc + 1);//right +x
				}
				if (j>0){
					cpgelement.neighbor(2, nc - nx);//front -y
				}
				if (j < ny - 1){
					cpgelement.neighbor(3, nc + nx);//back +y
				}
				if (k>0){
					cpgelement.neighbor(4, nc - nx*ny); //bottom -z
				}
				if (k < nz - 1){
					cpgelement.neighbor(5, nc + nx*ny); //top +z
				}
				int i0 = k*(nx + 1)*(ny + 1) + j*(nx + 1) + i;
				int i1 = k*(nx + 1)*(ny + 1) + j*(nx + 1) + i + 1;
				int i2 = k*(nx + 1)*(ny + 1) + (j + 1)*(nx + 1) + i;
				int i3 = k*(nx + 1)*(ny + 1) + (j + 1)*(nx + 1) + i + 1;
				int i4 = (k+1)*(nx + 1)*(ny + 1) + j*(nx + 1) + i;
				int i5 = (k+1)*(nx + 1)*(ny + 1) + j*(nx + 1) + i+1;
				int i6 = (k+1)*(nx + 1)*(ny + 1) + (j+1)*(nx + 1) + i;
				int i7 = (k + 1)*(nx + 1)*(ny + 1) + (j + 1)*(nx + 1) + i+1;
				cpgelement.node(0, i0);
				cpgelement.node(1, i1);
				cpgelement.node(2, i2);
				cpgelement.node(3, i3);
				cpgelement.node(4, i4);
				cpgelement.node(5, i5);
				cpgelement.node(6, i6);
				cpgelement.node(7, i7);
				double cx = (nodelist[i0].x() + nodelist[i1].x() + nodelist[i2].x() + nodelist[i3].x() + nodelist[i4].x() + nodelist[i5].x() + nodelist[i6].x() + nodelist[i7].x()) / 8.0;
				double cy = (nodelist[i0].y() + nodelist[i1].y() + nodelist[i2].y() + nodelist[i3].y() + nodelist[i4].y() + nodelist[i5].y() + nodelist[i6].y() + nodelist[i7].y()) / 8.0;
				double cz = (nodelist[i0].z() + nodelist[i1].z() + nodelist[i2].z() + nodelist[i3].z() + nodelist[i4].z() + nodelist[i5].z() + nodelist[i6].z() + nodelist[i7].z()) / 8.0;
				node_.x(cx); node_.y(cy); node_.z(cz);
				cpgelement.centnode(node_);
				cpgelement.volume(vol_);
				cpgelement.dx = car_dx;
				cpgelement.dy = car_dy;
				cpgelement.dz = car_dz;
				cpgelementlist.push_back(cpgelement);
			}
		}
	}
	std::cout << "build CPG done" << std::endl;
}


//void REGION::buildcpgfaces_car(){
//	CPGFACE cpgface_;
//	std::vector<std::vector<int>> markmatrix;
//	int nele = cpgelementlist.size();
//	markmatrix.resize(nele);
//	for (int i = 0; i < nele; i++){
//		markmatrix[i].resize(nele);
//	}
//	for (int ie = 0; ie < nele; ie++){
//		for (int j = 0; j < 6; j++){
//			int je = cpgelementlist[ie].neighbor(j);
//			if (je >= 0 && markmatrix[ie][je]==0){
//				cpgfacelist.push_back(cpgface_);
//				cpgelementlist[ie].cpgface(j, cpgfacelist.size() - 1);
//				markmatrix[ie][je] = 1;
//				markmatrix[je][ie] = 1;
//				int j2;
//				if (j == 0){
//					j2 = 1;
//				}
//				else if (j == 1){
//					j2 = 0;
//				}
//				else if (j == 2){
//					j2 = 3;
//				}
//				else if (j == 3){
//					j2 = 2;
//				}
//				else if (j == 4){
//					j2 = 5;
//				}
//				else if (j == 5){
//					j2 = 4;
//				}
//				cpgelementlist[je].cpgface(j2, cpgfacelist.size() - 1);
//			}
//		}
//	}
//
//
//
//
//}

void REGION::writedtof(char* file){
	std::ofstream output;
	output.open(file);
	for (int i = 0; i < cpgelementlist.size(); i++){
		output << cpgelementlist[i].tof() << std::endl;
	}

}

void REGION::writedtofana(char* file){
	std::ofstream output;
	output.open(file);
	for (int i = 0; i < cpgelementlist.size(); i++){
		output << cpgelementlist[i].tof_back() << std::endl;
	}

}

void REGION::writeresult_fmm_cartesian(char* file){
	int i, nelement;
	std::ofstream output;
	nelement = cpgelementlist.size();
	std::cout << "outputting FMM results for CPG" << std::endl;
	output.open(file);
	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Unstructured Grid" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET UNSTRUCTURED_GRID" << std::endl;
	output << "POINTS " << nodelist.size() << " double" << std::endl;
	for (i = 0; i < nodelist.size(); i++){
		output << nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
	};
	output << std::endl;
	output << "CELLS " << cpgelementlist.size() << " " << cpgelementlist.size() * 9 << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 8 << " " << cpgelementlist[i].node(0) << " " << cpgelementlist[i].node(1) << " " << cpgelementlist[i].node(3) << " " << cpgelementlist[i].node(2) << " " << cpgelementlist[i].node(4) << " " << cpgelementlist[i].node(5) << " " << cpgelementlist[i].node(7) << " " << cpgelementlist[i].node(6) << std::endl;
	}
	output << std::endl;
	output << "CELL_TYPES " << cpgelementlist.size() << std::endl;
	for (i = 0; i < cpgelementlist.size(); i++){
		output << 12 << std::endl;
	}

	output << "CELL_DATA" << " " << nelement << std::endl;

	if (dtofbutton_ == 1){
		output << "SCALARS DTOF double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].tof() << std::endl;
		}
		/*output << "SCALARS FMMVX double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].fmmv(0) << std::endl;
		}*/
	}
	if (dtofanabutton_ == 1){
		output << "SCALARS DTOFANA_SEC double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].tof_back() << std::endl;
		}
	}
	if (vis_permbutton_ == 1){
		output << "SCALARS Permeability double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].k(0, 0) / 1.0e-15 << std::endl;
		}
	}
	if (vis_porobutton_ == 1){
		output << "SCALARS Porosity double" << std::endl;
		output << "LOOKUP_TABLE default" << std::endl;
		for (i = 0; i < nelement; i++){
			output << cpgelementlist[i].porosity() << std::endl;
		}
	}
	output.close();

}

void REGION::cpgtrans_car_sp(){ //for cartesian fv=fd; similar as divide by volume in trans
	//TPFA trans. see MRST book
	int nele = cpgelementlist.size();
	std::cout << "compute cpg trans car sp" << std::endl;
	for (int ie = 0; ie < nele; ie++){
		double car_dx = cpgelementlist[ie].dx;
		double car_dy = cpgelementlist[ie].dy;
		double car_dz = cpgelementlist[ie].dz;
		for (int j = 0; j < 6; j++){
			int je = cpgelementlist[ie].neighbor(j);
			if (je >= 0){
				double k1, k2, tt, t1, mt1, t2, dd;
				mt1 = 1.0 / mu_o;
				if (j == 0 || j == 1){
					mt1 = mt1*car_dy*car_dz;
					k1 = cpgelementlist[ie].k(0, 0);
					k2 = cpgelementlist[je].k(0, 0);
					dd = car_dx / 2;
				}
				if (j == 2 || j == 3){
					mt1 = mt1*car_dx*car_dz;
					k1 = cpgelementlist[ie].k(1, 1);
					k2 = cpgelementlist[je].k(1, 1);
					dd = car_dy / 2;
				}
				if (j == 4 || j == 5){
					mt1 = mt1*car_dx*car_dy;

					k1 = cpgelementlist[ie].k(2, 2);
					k2 = cpgelementlist[je].k(2, 2);
					dd = car_dz / 2;
				}
				t1 = mt1*k1 / dd;
				t2 = mt1*k2 / dd;
				tt = 1 / (1 / t1 + 1 / t2);

				cpgelementlist[ie].trans(j, tt);
			}
		}
	}

}

void REGION::cpgtrans_car_temperature(){ //for cartesian fv=fd; similar as divide by volume in trans
	//TPFA trans. see MRST book
	int nele = cpgelementlist.size();
	std::cout << "compute cpg trans car sp" << std::endl;
	for (int ie = 0; ie < nele; ie++){
		double car_dx = cpgelementlist[ie].dx;
		double car_dy = cpgelementlist[ie].dy;
		double car_dz = cpgelementlist[ie].dz;
		for (int j = 0; j < 6; j++){
			int je = cpgelementlist[ie].neighbor(j);
			if (je >= 0){
				double k1, k2, aa, dd, tt;
				k1 = cpgelementlist[ie].kk;
				k2 = cpgelementlist[je].kk;
				if (j == 0 || j == 1){
					aa = car_dy*car_dz;
					dd = car_dx;
				}
				if (j == 2 || j == 3){
					aa = car_dx*car_dz;
					dd = car_dy;
				}
				if (j == 4 || j == 5){
					aa = car_dx*car_dy;
					dd = car_dz;
				}
				tt = 2 / (1 / k1 + 1 / k2)*aa/dd;

				cpgelementlist[ie].thermaltrans[j]=tt;
			}
		}
	}

}

void REGION::shapefunction(){
	int i, j, nelement;
	double aa[4], bb[4], cc[4], dd[4], xx4[4], yy4[4], zz4[4], vol;
	nelement = elementlist.size();
	for (i = 0; i < nelement; i++){
		for (j = 0; j < 4; j++){
			xx4[j] = nodelist[elementlist[i].node(j)].x();
			yy4[j] = nodelist[elementlist[i].node(j)].y();
			zz4[j] = nodelist[elementlist[i].node(j)].z();
		};
		vol = mathzz_.volume_tetra_signed(xx4, yy4, zz4); //same as abs((r12xr13)*r14) ***signed volume***
		aa[0] = mathzz_.determinant3(xx4[1], yy4[1], zz4[1], xx4[2], yy4[2], zz4[2], xx4[3], yy4[3], zz4[3]) / 6.0 / vol;
		aa[1] = -mathzz_.determinant3(xx4[0], yy4[0], zz4[0], xx4[2], yy4[2], zz4[2], xx4[3], yy4[3], zz4[3]) / 6.0 / vol;
		aa[2] = mathzz_.determinant3(xx4[0], yy4[0], zz4[0], xx4[1], yy4[1], zz4[1], xx4[3], yy4[3], zz4[3]) / 6.0 / vol;
		aa[3] = -mathzz_.determinant3(xx4[0], yy4[0], zz4[0], xx4[1], yy4[1], zz4[1], xx4[2], yy4[2], zz4[2]) / 6.0 / vol;
		//aa[3] = 1.0 - aa[0] - aa[1] - aa[2];

		bb[0] = -mathzz_.determinant3(1, yy4[1], zz4[1], 1, yy4[2], zz4[2], 1, yy4[3], zz4[3]) / 6.0 / vol;
		bb[1] = mathzz_.determinant3(1, yy4[0], zz4[0], 1, yy4[2], zz4[2], 1, yy4[3], zz4[3]) / 6.0 / vol;
		bb[2] = -mathzz_.determinant3(1, yy4[0], zz4[0], 1, yy4[1], zz4[1], 1, yy4[3], zz4[3]) / 6.0 / vol;
		bb[3] = mathzz_.determinant3(1, yy4[0], zz4[0], 1, yy4[1], zz4[1], 1, yy4[2], zz4[2]) / 6.0 / vol;
		//bb[3] = 0.0 - bb[0] - bb[1] - bb[2];

		cc[0] = mathzz_.determinant3(1, xx4[1], zz4[1], 1, xx4[2], zz4[2], 1, xx4[3], zz4[3]) / 6.0 / vol;
		cc[1] = -mathzz_.determinant3(1, xx4[0], zz4[0], 1, xx4[2], zz4[2], 1, xx4[3], zz4[3]) / 6.0 / vol;
		cc[2] = mathzz_.determinant3(1, xx4[0], zz4[0], 1, xx4[1], zz4[1], 1, xx4[3], zz4[3]) / 6.0 / vol;
		cc[3] = -mathzz_.determinant3(1, xx4[0], zz4[0], 1, xx4[1], zz4[1], 1, xx4[2], zz4[2]) / 6.0 / vol;
		//cc[3] = 0.0 - cc[0] - cc[1] - cc[2];

		dd[0] = -mathzz_.determinant3(1, xx4[1], yy4[1], 1, xx4[2], yy4[2], 1, xx4[3], yy4[3]) / 6.0 / vol;
		dd[1] = mathzz_.determinant3(1, xx4[0], yy4[0], 1, xx4[2], yy4[2], 1, xx4[3], yy4[3]) / 6.0 / vol;
		dd[2] = -mathzz_.determinant3(1, xx4[0], yy4[0], 1, xx4[1], yy4[1], 1, xx4[3], yy4[3]) / 6.0 / vol;
		dd[3] = mathzz_.determinant3(1, xx4[0], yy4[0], 1, xx4[1], yy4[1], 1, xx4[2], yy4[2]) / 6.0 / vol;
		//dd[3] = 0.0 - dd[0] - dd[1] - dd[2];

		for (j = 0; j < 4; j++){
			elementlist[i].Na(j, aa[j]);
			elementlist[i].Nb(j, bb[j]);
			elementlist[i].Nc(j, cc[j]);
			elementlist[i].Nd(j, dd[j]);
		}
	}
}

void REGION::callsamg(){ //explanation of the variables are in SAMG user guide
	//***samg primary parameters***
	int ifirst, ndiu, ndip, ncyc, nsolve, napproach, iswtch, iout, idump;
	double eps, a_cmplx, g_cmplx, w_avrge, p_cmplx, chktol;
	//***samg hidden parameters***
	int levelx, milu, ndim;
	std::cout << "call samg elliptic:" << std::endl;
	matrix = 12; //1 means symmetrix, 2 means not zero row sum matrix
	ifirst = 1; //0 maintain input initial guess; 1 initial guess=0
	eps = 1.0e-5;
	nsys = 1; //for scalar system
	iscale = new int[1];//for nsys=1
	ndiu = 1; //for nsys=1
	iu = new int[1];
	iu[0] = 0;
	ndip = 1; //for nsys=1
	ip = new int[1];
	ip[0] = 0;
	nsolve = 1; //for variable-based approach for scalar; variable-wise plain relexation,interpolation weights on original matrix
	ncyc = 11050;//V-cycle; CG as accelerator; at most 50 iterations
	iswtch = 51;//samg is called just once
	a_cmplx = 2.2; //estimated dimensioning
	g_cmplx = 1.7; //estimated dimensioning
	w_avrge = 2.4; //estimated dimensioning
	p_cmplx = 0.0; //estimated dimensioning (irrelevant for scalar case)
	chktol = -1.0; //no input checking
	iout = 2; //display important input, residual history, work&memory&timing statistics
	idump = 0; //printout of coarsening history
	/*
	uout1.open("uout1.txt");
	for (i = 0; i < nnu; i++){
		uout1 << u[i] << std::endl;
	}
	*/


    SAMG(&nnu, &nna, &nsys,&ia[0], &ja[0], &a[0], &f[0], &u[0], &iu[0], &ndiu, &ip[0], &ndip,&matrix, &iscale[0],
		&res_in, &res_out, &ncyc_done, &ierr,&nsolve, &ifirst, &eps, &ncyc, &iswtch,&a_cmplx,
		&g_cmplx, &p_cmplx, &w_avrge,&chktol, &idump, &iout);

	/*
	uout2.open("uout2.txt");
	for (i = 0; i < nnu; i++){
	   uout2 << u[i] << std::endl;
	}
	*/
	std::cout << "call samg elliptic Done" << std::endl;
}
void REGION::callsamg_advection(){ //explanation of the variables are in SAMG user guide
	//***samg primary parameters***
	int ifirst, ndiu, ndip, ncyc, nsolve, napproach, iswtch, iout, idump;
	double eps, a_cmplx, g_cmplx, w_avrge, p_cmplx, chktol;
	//***samg hidden parameters***
	int levelx, milu, ndim;
	std::cout << "call samg advection:" << std::endl;
	matrix = 22; //1 means symmetrix, 2 means not zero row sum matrix
	ifirst = 1; //0 maintain input initial guess; 1 initial guess=0
	eps = 1.0e-5;
	nsys = 1; //for scalar system
	iscale = new int[1];//for nsys=1
	ndiu = 1; //for nsys=1
	iu = new int[1];
	iu[0] = 0;
	ndip = 1; //for nsys=1
	ip = new int[1];
	ip[0] = 0;
	nsolve = 1; //for variable-based approach for scalar; variable-wise plain relexation,interpolation weights on original matrix
	ncyc = 11050;//V-cycle; CG as accelerator; at most 50 iterations
	iswtch = 51;//samg is called just once
	a_cmplx = 2.2; //estimated dimensioning
	g_cmplx = 1.7; //estimated dimensioning
	w_avrge = 2.4; //estimated dimensioning
	p_cmplx = 0.0; //estimated dimensioning (irrelevant for scalar case)
	chktol = -1.0; //no input checking
	iout = 2; //display important input, residual history, work&memory&timing statistics
	idump = 0; //printout of coarsening history
	/*
	uout1.open("uout1.txt");
	for (i = 0; i < nnu; i++){
	uout1 << u[i] << std::endl;
	}
	*/


	SAMG(&nnu, &nna, &nsys, &ia[0], &ja[0], &a[0], &f[0], &u[0], &iu[0], &ndiu, &ip[0], &ndip, &matrix, &iscale[0],
		&res_in, &res_out, &ncyc_done, &ierr, &nsolve, &ifirst, &eps, &ncyc, &iswtch, &a_cmplx,
		&g_cmplx, &p_cmplx, &w_avrge, &chktol, &idump, &iout);

	/*
	uout2.open("uout2.txt");
	for (i = 0; i < nnu; i++){
	uout2 << u[i] << std::endl;
	}
	*/
	std::cout << "call samg advection Done" << std::endl;
}

void REGION::callsamg_advection_tracer(){ //explanation of the variables are in SAMG user guide
	//***samg primary parameters***
	int ifirst, ndiu, ndip, ncyc, nsolve, napproach, iswtch, iout, idump;
	double eps, a_cmplx, g_cmplx, w_avrge, p_cmplx, chktol;
	//***samg hidden parameters***
	int levelx, milu, ndim;
	std::cout << "call samg advection:" << std::endl;
	matrix = 22; //1 means symmetrix, 2 means not zero row sum matrix
	ifirst = 1; //0 maintain input initial guess; 1 initial guess=0
	eps = 1.0e-50;
	nsys = 1; //for scalar system
	iscale = new int[1];//for nsys=1
	ndiu = 1; //for nsys=1
	iu = new int[1];
	iu[0] = 0;
	ndip = 1; //for nsys=1
	ip = new int[1];
	ip[0] = 0;
	nsolve = 1; //for variable-based approach for scalar; variable-wise plain relexation,interpolation weights on original matrix
	ncyc = 11050;//V-cycle; CG as accelerator; at most 50 iterations
	iswtch = 51;//samg is called just once
	a_cmplx = 2.2; //estimated dimensioning
	g_cmplx = 1.7; //estimated dimensioning
	w_avrge = 2.4; //estimated dimensioning
	p_cmplx = 0.0; //estimated dimensioning (irrelevant for scalar case)
	chktol = -1.0; //no input checking
	iout = 2; //display important input, residual history, work&memory&timing statistics
	idump = 0; //printout of coarsening history
	/*
	uout1.open("uout1.txt");
	for (i = 0; i < nnu; i++){
	uout1 << u[i] << std::endl;
	}
	*/


	SAMG(&nnu, &nna, &nsys, &ia[0], &ja[0], &a[0], &f[0], &u[0], &iu[0], &ndiu, &ip[0], &ndip, &matrix, &iscale[0],
		&res_in, &res_out, &ncyc_done, &ierr, &nsolve, &ifirst, &eps, &ncyc, &iswtch, &a_cmplx,
		&g_cmplx, &p_cmplx, &w_avrge, &chktol, &idump, &iout);

	/*
	uout2.open("uout2.txt");
	for (i = 0; i < nnu; i++){
	uout2 << u[i] << std::endl;
	}
	*/
	std::cout << "call samg advection Done" << std::endl;
}


/*void REGION::computepressure_CVFE(){ //full permeability tensor; TK to K slow
	std::cout << "compute pressure using CVFE" << std::endl;
	int i, j, ie, i1, i2, i3, count, count2, mark_, inode, jnode, nnode, nelement, nnode_nD;
	double bc_const, area, c11, c12, c13, c21, c22, c23, c31, c32, c33, vol, source_term, neumann_term, dirichlet_term, kk;
	double d1, d2, d3;
	std::vector<double> a_vec, f_vec, TRHS, RHS;
	std::vector<int> ja_vec, ia_vec, row2original, original2row;
	std::vector < std::vector<double> > TK, K;
	pressurebutton = 1;
	nnode = nodelist.size();
	nelement = elementlist.size();
	shapefunction();
	TK.resize(nnode);
	TRHS.resize(nnode);

	for (i = 0; i < nnode; i++){
		TK[i].resize(nnode);
		TRHS[i] = 0;
	}
	for (i = 0; i < nnode; i++){
		for (j = 0; j < nnode; j++){
			TK[i][j] = 0.0;
		}
	}

	//calculate total matrix including boundary rows and columes and rhs dirichlet&source (source for all at the moment)
	double time_matrix1 = GetTickCount64();

	for (ie = 0; ie < nelement; ie++){
		vol = elementlist[ie].volume(); //positive volume
		c11 = elementlist[ie].k(0, 0) / elementlist[ie].mu();//non-isotropic all terms
		c12 = elementlist[ie].k(0, 1) / elementlist[ie].mu();
		c13 = elementlist[ie].k(0, 2) / elementlist[ie].mu();
		c21 = elementlist[ie].k(1, 0) / elementlist[ie].mu();
		c22 = elementlist[ie].k(1, 1) / elementlist[ie].mu();
		c23 = elementlist[ie].k(1, 2) / elementlist[ie].mu();
		c31 = elementlist[ie].k(2, 0) / elementlist[ie].mu();
		c32 = elementlist[ie].k(2, 1) / elementlist[ie].mu();
		c33 = elementlist[ie].k(2, 2) / elementlist[ie].mu();
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			for (j = 0; j < 4; j++){
				jnode = elementlist[ie].node(j);
				if (nodelist[inode].markbc() != 1){//for internal or neumann
					d1 = c11*elementlist[ie].Nb(j) + c12*elementlist[ie].Nc(j) + c13*elementlist[ie].Nd(j);
					d2 = c21*elementlist[ie].Nb(j) + c22*elementlist[ie].Nc(j) + c23*elementlist[ie].Nd(j);
					d3 = c31*elementlist[ie].Nb(j) + c32*elementlist[ie].Nc(j) + c33*elementlist[ie].Nd(j);
					if (nodelist[jnode].markbc() != 1){ //jnode internal or Neumann
						kk = vol*(elementlist[ie].Nb(i)*d1 + elementlist[ie].Nc(i)*d2 + elementlist[ie].Nd(i)*d3);
						TK[inode][jnode] = TK[inode][jnode] + kk;
					}
					else{//Dirichlet boundary
						bc_const = nodelist[jnode].Po();
						dirichlet_term = -bc_const *vol * (elementlist[ie].Nb(i)*d1 + elementlist[ie].Nc(i)*d2 + elementlist[ie].Nd(i)*d3);
						TRHS[inode] = TRHS[inode] + dirichlet_term;
					}
				}
			}
			if (nodelist[inode].markbc() == -1){ //source nodes
				source_term = 0.25*nodelist[inode].source(); //source is volume rate no need *vol
				TRHS[inode] = TRHS[inode] + source_term;
			}
		}
	}

	for (i = 0; i < bfacetBC[2].size(); i++){ //loop over all Neumann boundary triangular faces but ignore no-flow boundary (which is effectively same as internal for CVFE)
		j = bfacetBC[2][i]; //number of facet
		mark_ = bfacetlist[j].markbsurface(); // number of boundary
		area = bfacetlist[j].area(); //positive area
		i1 = bfacetlist[j].node(0);//nodes of facet
		i2 = bfacetlist[j].node(1);
		i3 = bfacetlist[j].node(2);
		bc_const = model.bsurface(mark_).influx(); //influx=volumetric flow rate per unit area
		neumann_term = bc_const*area / 3.0;
		TRHS[i1] = TRHS[i1] + neumann_term;
		TRHS[i2] = TRHS[i2] + neumann_term;
		TRHS[i3] = TRHS[i3] + neumann_term;
	}
	double time_matrix2 = GetTickCount64();
	std::cout << "matrix time spent is " << (time_matrix2 - time_matrix1) / CLOCKS_PER_SEC << std::endl;

	double time_stiff1 = GetTickCount64();

	//obtain stiffness matrix from total matrix
	//count number of rows
	count = 0;
	for (i = 0; i < nnode; i++){
		if (nodelist[i].markbc() != 1){
			count++;
		}
	}
	nnode_nD = count;
	K.resize(nnode_nD);
	row2original.resize(nnode_nD);
	original2row.resize(nnode);
	RHS.resize(nnode_nD);
	for (i = 0; i < nnode_nD; i++){
		K[i].resize(nnode_nD);
	}
	//get stiffness matrix
	count = 0;
	for (i = 0; i < nnode; i++){
		if (nodelist[i].markbc() != 1){ //if internal or Neumann point or no-flow
			count++;
			RHS[count - 1] = TRHS[i];
			row2original[count - 1] = i; //maps from rows to original node number
			original2row[i] = count - 1;
			count2 = 0;
			for (j = 0; j < nnode; j++){
				if (nodelist[j].markbc() != 1){ //if internal or Neumann point or no-flow
					count2++;
					K[count - 1][count2 - 1] = TK[i][j];
				}
			}
		}
	}
	double time_stiff2 = GetTickCount64();
	std::cout << "stiffness matrix time spent is " << (time_stiff2 - time_stiff1) / CLOCKS_PER_SEC << std::endl;

	double time_row1 = GetTickCount64();
	//obtain the compressed row matrix of the stiffness matrix: ia, ja, a, u, f
	count = 0;
	//for left hand side
	for (i = 0; i < nnode_nD; i++){ //loop over all rows of stiffness matrix
		count++; //count starts from 1; 1 means the first location in a
		ia_vec.push_back(count); //at least each row (of internal point) has one diagonal point no matter it is zero or not;
		a_vec.push_back(K[i][i]);
		ja_vec.push_back(i + 1); //column j=i for diagonal point
		f_vec.push_back(RHS[i]);
		for (j = 0; j < nnode_nD; j++){ //only count internal points
			if (j != i && std::abs(K[i][j])>0){
				count++;
				a_vec.push_back(K[i][j]);
				ja_vec.push_back(j + 1); //in ja "1" means the first number
			}
		}
	}
	ia_vec.push_back(count + 1);
	nnu = nnode_nD;
	nna = a_vec.size();

	ia = new int[nnu + 1];
	ja = new int[nna];
	a = new double[nna];

	for (i = 0; i < nna; i++){
		ja[i] = ja_vec[i];
		a[i] = a_vec[i];
	}
	for (i = 0; i < nnu + 1; i++){
		ia[i] = ia_vec[i];
	}

	u = new double[nnu];
	f = new double[nnu];

	for (i = 0; i < nnu; i++){
		inode = row2original[i];
		u[i] = nodelist[inode].Po(); //initial guess equal to initial condition
		f[i] = f_vec[i];
	}
	double time_row2 = GetTickCount64();
	std::cout << "compress row time spent is " << (time_row2 - time_row1) / CLOCKS_PER_SEC << std::endl;

	//check

	//aout.open("aout.txt");
	//iaout.open("iaout.txt");
	//jaout.open("jaout.txt");
	//fout.open("fout.txt");

	//for (i = 0; i < nna; i++){
	//aout << a[i] << std::endl;
	//jaout << ja[i] << std::endl;
	//}
	//for (i = 0; i < nnu + 1; i++){
	//iaout << ia[i] << std::endl;
	//}
	//for (i = 0; i < nnu; i++){
	//fout << f[i] << std::endl;
	//}


	callsamg();

	//obtain P from u and boundary conditions
	for (i = 0; i < nnode; i++){
		mark_ = nodelist[i].markbsurface();
		if (nodelist[i].markbc() != 1){ //for non-Dirichelt points
			j = original2row[i];
			nodelist[i].P(u[j]);
		}
	}
	//clear vectors
	ia_vec.clear();
	ja_vec.clear();
	a_vec.clear();
	f_vec.clear();
	RHS.clear();
	TRHS.clear();
	TK.clear();
	K.clear();

	original2row.clear();
	row2original.clear();
}*/


void REGION::computepressure_tpfa(){
	std::cout << "compute pressure using tpfa" << std::endl;
	std::vector<double> a_vec;
	std::vector<int> ja_vec, ia_vec, nD2node, node2nD;
	std::vector<ROW> rowlist;
	std::vector<double> rowvaluelist, RHS;
	std::vector<int> rowpositionlist;

	int nnode = nodelist.size();
	node2nD.resize(nnode);
	int nnode_nD = 0;
	for (int i = 0; i < nnode; i++){
		if (nodelist[i].markbc() != 1){
			nnode_nD++;
			node2nD[i] = nnode_nD - 1;
		}
	}
	nD2node.resize(nnode_nD);
	nnode_nD = 0;
	for (int i = 0; i < nnode; i++){
		if (nodelist[i].markbc() != 1){
			nD2node[nnode_nD] = i;
			nnode_nD++;
		}
	}

	RHS.resize(nnode_nD);

	for (int i = 0; i < nnode_nD; i++){
		RHS[i] = 0;
	}
	rowlist.resize(nnode_nD);
	//calculate compressed-row matrix
	for (int inD = 0; inD < nnode_nD; inD++){
		int inode = nD2node[inD];
		if (nodelist[inode].markbc() == -1){
			RHS[inD] = nodelist[inode].source()*nodelist[inode].dualvolume();
		}
	}

	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		int inode = edgelist[iedge].node(0);
		int jnode = edgelist[iedge].node(1);
		double trans = edgelist[iedge].trans();
		int inD = node2nD[inode];
		int jnD = node2nD[jnode];
		if (nodelist[inode].markbc() != 1){
			rowlist[inD].addvalue(trans, inD);
			if (nodelist[jnode].markbc() != 1){
				rowlist[inD].addvalue(-trans, jnD);
			}
			else{
				RHS[inD] = RHS[inD] + trans*nodelist[jnode].Po();
			}
		}
		if (nodelist[jnode].markbc() != 1){
			rowlist[jnD].addvalue(trans, jnD);
			if (nodelist[inode].markbc() != 1){
				rowlist[jnD].addvalue(-trans, inD);
			}
			else{
				RHS[jnD] = RHS[jnD] + trans*nodelist[inode].Po();
			}
		}
	}
	if (linearsolverbutton_ == 2){
		int solver_id; //see Hypre ex5 for more options; For RRM design default setting, currently AMG alone
		MPI_Comm comm;
		comm = 0;
		int ilower = 0;
		int jlower = 0;
		int iupper, jupper;
		HYPRE_IJMatrix ij_matrix;
		HYPRE_ParCSRMatrix parcsr_matrix;
		int nrows;
		int ncols;
		int *rows;
		int *cols;
		double *values;
		int local_size = nnode_nD;
		iupper = local_size - 1; //nnode_nD is local_size
		jupper = local_size - 1;
		HYPRE_IJMatrixCreate(comm, ilower, iupper, jlower, jupper, &ij_matrix);
		//HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &ij_matrix);
		HYPRE_IJMatrixSetObjectType(ij_matrix, HYPRE_PARCSR);
		HYPRE_IJMatrixInitialize(ij_matrix);

		nrows = 1;

		//set matrix coefficients
		for (int i = ilower; i <= iupper; i++){
			rowvaluelist = rowlist[i].valuevec();
			rowpositionlist = rowlist[i].positionvec();
			ncols = rowvaluelist.size();
			cols = new int[ncols];
			values = new double[ncols];
			for (int j = 0; j < ncols; j++){
				cols[j] = rowpositionlist[j];
				values[j] = rowvaluelist[j];
			}
			HYPRE_IJMatrixSetValues(ij_matrix, 1, &ncols, &i, cols, values);
		}
		HYPRE_IJMatrixAssemble(ij_matrix);
		HYPRE_IJMatrixGetObject(ij_matrix, (void **)&parcsr_matrix);

		//create RHS and solution
		HYPRE_IJVector b;
		HYPRE_ParVector par_b;
		HYPRE_IJVector x;
		HYPRE_ParVector par_x;
		HYPRE_Solver solver, precond;
		HYPRE_IJVectorCreate(comm, ilower, iupper, &b);
		HYPRE_IJVectorSetObjectType(b, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(b);

		HYPRE_IJVectorCreate(comm, ilower, iupper, &x);
		HYPRE_IJVectorSetObjectType(x, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(x);

		/* Set the rhs values; set the solution to zero */
		double *rhs_values, *x_values;
		rhs_values = new double[local_size];
		x_values = new double[local_size];
		rows = new int[local_size];
		for (int i = 0; i < local_size; i++){
			rhs_values[i] = RHS[i];
			x_values[i] = 0.0;
			rows[i] = i;
		}

		HYPRE_IJVectorSetValues(b, local_size, rows, rhs_values);
		HYPRE_IJVectorSetValues(x, local_size, rows, x_values);
		delete x_values;
		delete rhs_values;
		delete rows;
		HYPRE_IJVectorAssemble(b);
		HYPRE_IJVectorGetObject(b, (void **)&par_b);

		HYPRE_IJVectorAssemble(x);
		HYPRE_IJVectorGetObject(x, (void **)&par_x);
		/*HYPRE_IJMatrixPrint(ij_matrix, "IJ.out.A");
		HYPRE_IJVectorPrint(b, "IJ.out.b");*/
		/* Choose a solver and solve the system */
		solver_id = 0;
		/* AMG */
		if (solver_id == 0){
			int num_iterations;
			double final_res_norm;
			HYPRE_BoomerAMGCreate(&solver);
			/* Set some parameters (See Reference Manual for more parameters) */
			HYPRE_BoomerAMGSetPrintLevel(solver, 3);  /* print solve info + parameters */
			HYPRE_BoomerAMGSetCoarsenType(solver, 6); /* Falgout coarsening */
			HYPRE_BoomerAMGSetRelaxType(solver, 3);   /* G-S/Jacobi hybrid relaxation */
			HYPRE_BoomerAMGSetNumSweeps(solver, 1);   /* Sweeeps on each level */
			HYPRE_BoomerAMGSetMaxLevels(solver, 20);  /* maximum number of levels */
			HYPRE_BoomerAMGSetTol(solver, 1e-7);      /* conv. tolerance */
			/* Now setup and solve! */
			HYPRE_BoomerAMGSetup(solver, parcsr_matrix, par_b, par_x);
			HYPRE_BoomerAMGSolve(solver, parcsr_matrix, par_b, par_x);
			/* Run info - needed logging turned on */
			HYPRE_BoomerAMGGetNumIterations(solver, &num_iterations);
			HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, &final_res_norm);
			printf("\n");
			printf("Iterations = %d\n", num_iterations);
			printf("Final Relative Residual Norm = %e\n", final_res_norm);
			printf("\n");
			HYPRE_BoomerAMGDestroy(solver);
		}
		int nvalues = local_size;
		rows = new int[nvalues];
		for (int i = 0; i < nvalues; i++) rows[i] = i;
		values = new double[nvalues];
		HYPRE_IJVectorGetValues(x, nvalues, rows, values);
		HYPRE_IJMatrixDestroy(ij_matrix);
		HYPRE_IJVectorDestroy(b);
		HYPRE_IJVectorDestroy(x);

		for (int i = 0; i < nnode_nD; i++){
			int inode = nD2node[i];
			nodelist[inode].Po(values[i]);
		}
	}
	else{
		"to use HYPRE need setlinearsolver 2";
	}

}


void REGION::computepressure_cvfe(){ //full permeability tensor direct compressedrow matrix
	std::cout << "compute pressure using CVFE" << std::endl;
	int i, j, ie, i1, i2, i3, count, count2, mark_, inode, jnode, nnode, nelement, nnode_nD, inD, jnD, ii;
	double bc_const, area, c11, c12, c13, c21, c22, c23, c31, c32, c33, vol, source_term, neumann_term, dirichlet_term, kk;
	double d1, d2, d3;
	std::vector<double> a_vec;
	//std::vector < std::vector<double> > M;
	std::vector<int> ja_vec, ia_vec, nD2node, node2nD;
	std::vector<ROW> rowlist;
	std::vector<double> rowvaluelist, RHS;
	std::vector<int> rowpositionlist;

	nnode = nodelist.size();
	nelement = elementlist.size();

	node2nD.resize(nnode);
	nnode_nD = 0;
	for (i = 0; i < nnode; i++){
		if (nodelist[i].markbc() != 1){
			nnode_nD++;
			node2nD[i] = nnode_nD - 1;
		}
	}
	nD2node.resize(nnode_nD);
	nnode_nD = 0;
	for (i = 0; i < nnode; i++){
		if (nodelist[i].markbc() != 1){
			nD2node[nnode_nD] = i;
			nnode_nD++;
		}
	}

	RHS.resize(nnode_nD);

	for (i = 0; i < nnode_nD; i++){
		RHS[i] = 0;
	}

	rowlist.resize(nnode_nD);

	//calculate compressed-row matrix including boundary rows and columes and rhs dirichlet&source (source for all at the moment) directly

	for (ie = 0; ie < nelement; ie++){
		vol = elementlist[ie].volume(); //positive volume
		double mt = elementlist[ie].mobit();
		c11 = mt*elementlist[ie].k(0, 0);//non-isotropic all terms
		c12 = mt*elementlist[ie].k(0, 1);
		c13 = mt*elementlist[ie].k(0, 2);
		c21 = mt*elementlist[ie].k(1, 0);
		c22 = mt*elementlist[ie].k(1, 1);
		c23 = mt*elementlist[ie].k(1, 2);
		c31 = mt*elementlist[ie].k(2, 0);
		c32 = mt*elementlist[ie].k(2, 1);
		c33 = mt*elementlist[ie].k(2, 2);
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			if (nodelist[inode].markbc() != 1){//for internal or neumann
				inD = node2nD[inode];
				//compressedrow structure for samg requires first entry for each row is diagonal
				j = i;
				jnode = inode;
				jnD = inD;
				d1 = c11*elementlist[ie].Nb(j) + c12*elementlist[ie].Nc(j) + c13*elementlist[ie].Nd(j);
				d2 = c21*elementlist[ie].Nb(j) + c22*elementlist[ie].Nc(j) + c23*elementlist[ie].Nd(j);
				d3 = c31*elementlist[ie].Nb(j) + c32*elementlist[ie].Nc(j) + c33*elementlist[ie].Nd(j);
				//if jnD==inD then jnode.markbc!=1 for sure
				kk = vol*(elementlist[ie].Nb(i)*d1 + elementlist[ie].Nc(i)*d2 + elementlist[ie].Nd(i)*d3);
				rowlist[inD].addvalue(kk, jnD);
				for (j = 0; j < 4; j++){
					if (j != i){
						jnode = elementlist[ie].node(j);
						jnD = node2nD[jnode];
						d1 = c11*elementlist[ie].Nb(j) + c12*elementlist[ie].Nc(j) + c13*elementlist[ie].Nd(j);
						d2 = c21*elementlist[ie].Nb(j) + c22*elementlist[ie].Nc(j) + c23*elementlist[ie].Nd(j);
						d3 = c31*elementlist[ie].Nb(j) + c32*elementlist[ie].Nc(j) + c33*elementlist[ie].Nd(j);
						if (nodelist[jnode].markbc() != 1){ //jnode internal or Neumann; if jnD==inD then jnode.markbc!=1 for sure
							kk = vol*(elementlist[ie].Nb(i)*d1 + elementlist[ie].Nc(i)*d2 + elementlist[ie].Nd(i)*d3);
							rowlist[inD].addvalue(kk, jnD);//note jnD is from 0
						}
						else{//Dirichlet boundary
							bc_const = nodelist[jnode].Po();
							dirichlet_term = -bc_const *vol * (elementlist[ie].Nb(i)*d1 + elementlist[ie].Nc(i)*d2 + elementlist[ie].Nd(i)*d3);
							RHS[inD] = RHS[inD] + dirichlet_term;
						}
					}
				}
				if (nodelist[inode].markbc() == -1){ //source nodes
					source_term = 0.25*nodelist[inode].source(); //source is volume rate no need *vol
					RHS[inD] = RHS[inD] + source_term;
				}
			}
		}
	}
	if (bfacetBC.size() >= 3){
		for (i = 0; i < bfacetBC[2].size(); i++){ //loop over all Neumann boundary triangular faces but ignore no-flow boundary (which is effectively same as internal for CVFE)
			j = bfacetBC[2][i]; //number of facet
			mark_ = bfacetlist[j].markbsurface(); // number of boundary
			area = bfacetlist[j].area(); //positive area
			bc_const = bsurfacelist[mark_].influx(); //influx=volumetric flow rate per unit area
			neumann_term = bc_const*area / 3.0;
			for (ii = 0; ii < 3; ii++){
				inode = bfacetlist[j].node(ii);
				if (nodelist[inode].markbc() != 1){
					inD = node2nD[inode];
					RHS[inD] = RHS[inD] + neumann_term;
				}
			}
		}
	}

	//obtain the compressed row matrix of the stiffness matrix: ia, ja, a, u, f
	if (linearsolverbutton_ == 1){
		nnu = nnode_nD;
		ia = new int[nnu + 1];
		ia[0] = 1;
		for (i = 0; i < nnode_nD; i++){
			rowvaluelist = rowlist[i].valuevec();
			rowpositionlist = rowlist[i].positionvec();
			ia[i + 1] = ia[i] + rowpositionlist.size();
			for (j = 0; j < rowpositionlist.size(); j++){
				a_vec.push_back(rowvaluelist[j]);
				ja_vec.push_back(rowpositionlist[j] + 1); //in ja "1" means the first number
			}
		}

		nna = a_vec.size();

		ja = new int[nna];
		a = new double[nna];

		for (i = 0; i < nna; i++){
			ja[i] = ja_vec[i];
			a[i] = a_vec[i];
		}

		u = new double[nnu];
		f = new double[nnu];

		for (i = 0; i < nnu; i++){
			inode = nD2node[i];
			u[i] = nodelist[inode].Po(); //initial guess equal to initial condition which is P=0
			f[i] = RHS[i];
		}

		callsamg();

		//obtain P from u and boundary conditions
		for (i = 0; i < nnode_nD; i++){
			inode = nD2node[i];
			nodelist[inode].Po(u[i]);
		}

	}
	else if (linearsolverbutton_ == 2){
		int solver_id; //see Hypre ex5 for more options; For RRM design default setting, currently AMG alone
		MPI_Comm comm;
		comm = 0;
		int ilower = 0;
		int jlower = 0;
		int iupper, jupper;
		HYPRE_IJMatrix ij_matrix;
		HYPRE_ParCSRMatrix parcsr_matrix;
		int nrows;
		int ncols;
		int *rows;
		int *cols;
		double *values;
		int local_size = nnode_nD;
		iupper = local_size - 1; //nnode_nD is local_size
		jupper = local_size - 1;
		HYPRE_IJMatrixCreate(comm, ilower, iupper, jlower, jupper, &ij_matrix);
		//HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &ij_matrix);
		HYPRE_IJMatrixSetObjectType(ij_matrix, HYPRE_PARCSR);
		HYPRE_IJMatrixInitialize(ij_matrix);

		nrows = 1;

		//set matrix coefficients
		for (i = ilower; i <= iupper; i++){
			rowvaluelist = rowlist[i].valuevec();
			rowpositionlist = rowlist[i].positionvec();
			ncols = rowvaluelist.size();
			cols = new int[ncols];
			values = new double[ncols];
			for (j = 0; j < ncols; j++){
				cols[j] = rowpositionlist[j];
				values[j] = rowvaluelist[j];
			}
			HYPRE_IJMatrixSetValues(ij_matrix, 1, &ncols, &i, cols, values);
		}
		HYPRE_IJMatrixAssemble(ij_matrix);
		HYPRE_IJMatrixGetObject(ij_matrix, (void **)&parcsr_matrix);

		//create RHS and solution
		HYPRE_IJVector b;
		HYPRE_ParVector par_b;
		HYPRE_IJVector x;
		HYPRE_ParVector par_x;
		HYPRE_Solver solver, precond;
		HYPRE_IJVectorCreate(comm, ilower, iupper, &b);
		HYPRE_IJVectorSetObjectType(b, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(b);

		HYPRE_IJVectorCreate(comm, ilower, iupper, &x);
		HYPRE_IJVectorSetObjectType(x, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(x);

		/* Set the rhs values; set the solution to zero */
		double *rhs_values, *x_values;
		rhs_values = new double[local_size];
		x_values = new double[local_size];
		rows = new int[local_size];
		for (i = 0; i < local_size; i++){
			rhs_values[i] = RHS[i];
			x_values[i] = 0.0;
			rows[i] = i;
		}

		HYPRE_IJVectorSetValues(b, local_size, rows, rhs_values);
		HYPRE_IJVectorSetValues(x, local_size, rows, x_values);
		delete x_values;
		delete rhs_values;
		delete rows;
		HYPRE_IJVectorAssemble(b);
		HYPRE_IJVectorGetObject(b, (void **)&par_b);

		HYPRE_IJVectorAssemble(x);
		HYPRE_IJVectorGetObject(x, (void **)&par_x);
		/*HYPRE_IJMatrixPrint(ij_matrix, "IJ.out.A");
		HYPRE_IJVectorPrint(b, "IJ.out.b");*/
		/* Choose a solver and solve the system */
		solver_id = 0;
		/* AMG */
		if (solver_id == 0){
			int num_iterations;
			double final_res_norm;
			HYPRE_BoomerAMGCreate(&solver);
			/* Set some parameters (See Reference Manual for more parameters) */
			HYPRE_BoomerAMGSetPrintLevel(solver, 3);  /* print solve info + parameters */
			HYPRE_BoomerAMGSetCoarsenType(solver, 6); /* Falgout coarsening */
			HYPRE_BoomerAMGSetRelaxType(solver, 3);   /* G-S/Jacobi hybrid relaxation */
			HYPRE_BoomerAMGSetNumSweeps(solver, 1);   /* Sweeeps on each level */
			HYPRE_BoomerAMGSetMaxLevels(solver, 20);  /* maximum number of levels */
			HYPRE_BoomerAMGSetTol(solver, 1e-7);      /* conv. tolerance */
			/* Now setup and solve! */
			HYPRE_BoomerAMGSetup(solver, parcsr_matrix, par_b, par_x);
			HYPRE_BoomerAMGSolve(solver, parcsr_matrix, par_b, par_x);
			/* Run info - needed logging turned on */
			HYPRE_BoomerAMGGetNumIterations(solver, &num_iterations);
			HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, &final_res_norm);
			printf("\n");
			printf("Iterations = %d\n", num_iterations);
			printf("Final Relative Residual Norm = %e\n", final_res_norm);
			printf("\n");
			HYPRE_BoomerAMGDestroy(solver);
		}
		int nvalues = local_size;
		rows = new int[nvalues];
		for (i = 0; i < nvalues; i++) rows[i] = i;
		values = new double[nvalues];
		HYPRE_IJVectorGetValues(x, nvalues, rows, values);
		HYPRE_IJMatrixDestroy(ij_matrix);
		HYPRE_IJVectorDestroy(b);
		HYPRE_IJVectorDestroy(x);

		for (i = 0; i < nnode_nD; i++){
			inode = nD2node[i];
			nodelist[inode].Po(values[i]);
		}
	}

}

void REGION::computepressure_cpgfv(){
	std::cout << "compute pressure using FV for CPG" << std::endl;
	int ie, je, nele, i, j, nele_nD, inD, jnD, count;
	std::vector<double> a_vec, f_vec;
	std::vector<int> ja_vec, ia_vec;

	std::vector<int> nD2ele;
	std::vector<int> ele2nD;
	double re, rw, hw, ddx, ddy, ddz, WI;
	NODE node1, node2;
	std::vector<ROW> rowlist;
	std::vector<double> rowvaluelist, RHS;
	std::vector<int> rowpositionlist;

	nele = cpgelementlist.size();
	ele2nD.resize(nele);
	nele_nD = 0;
	for (i = 0; i < nele; i++){
		if (cpgelementlist[i].markbc() != 1){
			nD2ele.push_back(i);
			nele_nD++;
			ele2nD[i] = nele_nD - 1;
		}
	}


	RHS.resize(nele_nD);

	rowlist.resize(nele_nD);
	rw = 0.1; //radius of well
	for (inD = 0; inD < nele_nD; inD++){
		ie = nD2ele[inD];
		if (cpgelementlist[ie].markbc() == -1){
			RHS[inD] = cpgelementlist[ie].source(); //volume source
		}
		for (j = 0; j < 6; j++){
			je = cpgelementlist[ie].neighbor(j);
			if (je >= 0){
				rowlist[inD].addvalue(cpgelementlist[ie].trans(j), inD);
				if (cpgelementlist[je].markbc() != 1){
					jnD = ele2nD[je];
					rowlist[inD].addvalue(-cpgelementlist[ie].trans(j), jnD);
				}
				else{
					RHS[inD] = RHS[inD] + cpgelementlist[ie].trans(j) * cpgelementlist[je].Po();
				}
			}
		}
	}

	if (linearsolverbutton_ == 2){
		int solver_id; //see Hypre ex5 for more options; For RRM design default setting, currently AMG alone
		MPI_Comm comm;
		comm = 0;
		int ilower = 0;
		int jlower = 0;
		int iupper, jupper;
		HYPRE_IJMatrix ij_matrix;
		HYPRE_ParCSRMatrix parcsr_matrix;
		int nrows;
		int ncols;
		int *rows;
		int *cols;
		double *values;
		int local_size = nele_nD;
		iupper = local_size - 1; //nnode_nD is local_size
		jupper = local_size - 1;
		HYPRE_IJMatrixCreate(comm, ilower, iupper, jlower, jupper, &ij_matrix);
		//HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &ij_matrix);
		HYPRE_IJMatrixSetObjectType(ij_matrix, HYPRE_PARCSR);
		HYPRE_IJMatrixInitialize(ij_matrix);

		nrows = 1;

		//set matrix coefficients
		for (int i = ilower; i <= iupper; i++){
			rowvaluelist = rowlist[i].valuevec();
			rowpositionlist = rowlist[i].positionvec();
			ncols = rowvaluelist.size();
			cols = new int[ncols];
			values = new double[ncols];
			for (int j = 0; j < ncols; j++){
				cols[j] = rowpositionlist[j];
				values[j] = rowvaluelist[j];
			}
			HYPRE_IJMatrixSetValues(ij_matrix, 1, &ncols, &i, cols, values);
		}
		HYPRE_IJMatrixAssemble(ij_matrix);
		HYPRE_IJMatrixGetObject(ij_matrix, (void **)&parcsr_matrix);

		//create RHS and solution
		HYPRE_IJVector b;
		HYPRE_ParVector par_b;
		HYPRE_IJVector x;
		HYPRE_ParVector par_x;
		HYPRE_Solver solver, precond;
		HYPRE_IJVectorCreate(comm, ilower, iupper, &b);
		HYPRE_IJVectorSetObjectType(b, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(b);

		HYPRE_IJVectorCreate(comm, ilower, iupper, &x);
		HYPRE_IJVectorSetObjectType(x, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(x);

		/* Set the rhs values; set the solution to zero */
		double *rhs_values, *x_values;
		rhs_values = new double[local_size];
		x_values = new double[local_size];
		rows = new int[local_size];
		for (int i = 0; i < local_size; i++){
			rhs_values[i] = RHS[i];
			x_values[i] = 0.0;
			rows[i] = i;
		}

		HYPRE_IJVectorSetValues(b, local_size, rows, rhs_values);
		HYPRE_IJVectorSetValues(x, local_size, rows, x_values);
		delete x_values;
		delete rhs_values;
		delete rows;
		HYPRE_IJVectorAssemble(b);
		HYPRE_IJVectorGetObject(b, (void **)&par_b);

		HYPRE_IJVectorAssemble(x);
		HYPRE_IJVectorGetObject(x, (void **)&par_x);
		//HYPRE_IJMatrixPrint(ij_matrix, "IJ.out.A");
		//HYPRE_IJVectorPrint(b, "IJ.out.b");
		/* Choose a solver and solve the system */
		solver_id = 0;
		/* AMG */
		if (solver_id == 0){
			int num_iterations;
			double final_res_norm;
			HYPRE_BoomerAMGCreate(&solver);
			/* Set some parameters (See Reference Manual for more parameters) */
			HYPRE_BoomerAMGSetPrintLevel(solver, 3);  /* print solve info + parameters */
			HYPRE_BoomerAMGSetCoarsenType(solver, 6); /* Falgout coarsening */
			HYPRE_BoomerAMGSetRelaxType(solver, 3);   /* G-S/Jacobi hybrid relaxation */
			HYPRE_BoomerAMGSetNumSweeps(solver, 1);   /* Sweeeps on each level */
			HYPRE_BoomerAMGSetMaxLevels(solver, 20);  /* maximum number of levels */
			HYPRE_BoomerAMGSetTol(solver, 1e-7);      /* conv. tolerance */
			/* Now setup and solve! */
			HYPRE_BoomerAMGSetup(solver, parcsr_matrix, par_b, par_x);
			HYPRE_BoomerAMGSolve(solver, parcsr_matrix, par_b, par_x);
			/* Run info - needed logging turned on */
			HYPRE_BoomerAMGGetNumIterations(solver, &num_iterations);
			HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, &final_res_norm);
			printf("\n");
			printf("Iterations = %d\n", num_iterations);
			printf("Final Relative Residual Norm = %e\n", final_res_norm);
			printf("\n");
			HYPRE_BoomerAMGDestroy(solver);
		}
		int nvalues = local_size;
		rows = new int[nvalues];
		for (int i = 0; i < nvalues; i++) rows[i] = i;
		values = new double[nvalues];
		HYPRE_IJVectorGetValues(x, nvalues, rows, values);
		HYPRE_IJMatrixDestroy(ij_matrix);
		HYPRE_IJVectorDestroy(b);
		HYPRE_IJVectorDestroy(x);

		//obtain P from u and boundary conditions
		for (inD = 0; inD < nele_nD; inD++){
			ie = nD2ele[inD];
			cpgelementlist[ie].Po(values[inD]);
		}
	}
	else{
		"to use HYPRE need setlinearsolver 2";
		std::exit(EXIT_FAILURE);
	}
}

void REGION::steadytemperature(){
	std::cout << "compute steady-state temperature using FV for Cartesiangrid" << std::endl;
	int ie, je, nele, i, j, nele_nD, inD, jnD, count;
	std::vector<double> a_vec, f_vec;
	std::vector<int> ja_vec, ia_vec;

	std::vector<int> nD2ele;
	std::vector<int> ele2nD;
	double re, rw, hw, ddx, ddy, ddz, WI;
	NODE node1, node2;
	std::vector<ROW> rowlist;
	std::vector<double> rowvaluelist, RHS;
	std::vector<int> rowpositionlist;

	nele = cpgelementlist.size();
	ele2nD.resize(nele);
	nele_nD = 0;
	for (i = 0; i < nele; i++){
		if (cpgelementlist[i].markbc() != 1){
			nD2ele.push_back(i);
			nele_nD++;
			ele2nD[i] = nele_nD - 1;
		}
	}


	RHS.resize(nele_nD);

	rowlist.resize(nele_nD);
	for (inD = 0; inD < nele_nD; inD++){
		ie = nD2ele[inD];
		double rhocpi = rho_w*cp_w;
		if (cpgelementlist[ie].markbc() == -1){
			RHS[inD] = cpgelementlist[ie].source(); //volume source
		}

		for (j = 0; j < 6; j++){
			je = cpgelementlist[ie].neighbor(j);
			if (je >= 0){
				double rhocpj = rho_w*cp_w;
				double rhocp = 0.5*(rhocpi + rhocpj);//*0.1
				//double rhocp = 2.0 / (1 / rhocpi + 1 / rhocpj);
				rowlist[inD].addvalue(cpgelementlist[ie].thermaltrans[j], inD);
				double flux = cpgelementlist[ie].flux(j);
				if (cpgelementlist[je].markbc() != 1){
					jnD = ele2nD[je];
					rowlist[inD].addvalue(-cpgelementlist[ie].thermaltrans[j], jnD);
					//upwind
					/*if (flux > 0){
						rowlist[inD].addvalue(flux*rhocp, inD);
					}
					else if (flux < 0){
						rowlist[inD].addvalue(flux*rhocp, jnD);
					}*/
					//central
					rowlist[inD].addvalue(0.5*flux*rhocp, inD);
					rowlist[inD].addvalue(0.5*flux*rhocp, jnD);
				}
				else{
					RHS[inD] = RHS[inD] + cpgelementlist[ie].thermaltrans[j] * cpgelementlist[je].temperature;
					//upwind
					/*if (flux > 0){
						RHS[inD] = RHS[inD] - rhocp*flux*cpgelementlist[ie].temperature;
					}
					else if (flux < 0){
						RHS[inD] = RHS[inD] - rhocp*flux*cpgelementlist[je].temperature;
					}*/
					//central
					rowlist[inD].addvalue(0.5*flux*rhocp, inD);
					RHS[inD] = RHS[inD] - 0.5*flux*rhocp*cpgelementlist[je].temperature;
				}
			}
		}
	}

	if (linearsolverbutton_ == 2){
		int solver_id; //see Hypre ex5 for more options; For RRM design default setting, currently AMG alone
		MPI_Comm comm;
		comm = 0;
		int ilower = 0;
		int jlower = 0;
		int iupper, jupper;
		HYPRE_IJMatrix ij_matrix;
		HYPRE_ParCSRMatrix parcsr_matrix;
		int nrows;
		int ncols;
		int *rows;
		int *cols;
		double *values;
		int local_size = nele_nD;
		iupper = local_size - 1; //nnode_nD is local_size
		jupper = local_size - 1;
		HYPRE_IJMatrixCreate(comm, ilower, iupper, jlower, jupper, &ij_matrix);
		//HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &ij_matrix);
		HYPRE_IJMatrixSetObjectType(ij_matrix, HYPRE_PARCSR);
		HYPRE_IJMatrixInitialize(ij_matrix);

		nrows = 1;

		//set matrix coefficients
		for (int i = ilower; i <= iupper; i++){
			rowvaluelist = rowlist[i].valuevec();
			rowpositionlist = rowlist[i].positionvec();
			ncols = rowvaluelist.size();
			cols = new int[ncols];
			values = new double[ncols];
			for (int j = 0; j < ncols; j++){
				cols[j] = rowpositionlist[j];
				values[j] = rowvaluelist[j];
			}
			HYPRE_IJMatrixSetValues(ij_matrix, 1, &ncols, &i, cols, values);
		}
		HYPRE_IJMatrixAssemble(ij_matrix);
		HYPRE_IJMatrixGetObject(ij_matrix, (void **)&parcsr_matrix);

		//create RHS and solution
		HYPRE_IJVector b;
		HYPRE_ParVector par_b;
		HYPRE_IJVector x;
		HYPRE_ParVector par_x;
		HYPRE_Solver solver, precond;
		HYPRE_IJVectorCreate(comm, ilower, iupper, &b);
		HYPRE_IJVectorSetObjectType(b, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(b);

		HYPRE_IJVectorCreate(comm, ilower, iupper, &x);
		HYPRE_IJVectorSetObjectType(x, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(x);

		/* Set the rhs values; set the solution to zero */
		double *rhs_values, *x_values;
		rhs_values = new double[local_size];
		x_values = new double[local_size];
		rows = new int[local_size];
		for (int i = 0; i < local_size; i++){
			rhs_values[i] = RHS[i];
			x_values[i] = 0.0;
			rows[i] = i;
		}

		HYPRE_IJVectorSetValues(b, local_size, rows, rhs_values);
		HYPRE_IJVectorSetValues(x, local_size, rows, x_values);
		delete x_values;
		delete rhs_values;
		delete rows;
		HYPRE_IJVectorAssemble(b);
		HYPRE_IJVectorGetObject(b, (void **)&par_b);

		HYPRE_IJVectorAssemble(x);
		HYPRE_IJVectorGetObject(x, (void **)&par_x);
		//HYPRE_IJMatrixPrint(ij_matrix, "IJ.out.A");
		//HYPRE_IJVectorPrint(b, "IJ.out.b");
		/* Choose a solver and solve the system */
		solver_id = 0;
		/* AMG */
		if (solver_id == 0){
			int num_iterations;
			double final_res_norm;
			HYPRE_BoomerAMGCreate(&solver);
			/* Set some parameters (See Reference Manual for more parameters) */
			HYPRE_BoomerAMGSetPrintLevel(solver, 3);  /* print solve info + parameters */
			HYPRE_BoomerAMGSetCoarsenType(solver, 6); /* Falgout coarsening */
			HYPRE_BoomerAMGSetRelaxType(solver, 3);   /* G-S/Jacobi hybrid relaxation */
			HYPRE_BoomerAMGSetNumSweeps(solver, 1);   /* Sweeeps on each level */
			HYPRE_BoomerAMGSetMaxLevels(solver, 20);  /* maximum number of levels */
			HYPRE_BoomerAMGSetTol(solver, 1e-7);      /* conv. tolerance */
			/* Now setup and solve! */
			HYPRE_BoomerAMGSetup(solver, parcsr_matrix, par_b, par_x);
			HYPRE_BoomerAMGSolve(solver, parcsr_matrix, par_b, par_x);
			/* Run info - needed logging turned on */
			HYPRE_BoomerAMGGetNumIterations(solver, &num_iterations);
			HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, &final_res_norm);
			printf("\n");
			printf("Iterations = %d\n", num_iterations);
			printf("Final Relative Residual Norm = %e\n", final_res_norm);
			printf("\n");
			HYPRE_BoomerAMGDestroy(solver);
		}
		int nvalues = local_size;
		rows = new int[nvalues];
		for (int i = 0; i < nvalues; i++) rows[i] = i;
		values = new double[nvalues];
		HYPRE_IJVectorGetValues(x, nvalues, rows, values);
		HYPRE_IJMatrixDestroy(ij_matrix);
		HYPRE_IJVectorDestroy(b);
		HYPRE_IJVectorDestroy(x);

		//obtain P from u and boundary conditions
		for (inD = 0; inD < nele_nD; inD++){
			ie = nD2ele[inD];
			cpgelementlist[ie].temperature=values[inD];
		}
	}
	else{
		"to use HYPRE need setlinearsolver 2";
		std::exit(EXIT_FAILURE);
	}
}
void REGION::computevelocity_cvfe_sf(){ //obtain Ux, Uy, Uz from P as in CG
	int i, ie, inode, nelement, j;
	double dpdx, dpdy, dpdz;
	nelement = elementlist.size();
	for (ie = 0; ie < nelement; ie++){
		dpdx = 0.0;
		dpdy = 0.0;
		dpdz = 0.0;
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			dpdx = dpdx + nodelist[inode].Po()*elementlist[ie].Nb(i);
			dpdy = dpdy + nodelist[inode].Po()*elementlist[ie].Nc(i);
			dpdz = dpdz + nodelist[inode].Po()*elementlist[ie].Nd(i);
		}
		//dpdz = dpdz + rho_o_*g_; //uncomment if absolute P; comment if corrected P
		//velocity considers all components of permeability
		elementlist[ie].Ux_t(-(elementlist[ie].k(0, 0)*dpdx + elementlist[ie].k(0, 1)*dpdy + elementlist[ie].k(0, 2)*dpdz)/mu_o);
		elementlist[ie].Uy_t(-(elementlist[ie].k(1, 0)*dpdx + elementlist[ie].k(1, 1)*dpdy + elementlist[ie].k(1, 2)*dpdz)/mu_o);
		elementlist[ie].Uz_t(-(elementlist[ie].k(2, 0)*dpdx + elementlist[ie].k(2, 1)*dpdy + elementlist[ie].k(2, 2)*dpdz)/mu_o);
	}

	/*std::cout << "approximate nodal velocity from elemental velocity" << std::endl; //only for visualisation in RRMintegrated
	for (ie = 0; ie < nelement; ie++){
		double volume_ = elementlist[ie].volume();
		double ux = elementlist[ie].Ux();
		double uy = elementlist[ie].Uy();
		double uz = elementlist[ie].Uz();
		for (j = 0; j < 4; j++){
			int jnode = elementlist[ie].node(j);
			if (nodelist[jnode].markbc() != 2){
				nodelist[jnode].Ux(ux*volume_*0.25 / nodelist[jnode].dualvolume() + nodelist[jnode].Ux());
				nodelist[jnode].Uy(uy*volume_*0.25 / nodelist[jnode].dualvolume() + nodelist[jnode].Uy());
				nodelist[jnode].Uz(uz*volume_*0.25 / nodelist[jnode].dualvolume() + nodelist[jnode].Uz());
			}
		}
	}*/

	/*check using Darcy's law k=1; mu=1 using weighted average
	ux_average = 0;
	p_average1 = 0;
	p_average2 = 0;


	for (ie = 0; ie < nelement; ie++){
		ux_average = ux_average + elementlist[ie].Ux()*elementlist[ie].volume()/1.0;
	}
	for (ie = 0; ie < bfacetBC[1].size(); ie++){
		j = bfacetBC[1][ie];
		for (i = 0; i < 3; i++){
			inode = bfacetlist[j].node(i);
			p_average1 = p_average1 + nodelist[inode].Po()*std::abs(bfacetlist[j].area()) / 3.0 / 1.0;
		}
	}
	for (ie = 0; ie < bfacetBC[2].size(); ie++){
		j = bfacetBC[2][ie];
		for (i = 0; i < 3; i++){
			inode = bfacetlist[j].node(i);
			p_average2 = p_average2 + nodelist[inode].Po()*std::abs(bfacetlist[j].area()) / 3.0 / 1.0;
		}
	}


	std::cout << "check using Darcy's law" << std::endl;
	std::cout << "Ux average:" << " " << ux_average<<std::endl;
	std::cout << "Darcy's law gives:" << " " << p_average2 - p_average1 << std::endl;
	*/
}

void REGION::computevelocity_cvfe_mf(){ //obtain Ux, Uy, Uz from P as in CG
	std::cout << "compute velocity using CVFE for multiphase" << std::endl;
	int i, ie, inode, nelement, j;
	double dpdx_o, dpdy_o, dpdz_o, vro_, vrw_, rho_o_, rho_w_, vt_;
	nelement = elementlist.size();
	for (ie = 0; ie < nelement; ie++){
		vt_ = elementlist[ie].mobit();
		dpdx_o = 0.0;
		dpdy_o = 0.0;
		dpdz_o = 0.0;
		for (i = 0; i < 4; i++){
			inode = elementlist[ie].node(i);
			dpdx_o = dpdx_o + nodelist[inode].Po()*elementlist[ie].Nb(i);
			dpdy_o = dpdy_o + nodelist[inode].Po()*elementlist[ie].Nc(i);
			dpdz_o = dpdz_o + nodelist[inode].Po()*elementlist[ie].Nd(i);
		}
		//dpdz_o = dpdz_o + rho_o_*g_;//in direction of +z, gravity decrease
		//dpdz_w = dpdz_w + rho_w_*g_;
		//gravity neglected

		//velocity considers all components of permeability
		elementlist[ie].Ux_t(-vt_*(elementlist[ie].k(0, 0) *dpdx_o + elementlist[ie].k(0, 1) *dpdy_o + elementlist[ie].k(0, 2) *dpdz_o));
		elementlist[ie].Uy_t(-vt_*(elementlist[ie].k(1, 0) *dpdx_o + elementlist[ie].k(1, 1) *dpdy_o + elementlist[ie].k(1, 2) *dpdz_o));
		elementlist[ie].Uz_t(-vt_*(elementlist[ie].k(2, 0) *dpdx_o + elementlist[ie].k(2, 1) *dpdy_o + elementlist[ie].k(2, 2) *dpdz_o));

		//elementlist[ie].Ux_w(-vrw_*(elementlist[ie].k(0, 0) *dpdx_o + elementlist[ie].k(0, 1) *dpdy_o + elementlist[ie].k(0, 2) *dpdz_o));
		//elementlist[ie].Uy_w(-vrw_*(elementlist[ie].k(1, 0) *dpdx_o + elementlist[ie].k(1, 1) *dpdy_o + elementlist[ie].k(1, 2) *dpdz_o));
		//elementlist[ie].Uz_w(-vrw_*(elementlist[ie].k(2, 0) *dpdx_o + elementlist[ie].k(2, 1) *dpdy_o + elementlist[ie].k(2, 2) *dpdz_o));
	}

}

void REGION::computevelocity_cvfe_w(){
	int nelement = elementlist.size();
	for (int ie = 0; ie < nelement; ie++){
		double frac_ = elementlist[ie].fracw();
		elementlist[ie].Ux_w(frac_*elementlist[ie].Ux_t());
		elementlist[ie].Uy_w(frac_*elementlist[ie].Uy_t());
		elementlist[ie].Uz_w(frac_*elementlist[ie].Uz_t());
	}
}

void REGION::upscaling(int dir_, char* result){
	//upscaling use unstructured mesh
	int ie;
	double u_, tv_, tm_, p1, p2, length_;
	//calculate average velocity in x direction
	u_ = 0;
	tv_ = 0;
	p1 = 100 * 100000;
	p2 = 0;
	if (dir_ == 1){
		for (ie = 0; ie < elementlist.size(); ie++){
			u_ = u_ + elementlist[ie].Ux_t()*elementlist[ie].volume();
			tv_ = tv_ + elementlist[ie].volume();
		}
		u_ = u_ / tv_; //average ux_
		length_ = meshinfo.xmaxav() - meshinfo.xminav(); //same as xmax in rrm as boundaries are straight
		tm_ = u_*length_ / (p1 - p2)*1.0e-3 / 0.987e-15;
	}
	else if (dir_ == 2){
		for (ie = 0; ie < elementlist.size(); ie++){
			u_ = u_ + elementlist[ie].Uy_t()*elementlist[ie].volume();
			tv_ = tv_ + elementlist[ie].volume();
		}
		u_ = u_ / tv_; //average ux_
		length_ = meshinfo.ymaxav() - meshinfo.yminav();
		tm_ = u_*length_ / (p1 - p2)*1.0e-3 / 0.987e-15;
	}
	else if (dir_ == 3){
		for (ie = 0; ie < elementlist.size(); ie++){
			u_ = u_ + elementlist[ie].Uz_t()*elementlist[ie].volume();
			tv_ = tv_ + elementlist[ie].volume();
		}
		u_ = u_ / tv_; //average ux_
		length_ = meshinfo.zmaxav() - meshinfo.zminav();
		tm_ = u_*length_ / (p1 - p2)*1.0e-3 / 0.987e-15;
	}

}

void REGION::inoutflowb(){
	int i;
	FLOWB flowb_;
	inflowblist.clear();
	outflowblist.clear();
	for (i = 0; i < welllist.size(); i++){//check all wells
		flowb_.type(1); //from a well
		flowb_.mark(i); //mark which well
		if (welllist[i].sign() == 1){ //injector well
			inflowblist.push_back(flowb_);
		}
		else if (welllist[i].sign() == -1){
			outflowblist.push_back(flowb_);
		}
	}
	if (meshinfo.type() == 1){ //only for unstructured mesh
		for (i = 0; i < bsurfacelist.size(); i++){//check all boundary faces
			flowb_.type(2); //from a bsurface
			flowb_.mark(i); //mark which bsurface
			if (bsurfacelist[i].sign() == 1){//injector bsurface
				inflowblist.push_back(flowb_);
			}
			else if (bsurfacelist[i].sign() == -1){
				outflowblist.push_back(flowb_);
			}
		}
	}

}

void REGION::inoutflowb_wells(std::vector<int> injectors, std::vector<int> producers){
	int i;
	FLOWB flowb_;
	inflowblist.clear();
	outflowblist.clear();
	for (i = 0; i < injectors.size(); i++){//check all wells
		flowb_.type(1); //from a well
		flowb_.mark(injectors[i]); //mark which well
		inflowblist.push_back(flowb_);
	}
	for (i = 0; i < producers.size(); i++){//check all wells
		flowb_.type(1); //from a well
		flowb_.mark(producers[i]); //mark which well
		outflowblist.push_back(flowb_);
	}
}


void REGION::tracingboundarycondition(){
	//local variables
	int i, j, ie, inode, marktracingb_, mark_, nnode, nelement;
	nnode = nodelist.size();
	nelement = elementlist.size();
	tracingblist.clear();

	if (graph.tracingsign() == 1){//forward Tracer
		tracingblist = inflowblist;
	}
	else if (graph.tracingsign() == -1){
		tracingblist = outflowblist;
	}
	graph.ntracingb(tracingblist.size());

	//mark tracer boundary nodes
	for (i = 0; i < nnode; i++){ //clear marktofb first
		nodelist[i].marktracingb(-1);
	}

	for (i = 0; i < graph.ntracingb(); i++){
		mark_ = tracingblist[i].mark();
		if (tracingblist[i].type() == 1){
			for (j = 0; j < nnode; j++){
				if (nodelist[j].markwell() == mark_){
					nodelist[j].marktracingb(i);
				}
			}
		}
		if (tracingblist[i].type() == 2){
			for (j = 0; j < nnode; j++){
				if (nodelist[j].markbsurface() == mark_){
					nodelist[j].marktracingb(i);
				}
			}
		}
	}

}

void REGION::tracingbset(){
	graphbnodeset.clear();
	for (int inode = 0; inode < nodelist.size(); inode++){
		nodelist[inode].cleargraphinfo();
		if (nodelist[inode].marktracingb() >= 0){
			graphbnodeset.push_back(inode);
		}
	}
}

void REGION::tracerboundarycondition_cpgfv(){
	int i, mark_, ie, nelement;
	nelement = cpgelementlist.size();
	tracingblist.clear();
	if (graph.tracingsign() == 1){//forward Tracer
		tracingblist = inflowblist;
	}
	else if (graph.tracingsign() == -1){
		tracingblist = outflowblist;
	}
	graph.ntracingb(tracingblist.size());

	for (i = 0; i < nelement; i++){
		cpgelementlist[i].marktracingb(-1); //clear marktracingb first
	}

	//mark tracer boundary cpg elements
	for (i = 0; i < graph.ntracingb(); i++){
		mark_ = tracingblist[i].mark();
		if (tracingblist[i].type() != 1){
			std::cout << "cpg element tracer boundary type not 1 (press)" << std::endl;
			std::exit(EXIT_FAILURE);
		}
		for (ie = 0; ie < cpgelementlist.size(); ie++){
			if (cpgelementlist[ie].markwell() == mark_){
				cpgelementlist[ie].marktracingb(i);
			}
		}
	}
}



void REGION::integratecycles(int i){
	std::vector<int> cyclenumbers = graphnodelist[i].markcycle();
	int j, k, knode, n, jcycle;
	for (j = 0; j < cyclenumbers.size(); j++){
		jcycle = cyclenumbers[j];
		for (k = 0; k < cycles[jcycle].size(); k++){
			knode = cycles[jcycle][k];
			bigcycle.push_back(knode);
			n = graphnodelist[knode].markcycle().size();
			if (n>0){
				integratecycles(knode);
			}
		}
	}
}

void REGION::integratecyclesnode(int i){
	std::vector<int> cyclenumbers = nodelist[i].markcycle();
	int j, k, knode, n, jcycle;
	for (j = 0; j < cyclenumbers.size(); j++){
		jcycle = cyclenumbers[j];
		for (k = 0; k < cycles[jcycle].size(); k++){
			knode = cycles[jcycle][k];
			if (markinside[knode] == 0){
				markinside[knode] = 1;
				bigcycle.push_back(knode);
				n = nodelist[knode].markcycle().size();
				if (n>0){
					integratecyclesnode(knode);
				}
			}
		}
	}
}

double REGION::distancebetweenelements(int ie, int je){
	int n1, n2, n3, n4, m1, m2, m3, m4;
	double d, x1, y1, z1, x2, y2, z2;
	n1 = elementlist[ie].node(0);
	n2 = elementlist[ie].node(1);
	n3 = elementlist[ie].node(2);
	n4 = elementlist[ie].node(3);
	m1 = elementlist[je].node(0);
	m2 = elementlist[je].node(1);
	m3 = elementlist[je].node(2);
	m4 = elementlist[je].node(3);
	x1 = 0.25*(nodelist[n1].x() + nodelist[n2].x() + nodelist[n3].x() + nodelist[n4].x());
	y1 = 0.25*(nodelist[n1].y() + nodelist[n2].y() + nodelist[n3].y() + nodelist[n4].y());
	z1 = 0.25*(nodelist[n1].z() + nodelist[n2].z() + nodelist[n3].z() + nodelist[n4].z());
	x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x());
	y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
	z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
	d = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) + (z2 - z1)*(z2 - z1));
	return(d);
}

double REGION::distance2elementface(int ie, int iface){
	int n1, n2, n3, n4, m1, m2, m3;
	double d, x1, y1, z1, x2, y2, z2;
	n1 = elementlist[ie].node(0);
	n2 = elementlist[ie].node(1);
	n3 = elementlist[ie].node(2);
	n4 = elementlist[ie].node(3);
	m1 = elementlist[ie].face(iface, 0);
	m2 = elementlist[ie].face(iface, 1);
	m3 = elementlist[ie].face(iface, 2);

	x1 = 0.25*(nodelist[n1].x() + nodelist[n2].x() + nodelist[n3].x() + nodelist[n4].x());
	y1 = 0.25*(nodelist[n1].y() + nodelist[n2].y() + nodelist[n3].y() + nodelist[n4].y());
	z1 = 0.25*(nodelist[n1].z() + nodelist[n2].z() + nodelist[n3].z() + nodelist[n4].z());
	x2 =(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x())/3.0;
	y2 =(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y())/3.0;
	z2 =(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z())/3.0;
	d = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) + (z2 - z1)*(z2 - z1));
	return(d);
}

void REGION::updatenodalSw_flux(double dt){
	for (int inode = 0; inode < nodelist.size(); inode++){
		if (nodelist[inode].markbc_Sw() != 1){
			double tfluxsw_ = 0;
			double tfluxin_ = 0;
			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
				int jed = nodelist[inode].edgeinflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double influx_ = -std::abs(edgelist[jed].flux());
				tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].fracw();
				tfluxin_ = tfluxin_ + influx_;
			}
			double tfluxout_ = -tfluxin_;
			tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].fracw();

			double vol = nodelist[inode].dualvolume();
			double poro = nodelist[inode].porosity();
			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
		}
	}
}

void REGION::updatenodalSw_flux_monotone(double dt){
	int cno = 0;
	std::vector<int> avlist, avlist2, mark;
	mark.resize(nodelist.size());
	for (int inode = 0; inode < nodelist.size(); inode++){
		if (nodelist[inode].markbc_Sw() != 1){
			double tfluxsw_ = 0;
			double tfluxin_ = 0;
			if (nodelist[inode].numberofedgeinflux() == 0){
				cno++;
				avlist.push_back(inode);
			}
			else{
				for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
					int jed = nodelist[inode].edgeinflux(j);
					int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
					double influx_ = -std::abs(edgelist[jed].flux());
					tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].fracw();
					tfluxin_ = tfluxin_ + influx_;
				}

				double tfluxout_ = -tfluxin_;
				tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].fracw();

				double vol = nodelist[inode].dualvolume();
				double poro = nodelist[inode].porosity();
				double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
				nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
				mark[inode] = 1;
			}
		}
	}
	std::cout << "number of noinflux nodes " << cno << std::endl;
	while (avlist.size() > 0){
		avlist2.clear();
		for (int i = 0; i < avlist.size(); i++){
			int inode = avlist[i];
			int cc = 0;
			double sw = 0;
			for (int j = 0; j < nodelist[inode].numberofedgein(); j++){
				int jed = nodelist[inode].edgein(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				if (mark[jnode] == 1){
					sw = sw + nodelist[jnode].Sw();
					cc++;
				}
			}
			for (int j = 0; j < nodelist[inode].numberofedgeout(); j++){
				int jed = nodelist[inode].edgeout(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				if (mark[jnode] == 1){
					sw = sw + nodelist[jnode].Sw();
					cc++;
				}
			}
			if (cc>0){
				sw = sw / cc;
				nodelist[inode].Sw(sw);
				mark[inode] = 1;
			}
			else{
				avlist2.push_back(inode);
			}
		}
		avlist.clear();
		avlist = avlist2;
		avlist2.clear();
		std::cout << "avlist2 size " << avlist2.size() << std::endl;
	}

}

void REGION::updatenodalSw_fluxw(double dt){
	for (int inode = 0; inode < nodelist.size(); inode++){
		if (nodelist[inode].markbc_Sw() != 1){
			double tfluxsw_ = 0;
			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
				int jed = nodelist[inode].edgeinflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double influx_ = -std::abs(edgelist[jed].flux());
				tfluxsw_ = tfluxsw_ + influx_;
			}
			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){//not efficient
				int jed = nodelist[inode].edgeoutflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				tfluxsw_ = tfluxsw_ + std::abs(edgelist[jed].flux());
			}
			double vol = nodelist[inode].dualvolume();
			double poro = nodelist[inode].porosity();
			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
		}
	}
}

void REGION::updatenodalSw_fluxw_edgesdirect(double dt){

	for (int ied = 0; ied < nedge; ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		double fluxn2_ = edgelist[ied].flux();
		double fluxn1_ = -edgelist[ied].flux();

		double vol = nodelist[n1].dualvolume();
		double poro = nodelist[n1].porosity();
		double term1_ = dt / vol / poro*fluxn1_;
		if (nodelist[n1].markbc_Sw() != 1){
			nodelist[n1].Sw(nodelist[n1].Sw() + term1_);
		}

		vol = nodelist[n2].dualvolume();
		poro = nodelist[n2].porosity();
		term1_ = dt / vol / poro*fluxn2_;
		if (nodelist[n2].markbc_Sw() != 1){
			nodelist[n2].Sw(nodelist[n2].Sw() + term1_);
		}

	}

}

void REGION::updatenodalSw_fluxhybrid(double dt){

	for (int ied = 0; ied < nedge; ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		double fluxn2_ = edgelist[ied].fluxh()*0.5;
		double fluxn1_ = -edgelist[ied].fluxh()*0.5;
		if (edgelist[ied].flux() > 0){
			fluxn2_ = fluxn2_ + edgelist[ied].flux()*nodelist[n1].fracw()*0.5;
			fluxn1_ = fluxn1_ - edgelist[ied].flux()*nodelist[n1].fracw()*0.5;
		}
		else if (edgelist[ied].flux() < 0){
			fluxn2_ = fluxn2_ + edgelist[ied].flux()*nodelist[n2].fracw()*0.5;
			fluxn1_ = fluxn1_ - edgelist[ied].flux()*nodelist[n2].fracw()*0.5;
		}
		double vol = nodelist[n1].dualvolume();
		double poro = nodelist[n1].porosity();
		double term1_ = dt / vol / poro*fluxn1_;
		if (nodelist[n1].markbc_Sw() != 1){
			nodelist[n1].Sw(nodelist[n1].Sw() + term1_);
		}

		vol = nodelist[n2].dualvolume();
		poro = nodelist[n2].porosity();
		term1_ = dt / vol / poro*fluxn2_;
		if (nodelist[n2].markbc_Sw() != 1){
			nodelist[n2].Sw(nodelist[n2].Sw() + term1_);
		}

	}

}

//void REGION::updatenodalSw_cvfe_directTVD(double dt){
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double tfluxsw_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeincvfeflux(); j++){
//				int jed = nodelist[inode].edgeincvfeflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				tfluxsw_ = tfluxsw_ - std::abs(edgelist[jed].fluxcvfe())*nodelist[jnode].mobiw() / nodelist[jnode].mobit();
//			}
//			for (int j = 0; j < nodelist[inode].numberofedgeoutcvfeflux(); j++){
//				int jed = nodelist[inode].edgeoutcvfeflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				tfluxsw_ = tfluxsw_ + std::abs(edgelist[jed].fluxcvfe())*nodelist[inode].mobiw() / nodelist[inode].mobit();
//			}
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//
//			//direct TVD
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			//Swmax = 1; //physical bound
//			//Swmin = 0;
//			if (nodelist[inode].Sw() > Swmax){
//				nodelist[inode].Sw(Swmax);
//			}
//			if (nodelist[inode].Sw() < Swmin){
//				nodelist[inode].Sw(Swmin);
//			}
//		}
//	}
//}

//void REGION::updatenodalSw_cvfe_directTVD_w(double dt){
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double tfluxsw_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeincvfeflux(); j++){
//				int jed = nodelist[inode].edgeincvfeflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				tfluxsw_ = tfluxsw_ - std::abs(edgelist[jed].fluxcvfe());
//			}
//			for (int j = 0; j < nodelist[inode].numberofedgeoutcvfeflux(); j++){
//				int jed = nodelist[inode].edgeoutcvfeflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				tfluxsw_ = tfluxsw_ + std::abs(edgelist[jed].fluxcvfe());
//			}
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//
//			//direct TVD
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			//Swmax = 1; //physical bound 01
//			//Swmin = 0;
//			if (nodelist[inode].Sw() > Swmax){
//				nodelist[inode].Sw(Swmax);
//			}
//			if (nodelist[inode].Sw() < Swmin){
//				nodelist[inode].Sw(Swmin);
//			}
//		}
//	}
//}


void REGION::updatecpgeleSw_tpfa_car(double dt){
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		double car_dx = cpgelementlist[ie].dx;
		double car_dy = cpgelementlist[ie].dy;
		double car_dz = cpgelementlist[ie].dz;
		if (cpgelementlist[ie].markbc_Sw() != 1){
			double tfluxsw_ = 0;
			double tfluxin_ = 0;
			double pi = cpgelementlist[ie].Po();
			for (int i = 0; i < 6; i++){
				int je = cpgelementlist[ie].neighbor(i);
				if (je >= 0){
					double pj = cpgelementlist[je].Po();
					if (pj>pi){
						double fluxin_ = (pi - pj)*cpgelementlist[ie].trans(i);
						tfluxin_ += fluxin_;
						tfluxsw_ += fluxin_*cpgelementlist[je].mobiw() / cpgelementlist[je].mobit();
					}
				}
			}
			double tfluxout_ = -tfluxin_;
			tfluxsw_ += tfluxout_*cpgelementlist[ie].mobiw() / cpgelementlist[ie].mobit();
			double poro_ = cpgelementlist[ie].porosity();
			double term_ = -dt / poro_*tfluxsw_/(car_dx*car_dy*car_dz);
			cpgelementlist[ie].Sw(cpgelementlist[ie].Sw() + term_);//source=0
		}
	}
}

void REGION::setlimitc(double d){
	limit_c = d;
}

void REGION::setlimitg(double d){
	limit_g = d;
}

int REGION::checksteadystate(){
	int flag = 1;
	for (int inode = 0; inode < nodelist.size(); inode++){
		if (std::abs(nodelist[inode].Sw() - oldnodalSw[inode])>1e-2){
			flag = 0;
		}
	}
	return flag;
}

void REGION::partitionSwshare_5spot_car(){
	double ts1, ts2, ts3, ts4;
	ts1 = 0; ts2 = 0; ts3 = 0; ts4 = 0;
	double xmid = 500;
	double ymid = 500;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		int i1 = cpgelementlist[ie].node(0);
		int i2 = cpgelementlist[ie].node(1);
		int i3 = cpgelementlist[ie].node(2);
		int i4 = cpgelementlist[ie].node(3);
		double x1 = nodelist[i1].x(); double y1 = nodelist[i1].y();
		double x2 = nodelist[i2].x(); double y2 = nodelist[i2].y();
		double x3 = nodelist[i3].x(); double y3 = nodelist[i3].y();
		double x4 = nodelist[i4].x(); double y4 = nodelist[i4].y();
		if (x1 < xmid && x2 < xmid && x3 < xmid && x4 < xmid){
			if (y1 < ymid && y2 < ymid && y3 < ymid && y4 < ymid){
				ts1 += cpgelementlist[ie].Sw();
			}
			else if (y1 > ymid && y2 > ymid && y3 > ymid && y4 > ymid){
				ts4 += cpgelementlist[ie].Sw();
			}
		}
		else if (x1 > xmid && x2 > xmid && x3 > xmid && x4 > xmid){
			if (y1 < ymid && y2 < ymid && y3 < ymid && y4 < ymid){
				ts2 += cpgelementlist[ie].Sw();
			}
			else if (y1 > ymid && y2 > ymid && y3 > ymid && y4 > ymid){
				ts3 += cpgelementlist[ie].Sw();
			}
		}
	}
	double ts = ts1 + ts2 + ts3 + ts4;
	ts1 = ts1 / ts;
	ts2 = ts2 / ts;
	ts3 = ts3 / ts;
	ts4 = ts4 / ts;
	std::cout << "Sw shares: " << ts1 << " " << ts2 << " " << ts3 << " " << ts4 << std::endl;
}

void REGION::partitionSwshare_5spot(){
	double ts1, ts2, ts3, ts4;
	ts1 = 0; ts2 = 0; ts3 = 0; ts4 = 0;
	double xmid = 500;
	double ymid = 500;
	for (int i = 0; i < nodelist.size(); i++){
		double x = nodelist[i].x();
		double y = nodelist[i].y();
		if (x < xmid){
			if (y < ymid){
				ts1 += nodelist[i].Sw()*nodelist[i].dualvolume();
			}
			else if (y>ymid){
				ts4 += nodelist[i].Sw()*nodelist[i].dualvolume();
			}
		}
		else if (x>xmid){
			if (y < ymid){
				ts2 += nodelist[i].Sw()*nodelist[i].dualvolume();
			}
			else if (y>ymid){
				ts3 += nodelist[i].Sw()*nodelist[i].dualvolume();
			}
		}
	}
	double ts = ts1 + ts2 + ts3 + ts4;
	ts1 = ts1 / ts;
	ts2 = ts2 / ts;
	ts3 = ts3 / ts;
	ts4 = ts4 / ts;
	std::cout << "Sw shares: " << ts1 << " " << ts2 << " " << ts3 << " " << ts4 << std::endl;

}


double REGION::totalSw_car(){//for car all elements have same volume
	double ts = 0;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		ts += cpgelementlist[ie].Sw();
	}
	ts = ts / cpgelementlist.size();
	return ts;
}

double REGION::totalSw(){
	double ts = 0;
	double tv = 0;
	for (int inode = 0; inode < nodelist.size(); inode++){
		ts += nodelist[inode].Sw()*nodelist[inode].dualvolume()*nodelist[inode].porosity();
		tv += nodelist[inode].dualvolume()*nodelist[inode].porosity();
	}
	ts = ts / tv;
	return ts;
}

void REGION::computenodalSwbound(){
	nodalSwmax.resize(nodelist.size());
	nodalSwmin.resize(nodelist.size());
	for (int inode = 0; inode < nodelist.size(); inode++){
		double Swmax = nodelist[inode].Sw();
		double Swmin = nodelist[inode].Sw();
		for (int ied = 0; ied < nodelist[inode].numberofedgein(); ied++){
			int iedge = nodelist[inode].edgein(ied);
			int jnode = edgelist[iedge].node(0) + edgelist[iedge].node(1)-inode;
			if (nodelist[jnode].Sw() > Swmax){
				Swmax = nodelist[jnode].Sw();
			}
			if (nodelist[jnode].Sw() < Swmin){
				Swmin = nodelist[jnode].Sw();
			}
		}
		for (int ied = 0; ied < nodelist[inode].numberofedgeout(); ied++){
			int iedge = nodelist[inode].edgeout(ied);
			int jnode = edgelist[iedge].node(0) + edgelist[iedge].node(1) - inode;
			if (nodelist[jnode].Sw() > Swmax){
				Swmax = nodelist[jnode].Sw();
			}
			if (nodelist[jnode].Sw() < Swmin){
				Swmin = nodelist[jnode].Sw();
			}
		}
		if (Swmax>1){
			Swmax = 1;
		}
		if (Swmin < 0){
			Swmin = 0;
		}
		nodalSwmax[inode] = Swmax;
		nodalSwmin[inode] = Swmin;
	}
}

void REGION::computenodalSwbound_pguide(){
	nodalSwmax.resize(nodelist.size());
	nodalSwmin.resize(nodelist.size());
	for (int inode = 0; inode < nodelist.size(); inode++){
		double Swmax = nodelist[inode].Sw();
		double Swmin = nodelist[inode].Sw();
		for (int ied = 0; ied < nodelist[inode].numberofedgein(); ied++){
			int iedge = nodelist[inode].edgein(ied);
			int jnode = edgelist[iedge].node(0) + edgelist[iedge].node(1) - inode;
			if (nodelist[jnode].Sw() > Swmax && nodelist[jnode].Po()>nodelist[inode].Po()){
				Swmax = nodelist[jnode].Sw();
			}
			if (nodelist[jnode].Sw() < Swmin && nodelist[jnode].Po()<nodelist[inode].Po()){
				Swmin = nodelist[jnode].Sw();
			}
		}
		for (int ied = 0; ied < nodelist[inode].numberofedgeout(); ied++){
			int iedge = nodelist[inode].edgeout(ied);
			int jnode = edgelist[iedge].node(0) + edgelist[iedge].node(1) - inode;
			if (nodelist[jnode].Sw() > Swmax && nodelist[jnode].Po()>nodelist[inode].Po()){
				Swmax = nodelist[jnode].Sw();
			}
			if (nodelist[jnode].Sw() < Swmin && nodelist[jnode].Po()<nodelist[inode].Po()){
				Swmin = nodelist[jnode].Sw();
			}
		}
		nodalSwmax[inode] = Swmax;
		nodalSwmin[inode] = Swmin;
	}
}

void REGION::updatenodalSw_antiflux_kuzminlimit(double dt){
	nodecvalue.resize(nodelist.size());
	double cmin = 1.0; //cmin not useful here
	for (int inode = 0; inode < nodelist.size(); inode++){
		if (nodelist[inode].markbc_Sw() != 1){
			double vol = nodelist[inode].dualvolume();
			double poro = nodelist[inode].porosity();
			double Swmax = nodalSwmax[inode];
			double Swmin = nodalSwmin[inode];
			double qp = Swmax - nodelist[inode].Sw(); //allowable positive increment
			double qn = Swmin - nodelist[inode].Sw();
			double tfluxin_ = 0;
			double tflux_incrsw = 0;
			double tflux_decrsw = 0;
			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
				int jed = nodelist[inode].edgeinflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double influx_ = -std::abs(cmin*edgelist[jed].fluxc());
				tflux_incrsw = tflux_incrsw + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*dt / vol / poro;
				tfluxin_ = tfluxin_ + influx_;
			}
			double tfluxout_ = -tfluxin_;
			tflux_decrsw = tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*dt / vol / poro;
			double ccp, ccn;
			if (tflux_incrsw == 0){
				ccp = 1;
			}
			else{
				ccp = std::abs(qp / tflux_incrsw);
			}
			if (ccp > 1){
				ccp = 1;
			}
			if (tflux_decrsw == 0){
				ccn = 1;
			}
			else{
				ccn = std::abs(qn / tflux_decrsw);
			}
			if (ccn > 1){
				ccn = 1;
			}
			double tfluxsw_ = 0;
			double cc = min(ccp, ccn)*limit_c;
			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
				int jed = nodelist[inode].edgeinflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double influx_ = -std::abs(cmin*edgelist[jed].fluxc());
				tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*cc;
			}
			//for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
			//	int jed = nodelist[inode].edgeoutflux(j);
			//	int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
			//	double outflux_ = std::abs(cmin*edgelist[jed].fluxcvfe() + (1 - cmin)*edgelist[jed].fluxtpfa());
			//	//tfluxsw_ = tfluxsw_ + outflux_*nodelist[inode].mobiw() / nodelist[inode].mobit();
			//
			//}
			tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*cc;

			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
			/*if (nodelist[inode].Sw() > 1 || nodelist[inode].Sw() < 0){
			std::cout << inode << " "<<nodelist[inode].Sw()<<std::endl;
			std::exit(EXIT_FAILURE);
			}*/

		}
	}
}

void REGION::updatenodalSw_antiflux_kuzminlimit_w(double dt){
	nodecvalue.resize(nodelist.size());
	for (int inode = 0; inode < nodelist.size(); inode++){
		if (nodelist[inode].markbc_Sw() != 1){
			double vol = nodelist[inode].dualvolume();
			double poro = nodelist[inode].porosity();
			double Swmax = nodalSwmax[inode];
			double Swmin = nodalSwmin[inode];
			double qp = Swmax - nodelist[inode].Sw(); //allowable positive increment
			double qn = Swmin - nodelist[inode].Sw();
			double tflux_incrsw = 0;
			double tflux_decrsw = 0;
			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
				int jed = nodelist[inode].edgeinflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double influx_ = -std::abs(edgelist[jed].fluxc());
				tflux_incrsw = tflux_incrsw + influx_*dt / vol / poro;
			}
			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
				int jed = nodelist[inode].edgeoutflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double outflux_ = std::abs(edgelist[jed].fluxc());
				tflux_decrsw = tflux_decrsw + outflux_*dt / vol / poro;

			}

			double ccp, ccn;
			if (tflux_incrsw == 0){
				ccp = 1;
			}
			else{
				ccp = std::abs(qp / tflux_incrsw);
			}
			if (ccp > 1){
				ccp = 1;
			}
			if (tflux_decrsw == 0){
				ccn = 1;
			}
			else{
				ccn = std::abs(qn / tflux_decrsw);
			}
			if (ccn > 1){
				ccn = 1;
			}
			double tfluxsw_ = 0;
			double cc = min(ccp, ccn)*limit_c;
			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
				int jed = nodelist[inode].edgeinflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double influx_ = -std::abs(edgelist[jed].fluxc());
				tfluxsw_ = tfluxsw_ + influx_*cc;
			}
			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
				int jed = nodelist[inode].edgeoutflux(j);
				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
				double outflux_ = std::abs(edgelist[jed].fluxc());
				tfluxsw_ = tfluxsw_ + outflux_*cc;
			}
			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
			/*if (nodelist[inode].Sw() > 1 || nodelist[inode].Sw() < 0){
				std::cout << inode << " " << nodelist[inode].Sw() << std::endl;
				std::exit(EXIT_FAILURE);
			}*/
		}
	}
}

//void REGION::updatenodalSw_antiflux_ec(double dt){
//	nodecvalue.resize(nodelist.size());
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			double qp = Swmax - nodelist[inode].Sw(); //allowable positive increment
//			double qn = Swmin - nodelist[inode].Sw();
//			double tflux_incrsw = 0;
//			double tflux_decrsw = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(edgelist[jed].fluxfct());
//				tflux_incrsw = tflux_incrsw + influx_*dt / vol / poro;
//			}
//			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//				int jed = nodelist[inode].edgeoutflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double outflux_ = std::abs(edgelist[jed].fluxfct());
//				tflux_decrsw = tflux_decrsw + outflux_*dt / vol / poro;
//
//			}
//
//			double ccp, ccn;
//			if (tflux_incrsw == 0){
//				ccp = 1;
//			}
//			else{
//				ccp = std::abs(qp / tflux_incrsw);
//			}
//			if (ccp > 1){
//				ccp = 1;
//			}
//			if (tflux_decrsw == 0){
//				ccn = 1;
//			}
//			else{
//				ccn = std::abs(qn / tflux_decrsw);
//			}
//			if (ccn > 1){
//				ccn = 1;
//			}
//			double tfluxsw_ = 0;
//			double cc = min(ccp, ccn)*limit_c;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + influx_*cc;
//			}
//			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//				int jed = nodelist[inode].edgeoutflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double outflux_ = std::abs(edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + outflux_*cc;
//			}
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//			/*if (nodelist[inode].Sw() > 1 || nodelist[inode].Sw() < 0){
//			std::cout << inode << " "<<nodelist[inode].Sw()<<std::endl;
//			std::exit(EXIT_FAILURE);
//			}*/
//
//		}
//	}
//}
//void REGION::updatenodalSw_antiflux_kuzminlimit2(double dt){
//	nodecvalue.resize(nodelist.size());
//	double cmin = limit_g; //cmin not useful here
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			double qp = Swmax - nodelist[inode].Sw(); //allowable positive increment
//			double qn = Swmin - nodelist[inode].Sw();
//			double tfluxin_ = 0;
//			double tflux_incrsw = 0;
//			double tflux_decrsw = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(cmin*edgelist[jed].fluxfct());
//				tflux_incrsw = tflux_incrsw + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*dt/vol/poro;
//				tfluxin_ = tfluxin_ + influx_;
//			}
//			double tfluxout_ = -tfluxin_;
//			tflux_decrsw = tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*dt / vol / poro;
//			double tfluxsw_ = tflux_incrsw + tflux_decrsw;
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			double csw = nodelist[inode].Sw() + term1_;
//			if (csw<=Swmax && csw>=Swmin){
//				nodelist[inode].Sw(csw);
//			}
//			else{
//				double ccp, ccn;
//				if (tflux_incrsw == 0){
//					ccp = 1;
//				}
//				else{
//					ccp = std::abs(qp / tflux_incrsw);
//				}
//				if (ccp > 1){
//					ccp = 1;
//				}
//				if (tflux_decrsw == 0){
//					ccn = 1;
//				}
//				else{
//					ccn = std::abs(qn / tflux_decrsw);
//				}
//				if (ccn > 1){
//					ccn = 1;
//				}
//				double tfluxsw_ = 0;
//				double cc = min(ccp, ccn)*limit_c;
//				for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//					int jed = nodelist[inode].edgeinflux(j);
//					int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//					double influx_ = -std::abs(cmin*edgelist[jed].fluxfct());
//					tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*cc;
//				}
//				//for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//				//	int jed = nodelist[inode].edgeoutflux(j);
//				//	int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				//	double outflux_ = std::abs(cmin*edgelist[jed].fluxcvfe() + (1 - cmin)*edgelist[jed].fluxtpfa());
//				//	//tfluxsw_ = tfluxsw_ + outflux_*nodelist[inode].mobiw() / nodelist[inode].mobit();
//				//
//				//}
//				tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*cc;
//
//				term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//
//				nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//				/*if (nodelist[inode].Sw() > 1 || nodelist[inode].Sw() < 0){
//				std::cout << inode << " "<<nodelist[inode].Sw()<<std::endl;
//				std::exit(EXIT_FAILURE);
//				}*/
//			}
//		}
//	}
//}
//
//void REGION::updatenodalSw_antiflux_kuzminlimit3(double dt){
//	nodecvalue.resize(nodelist.size());
//
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			double qp = Swmax - nodelist[inode].Sw(); //allowable positive increment
//			double qn = Swmin - nodelist[inode].Sw();
//			double tfluxin_ = 0;
//			double tflux_incrsw = 0;
//			double tflux_decrsw = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(edgelist[jed].fluxfct());
//				tflux_incrsw = tflux_incrsw + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*dt / vol / poro;
//				tfluxin_ = tfluxin_ + influx_;
//			}
//			double tfluxout_ = -tfluxin_;
//			tflux_decrsw = tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*dt / vol / poro;
//			double tfluxsw_ = tflux_incrsw + tflux_decrsw;
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			double csw = nodelist[inode].Sw() + term1_;
//			if (csw <= Swmax && csw >= Swmin){
//				nodelist[inode].Sw(nodelist[inode].Sw() + term1_*limit_g);
//			}
//			else{
//				double ccp, ccn;
//				if (tflux_incrsw == 0){
//					ccp = 1;
//				}
//				else{
//					ccp = std::abs(qp / tflux_incrsw);
//				}
//				if (ccp > 1){
//					ccp = 1;
//				}
//				if (tflux_decrsw == 0){
//					ccn = 1;
//				}
//				else{
//					ccn = std::abs(qn / tflux_decrsw);
//				}
//				if (ccn > 1){
//					ccn = 1;
//				}
//				double tfluxsw_ = 0;
//				double cc = min(ccp, ccn)*limit_c;
//				for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//					int jed = nodelist[inode].edgeinflux(j);
//					int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//					double influx_ = -std::abs(edgelist[jed].fluxfct());
//					tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*cc;
//				}
//				//for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//				//	int jed = nodelist[inode].edgeoutflux(j);
//				//	int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				//	double outflux_ = std::abs(cmin*edgelist[jed].fluxcvfe() + (1 - cmin)*edgelist[jed].fluxtpfa());
//				//	//tfluxsw_ = tfluxsw_ + outflux_*nodelist[inode].mobiw() / nodelist[inode].mobit();
//				//
//				//}
//				tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*cc;
//
//				term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//
//				nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//				/*if (nodelist[inode].Sw() > 1 || nodelist[inode].Sw() < 0){
//				std::cout << inode << " "<<nodelist[inode].Sw()<<std::endl;
//				std::exit(EXIT_FAILURE);
//				}*/
//			}
//		}
//	}
//}
//
//void REGION::updatenodalSw_antiflux_boundlimit(double dt){
//	nodecvalue.resize(nodelist.size());
//
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double cmin = 0.5;
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			double qp = Swmax - nodelist[inode].Sw(); //allowable positive increment
//			double qn = Swmin - nodelist[inode].Sw();
//			double tfluxin_ = 0;
//			double tfluxsw_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*dt / vol / poro;
//				tfluxin_ = tfluxin_ + influx_;
//			}
//			double tfluxout_ = -tfluxin_;
//			tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*dt / vol / poro;
//			double ccp;
//			if (tfluxsw_ == 0){
//				ccp = 1;
//			}
//			else{
//				if (tfluxsw_ < 0){
//					ccp = std::abs(qp / tfluxsw_);
//				}
//				if (tfluxsw_ > 0){
//					ccp = std::abs(qn / tfluxsw_);
//				}
//				if (ccp > 1){
//					ccp = 1;
//				}
//			}
//			ccp = ccp *limit_c; //if 1 then same as directbound
//			tfluxsw_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit()*ccp;
//			}
//			tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit()*ccp;
//
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//		}
//	}
//}
//
//void REGION::updatenodalSw_antiflux_boundlimit_w(double dt){
//	nodecvalue.resize(nodelist.size());
//
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double cmin = 0.5;
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			double qp = Swmax - nodelist[inode].Sw(); //allowable positive increment
//			double qn = Swmin - nodelist[inode].Sw();
//			double tfluxsw_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(cmin*edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + influx_*dt / vol / poro;
//			}
//			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//				int jed = nodelist[inode].edgeoutflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double outflux_ = std::abs(cmin*edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + outflux_*dt / vol / poro;
//			}
//
//			double ccp;
//			if (tfluxsw_ == 0){
//				ccp = 1;
//			}
//			else{
//				if (tfluxsw_ < 0){
//					ccp = std::abs(qp / tfluxsw_);
//				}
//				if (tfluxsw_ > 0){
//					ccp = std::abs(qn / tfluxsw_);
//				}
//				if (ccp > 1){//bound bigger
//					ccp = 1.0;
//				}
//			}
//			ccp = ccp*limit_c;
//			tfluxsw_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + influx_*ccp;
//			}
//			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//				int jed = nodelist[inode].edgeoutflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double outflux_ = std::abs(edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + outflux_*ccp;
//			}
//
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//
//		}
//	}
//}
//
//void REGION::updatenodalSw_antiflux_directbound(double dt){
//	nodecvalue.resize(nodelist.size());
//	double cmin=1; //0.5 slightly better
//
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			/*double Swmax = nodelist[inode].Sw();//compute TVD monotone bound
//			double Swmin = nodelist[inode].Sw();
//			for (int ied = 0; ied < nodelist[inode].numberofedgein(); ied++){
//			int iedge = nodelist[inode].edgein(ied);
//			int jnode = edgelist[iedge].node(0);
//			if (nodelist[jnode].Sw() > Swmax){
//			Swmax = nodelist[jnode].Sw();
//			}
//			if (nodelist[jnode].Sw() < Swmin){
//			Swmin = nodelist[jnode].Sw();
//			}
//			}
//			for (int ied = 0; ied < nodelist[inode].numberofedgeout(); ied++){
//			int iedge = nodelist[inode].edgeout(ied);
//			int jnode = edgelist[iedge].node(1);
//			if (nodelist[jnode].Sw() > Swmax){
//			Swmax = nodelist[jnode].Sw();
//			}
//			if (nodelist[jnode].Sw() < Swmin){
//			Swmin = nodelist[jnode].Sw();
//			}
//			}*/
//
//			/*double cmin = 1.0; //compute flux limiting coefficient
//			std::vector<int> localedgelist;
//			for (int i = 0; i < nodelist[inode].numberofedgein(); i++){
//			int ied = nodelist[inode].edgein(i);
//			localedgelist.push_back(ied);
//			}
//			for (int i = 0; i < nodelist[inode].numberofedgeout(); i++){
//			int ied = nodelist[inode].edgeout(i);
//			localedgelist.push_back(ied);
//			}
//			for (int ilocal = 0; ilocal < localedgelist.size(); ilocal++){
//			int iedge = localedgelist[ilocal];
//			double fl = edgelist[iedge].fluxtpfa();
//			double fh = edgelist[iedge].fluxcvfe();
//			if (fl*fh < 0){
//			double c = (1 - a)*fl / (fl - fh);
//			if (c < cmin){
//			cmin = c;
//			}
//			}
//			}
//			if (cmin < 0 || cmin>1){
//			std::cout << "error cmin value not in [0,1]" << std::endl;
//			std::exit(EXIT_FAILURE);
//			}
//			nodecvalue[inode] = cmin;*/
//
//			double tfluxsw_ = 0;
//			double tfluxin_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(cmin*edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + influx_*nodelist[jnode].mobiw() / nodelist[jnode].mobit();
//				tfluxin_ = tfluxin_ + influx_;
//			}
//			//for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//			//	int jed = nodelist[inode].edgeoutflux(j);
//			//	int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//			//	double outflux_ = std::abs(cmin*edgelist[jed].fluxcvfe() + (1 - cmin)*edgelist[jed].fluxtpfa());
//			//	//tfluxsw_ = tfluxsw_ + outflux_*nodelist[inode].mobiw() / nodelist[inode].mobit();
//			//
//			//}
//			double tfluxout_ = -tfluxin_;
//			tfluxsw_ = tfluxsw_ + tfluxout_*nodelist[inode].mobiw() / nodelist[inode].mobit();
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			if (nodelist[inode].Sw() > Swmax){
//				nodelist[inode].Sw(Swmax);
//			}
//			if (nodelist[inode].Sw() < Swmin){
//				nodelist[inode].Sw(Swmin);
//			}
//		}
//	}
//}
//
//void REGION::updatenodalSw_antiflux_directbound_w(double dt){
//	nodecvalue.resize(nodelist.size());
//	double cmin = 1.0/3.0;
//
//	for (int inode = 0; inode < nodelist.size(); inode++){
//		if (nodelist[inode].markbc_Sw() != 1){
//			double tfluxsw_ = 0;
//			for (int j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
//				int jed = nodelist[inode].edgeinflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double influx_ = -std::abs(cmin*edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + influx_;
//			}
//			for (int j = 0; j < nodelist[inode].numberofedgeoutflux(); j++){
//				int jed = nodelist[inode].edgeoutflux(j);
//				int jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
//				double outflux_ = std::abs(cmin*edgelist[jed].fluxfct());
//				tfluxsw_ = tfluxsw_ + outflux_;
//			}
//			double vol = nodelist[inode].dualvolume();
//			double poro = nodelist[inode].porosity();
//			double term1_ = -dt / vol / poro*tfluxsw_; //source term =0
//			nodelist[inode].Sw(nodelist[inode].Sw() + term1_);
//
//			double Swmax = nodalSwmax[inode];
//			double Swmin = nodalSwmin[inode];
//			if (nodelist[inode].Sw() > Swmax){
//				nodelist[inode].Sw(Swmax);
//			}
//			if (nodelist[inode].Sw() < Swmin){
//				nodelist[inode].Sw(Swmin);
//			}
//		}
//	}
//}

void REGION::dt2cfl_fe(double dt){


	double cfl_max= 0;
	double cfl_av = 0;

	for (int ie = 0; ie < elementlist.size(); ie++){
		double dd = elementlist[ie].volume();
		double vx = elementlist[ie].Ux_t();
		double vy = elementlist[ie].Uy_t();
		double vz = elementlist[ie].Uz_t();
		double vv = std::sqrt(vx*vx + vy*vy + vz*vz);
		double cfl_ = dt*vv / pow(dd, 1.0/3.0);
		cfl_av = cfl_av + cfl_;
		if (cfl_ >cfl_max){
			cfl_max = cfl_;
		}
	}
	cfl_av = cfl_av / elementlist.size();

	std::cout << "cfl average: " << cfl_av << std::endl;
	std::cout << "cfl max: " << cfl_max << std::endl;
}

void REGION::dt2cfl_cvfe(double dt){//need to change to control volume based
	double cfl_av = 0;
	double cfl_max = 0;

	for (int inode = 0; inode < nodelist.size(); inode++){
		double tinflux_ = 0;
		for (int ie = 0; ie < nodelist[inode].numberofedgeinflux(); ie++){
			int iedge = nodelist[inode].edgeinflux(ie);
			tinflux_ = tinflux_ + std::abs(edgelist[iedge].flux());
		}
		double cfl_ = tinflux_*dt / nodelist[inode].dualvolume() / nodelist[inode].porosity();
		cfl_av = cfl_av + cfl_;
		if (cfl_ >cfl_max){
			cfl_max = cfl_;
		}
	}
	cfl_av = cfl_av / nodelist.size();


	std::cout << "cfl average: " << cfl_av << std::endl;
	std::cout << "cfl max: " << cfl_max << std::endl;
}

void REGION::dt2cfl_cpg_tpfa_car(double dt){
	double cfl_av, cfl_max, cfl_;

	int ie = 0;
	int je = cpgelementlist[ie].neighbor(0);
	double vv;

	cfl_max = 0;
	cfl_av = 0;

	for (ie= 0; ie < cpgelementlist.size(); ie++){
		double dd = cpgelementlist[ie].dx;
		je = cpgelementlist[ie].neighbor(0);
		if (je >= 0){
			vv = std::abs(cpgelementlist[ie].Po() - cpgelementlist[je].Po())*cpgelementlist[ie].trans(0);
			cfl_ = dt*vv / dd;
			cfl_av += cfl_;
			if (cfl_ >cfl_max){
				cfl_max = cfl_;
			}
		}
	}
	cfl_av = cfl_av / cpgelementlist.size();
	std::cout << "cfl average x: " << cfl_av <<" max: "<<cfl_max<< std::endl;
	cfl_max = 0;
	cfl_av = 0;


	for (ie = 1; ie < cpgelementlist.size(); ie++){
		double dd = cpgelementlist[ie].dy;
		je = cpgelementlist[ie].neighbor(2);
		if (je >= 0){
			vv = std::abs(cpgelementlist[ie].Po() - cpgelementlist[je].Po())*cpgelementlist[ie].trans(2);
			cfl_ = dt*vv / dd;
			cfl_av += cfl_;
			if (cfl_ >cfl_max){
				cfl_max = cfl_;
			}
		}
	}
	cfl_av = cfl_av / cpgelementlist.size();
	std::cout << "cfl average y: " << cfl_av << " max: " << cfl_max << std::endl;

	cfl_max = 0;
	cfl_av = 0;
	for (ie = 1; ie < cpgelementlist.size(); ie++){
		double dd = cpgelementlist[ie].dz;
		je = cpgelementlist[ie].neighbor(4);
		if (je >= 0){
			vv = std::abs(cpgelementlist[ie].Po() - cpgelementlist[je].Po())*cpgelementlist[ie].trans(4);
			cfl_ = dt*vv / dd;
			cfl_av += cfl_;
			if (cfl_ >cfl_max){
				cfl_max = cfl_;
			}
		}
	}
	cfl_av = cfl_av / cpgelementlist.size();

	std::cout << "cfl average z: " << cfl_av << " max: " << cfl_max << std::endl;

}

double REGION::cfl2dt_cvfe(double cfl_){

	double dd = std::pow(elementlist[0].volume(), 1.0 / 3.0);
	double ux = elementlist[0].Ux_t();
	double uy = elementlist[0].Uy_t();
	double uz = elementlist[0].Uz_t();
	double vv = std::sqrt(ux*ux + uy*uy + uz*uz);
	double dt = cfl_*dd / vv;
	for (int ie = 1; ie < elementlist.size(); ie++){
		dd = std::pow(elementlist[ie].volume(), 1.0 / 3.0);
		ux = elementlist[ie].Ux_t();
		uy = elementlist[ie].Uy_t();
		uz = elementlist[ie].Uz_t();
		vv = std::sqrt(ux*ux + uy*uy + uz*uz);
		double dtlocal = cfl_*dd / vv;
		if (dtlocal < dt){
			dt = dtlocal;
		}
	}
	return dt;
}


void REGION::nodeinoutfluxedges_h(){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].clearedgeinfluxh();
		nodelist[i].clearedgeoutfluxh();
	}

	for (int ied = 0; ied < nedge; ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		if (edgelist[ied].fluxh() > 0){
			nodelist[n1].addedgeoutfluxh(ied);
			nodelist[n2].addedgeinfluxh(ied);
		}
		if (edgelist[ied].fluxh() < 0){
			nodelist[n1].addedgeinfluxh(ied);
			nodelist[n2].addedgeoutfluxh(ied);
		}
	}
}


void REGION::nodeinoutfluxedges_hmonotone(){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].clearedgeinfluxh();
		nodelist[i].clearedgeoutfluxh();
	}


	for (int ied = 0; ied < nedge; ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		if (edgelist[ied].fluxh() > 0 && nodelist[n1].Po()>nodelist[n2].Po()){
			nodelist[n1].addedgeoutfluxh(ied);
			nodelist[n2].addedgeinfluxh(ied);
		}
		if (edgelist[ied].fluxh() < 0 && nodelist[n1].Po()<nodelist[n2].Po()){
			nodelist[n1].addedgeinfluxh(ied);
			nodelist[n2].addedgeoutfluxh(ied);
		}
	}
}

void REGION::nodeinoutfluxedges(){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].clearedgeinflux();
		nodelist[i].clearedgeoutflux();
	}
	for (int ied = 0; ied < nedge; ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux() > 0){
			nodelist[n1].addedgeoutflux(ied);
			nodelist[n2].addedgeinflux(ied);
		}
		if (edgelist[ied].flux() < 0){
			nodelist[n1].addedgeinflux(ied);
			nodelist[n2].addedgeoutflux(ied);
		}
	}
}

void REGION::nodeinoutfluxedges_monotone(){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].clearedgeinflux();
		nodelist[i].clearedgeoutflux();
	}
	for (int ied = 0; ied < nedge; ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux() > 0 && nodelist[n1].Po()>nodelist[n2].Po()){
			nodelist[n1].addedgeoutflux(ied);
			nodelist[n2].addedgeinflux(ied);
		}
		if (edgelist[ied].flux() < 0 && nodelist[n1].Po()<nodelist[n2].Po()){
			nodelist[n1].addedgeinflux(ied);
			nodelist[n2].addedgeoutflux(ied);
		}
	}
}

void REGION::nodeinoutfluxedges_c(){
	for (int i = 0; i < nodelist.size(); i++){
		nodelist[i].clearedgeinflux();
		nodelist[i].clearedgeoutflux();
	}

	for (int ied = 0; ied < nedge; ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		if (edgelist[ied].fluxc() > 0){
			nodelist[n1].addedgeoutflux(ied);
			nodelist[n2].addedgeinflux(ied);
		}
		if (edgelist[ied].fluxc() < 0){
			nodelist[n1].addedgeinflux(ied);
			nodelist[n2].addedgeoutflux(ied);
		}
	}
}

void REGION::buildgraph_cvfeflux(){
	int ied, inode, n1, n2, nnode;
	double flux_;
	std::vector<double> checknodeflux;
	nnode = nodelist.size();
	checknodeflux.resize(nnode);

	graphbnodeset.clear();
	for (inode = 0; inode < nnode; inode++){
		nodelist[inode].cleargraphinfo();
		if (nodelist[inode].marktracingb() >= 0){
			graphbnodeset.push_back(inode);
		}
	}

	if (graph.tracingsign() == -1){//backward tracer; change the sign of velocity
		for (ied = 0; ied < nedge; ied++){
			edgelist[ied].flux(-edgelist[ied].flux());
			edgelist[ied].fluxh(-edgelist[ied].fluxh());
		}
	}


	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].fluxh() > 0){
			nodelist[n1].addedgeoutflux(ied);
			nodelist[n2].addedgeinflux(ied);
			nodelist[n1].addedgeoutfluxh(ied);
			nodelist[n2].addedgeinfluxh(ied);

		}
		if (edgelist[ied].fluxh() < 0){
			nodelist[n1].addedgeinfluxh(ied);
			nodelist[n2].addedgeoutfluxh(ied);
			nodelist[n1].addedgeinflux(ied);
			nodelist[n2].addedgeoutflux(ied);
		}
		checknodeflux[n1] = checknodeflux[n1] + edgelist[ied].flux();
		checknodeflux[n2] = checknodeflux[n2] - edgelist[ied].flux();
	}

	if (graph.tracingsign() == 1){
		std::cout << "cvfe graph built for Forward tracing" << std::endl;
	}
	else if (graph.tracingsign() == -1){
		std::cout << "cvfe graph built for Backward tracing" << std::endl;
	}
}

void REGION::buildgraph_fluxh_monotone(){
	int ied, inode, n1, n2, nnode;
	double flux_;
	std::vector<double> checknodeflux;
	nnode = nodelist.size();
	checknodeflux.resize(nnode);

	graphbnodeset.clear();
	for (inode = 0; inode < nnode; inode++){
		nodelist[inode].cleargraphinfo();
		if (nodelist[inode].marktracingb() >= 0){
			graphbnodeset.push_back(inode);
		}
	}

	if (graph.tracingsign() == -1){//backward tracer; change the sign of velocity
		for (ied = 0; ied < nedge; ied++){
			edgelist[ied].flux(-edgelist[ied].flux());
			edgelist[ied].fluxh(-edgelist[ied].fluxh());
		}
	}


	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (n1 == 0 || n2 == 0){
			izz = 1;
		}
		if (edgelist[ied].fluxh() > 0){
			nodelist[n1].addedgeoutfluxh(ied);
			nodelist[n2].addedgeinfluxh(ied);
			if (edgelist[ied].flux() > 0){
				nodelist[n1].addedgeoutflux(ied);
				nodelist[n2].addedgeinflux(ied);
			}

		}
		if (edgelist[ied].fluxh() < 0){
			nodelist[n1].addedgeinfluxh(ied);
			nodelist[n2].addedgeoutfluxh(ied);
			if (edgelist[ied].flux() < 0){
				nodelist[n1].addedgeinflux(ied);
				nodelist[n2].addedgeoutflux(ied);
			}
		}
		//checknodeflux[n1] = checknodeflux[n1] + edgelist[ied].flux();
		//checknodeflux[n2] = checknodeflux[n2] - edgelist[ied].flux();
	}

	if (graph.tracingsign() == 1){
		std::cout << "cvfe graph built for Forward tracing" << std::endl;
	}
	else if (graph.tracingsign() == -1){
		std::cout << "cvfe graph built for Backward tracing" << std::endl;
	}
}

void REGION::buildgraph_flux(){
	int ied, inode, n1, n2, nnode;
	double flux_;
	//std::vector<double> checknodeflux;
	nnode = nodelist.size();
	//checknodeflux.resize(nnode);

	graphbnodeset.clear();
	for (inode = 0; inode < nnode; inode++){
		nodelist[inode].cleargraphinfo();
		if (nodelist[inode].marktracingb() >= 0){
			graphbnodeset.push_back(inode);
		}
	}

	if (graph.tracingsign() == -1){//backward tracer; change the sign of velocity
		for (ied = 0; ied < nedge; ied++){
			edgelist[ied].flux(-edgelist[ied].flux());
			edgelist[ied].fluxh(-edgelist[ied].fluxh());
		}
	}


	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux() > 0){
			nodelist[n1].addedgeoutflux(ied);
			nodelist[n2].addedgeinflux(ied);
		}
		if (edgelist[ied].flux() < 0){
			nodelist[n1].addedgeinflux(ied);
			nodelist[n2].addedgeoutflux(ied);
		}
		//checknodeflux[n1] = checknodeflux[n1] + edgelist[ied].flux();
		//checknodeflux[n2] = checknodeflux[n2] - edgelist[ied].flux();
	}

	if (graph.tracingsign() == 1){
		std::cout << "monotone cvfe graph built for Forward tracing" << std::endl;
	}
	else if (graph.tracingsign() == -1){
		std::cout << "monotone cvfe graph built for Backward tracing" << std::endl;
	}
}

void REGION::buildgraph_tpfa(){
	int ied, inode, n1, n2, nnode;
	double flux_;
	std::vector<double> checknodeflux;
	nnode = nodelist.size();
	checknodeflux.resize(nnode);

	graphbnodeset.clear();
	for (inode = 0; inode < nnode; inode++){
		nodelist[inode].cleargraphinfo();
		if (nodelist[inode].marktracingb() >= 0){
			graphbnodeset.push_back(inode);
		}
	}

	if (graph.tracingsign() == -1){//backward tracer; change the sign of velocity
		for (ied = 0; ied < nedge; ied++){
			edgelist[ied].flux(-edgelist[ied].flux());
		}
	}


	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux() > 0){
			nodelist[n1].addedgeoutflux(ied);
			nodelist[n2].addedgeinflux(ied);

		}
		if (edgelist[ied].flux() < 0){
			nodelist[n1].addedgeinflux(ied);
			nodelist[n2].addedgeoutflux(ied);
		}
		/*checknodeflux[n1] = checknodeflux[n1] + edgelist[ied].fluxtpfa();
		checknodeflux[n2] = checknodeflux[n2] - edgelist[ied].fluxtpfa();*/
	}

	if (graph.tracingsign() == 1){
		std::cout << "tpfa graph built for Forward tracing" << std::endl;
	}
	else if (graph.tracingsign() == -1){
		std::cout << "tpfa graph built for Backward tracing" << std::endl;
	}
}

void REGION::buildgraph_cpgfv(){
	int nele;
	nele = cpgelementlist.size();
	//graphnode: elements number of graphnodes = nelement; grapedge: flux between elements through elemental faces
	graphnodelist.clear();
	graphedgelist.clear();
	graphbeleset.clear();

	graphnodelist.resize(nele);
	std::vector<double> checkeleflux;

	checkeleflux.resize(nele);

	graphbeleset.clear();
	for (int ie = 0; ie < nele; ie++){
		if (cpgelementlist[ie].marktracingb() >= 0){
			graphbeleset.push_back(ie);
		}
	}

	if (graph.tracingsign() == -1){//backward tof; change the sign of velocity
		for (int ie = 0; ie < cpgelementlist.size(); ie++){
			for (int iface = 0; iface < 6; iface++){
				double flux_ = cpgelementlist[ie].flux(iface);
				cpgelementlist[ie].flux(iface, -flux_);
			}
		}
	}

	int count = 0;

	for (int ie = 0; ie < nele; ie++){
		for (int iface = 0; iface < 6; iface++){ //loop over all faces
			int je = cpgelementlist[ie].neighbor(iface);
			if (je >= 0){
				double flux_ = cpgelementlist[ie].flux(iface);
				if (flux_ > 0){ //positive flux means graphedge from the current node
					count++;
					graphedge_.graphnode(0, ie);
					graphedge_.graphnode(1, je);
					graphedgelist.push_back(graphedge_);
					graphnodelist[ie].addedgeout(count - 1);
				}
			}
		}
	}



	//std::ofstream graphoutput, eleoutput1;
	//int i, j, k;
	////output graph
	//graphoutput.open("graphoutput.txt");
	//eleoutput1.open("eleoutput1.txt");
	//for (ie = 0; ie < nelement; ie++){
	//	graphoutput << ie << " ";
	//	for (i = 0; i < graphnodelist[ie].numberofedgeout(); i++){
	//		j = graphnodelist[ie].edgeout(i);
	//		k = graphedgelist[j].graphnode(1);
	//		graphoutput << k << " ";
	//	}
	//	graphoutput << std::endl;
	//}
	//for (ie = 0; ie < graphbeleset.size(); ie++){
	//	eleoutput1 << graphbeleset[ie] << std::endl;
	//}

}

void REGION::reordernodes(){
	int mark_, i, inode, nnode;
	nnode = nodelist.size();
	std::cout << "reorder nodes using DFS" << std::endl;
	reversepostordering.clear();
	cycles.clear();
	for (i = 0; i < graphbnodeset.size(); i++) {
		inode = graphbnodeset[i];
		if (nodelist[inode].status() != 2) {
			DFSnode(inode);
		}
	}

	std::cout << "graph DFS finished" << std::endl;
	notintreenodes.clear();
	ngnode = postordering.size(); //ngnode not necesarily equal to nelement
	reversepostordering.resize(ngnode);
	markintree.clear();
	markintree.resize(nodelist.size());
	for (i = 0; i < ngnode; i++){
		inode = postordering[ngnode - 1 - i];
		reversepostordering[i] = inode;
		markintree[inode] = 1;
	}

	for (i = 0; i < nnode; i++){
		if (markintree[i] != 1){
			notintreenodes.push_back(i);
		}
	}
	postordering.clear();
}

void REGION::reordernodes_cycle(){
	int mark_, i, inode, nnode;
	nnode = nodelist.size();
	std::cout << "reorder nodes using DFS" << std::endl;
	reversepostordering.clear();
	cycles.clear();

	for (i = 0; i < graphbnodeset.size(); i++) {
		inode = graphbnodeset[i];
		if (nodelist[inode].status() != 2) {
			DFSnode_cycle(inode);
		}
	}

	std::cout << "graph DFS finished" << std::endl;
	notintreenodes.clear();
	ngnode = postordering.size(); //ngnode not necesarily equal to nelement
	reversepostordering.resize(ngnode);
	markintree.clear();
	markintree.resize(nodelist.size());
	for (i = 0; i < ngnode; i++){
		inode = postordering[ngnode - 1 - i];
		reversepostordering[i] = inode;
		markintree[inode] = 1;
	}

	for (i = 0; i < nnode; i++){
		if (markintree[i] != 1){
			notintreenodes.push_back(i);
		}
	}
	postordering.clear();
}

void REGION::reordercpgelements(){

	int mark_, i, ie;
	cycles.clear();
	reversepostordering.clear();
	std::cout << "reorder elements using DFS" << std::endl;

	for (i = 0; i < graphbeleset.size(); i++){
		ie = graphbeleset[i];
		if (graphnodelist[ie].status() != 2){
			DFSgraphnode(ie);
		}
	}

	std::cout << "graph DFS finished" << std::endl;

	if (cpgelementlist.size() != postordering.size()){
		std::cout << "nelement " << cpgelementlist.size() << "postordering size " << postordering.size() << std::endl;
		std::cout << "warning!! some points are not reached from the current tof or tracer graph boundary" << std::endl;
	}
	else{
		std::cout << "all elements are visited by DFS" << std::endl;
	}
	ngnode = postordering.size(); //ngnode not necesarily equal to nelement
	reversepostordering.resize(ngnode);
	for (i = 0; i < ngnode; i++){
		ie = postordering[ngnode - 1 - i];
		reversepostordering[i] = ie;
	}

	postordering.clear();

}

void REGION::DFSgraphnode(int inode){
	int i, k, jnode, knode; //local iterator so the loops and main function doesn't affect each other
	std::vector<int> onecycle;
	//preordering.push_back(inode);
	graphnodelist[inode].status(1);

	for (i = 0; i < graphnodelist[inode].numberofedgeout(); i++){
		k = graphnodelist[inode].edgeout(i);
		jnode = graphedgelist[k].graphnode(1); //second node

		if (graphnodelist[jnode].status() == 0){
			graphnodelist[jnode].marknodein(inode);
			DFSgraphnode(jnode);
		}
		else if (graphnodelist[jnode].status() == 1){
			std::cout << "cycle detected" << std::endl;
			std::exit(EXIT_FAILURE);
			//find out the cycle by marknodein
			knode = inode;
			do{
				onecycle.push_back(knode);
				knode = graphnodelist[knode].marknodein();
			} while (knode != jnode);
			//store the cycle
			cycles.push_back(onecycle);
			onecycle.clear();
			graphnodelist[jnode].markcycle(cycles.size() - 1);
		}
	}
	graphnodelist[inode].status(2);
	postordering.push_back(inode);
	graphnodelist[inode].markfromgraphb(markb_); //mark the elements connected to which well
}

void REGION::DFSgraphnode_reversed(int inode){
	int i, k, jnode, knode; //local iterator so the loops and main function doesn't affect each other
	std::vector<int> onecycle;
	//preordering.push_back(inode);
	graphnodelist[inode].status(1);

	for (i = 0; i < graphnodelist[inode].numberofedgein(); i++){
		k = graphnodelist[inode].edgein(i);
		jnode = graphedgelist[k].graphnode(0); //first node

		if (graphnodelist[jnode].status() == 0){
			graphnodelist[jnode].marknodein(inode);
			DFSgraphnode_reversed(jnode);
		}
		else if (graphnodelist[jnode].status() == 1){
			std::cout << "cycle detected" << std::endl;
			std::exit(EXIT_FAILURE);
			//find out the cycle by marknodein
			knode = inode;
			do{
				onecycle.push_back(knode);
				knode = graphnodelist[knode].marknodein();
			} while (knode != jnode);
			//store the cycle
			cycles.push_back(onecycle);
			onecycle.clear();
			graphnodelist[jnode].markcycle(cycles.size() - 1);
		}
	}
	graphnodelist[inode].status(2);
	postordering.push_back(inode);
	graphnodelist[inode].markfromgraphb(markb_); //mark the elements connected to which well
}

void REGION::DFSgraphnode_rh(int inode){
	int i, k, jnode, knode; //local iterator so the loops and main function doesn't affect each other
	std::vector<int> onecycle;
	//preordering.push_back(inode);
	graphnodelist[inode].status(1);

	for (i = graphnodelist[inode].numberofedgein()-1; i>-1; i--){
		k = graphnodelist[inode].edgein(i);
		jnode = graphedgelist[k].graphnode(0); //first node

		if (graphnodelist[jnode].status() == 0){
			graphnodelist[jnode].marknodein(inode);
			DFSgraphnode_rh(jnode);
		}
		else if (graphnodelist[jnode].status() == 1){
			std::cout << "cycle detected" << std::endl;
			std::exit(EXIT_FAILURE);
			//find out the cycle by marknodein
			knode = inode;
			do{
				onecycle.push_back(knode);
				knode = graphnodelist[knode].marknodein();
			} while (knode != jnode);
			//store the cycle
			cycles.push_back(onecycle);
			onecycle.clear();
			graphnodelist[jnode].markcycle(cycles.size() - 1);
		}
	}
	graphnodelist[inode].status(2);
	postordering.push_back(inode);
	graphnodelist[inode].markfromgraphb(markb_); //mark the elements connected to which well
}

void REGION::DFSnode(int inode){
	int jnode, knode;
	std::vector<int> onecycle;
	//preordering.push_back(inode);
	nodelist[inode].status(1);
	int i, j; //local iterator so the loops and main function doesn't affect each other

	for (i = 0; i < nodelist[inode].numberofedgeoutflux(); i++){
		j = nodelist[inode].edgeoutflux(i);
		jnode = edgelist[j].node(0) + edgelist[j].node(1) - inode; //second node
		if (nodelist[jnode].status() == 0){
			nodelist[jnode].marknodein(inode);
			DFSnode(jnode);
		}
		else if (nodelist[jnode].status() == 1){
			std::cout << "cycle detected" << std::endl;
			std::exit(EXIT_FAILURE);
			//find out the cycle by marknodein
			knode = inode;
			do{
				onecycle.push_back(knode);
				knode = nodelist[knode].marknodein();
			} while (knode != jnode);
			//store the cycle
			cycles.push_back(onecycle);
			onecycle.clear();
			nodelist[jnode].markcycle(cycles.size() - 1);
		}
	}
	nodelist[inode].status(2);
	postordering.push_back(inode);
}

void REGION::DFSnode_cycle(int inode){
	int jnode, knode;
	std::vector<int> onecycle;
	//preordering.push_back(inode);
	nodelist[inode].status(1);
	int i, j; //local iterator so the loops and main function doesn't affect each other

	for (i = 0; i < nodelist[inode].numberofedgeoutflux(); i++){
		j = nodelist[inode].edgeoutflux(i);
		jnode = edgelist[j].node(0) + edgelist[j].node(1) - inode; //second node
		if (nodelist[jnode].status() == 0){
			nodelist[jnode].marknodein(inode);
			DFSnode_cycle(jnode);
		}
		else if (nodelist[jnode].status() == 1){

			//find out the cycle by marknodein
			knode = inode;
			do{
				onecycle.push_back(knode);
				knode = nodelist[knode].marknodein();
			} while (knode != jnode);
			//store the cycle
			cycles.push_back(onecycle);
			onecycle.clear();
			nodelist[jnode].markcycle(cycles.size() - 1);
		}
	}
	nodelist[inode].status(2);
	postordering.push_back(inode);
}

void REGION::outputcycles(){
	markinside.clear();
	markinside.resize(nodelist.size());
	if (cycles.size() > 0){
		std::cout << "number of cycles: " <<cycles.size()<< std::endl;
		/*for (int i = 0; i < cycles.size(); i++){
			std::cout << "cycle size: " << cycles[i].size() << std::endl;
		}*/
		for (int inode = 0; inode < nodelist.size(); inode++){
			if (markinside[inode] == 0){
				int numberofcycles_ = nodelist[inode].markcycle().size();
				if (numberofcycles_ > 0){
					bigcycle.clear();
					bigcycle.push_back(inode); //inode is included into bigcycle
					integratecyclesnode(inode); //integrate all cycles connected to the current cycle
					for (int j = 0; j < bigcycle.size(); j++){
						markinside[bigcycle[j]] = 1;
					}
					int bigcyclesize = bigcycle.size();
					std::cout << "Big cycle size " << bigcyclesize << std::endl;
				}
			}
		}
		int cc = 0;
		for (int i = 0; i < markinside.size(); i++){
			if (markinside[i] == 1){
				cc++;
			}
		}
		std::cout << "Number of nodes in cycle: " << cc << std::endl;
		std::cout << "Total Number of nodes: " << nodelist.size() << std::endl;
	}
	else{
		std::cout << "no cycles found" << std::endl;
	}

}

void REGION::computetoftpfa_local(){
	int i, j, ie, itofb, iwell, numberofcycles_, bigcyclesize, inode, iorder, jnode, jed, irow, jrow, count, nnode;
	double porosity_, flux_, rhs_convec_, rhs_source_, totaloutflux_, influx_, totalinflux_, value, tofmax;
	std::vector<double> MRHS, Msolution, nodetoflist;
	MATRIX M;
	std::vector<int> markcalculated, node2row, markinmatrix; //not in NODE because only useful here
	nnode = nodelist.size();
	markcalculated.resize(nnode);
	markinmatrix.resize(nnode);

	std::cout << "solve nodal tof after DFS reordering with TPFA" << std::endl;
	node2row.resize(nnode);
	nodetoflist.resize(nnode);

	std::vector<int> negnodes;
	int cneg = 0;
	int cav = 0;
	for (iorder = 0; iorder < ngnode; iorder++){
		inode = reversepostordering[iorder];
		porosity_ = nodelist[inode].porosity();
		if (nodelist[inode].marktracingb() >= 0){
			markcalculated[inode] = 1;
			nodetoflist[inode] = 0;
		}
		else if (markcalculated[inode] == 0){
			numberofcycles_ = nodelist[inode].markcycle().size();
			if (numberofcycles_ == 0){
				rhs_source_ = nodelist[inode].dualvolume()*nodelist[inode].porosity();
				rhs_convec_ = 0;
				totalinflux_ = 0;
				for (j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
					jed = nodelist[inode].edgeinflux(j);
					jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
					if (markcalculated[jnode] == 1) {
						influx_ = -std::abs(edgelist[jed].flux());//equivalent to using if n1 then flux if n2 then -flux
						totalinflux_ = totalinflux_ + influx_;
						rhs_convec_ = rhs_convec_ - nodetoflist[jnode] * influx_;
					}
				}
				if (totalinflux_ == 0){
					std::cout << "totalinflux==0 wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				totaloutflux_ = -totalinflux_;
				nodetoflist[inode] = rhs_convec_ / totaloutflux_ + rhs_source_ / totaloutflux_;
				markcalculated[inode] = 1;

				if (nodetoflist[inode]<0){
				std::cout << "negative tof" << inode << " " << jnode << " " << jed << std::endl;
				std::exit(EXIT_FAILURE);
				}
			}

		}
	}


	//not in tree nodes averaged
	/*std::cout << "notintreenodes size: " << notintreenodes.size() << " averaged" << std::endl;
	if (notintreenodes.size() > 0){
		int check = 1;
		int cloop = 0;
		while (check == 1) {
			cloop++;
			check = 0;
			for (i = 0; i < notintreenodes.size(); i++){
				inode = notintreenodes[i];
				value = 0;
				int count_ = 0;
				for (j = 0; j < nodelist[inode].numberofsunode(); j++){
					jnode = nodelist[inode].sunode(j);
					if (markcalculated[jnode] == 1){
						value = value + nodetoflist[jnode];
						count_++;
					}
				}
				if (count_ == 0){
					check = 1;
				}
				else{
					value = value / count_;
					nodetoflist[inode] = value;
					markcalculated[inode] = 1;
				}
			}
		}
		std::cout << "average loop times: " << cloop << std::endl;
	}*/
	//average not calculated nodes
	for (inode = 0; inode < nnode; inode++){
		if (markcalculated[inode] == 0){
			std::cout << "there are nodes not calculated after DFS reorder computing tof with TPFA " << inode << std::endl;
			std::exit(EXIT_FAILURE);
	//		/*value = 0;
	//		int count_ = 0;
	//		for (j = 0; j < nodelist[inode].numberofsunode(); j++){
	//			jnode = nodelist[inode].sunode(j);
	//			if (markcalculated[jnode] == 1 && nodetoflist[jnode] >= 0){
	//				value = value + nodetoflist[jnode];
	//				count_++;
	//			}
	//		}
	//		if (count_ == 0){
	//			std::cout << "no calculated node around when average negnodes" << inode << std::endl;
	//			std::exit(EXIT_FAILURE);
	//		}
	//		value = value / nodelist[inode].numberofsunode();
	//		nodetoflist[inode] = value;*/
		}
	}

	//pass nodetoflist to tof or tof_back in class node
	if (graph.tracingsign() == 1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tof(nodetoflist[inode]);
		}
	}
	else if (graph.tracingsign() == -1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tof_back(nodetoflist[inode]);
		}
		//reverse back facial flux
		for (jed = 0; jed < nedge; jed++){
			edgelist[jed].flux(-edgelist[jed].flux());
		}
	}

}

void REGION::computetofcvfe_local(){
	int i, j, ie, itofb, iwell, numberofcycles_, bigcyclesize, inode, iorder, jnode, jed, irow, jrow, count, nnode;
	double porosity_, flux_, rhs_convec_, rhs_source_, totaloutflux_,totalcvfeinflux_, totalcvfeoutflux_, influx_,incvfeflux_, totalinflux_, value, tofmax;
	std::vector<double> MRHS, Msolution, nodetoflist;
	MATRIX M;
	std::vector<int> markcalculated; //not in NODE because only useful here
	nnode = nodelist.size();
	markcalculated.resize(nnode);


	std::cout << "solve nodal tof after DFS reordering with CVFE and monotone graph" << std::endl;
	nodetoflist.resize(nnode);

	std::vector<int> negnodes;
	int cneg = 0;
	int cav = 0;
	for (iorder = 0; iorder < ngnode; iorder++){
		inode = reversepostordering[iorder];
		porosity_ = nodelist[inode].porosity();
		if (nodelist[inode].marktracingb() >= 0){
			markcalculated[inode] = 1;
			nodetoflist[inode] = 0;
		}
		else if (markcalculated[inode] == 0){
			numberofcycles_ = nodelist[inode].markcycle().size();
			if (numberofcycles_ == 0){
				rhs_source_ = nodelist[inode].dualvolume()*nodelist[inode].porosity();
				rhs_convec_ = 0;
				totalinflux_ = 0;
				totalcvfeinflux_ = 0;
				for (j = 0; j < nodelist[inode].numberofedgeinfluxh(); j++){
					jed = nodelist[inode].edgeinfluxh(j);
					jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
					incvfeflux_ = -std::abs(edgelist[jed].fluxh());
					totalcvfeinflux_ = totalcvfeinflux_ + incvfeflux_;
				}
				for (j = 0; j < nodelist[inode].numberofedgeinflux(); j++){
					jed = nodelist[inode].edgeinflux(j);
					jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
					if (markcalculated[jnode] == 1) {
						influx_ = -std::abs(edgelist[jed].fluxh());
						totalinflux_ = totalinflux_ + influx_;//for convection flux
						rhs_convec_ = rhs_convec_ - nodetoflist[jnode] * influx_; //equivalent to using if n1 then flux if n2 then -flux
					}
				}
				if (totalinflux_ == 0){
					std::cout << "totalinflux==0 wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				totaloutflux_ = -totalinflux_;
				totalcvfeoutflux_ = -totalcvfeinflux_;
				nodetoflist[inode] = rhs_convec_ / totaloutflux_ + rhs_source_/totalcvfeoutflux_;
				markcalculated[inode] = 1;

				/*if (nodetoflist[inode]<0){
					std::cout << "negative tof" << inode << " " << jnode << " " << jed << std::endl;
					std::exit(EXIT_FAILURE);
					nodetoflist[inode] = 0;
					negnodes.push_back(inode);
				}*/
			}

			//if (numberofcycles_ > 0){ //build matrix for cycle (numberofcycles_ is the number of cycles)
			//	std::cout << "cycle exist" << std::endl;
			//	std::exit(EXIT_FAILURE);
			//}
		}
	}


	//not in tree nodes averaged
	std::cout << "notintreenodes size: " << notintreenodes.size() <<" averaged"<< std::endl;
	if (notintreenodes.size() > 0){
		int check = 1;
		int cloop = 0;
		while (check == 1) {
			cloop++;
			check = 0;
			for (i = 0; i < notintreenodes.size(); i++){
				inode = notintreenodes[i];
				value = 0;
				int count_ = 0;
				for (j = 0; j < nodelist[inode].numberofsunode(); j++){
					jnode = nodelist[inode].sunode(j);
					if (markcalculated[jnode] == 1){
						value = value + nodetoflist[jnode];
						count_++;
					}
				}
				if (count_ == 0){
					check = 1;
				}
				else{
					value = value / count_;
					nodetoflist[inode] = value;
					markcalculated[inode] = 1;
				}
			}
		}
		std::cout << "average loop times: " << cloop << std::endl;
	}
	//average not calculated nodes
	//for (inode = 0; inode < nnode; inode++){
	//	if (markcalculated[inode] == 0){
	//		std::cout << "there are nodes not calculated after computing notintreenodes" << inode << std::endl;
	//		std::exit(EXIT_FAILURE);
	//		/*value = 0;
	//		int count_ = 0;
	//		for (j = 0; j < nodelist[inode].numberofsunode(); j++){
	//			jnode = nodelist[inode].sunode(j);
	//			if (markcalculated[jnode] == 1 && nodetoflist[jnode] >= 0){
	//				value = value + nodetoflist[jnode];
	//				count_++;
	//			}
	//		}
	//		if (count_ == 0){
	//			std::cout << "no calculated node around when average negnodes" << inode << std::endl;
	//			std::exit(EXIT_FAILURE);
	//		}
	//		value = value / nodelist[inode].numberofsunode();
	//		nodetoflist[inode] = value;*/
	//	}
	//}

	//pass nodetoflist to tof or tof_back in class node
	if (graph.tracingsign() == 1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tof(nodetoflist[inode]);
		}
	}
	else if (graph.tracingsign() == -1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tof_back(nodetoflist[inode]);
		}
		//reverse back facial flux
		for (jed = 0; jed < nedge; jed++){
			edgelist[jed].flux(-edgelist[jed].flux());
		}
	}

}

void REGION::computetof_cpgfv_local(){

	//std::vector<double> Msolution, MRHS;
	//std::vector<int> markinmatrix;
	//MATRIX M;
	std::vector<double> eletoflist;
	std::vector<int> markcalculated, ele2row;

	int nele = cpgelementlist.size();
	std::cout << "solve elemental tof after DFS reordering with FV and facial velocity" << std::endl;

	markcalculated.resize(nele);
	//markinmatrix.resize(nele);
	ele2row.resize(nele);
	eletoflist.resize(nele);

	for (int iorder = 0; iorder < nele; iorder++){
		int ie = reversepostordering[iorder];
		double porosity_ = cpgelementlist[ie].porosity();
		if (cpgelementlist[ie].marktracingb() >= 0){ //tofb well element
			eletoflist[ie] = 0;
			markcalculated[ie]=1;//use graphnode here but not in the unstructured case
		}

		else if (markcalculated[ie] == 0){ //ie not at tof line well boundary
			int numberofcycles_ = graphnodelist[ie].markcycle().size();
			if (numberofcycles_ == 0){
				double coef1 = 0;
				double rhs1 = cpgelementlist[ie].volume() * cpgelementlist[ie].porosity();
				for (int iface = 0; iface < 6; iface++){ //loop over all faces
					int je = cpgelementlist[ie].neighbor(iface);
					if (je >= 0){
						double flux_ = cpgelementlist[ie].flux(iface);
						if (flux_ < 0){ //choose neighboring element
							coef1 = coef1 + flux_;//total outflux == total influx
							if (markcalculated[je] == 1){
								rhs1 = rhs1 - flux_*eletoflist[je];
							}
						}
					}
				}
				eletoflist[ie] = -rhs1 / coef1;
				markcalculated[ie] = 1;
			}
			else if (numberofcycles_ > 0){ //build matrix for cycle (numberofcycles_ is the number of cycles
			//	std::cout << "for cycle" << std::endl;
			//	bigcycle.clear();
			//	bigcycle.push_back(ie); //ie is included into bigcycle
			//	integratecycles(ie); //integrate all cycles connected to the current cycle
			//	bigcyclesize = bigcycle.size();
			//	M.clear();
			//	MRHS.clear();
			//	M.size(bigcyclesize); //M is MATRIX
			//	MRHS.resize(bigcyclesize);
			//	for (i = 0; i < bigcyclesize; i++){ //firstly which element (graphnode) is on which row, so that we don't need to calculate H based on TH like in computing pressure
			//		ie = bigcycle[i];
			//		graphnode2row[ie] = i;
			//		graphnodelist[ie].markmatrix(1); //remember calculated
			//	}
			//	//build matrix
			//	for (irow = 0; irow < bigcyclesize; irow++){
			//		coef1 = 0;
			//		ie = bigcycle[irow];
			//		vol = cpgelementlist[ie].volume();
			//		MRHS[irow] = vol*cpgelementlist[ie].porosity(); //xin jia
			//		for (iface = 0; iface < 6; iface++){ //loop over all 4 faces
			//			je = cpgelementlist[ie].neighbor(iface);
			//			if (je >= 0){
			//				//flux_ = cpgfacelist[cpgelementlist[ie].cpgface(iface)].flux();
			//				if (flux_ < -0){ //choose neighboring element
			//					coef1 = coef1 + flux_;
			//					if (graphnodelist[je].markcalculated() == 1){
			//						MRHS[irow] = MRHS[irow] - flux_*eletoflist[je];
			//					}
			//					else if (graphnodelist[je].markmatrix() == 1){
			//						jrow = graphnode2row[je];
			//						M.entry(irow, jrow, M.entry(irow, jrow) + flux_);
			//					}
			//					else{
			//						std::cout << "influx from a non-reachable element" << ie << std::endl;
			//						std::exit(EXIT_FAILURE);
			//					}
			//				}
			//			}
			//		}
			//		M.entry(irow, irow, -coef1); //xin gai
			//	} //matrix built
			//	Msolution = mathzz_.Gaussianelimination(M, MRHS);
			//	for (irow = 0; irow < bigcyclesize; irow++){
			//		ie = bigcycle[irow];
			//		graphnodelist[ie].markmatrix(0); //for next cycle
			//		value = Msolution[irow];
			//		eletoflist[ie]=value;
			//		graphnodelist[ie].markcalculated(1); //remember calculated
			//	}
			//
			}
		}
	}

	/*for (i = 0; i < nelement; i++){
	if (graphnodelist[i].markcalculated() == 1){
	}
	if (graphnodelist[i].markcalculated() == 0){ //a good model shouldn't have these non-reachable elements
	elementlist[i].tof(-100); //tof=infinity
	}
	graphnodelist[i].markcalculated(0);
	}
	*/
	if (graph.tracingsign() == 1){
		for (int ie = 0; ie < nele; ie++){
			cpgelementlist[ie].tof(eletoflist[ie]);
		}
	}
	else if (graph.tracingsign() == -1){
		for (int ie = 0; ie < nele; ie++){
			cpgelementlist[ie].tof_back(eletoflist[ie]);
			//for (int iface = 0; iface < 6; iface++){ //reverse back facial flux
			//	double flux_ = cpgelementlist[ie].flux(iface);
			//	cpgelementlist[ie].flux(iface, -flux_);
			//}
		}
	}

}

void REGION::computetracer_cpgfv_local(){
	//std::vector<double> Msolution, MRHS;
	//MATRIX M;
	std::vector<std::vector<double>> eletracerlist;
	std::vector<int> markcalculated, ele2row;
	std::vector<double> rhs_;
	std::cout << "reordered CPGFV for tracer concentrations" << std::endl;
	int nele = cpgelementlist.size();
	ele2row.resize(nele);
	eletracerlist.resize(nele);
	markcalculated.resize(nele);
	for (int ie = 0; ie < nele; ie++){
		eletracerlist[ie].resize(graph.ntracingb());
	}
	rhs_.resize(graph.ntracingb());


	for (int iorder = 0; iorder < nele; iorder++){
		int ie = reversepostordering[iorder];

		if (cpgelementlist[ie].marktracingb() >=0){ //element ajacent to well or surface boundary (even just one node)
			eletracerlist[ie][cpgelementlist[ie].marktracingb()] = 1;
			markcalculated[ie] = 1;
		}
		else if (markcalculated[ie] == 0){
			int numberofcycles_ = graphnodelist[ie].markcycle().size();
			if (numberofcycles_ == 0){
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					rhs_[icc] = 0;
				}
				double totalinflux_ = 0;
				for (int iface = 0; iface < 6; iface++){ //loop over all 6 faces
					int je = cpgelementlist[ie].neighbor(iface);
					if (je >= 0){
						double flux_ = cpgelementlist[ie].flux(iface);
						if (flux_ < 0){ //choose neighboring element
							totalinflux_ = totalinflux_ + flux_;
							if (markcalculated[je] == 1){
								for (int icc = 0; icc < graph.ntracingb(); icc++){
									rhs_[icc] = rhs_[icc] - eletracerlist[je][icc] * flux_;
								}
							}
						}
					}
				}
				//if (cpgelementlist[ie].markbc() == -1){ //need check
					//totaloutflux_ = -coef1 + cpgelementlist[ie].source();
				//}
				//else{
				double totaloutflux_ = -totalinflux_;
				//}
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					eletracerlist[ie][icc] = rhs_[icc] / totaloutflux_;
				}
				markcalculated[ie]=1;
			}
			//if (numberofcycles_ > 0){ //build matrix for cycle (numberofcycles_ is the number of cycles
			//	std::cout << "for cycle" << std::endl;
			//	bigcycle.clear();
			//	bigcycle.push_back(ie); //ie is included into bigcycle
			//	integratecycles(ie); //integrate all cycles connected to the current cycle
			//	bigcyclesize = bigcycle.size();
			//	M.clear();
			//	MRHS.clear();
			//	M.size(bigcyclesize); //M is MATRIX
			//	MRHS.resize(bigcyclesize);
			//	for (i = 0; i < bigcyclesize; i++){ //firstly which element (graphnode) is on which row, so that we don't need to calculate H based on TH like in computing pressure
			//		ie = bigcycle[i];
			//		graphnode2row[ie] = i;
			//		graphnodelist[ie].markmatrix(1); //remember calculated
			//	}
			//	//build matrix
			//	for (irow = 0; irow < bigcyclesize; irow++){
			//		coef1 = 0;
			//		ie = bigcycle[irow];
			//		vol = cpgelementlist[ie].volume();
			//		for (iface = 0; iface < 4; iface++){ //loop over all 4 faces
			//			je = cpgelementlist[ie].neighbor(iface);
			//			if (je >= 0) { //neighboring element exist
			//				flux_ = cpgelementlist[ie].cpgface(iface).flux();
			//				if (flux_ < -0){ //choose neighboring element
			//					coef1 = coef1 + flux_;
			//					if (graphnodelist[je].markcalculated() == 1){
			//						MRHS[irow] = MRHS[irow] - flux_*eletracerlist[je];
			//					}
			//					else if (graphnodelist[je].markmatrix() == 1){
			//						jrow = graphnode2row[je];
			//						M.entry(irow, jrow, M.entry(irow, jrow) + flux_);
			//					}
			//				}
			//			}
			//		}
			//		M.entry(irow, irow, -coef1); //xin gai
			//	} //matrix built
			//	Msolution = mathzz_.Gaussianelimination(M, MRHS);
			//	for (irow = 0; irow < bigcyclesize; irow++){
			//		ie = bigcycle[irow];
			//		graphnodelist[ie].markmatrix(0); //for next cycle
			//		value = Msolution[irow];
			//		eletracerlist[ie] = value;
			//		graphnodelist[ie].markcalculated(1); //remember calculated
			//	}
			//}
		}
	}

	//pass tracer value from eletracerlist to cpgelement
	if (graph.tracingsign() == 1){
		for (int ie = 0; ie < nele; ie++){
			cpgelementlist[ie].tracersize(graph.ntracingb());
			for (int icc = 0; icc < graph.ntracingb(); icc++){
				cpgelementlist[ie].tracer(icc, eletracerlist[ie][icc]);
			}
		}
	}
	else if (graph.tracingsign() == -1){
		for (int ie = 0; ie < nele; ie++){
			for (int ie = 0; ie < nele; ie++){
				cpgelementlist[ie].tracersize_back(graph.ntracingb());
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					cpgelementlist[ie].tracer_back(icc, eletracerlist[ie][icc]);
				}
			}
			//for (int iface = 0; iface < 6; iface++){ //reverse back facial flux
			//	double flux_ = cpgelementlist[ie].flux(iface);
			//	cpgelementlist[ie].flux(iface, -flux_);
			//}
		}
	}


}

void REGION::computetracercvfe_local() {
	int i, j, ie, iorder, iwell, numberofcycles_, bigcyclesize, inode, jnode, jed, irow, jrow, nnode;
	double porosity_, flux_, totaloutflux_, totalinflux_, influx_;
	std::vector<double> MRHS, Msolution, rhs_;
	MATRIX M;
	std::vector<int> markcalculated, node2row, markinmatrix; //not in NODE because only useful here
	std::vector<std::vector<double>> nodetracerlist;
	std::vector<int> negnodes, bignodes;
	nnode = nodelist.size();
	markcalculated.resize(nnode);
	markinmatrix.resize(nnode);
	nodetracerlist.resize(nnode);
	std::cout << "solve nodal tracer after DFS reordering with edgenodalFV" << std::endl;
	node2row.resize(nnode);
	rhs_.resize(graph.ntracingb());

	for (inode = 0; inode < nnode; inode++){
		nodetracerlist[inode].resize(graph.ntracingb());
	}

	for (iorder = 0; iorder < ngnode; iorder++) {
		inode = reversepostordering[iorder];
		if (nodelist[inode].marktracingb()>=0) {
			nodetracerlist[inode][nodelist[inode].marktracingb()]= 1;
			markcalculated[inode] = 1;
		}
		else if (markcalculated[inode] == 0) {
			numberofcycles_ = nodelist[inode].markcycle().size();
			if (numberofcycles_ == 0) {
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					rhs_[icc] = 0;
				}
				totalinflux_ = 0;
				for (j = 0; j < nodelist[inode].numberofedgeinflux(); j++) {
					jed = nodelist[inode].edgeinflux(j);
					jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
					if (markcalculated[jnode] == 1) {
						influx_ = -std::abs(edgelist[jed].fluxh()); //here different from TOF
						totalinflux_ = totalinflux_ + influx_;
						for (int icc = 0; icc < graph.ntracingb(); icc++){
							rhs_[icc] = rhs_[icc] - nodetracerlist[jnode][icc] * influx_;
						}
					}
					// else influx from nottintree node ignored
				} //problem with edgeoutflux directly is boundary well or surface nodes

				if (totalinflux_ == 0){
					std::cout << "totalinflux==0 wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				totaloutflux_ = -totalinflux_;
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					nodetracerlist[inode][icc] = rhs_[icc] / totaloutflux_;
				}
				markcalculated[inode] = 1;
			}
			//if (numberofcycles_ > 0) { //build matrix for cycle (numberofcycles_ is the number of cycles)
			//	std::cout << "cycle exist" << std::endl;
			//	std::exit(EXIT_FAILURE);
			//}
		}
	}
	std::cout << "notintreenodes size: " << notintreenodes.size() << " averaged" << std::endl;
	int check = 1;
	int cloop = 0;
	std::vector<double> value;
	value.resize(graph.ntracingb());
	while (check == 1) {
		cloop++;
		check = 0;
		for (i = 0; i < notintreenodes.size(); i++){
			inode = notintreenodes[i];
			for (int icc = 0; icc < graph.ntracingb(); icc++){
				value[icc] = 0;
			}
			int count_ = 0;
			for (j = 0; j < nodelist[inode].numberofsunode(); j++){
				jnode = nodelist[inode].sunode(j);
				if (markcalculated[jnode] == 1){
					for (int icc = 0; icc < graph.ntracingb(); icc++){
						value[icc] = value[icc] + nodetracerlist[jnode][icc];
					}
					count_++;
				}
			}
			if (count_ == 0){
				check = 1;
			}
			else{
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					value[icc] = value[icc] / count_;
					nodetracerlist[inode][icc] = value[icc];
				}
				markcalculated[inode] = 1;
			}
		}
	}
	std::cout << "average loop times: " << cloop << std::endl;

	//pass tracer value from nodetracerlist to node (not in tree common because of tracer partitioning)
	if (graph.tracingsign() == 1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tracersize(graph.ntracingb());
			for (int icc = 0; icc < graph.ntracingb(); icc++){
				nodelist[inode].tracer(icc, nodetracerlist[inode][icc]);
			}
		}
	}
	else if (graph.tracingsign() == -1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tracersize_back(graph.ntracingb());
			for (int icc = 0; icc < graph.ntracingb(); icc++){
				nodelist[inode].tracer_back(icc, nodetracerlist[inode][icc]);
			}
		}
		for (jed = 0; jed < nedge; jed++) { //reverse back edge facial flux (no need if tof&tracer compute together)
			edgelist[jed].flux(-edgelist[jed].flux());

		}
	}

}

void REGION::computetracertpfa_local() {
	int i, j, ie, iorder, iwell, numberofcycles_, bigcyclesize, inode, jnode, jed, irow, jrow, nnode;
	double porosity_, flux_, totaloutflux_, totalinflux_, influx_;
	std::vector<double> MRHS, Msolution, rhs_;
	MATRIX M;
	std::vector<int> markcalculated, node2row, markinmatrix; //not in NODE because only useful here
	std::vector<std::vector<double>> nodetracerlist;
	std::vector<int> negnodes, bignodes;
	nnode = nodelist.size();
	markcalculated.resize(nnode);
	markinmatrix.resize(nnode);
	nodetracerlist.resize(nnode);
	std::cout << "solve nodal tracer after DFS reordering with TPFA" << std::endl;
	node2row.resize(nnode);
	rhs_.resize(graph.ntracingb());

	for (inode = 0; inode < nnode; inode++){
		nodetracerlist[inode].resize(graph.ntracingb());
	}

	for (iorder = 0; iorder < ngnode; iorder++) {
		inode = reversepostordering[iorder];
		if (nodelist[inode].marktracingb() >= 0) {
			nodetracerlist[inode][nodelist[inode].marktracingb()] = 1;
			markcalculated[inode] = 1;
		}
		else if (markcalculated[inode] == 0) {
			numberofcycles_ = nodelist[inode].markcycle().size();
			if (numberofcycles_ == 0) {
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					rhs_[icc] = 0;
				}
				totalinflux_ = 0;
				for (j = 0; j < nodelist[inode].numberofedgeinflux(); j++) {
					jed = nodelist[inode].edgeinflux(j);
					jnode = edgelist[jed].node(0) + edgelist[jed].node(1) - inode;
					if (markcalculated[jnode] == 1) {
						influx_ = -std::abs(edgelist[jed].flux()); //here different from TOF
						totalinflux_ = totalinflux_ + influx_;
						for (int icc = 0; icc < graph.ntracingb(); icc++){
							rhs_[icc] = rhs_[icc] - nodetracerlist[jnode][icc] * influx_;
						}
					}
					// else influx from nottintree node ignored
				} //problem with edgeoutflux directly is boundary well or surface nodes

				if (totalinflux_ == 0){
					std::cout << "totalinflux==0 wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				totaloutflux_ = -totalinflux_;
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					nodetracerlist[inode][icc] = rhs_[icc] / totaloutflux_;
				}
				markcalculated[inode] = 1;
			}
			//if (numberofcycles_ > 0) { //build matrix for cycle (numberofcycles_ is the number of cycles)
			//	std::cout << "cycle exist" << std::endl;
			//	std::exit(EXIT_FAILURE);
			//}
		}
	}
	/*std::cout << "notintreenodes size: " << notintreenodes.size() << " averaged" << std::endl;
	int check = 1;
	int cloop = 0;
	std::vector<double> value;
	value.resize(graph.ntracingb());
	while (check == 1) {
		cloop++;
		check = 0;
		for (i = 0; i < notintreenodes.size(); i++){
			inode = notintreenodes[i];
			for (int icc = 0; icc < graph.ntracingb(); icc++){
				value[icc] = 0;
			}
			int count_ = 0;
			for (j = 0; j < nodelist[inode].numberofsunode(); j++){
				jnode = nodelist[inode].sunode(j);
				if (markcalculated[jnode] == 1){
					for (int icc = 0; icc < graph.ntracingb(); icc++){
						value[icc] = value[icc] + nodetracerlist[jnode][icc];
					}
					count_++;
				}
			}
			if (count_ == 0){
				check = 1;
			}
			else{
				for (int icc = 0; icc < graph.ntracingb(); icc++){
					value[icc] = value[icc] / count_;
					nodetracerlist[inode][icc] = value[icc];
				}
				markcalculated[inode] = 1;
			}
		}
	}
	std::cout << "average loop times: " << cloop << std::endl;*/
	for (inode = 0; inode < nnode; inode++){
		if (markcalculated[inode] == 0){
			std::cout << "there are nodes not calculated after DFS reorder computing tracer with TPFA" << inode << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}
	//pass tracer value from nodetracerlist to node (not in tree common because of tracer partitioning)
	if (graph.tracingsign() == 1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tracersize(graph.ntracingb());
			for (int icc = 0; icc < graph.ntracingb(); icc++){
				nodelist[inode].tracer(icc, nodetracerlist[inode][icc]);
			}
		}
	}
	else if (graph.tracingsign() == -1){
		for (inode = 0; inode < nnode; inode++){
			nodelist[inode].tracersize_back(graph.ntracingb());
			for (int icc = 0; icc < graph.ntracingb(); icc++){
				nodelist[inode].tracer_back(icc, nodetracerlist[inode][icc]);
			}
		}
		for (jed = 0; jed < nedge; jed++) { //reverse back edge facial flux (no need if tof&tracer compute together)
			edgelist[jed].flux(-edgelist[jed].flux());
		}
	}

}


void REGION::computeedgeflux_tpfa(){
	std::cout << "Compute edge TPFA flux" << std::endl;
	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		int inode = edgelist[iedge].node(0);
		int jnode = edgelist[iedge].node(1);
		double flux_ = edgelist[iedge].trans()*(nodelist[inode].Po() - nodelist[jnode].Po());
		edgelist[iedge].flux(flux_);
	}
}

void REGION::computeedgeflux_w(){
	std::cout << "Compute edge flux w" << std::endl;
	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		int inode = edgelist[iedge].node(0);
		int jnode = edgelist[iedge].node(1);
		double flux_ = edgelist[iedge].flux();
		double frac;
		if (flux_>=0){
			frac = nodelist[inode].fracw();
		}
		else{
			frac = nodelist[jnode].fracw();
		}
		edgelist[iedge].flux(flux_*frac);
	}
}

void REGION::computeedgefluxh_w(){
	std::cout << "Compute edge flux w by central approximation" << std::endl;
	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		int inode = edgelist[iedge].node(0);
		int jnode = edgelist[iedge].node(1);
		double flux_ = edgelist[iedge].flux();
		double frac;
		frac = 0.5*(nodelist[inode].fracw()+nodelist[jnode].fracw());
		edgelist[iedge].fluxh(flux_*frac);
	}
}

void REGION::computeedgeantiflux(){
	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		double fl = edgelist[iedge].flux();
		double fh = edgelist[iedge].fluxh();
		edgelist[iedge].fluxc(fh - fl);

		/*int i1 = edgelist[iedge].node(0);
		int i2 = edgelist[iedge].node(1);
		if ((nodelist[i1].Po() - nodelist[i2].Po())*fh < 0){
			edgelist[iedge].fluxfct(0);
		}*/
	}
}


//void REGION::computeedgeflux_fct(){
//	std::cout << "Compute edge FCT flux globally" << std::endl;
//	//calculate limiting coefficient
//	//double a = 0.01;
//	double c, fl, fh, ff;
//	/*double cmin = 1.0;
//	int count = 0;
//	for (int iedge = 0; iedge < edgelist.size(); iedge++){
//		fl = edgelist[iedge].fluxtpfa();
//		fh = edgelist[iedge].fluxcvfe();
//		if ( fl*fh < 0){
//			count++;
//			c = (1 - a)*fl / (fl - fh);
//			if (c<cmin){
//				cmin = c;
//			}
//		}
//	}
//	std::cout << "number of fh*fl<0 edges " <<count<< std::endl;
//	if (cmin<0 || cmin>1){
//		std::cout << "error cmin value not in [0,1]" << std::endl;
//		std::exit(EXIT_FAILURE);
//	}
//	else{
//		std::cout << "Limiting Coefficient is " << cmin << std::endl;
//	}*/
//	double cmin = 0.5;
//	for (int iedge = 0; iedge < edgelist.size(); iedge++){
//		fl = edgelist[iedge].fluxtpfa();
//		fh = edgelist[iedge].fluxcvfe();
//		ff = cmin*fh + (1.0 - cmin)*fl;
//		/*if (fl*ff < 0){
//			std::cout << "error fct flux" << std::endl;
//			std::exit(EXIT_FAILURE);
//		}*/
//		edgelist[iedge].fluxfct(ff);
//	}
//}

void REGION::computeedgeflux_edgefd(){
	for (int ied = 0; ied < edgelist.size(); ied++){
		int n1 = edgelist[ied].node(0);
		int n2 = edgelist[ied].node(1);
		edgelist[ied].flux(nodelist[n1].Po() - nodelist[n2].Po()); //only for ordering
	}
}

void REGION::computeedgefluxh_cvfe(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].fluxh(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				edgelist[iedge].fluxh(edgelist[iedge].fluxh() + fluxcvfe_); //flux positive means goes to i2
			}
		}
	}

	//for checking
	nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < edgelist.size(); ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		edgelist[ied].flux(nodelist[n1].Po() - nodelist[n2].Po());
		if (edgelist[ied].fluxh()*edgelist[ied].flux() < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1]+1;
			nodeedgemark[n2] = nodeedgemark[n2]+1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::computeedgefluxw_cvfeexup1d(){//md up using velocity
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge fluxw and store in edge.flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].flux(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();//small to big edgenode
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			/*length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);*/

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				//compute upwind fw
				double fwup_ = 0;
				double v1 = elementlist[ie].Ux_t();
				double v2 = elementlist[ie].Uy_t();
				double v3 = elementlist[ie].Uz_t();
				if (edx*v1 + edy*v2 + edz*v3>=0){
					fwup_ = nodelist[imin].fracw();
				}
				else{
					fwup_ = nodelist[imax].fracw();
				}
				edgelist[iedge].flux(edgelist[iedge].flux() + fluxcvfe_*fwup_); //flux positive means goes to i2
			}
		}
	}

	//for checking
	/*nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;*/
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::compute_cvfeexupmdnodelist(){//md up using velocity
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute nodes' upwind nodelist and weightinglist" << std::endl;
	upmdnodelist.resize(elementlist.size() * 6 * 2*3);
	upmdweightinglist.resize(elementlist.size() * 6 * 2*3);
	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].fluxh(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;
	int count = -1;
	for (ie = 0; ie < nelement; ie++){
		if (ie == 16836){
			izz = 1;
		}
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			/*length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);*/

			for (i = 0; i < 2; i++){//two faces of this edge
				count++;
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				//compute upwind fw
				double fwup_ = 0;
				double v1 = elementlist[ie].Ux_t();
				double v2 = elementlist[ie].Uy_t();
				double v3 = elementlist[ie].Uz_t();
				double m1 = (x1 + x2 + x3) / 3.0;//small face center
				double m2 = (y1 + y2 + y3) / 3.0;
				double m3 = (z1 + z2 + z3) / 3.0;
				int markinter = 0;
				for (int iii = 0; iii < 4; iii++){
					int ii0 = elementlist[ie].node(lpofa[iii][0]);
					int ii1 = elementlist[ie].node(lpofa[iii][1]);
					int ii2 = elementlist[ie].node(lpofa[iii][2]);
					fvec1[0] = nodelist[ii0].x() - nodelist[ii2].x();
					fvec1[1] = nodelist[ii0].y() - nodelist[ii2].y();
					fvec1[2] = nodelist[ii0].z() - nodelist[ii2].z();
					fvec2[0] = nodelist[ii1].x() - nodelist[ii2].x();
					fvec2[1] = nodelist[ii1].y() - nodelist[ii2].y();
					fvec2[2] = nodelist[ii1].z() - nodelist[ii2].z();
					vec_ = mathzz_.crossproduct(fvec1, fvec2);
					double vp1 = vec_[0];
					double vp2 = vec_[1];
					double vp3 = vec_[2];
					double n1 = nodelist[ii0].x();
					double n2 = nodelist[ii0].y();
					double n3 = nodelist[ii0].z();
					double vpt = v1 * vp1 + v2 * vp2 + v3 * vp3;
					if (vpt != 0){
						double tt = ((n1 - m1) * vp1 + (n2 - m2) * vp2 + (n3 - m3) * vp3) / vpt;
						double xinter = m1 + v1*tt;
						double yinter = m2 + v2*tt;
						double zinter = m3 + v3*tt;
						xx[0] = xinter;
						yy[0] = yinter;
						zz[0] = zinter;
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii1].x();
						yy[2] = nodelist[ii1].y();
						zz[2] = nodelist[ii1].z();
						double area1_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area2_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii1].x();
						yy[1] = nodelist[ii1].y();
						zz[1] = nodelist[ii1].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area3_ = mathzz_.area_triangle(xx, yy, zz);
						xx[0] = nodelist[ii0].x();
						yy[0] = nodelist[ii0].y();
						zz[0] = nodelist[ii0].z();
						double area_ = mathzz_.area_triangle(xx, yy, zz);
						if (std::abs(area1_ + area2_ + area3_ - area_) < 0.0000001*std::sqrt(area_)){
							double vvv1 = m1 - xinter;
							double vvv2 = m2 - yinter;
							double vvv3 = m3 - zinter;
							if (vvv1*v1 + vvv2*v2 + vvv3*v3>0){
								//fwup_ = (area1_*nodelist[ii2].fracw() + area2_*nodelist[ii1].fracw() + area3_*nodelist[ii0].fracw()) / area_;
								markinter = 1;
								upmdnodelist[count * 3] = ii2;
								upmdweightinglist[count * 3] = area1_ / area_;
								upmdnodelist[count * 3+1] = ii1;
								upmdweightinglist[count * 3] = area2_ / area_;
								upmdnodelist[count * 3+2] = ii0;
								upmdweightinglist[count * 3] = area3_ / area_;
								break;
							}
						}
					}
				}
				if (markinter == 0){
					std::cout << "marinter in multiupwind wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				//edgelist[iedge].fluxh(edgelist[iedge].fluxh() + fluxcvfe_*fwup_); //flux positive means goes to i2
			}
		}
	}
}


void REGION::computeedgefluxhw_cvfeexupmd_upmdnodelist(){//md up using velocity and upmdnodelist
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].fluxh(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;
	int count = -1;
	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			/*length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);*/

			for (i = 0; i < 2; i++){//two faces of this edge
				count++;
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				int inode1 = upmdnodelist[count * 3];
				int inode2 = upmdnodelist[count * 3+1];
				int inode3 = upmdnodelist[count * 3+2];
				double w1 = upmdweightinglist[count * 3];
				double w2 = upmdweightinglist[count * 3+1];
				double w3 = upmdweightinglist[count * 3+2];
				double fwup_ = w1*nodelist[inode1].fracw() + w2*nodelist[inode2].fracw() + w3*nodelist[inode3].fracw();
				edgelist[iedge].fluxh(edgelist[iedge].fluxh() + fluxcvfe_*fwup_); //flux positive means goes to i2
			}
		}
	}

}

void REGION::computeedgefluxw_cvfeexupmd(){//md up using velocity
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].flux(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				//compute upwind fw
				double fwup_ = 0;
				double v1 = elementlist[ie].Ux_t();//velocity
				double v2 = elementlist[ie].Uy_t();
				double v3 = elementlist[ie].Uz_t();
				double m1 = (x1 + x2 + x3) / 3.0;
				double m2 = (y1 + y2 + y3) / 3.0;
				double m3 = (z1 + z2 + z3) / 3.0;
				int markinter = 0;
				for (int iii = 0; iii < 4; iii++){
					int ii0 = elementlist[ie].node(lpofa[iii][0]);
					int ii1 = elementlist[ie].node(lpofa[iii][1]);
					int ii2 = elementlist[ie].node(lpofa[iii][2]);
					fvec1[0] = nodelist[ii0].x() - nodelist[ii2].x();
					fvec1[1] = nodelist[ii0].y() - nodelist[ii2].y();
					fvec1[2] = nodelist[ii0].z() - nodelist[ii2].z();
					fvec2[0] = nodelist[ii1].x() - nodelist[ii2].x();
					fvec2[1] = nodelist[ii1].y() - nodelist[ii2].y();
					fvec2[2] = nodelist[ii1].z() - nodelist[ii2].z();
					vec2_ = mathzz_.crossproduct(fvec1, fvec2);
					double vp1 = vec2_[0];//big face normal
					double vp2 = vec2_[1];
					double vp3 = vec2_[2];
					double n1 = nodelist[ii0].x();
					double n2 = nodelist[ii0].y();
					double n3 = nodelist[ii0].z();
					double vpt = v1 * vp1 + v2 * vp2 + v3 * vp3;
					if (vpt != 0){
						double tt = ((n1 - m1) * vp1 + (n2 - m2) * vp2 + (n3 - m3) * vp3) / vpt;
						double xinter = m1 + v1*tt;
						double yinter = m2 + v2*tt;
						double zinter = m3 + v3*tt;
						xx[0] = xinter;
						yy[0] = yinter;
						zz[0] = zinter;
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii1].x();
						yy[2] = nodelist[ii1].y();
						zz[2] = nodelist[ii1].z();
						double area1_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area2_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii1].x();
						yy[1] = nodelist[ii1].y();
						zz[1] = nodelist[ii1].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area3_ = mathzz_.area_triangle(xx, yy, zz);
						xx[0] = nodelist[ii0].x();
						yy[0] = nodelist[ii0].y();
						zz[0] = nodelist[ii0].z();
						double area_ = mathzz_.area_triangle(xx, yy, zz);
						if (std::abs(area1_ + area2_ + area3_ - area_) < 0.0000001*std::sqrt(area_)){
							double vvv1 = m1 - xinter;
							double vvv2 = m2 - yinter;
							double vvv3 = m3 - zinter;
							if (vvv1*v1 + vvv2*v2 + vvv3*v3>0){
								fwup_ = (area1_*nodelist[ii2].fracw() + area2_*nodelist[ii1].fracw() + area3_*nodelist[ii0].fracw()) / area_;
								markinter = 1;
								break;
							}
						}
					}
				}
				if (markinter == 0){
					std::cout << "marinter in multiupwind wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				edgelist[iedge].flux(edgelist[iedge].flux() + fluxcvfe_*fwup_); //flux positive means goes to i2
			}
		}
	}

	//for checking
	nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}



void REGION::computeedgefluxhw_cvfeexupmd(){//md up using velocity
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].fluxh(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				//compute upwind fw
				double fwup_ = 0;
				double v1 = elementlist[ie].Ux_t();//velocity which is line normal
				double v2 = elementlist[ie].Uy_t();
				double v3 = elementlist[ie].Uz_t();
				double m1 = (x1 + x2 + x3) / 3.0;//mid point of small face, also a node on line
				double m2 = (y1 + y2 + y3) / 3.0;
				double m3 = (z1 + z2 + z3) / 3.0;
				int markinter = 0;
				for (int iii = 0; iii < 4; iii++){
					int ii0 = elementlist[ie].node(lpofa[iii][0]);
					int ii1 = elementlist[ie].node(lpofa[iii][1]);
					int ii2 = elementlist[ie].node(lpofa[iii][2]);
					fvec1[0] = nodelist[ii0].x() - nodelist[ii2].x();
					fvec1[1] = nodelist[ii0].y() - nodelist[ii2].y();
					fvec1[2] = nodelist[ii0].z() - nodelist[ii2].z();
					fvec2[0] = nodelist[ii1].x() - nodelist[ii2].x();
					fvec2[1] = nodelist[ii1].y() - nodelist[ii2].y();
					fvec2[2] = nodelist[ii1].z() - nodelist[ii2].z();
					vec_ = mathzz_.crossproduct(fvec1, fvec2);
					double vp1 = vec_[0];//big face normal
					double vp2 = vec_[1];
					double vp3 = vec_[2];
					double n1 = nodelist[ii0].x();//a node on face
					double n2 = nodelist[ii0].y();
					double n3 = nodelist[ii0].z();
					double vpt = v1 * vp1 + v2 * vp2 + v3 * vp3;
					if (vpt != 0){
						double tt = ((n1 - m1) * vp1 + (n2 - m2) * vp2 + (n3 - m3) * vp3) / vpt;
						double xinter = m1 + v1*tt;
						double yinter = m2 + v2*tt;
						double zinter = m3 + v3*tt;
						xx[0] = xinter;
						yy[0] = yinter;
						zz[0] = zinter;
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii1].x();
						yy[2] = nodelist[ii1].y();
						zz[2] = nodelist[ii1].z();
						double area1_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area2_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii1].x();
						yy[1] = nodelist[ii1].y();
						zz[1] = nodelist[ii1].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area3_ = mathzz_.area_triangle(xx, yy, zz);
						xx[0] = nodelist[ii0].x();
						yy[0] = nodelist[ii0].y();
						zz[0] = nodelist[ii0].z();
						double area_ = mathzz_.area_triangle(xx, yy, zz);
						if (std::abs(area1_ + area2_ + area3_ - area_) < 0.0000001*std::sqrt(area_)){
							double vvv1 = m1 - xinter;
							double vvv2 = m2 - yinter;
							double vvv3 = m3 - zinter;
							if (vvv1*v1 + vvv2*v2 + vvv3*v3>0){
								fwup_ = (area1_*nodelist[ii2].fracw() + area2_*nodelist[ii1].fracw() + area3_*nodelist[ii0].fracw()) / area_;
								markinter = 1;
								break;
							}
						}
					}
				}
				if (markinter == 0){
					std::cout << "marinter in multiupwind wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				edgelist[iedge].fluxh(edgelist[iedge].fluxh() + fluxcvfe_*fwup_); //flux positive means goes to i2
			}
		}
	}

	////for checking
	//nodeedgemark.resize(nodelist.size());
	//int cc = 0;
	//for (ied = 0; ied < nedge; ied++){
	//	n1 = edgelist[ied].node(0);
	//	n2 = edgelist[ied].node(1);
	//	if (edgelist[ied].flux()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
	//		cc++;
	//		nodeedgemark[n1] = nodeedgemark[n1] + 1;
	//		nodeedgemark[n2] = nodeedgemark[n2] + 1;
	//	}
	//}
	//std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	//std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::computeedgefluxw_cvfeexupmd2(){//md up using subface normal
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].flux(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				//compute upwind fw
				double fwup_ = 0;
				double v1 = vec_[0];
				double v2 = vec_[1];
				double v3 = vec_[2];
				if (v1*elementlist[ie].Ux_t() + v2*elementlist[ie].Uy_t() + v3*elementlist[ie].Uz_t() < 0){
					v1 = -v1;
					v2 = -v2;
					v3 = -v3;
				}
				double m1 = (x1 + x2 + x3) / 3.0;//small face mid
				double m2 = (y1 + y2 + y3) / 3.0;
				double m3 = (z1 + z2 + z3) / 3.0;
				int markinter = 0;
				for (int iii = 0; iii < 4; iii++){
					int ii0 = elementlist[ie].node(lpofa[iii][0]);
					int ii1 = elementlist[ie].node(lpofa[iii][1]);
					int ii2 = elementlist[ie].node(lpofa[iii][2]);
					fvec1[0] = nodelist[ii0].x() - nodelist[ii2].x();
					fvec1[1] = nodelist[ii0].y() - nodelist[ii2].y();
					fvec1[2] = nodelist[ii0].z() - nodelist[ii2].z();
					fvec2[0] = nodelist[ii1].x() - nodelist[ii2].x();
					fvec2[1] = nodelist[ii1].y() - nodelist[ii2].y();
					fvec2[2] = nodelist[ii1].z() - nodelist[ii2].z();
					vec_ = mathzz_.crossproduct(fvec1, fvec2);
					double vp1 = vec_[0];//big face normal vector
					double vp2 = vec_[1];
					double vp3 = vec_[2];
					double n1 = nodelist[ii0].x();//a node on big face
					double n2 = nodelist[ii0].y();
					double n3 = nodelist[ii0].z();
					double vpt = v1 * vp1 + v2 * vp2 + v3 * vp3;//v is small face normal
					if (vpt != 0){
						double tt = ((n1 - m1) * vp1 + (n2 - m2) * vp2 + (n3 - m3) * vp3) / vpt;//projection
						double xinter = m1 + v1*tt;
						double yinter = m2 + v2*tt;
						double zinter = m3 + v3*tt;
						xx[0] = xinter;
						yy[0] = yinter;
						zz[0] = zinter;
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii1].x();
						yy[2] = nodelist[ii1].y();
						zz[2] = nodelist[ii1].z();
						double area1_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area2_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii1].x();
						yy[1] = nodelist[ii1].y();
						zz[1] = nodelist[ii1].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area3_ = mathzz_.area_triangle(xx, yy, zz);
						xx[0] = nodelist[ii0].x();
						yy[0] = nodelist[ii0].y();
						zz[0] = nodelist[ii0].z();
						double area_ = mathzz_.area_triangle(xx, yy, zz);
						if (std::abs(area1_ + area2_ + area3_ - area_) < 0.0000001*std::sqrt(area_)){
							double vvv1 = m1 - xinter;
							double vvv2 = m2 - yinter;
							double vvv3 = m3 - zinter;
							if (vvv1*v1 + vvv2*v2 + vvv3*v3>0){
								fwup_ = (area1_*nodelist[ii2].fracw() + area2_*nodelist[ii1].fracw() + area3_*nodelist[ii0].fracw()) / area_;
								markinter = 1;
								break;
							}
						}
					}
				}
				if (markinter == 0){
					std::cout << "marinter in multiupwind wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				edgelist[iedge].flux(edgelist[iedge].flux() + fluxcvfe_*fwup_); //flux positive means goes to i2
			}
		}
	}

	//for checking
	nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::computeedgefluxw_cvfeexupmd3(){//md up using velocity
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].flux(0);
		edgelist[i].fluxh(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				//compute upwind fw
				double fwup_ = 0;
				double v1 = elementlist[ie].Ux_t();
				double v2 = elementlist[ie].Uy_t();
				double v3 = elementlist[ie].Uz_t();
				double m1 = (x1 + x2 + x3) / 3.0;
				double m2 = (y1 + y2 + y3) / 3.0;
				double m3 = (z1 + z2 + z3) / 3.0;
				int markinter = 0;
				for (int iii = 0; iii < 4; iii++){
					int ii0 = elementlist[ie].node(lpofa[iii][0]);
					int ii1 = elementlist[ie].node(lpofa[iii][1]);
					int ii2 = elementlist[ie].node(lpofa[iii][2]);
					fvec1[0] = nodelist[ii0].x() - nodelist[ii2].x();
					fvec1[1] = nodelist[ii0].y() - nodelist[ii2].y();
					fvec1[2] = nodelist[ii0].z() - nodelist[ii2].z();
					fvec2[0] = nodelist[ii1].x() - nodelist[ii2].x();
					fvec2[1] = nodelist[ii1].y() - nodelist[ii2].y();
					fvec2[2] = nodelist[ii1].z() - nodelist[ii2].z();
					vec_ = mathzz_.crossproduct(fvec1, fvec2);
					double vp1 = vec_[0];
					double vp2 = vec_[1];
					double vp3 = vec_[2];
					double n1 = nodelist[ii0].x();
					double n2 = nodelist[ii0].y();
					double n3 = nodelist[ii0].z();
					double vpt = v1 * vp1 + v2 * vp2 + v3 * vp3;
					if (vpt != 0){
						double tt = ((n1 - m1) * vp1 + (n2 - m2) * vp2 + (n3 - m3) * vp3) / vpt;
						double xinter = m1 + v1*tt;
						double yinter = m2 + v2*tt;
						double zinter = m3 + v3*tt;
						xx[0] = xinter;
						yy[0] = yinter;
						zz[0] = zinter;
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii1].x();
						yy[2] = nodelist[ii1].y();
						zz[2] = nodelist[ii1].z();
						double area1_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area2_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii1].x();
						yy[1] = nodelist[ii1].y();
						zz[1] = nodelist[ii1].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area3_ = mathzz_.area_triangle(xx, yy, zz);
						xx[0] = nodelist[ii0].x();
						yy[0] = nodelist[ii0].y();
						zz[0] = nodelist[ii0].z();
						double area_ = mathzz_.area_triangle(xx, yy, zz);
						if (std::abs(area1_ + area2_ + area3_ - area_) < 0.0000001*std::sqrt(area_)){
							double vvv1 = m1 - xinter;
							double vvv2 = m2 - yinter;
							double vvv3 = m3 - zinter;
							if (vvv1*v1 + vvv2*v2 + vvv3*v3>0){
								fwup_ = (area1_*nodelist[ii2].fracw() + area2_*nodelist[ii1].fracw() + area3_*nodelist[ii0].fracw()) / area_;
								markinter = 1;
								break;
							}
						}
					}
				}
				if (markinter == 0){
					std::cout << "marinter in multiupwind wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				edgelist[iedge].flux(edgelist[iedge].flux() + fluxcvfe_); //flux positive means goes to i2
				edgelist[iedge].fluxh(edgelist[iedge].fluxh() + fluxcvfe_*fwup_);
			}
		}
	}

	//for checking
	nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::computeedgefluxw_cvfeexupmd4(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	double fvec1[3], fvec2[3], xx[3], yy[3], zz[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].flux(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				//compute upwind fw
				double fwup1d_ = 0;
				double fwup_ = 0;
				double v1 = elementlist[ie].Ux_t();
				double v2 = elementlist[ie].Uy_t();
				double v3 = elementlist[ie].Uz_t();
				if (edx*v1 + edy*v2 + edz*v3 >= 0){
					fwup1d_ = nodelist[imin].fracw();
				}
				else{
					fwup1d_ = nodelist[imax].fracw();
				}
				double m1 = (x1 + x2 + x3) / 3.0;
				double m2 = (y1 + y2 + y3) / 3.0;
				double m3 = (z1 + z2 + z3) / 3.0;
				int markinter = 0;
				for (int iii = 0; iii < 4; iii++){
					int ii0 = elementlist[ie].node(lpofa[iii][0]);
					int ii1 = elementlist[ie].node(lpofa[iii][1]);
					int ii2 = elementlist[ie].node(lpofa[iii][2]);
					fvec1[0] = nodelist[ii0].x() - nodelist[ii2].x();
					fvec1[1] = nodelist[ii0].y() - nodelist[ii2].y();
					fvec1[2] = nodelist[ii0].z() - nodelist[ii2].z();
					fvec2[0] = nodelist[ii1].x() - nodelist[ii2].x();
					fvec2[1] = nodelist[ii1].y() - nodelist[ii2].y();
					fvec2[2] = nodelist[ii1].z() - nodelist[ii2].z();
					vec_ = mathzz_.crossproduct(fvec1, fvec2);
					double vp1 = vec_[0];
					double vp2 = vec_[1];
					double vp3 = vec_[2];
					double n1 = nodelist[ii0].x();
					double n2 = nodelist[ii0].y();
					double n3 = nodelist[ii0].z();
					double vpt = v1 * vp1 + v2 * vp2 + v3 * vp3;
					if (vpt != 0){
						double tt = ((n1 - m1) * vp1 + (n2 - m2) * vp2 + (n3 - m3) * vp3) / vpt;
						double xinter = m1 + v1*tt;
						double yinter = m2 + v2*tt;
						double zinter = m3 + v3*tt;
						xx[0] = xinter;
						yy[0] = yinter;
						zz[0] = zinter;
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii1].x();
						yy[2] = nodelist[ii1].y();
						zz[2] = nodelist[ii1].z();
						double area1_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii0].x();
						yy[1] = nodelist[ii0].y();
						zz[1] = nodelist[ii0].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area2_ = mathzz_.area_triangle(xx, yy, zz);
						xx[1] = nodelist[ii1].x();
						yy[1] = nodelist[ii1].y();
						zz[1] = nodelist[ii1].z();
						xx[2] = nodelist[ii2].x();
						yy[2] = nodelist[ii2].y();
						zz[2] = nodelist[ii2].z();
						double area3_ = mathzz_.area_triangle(xx, yy, zz);
						xx[0] = nodelist[ii0].x();
						yy[0] = nodelist[ii0].y();
						zz[0] = nodelist[ii0].z();
						double area_ = mathzz_.area_triangle(xx, yy, zz);
						if (std::abs(area1_ + area2_ + area3_ - area_) < 0.0000001*std::sqrt(area_)){
							double vvv1 = m1 - xinter;
							double vvv2 = m2 - yinter;
							double vvv3 = m3 - zinter;
							if (vvv1*v1 + vvv2*v2 + vvv3*v3>0){
								fwup_ = (area1_*nodelist[ii2].fracw() + area2_*nodelist[ii1].fracw() + area3_*nodelist[ii0].fracw()) / area_;
								markinter = 1;
								break;
							}
						}
					}
				}
				if (markinter == 0){
					std::cout << "marinter in multiupwind wrong" << std::endl;
					std::exit(EXIT_FAILURE);
				}
				edgelist[iedge].flux(edgelist[iedge].flux() + fluxcvfe_*(fwup_+fwup1d_)*0.5); //flux positive means goes to i2
			}
		}
	}

	//for checking
	/*nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;*/

}

void REGION::computeedgeflux_cvfe(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE flux" << std::endl;

	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].flux(0);
	}

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_t()*vec_[0] + elementlist[ie].Uy_t()*vec_[1] + elementlist[ie].Uz_t()*vec_[2])*coef*area;
				edgelist[iedge].flux(edgelist[iedge].flux() + fluxcvfe_); //flux positive means goes to i2
			}
		}
	}

	//for checking
	nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].flux()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::averageedgelength(){
	double avedgelength_ = 0;
	int nedge = edgelist.size();
	for (int iedge = 0; iedge < nedge; iedge++){
		int n1 = edgelist[iedge].node(0);
		int n2 = edgelist[iedge].node(1);
		avedgelength_ = avedgelength_ + edgelist[iedge].length();
	}
	avedgelength_ = avedgelength_ / nedge;
	std::cout << "Average Edge Length= " << avedgelength_ << std::endl;
}

void REGION::computeedgeflux_cvfe_w(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE water flux" << std::endl;
	//for check total volume
	//std::vector<double> dualV;
	//dualV.resize(nnode);
	nelement = elementlist.size();
	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].flux(0);
	}

	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_w()*vec_[0] + elementlist[ie].Uy_w()*vec_[1] + elementlist[ie].Uz_w()*vec_[2])*coef*area;
				edgelist[iedge].flux(edgelist[iedge].flux() + fluxcvfe_); //flux positive means goes to i2
			}
		}
	}

	//for checking
	nodeedgemark.clear();
	nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].fluxh()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::computeedgefluxh_cvfe_w(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, volumei2, coef, coef2, edx, edy, edz, area, area2, length_, fluxcvfe_;
	double edgevec1[3], edgevec2[3];
	std::vector<double> vec_, vec2_;
	vec2_.resize(3);
	std::cout << "Compute edge CVFE water flux" << std::endl;
	//for check total volume
	//std::vector<double> dualV;
	//dualV.resize(nnode);
	nelement = elementlist.size();
	for (i = 0; i < edgelist.size(); i++){
		edgelist[i].fluxh(0);
	}

	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.unitnormal(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				area = 0.5*mathzz_.area_vector(edgevec1, edgevec2);
				fluxcvfe_ = (elementlist[ie].Ux_w()*vec_[0] + elementlist[ie].Uy_w()*vec_[1] + elementlist[ie].Uz_w()*vec_[2])*coef*area;
				edgelist[iedge].fluxh(edgelist[iedge].fluxh() + fluxcvfe_); //flux positive means goes to i2
			}
		}
	}

	//for checking
	nodeedgemark.clear();
	nodeedgemark.resize(nodelist.size());
	int cc = 0;
	for (ied = 0; ied < nedge; ied++){
		n1 = edgelist[ied].node(0);
		n2 = edgelist[ied].node(1);
		if (edgelist[ied].fluxh()*(nodelist[n1].Po() - nodelist[n2].Po()) < 0){
			cc++;
			nodeedgemark[n1] = nodeedgemark[n1] + 1;
			nodeedgemark[n2] = nodeedgemark[n2] + 1;
		}
	}
	std::cout << "number of edges with unphysical flux: " << cc << std::endl;
	std::cout << "total number of edges: " << edgelist.size() << std::endl;

}

void REGION::graphtracingsign(int i){
	graph.tracingsign(i);
}

void REGION::computeedgesxsysz_and_k(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, coef, edx, edy, edz, area_;
	double edgevec1[3], edgevec2[3];
	std::vector<double> vec_;
	std::cout << "Compute edge sx sy sz and permeability" << std::endl;

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < edgelist.size(); ie++){
		edgelist[ie].sx(0);
		edgelist[ie].sy(0);
		edgelist[ie].sz(0);
	}

	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			edgelist[iedge].addsuele(ie);
			for (i = 0; i < 3; i++){
				edgelist[iedge].k(i, i, edgelist[iedge].k(i, i) + elementlist[ie].k(i, i));
			}
			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			//double length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			///*edx = edx / length_;
			//edy = edy / length_;
			//edz = edz / length_;*/
			//edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;
				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.crossproduct(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				sx = vec_[0] * coef*0.5;
				sy = vec_[1] * coef*0.5;
				sz = vec_[2] * coef*0.5;

				edgelist[iedge].sx(sx + edgelist[iedge].sx());
				edgelist[iedge].sy(sy + edgelist[iedge].sy());
				edgelist[iedge].sz(sz + edgelist[iedge].sz());

			}
		}
	}

	for (ie = 0; ie < edgelist.size(); ie++){
		for (i = 0; i < 3; i++){
			edgelist[ie].k(i, i, edgelist[ie].k(i, i) / edgelist[ie].numberofsuele());
		}
		sx = edgelist[ie].sx();
		sy = edgelist[ie].sy();
		sz = edgelist[ie].sz();
		double area_ = std::sqrt(sx*sx + sy*sy + sz*sz);
		edgelist[ie].area(area_);
	}
}

void REGION::computeedgelength(){

	for (int i = 0; i < edgelist.size(); i++){
		int i1 = edgelist[i].node(0);
		int i2 = edgelist[i].node(1);
		double edx = nodelist[i2].x(); -nodelist[i1].x();
		double edy = nodelist[i2].y() - nodelist[i1].y();
		double edz = nodelist[i2].z() - nodelist[i1].z();
		double length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
		edgelist[i].length(length_);
	}





}

void REGION::computeaveragefmmv(){
	fmm_avvx = 0;
	fmm_avvy = 0;
	fmm_avvz = 0;
	for (int ie = 0; ie < edgelist.size(); ie++){
		fmm_avvx += edgelist[ie].fmmv(0);
		fmm_avvy += edgelist[ie].fmmv(1);
		fmm_avvz += edgelist[ie].fmmv(2);
	}
	fmm_avvx = fmm_avvx / edgelist.size();
	fmm_avvy = fmm_avvy / edgelist.size();
	fmm_avvz = fmm_avvz / edgelist.size();
}

void REGION::computeaveragefmmvcar(){
	fmm_avvx = 0;
	fmm_avvy = 0;
	fmm_avvz = 0;
	for (int ie = 0; ie < cpgelementlist.size(); ie++){
		fmm_avvx += cpgelementlist[ie].fmmv(0);
		fmm_avvy += cpgelementlist[ie].fmmv(1);
		fmm_avvz += cpgelementlist[ie].fmmv(2);
	}
	fmm_avvx = fmm_avvx / cpgelementlist.size();
	fmm_avvy = fmm_avvy / cpgelementlist.size();
	fmm_avvz = fmm_avvz / cpgelementlist.size();
}

void REGION::averagefmmv2iso(){
	double ff = (fmm_avvx + fmm_avvy + fmm_avvz) / 3.0;
	fmm_avvx = ff;
	fmm_avvy = ff;
	fmm_avvz = ff;
}

void REGION::setfmmv_tet_properties(){
	for (int ie = 0; ie < elementlist.size(); ie++){
		double fx = std::sqrt(elementlist[ie].k(0, 0) / mu_o / ct_ / elementlist[ie].porosity());
		double fy = std::sqrt(elementlist[ie].k(1, 1) / mu_o / ct_ / elementlist[ie].porosity());
		double fz = std::sqrt(elementlist[ie].k(2, 2) / mu_o / ct_ / elementlist[ie].porosity());
		elementlist[ie].fmmv(0, fx);
		elementlist[ie].fmmv(1, fy);
		elementlist[ie].fmmv(2, fz);
	}
}

void REGION::computeedgesxsysz_and_fmmv(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, coef, edx, edy, edz;
	double edgevec1[3], edgevec2[3];
	std::vector<double> vec_;
	std::cout << "Compute edge sx sy sz and fmm velocity**2" << std::endl;

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;

	for (ie = 0; ie < edgelist.size(); ie++){
		edgelist[ie].sx(0);
		edgelist[ie].sy(0);
		edgelist[ie].sz(0);
	}


	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			edgelist[iedge].addsuele(ie);
			edgelist[iedge].fmmv(0, edgelist[iedge].fmmv(0) + elementlist[ie].fmmv(0));
			edgelist[iedge].fmmv(1, edgelist[iedge].fmmv(1) + elementlist[ie].fmmv(1));
			edgelist[iedge].fmmv(2, edgelist[iedge].fmmv(2) + elementlist[ie].fmmv(2));

			x1 = 0.5*(nodelist[n1].x() + nodelist[n2].x()); //edge mid
			y1 = 0.5*(nodelist[n1].y() + nodelist[n2].y());
			z1 = 0.5*(nodelist[n1].z() + nodelist[n2].z());
			x4 = nodelist[imax].x(); //bigger node end
			y4 = nodelist[imax].y();
			z4 = nodelist[imax].z();
			edx = x4 - nodelist[imin].x();
			edy = y4 - nodelist[imin].y();
			edz = z4 - nodelist[imin].z();
			double length_ = std::sqrt(edx*edx + edy*edy + edz*edz);
			/*edx = edx / length_;
			edy = edy / length_;
			edz = edz / length_;*/
			edgelist[iedge].length(length_);

			for (i = 0; i < 2; i++){//two faces of this edge
				fe = ilfe[ied][i];
				k1 = elementlist[ie].face(fe, 0);
				k2 = elementlist[ie].face(fe, 1);
				k3 = elementlist[ie].face(fe, 2);
				x3 = (nodelist[k1].x() + nodelist[k2].x() + nodelist[k3].x()) / 3.0; //face mid
				y3 = (nodelist[k1].y() + nodelist[k2].y() + nodelist[k3].y()) / 3.0;
				z3 = (nodelist[k1].z() + nodelist[k2].z() + nodelist[k3].z()) / 3.0;

				edgevec1[0] = x1 - x2;
				edgevec1[1] = y1 - y2;
				edgevec1[2] = z1 - z2;
				edgevec2[0] = x3 - x2;
				edgevec2[1] = y3 - y2;
				edgevec2[2] = z3 - z2;
				vec_ = mathzz_.crossproduct(edgevec1, edgevec2);
				coef = 1.0;
				if ((vec_[0] * edx + vec_[1] * edy + vec_[2] * edz) < 0){ //normal point to edge second node
					coef = -1.0;
				}
				sx = vec_[0] * coef*0.5;
				sy = vec_[1] * coef*0.5;
				sz = vec_[2] * coef*0.5;

				edgelist[iedge].sx(sx + edgelist[iedge].sx());
				edgelist[iedge].sy(sy + edgelist[iedge].sy());
				edgelist[iedge].sz(sz + edgelist[iedge].sz());
			}
		}
	}

	for (ie = 0; ie < edgelist.size(); ie++){
		for (i = 0; i < 3; i++){
			edgelist[ie].fmmv(i, edgelist[ie].fmmv(i) / edgelist[ie].numberofsuele());
		}
		sx = edgelist[ie].sx();
		sy = edgelist[ie].sy();
		sz = edgelist[ie].sz();
		double area_ = std::sqrt(sx*sx + sy*sy + sz*sz);
		edgelist[ie].area(area_);
	}


}

void REGION::computeedgefmmv(){
	int i, j, ie, ied, iedge, n1, n2, imin, imax, ied2, iedge2, fe, m1, m2, m3, m4, k1, k2, k3, inode, jnode, nelement;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, sx, sy, sz, coef, edx, edy, edz;
	double edgevec1[3], edgevec2[3];
	std::vector<double> vec_;
	std::cout << "Compute edge fmmv" << std::endl;

	nelement = elementlist.size();
	lpoed.resize(6);
	ilfe.resize(6);
	for (i = 0; i < 6; i++){
		lpoed[i].resize(2);
		ilfe[i].resize(2);
	}
	lpofa.resize(4);
	for (i = 0; i < 4; i++){
		lpofa[i].resize(3);
	}


	//ordered points of edges locally
	lpoed[0][0] = 0;
	lpoed[0][1] = 1;
	lpoed[1][0] = 1;
	lpoed[1][1] = 2;
	lpoed[2][0] = 2;
	lpoed[2][1] = 0;
	lpoed[3][0] = 0;
	lpoed[3][1] = 3;
	lpoed[4][0] = 1;
	lpoed[4][1] = 3;
	lpoed[5][0] = 2;
	lpoed[5][1] = 3;

	//ordered faces of edges locally
	ilfe[0][0] = 3;
	ilfe[0][1] = 2;
	ilfe[1][0] = 3;
	ilfe[1][1] = 0;
	ilfe[2][0] = 1;
	ilfe[2][1] = 3;
	ilfe[3][0] = 2;
	ilfe[3][1] = 1;
	ilfe[4][0] = 0;
	ilfe[4][1] = 2;
	ilfe[5][0] = 1;
	ilfe[5][1] = 0;

	//ordered nodes of faces locally
	lpofa[0][0] = 1;
	lpofa[0][1] = 2;
	lpofa[0][2] = 3;
	lpofa[1][0] = 2;
	lpofa[1][1] = 0;
	lpofa[1][2] = 3;
	lpofa[2][0] = 0;
	lpofa[2][1] = 1;
	lpofa[2][2] = 3;
	lpofa[3][0] = 0;
	lpofa[3][1] = 2;
	lpofa[3][2] = 1;


	for (ie = 0; ie < nelement; ie++){
		m1 = elementlist[ie].node(0);
		m2 = elementlist[ie].node(1);
		m3 = elementlist[ie].node(2);
		m4 = elementlist[ie].node(3);
		x2 = 0.25*(nodelist[m1].x() + nodelist[m2].x() + nodelist[m3].x() + nodelist[m4].x()); //element mid
		y2 = 0.25*(nodelist[m1].y() + nodelist[m2].y() + nodelist[m3].y() + nodelist[m4].y());
		z2 = 0.25*(nodelist[m1].z() + nodelist[m2].z() + nodelist[m3].z() + nodelist[m4].z());
		for (ied = 0; ied < 6; ied++){
			n1 = elementlist[ie].node(lpoed[ied][0]);
			n2 = elementlist[ie].node(lpoed[ied][1]);
			imax = max(n1, n2);
			imin = n1 + n2 - imax;
			for (ied2 = 0; ied2 < nodelist[imin].numberofedgeout(); ied2++){//find corresponding global edge
				iedge2 = nodelist[imin].edgeout(ied2);
				if (edgelist[iedge2].node(1) == imax){
					iedge = iedge2;
					break;
				}
			}
			edgelist[iedge].addsuele(ie);
			edgelist[iedge].fmmv(0, edgelist[iedge].fmmv(0) + elementlist[ie].fmmv(0));
			edgelist[iedge].fmmv(1, edgelist[iedge].fmmv(1) + elementlist[ie].fmmv(1));
			edgelist[iedge].fmmv(2, edgelist[iedge].fmmv(2) + elementlist[ie].fmmv(2));

		}
	}

	for (ie = 0; ie < edgelist.size(); ie++){
		for (i = 0; i < 3; i++){
			edgelist[ie].fmmv(i, edgelist[ie].fmmv(i) / edgelist[ie].numberofsuele());
		}

	}


}

void REGION::computeedgetrans_tpfa_sf(){
	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		double sx = edgelist[iedge].sx();
		double sy = edgelist[iedge].sy();
		double sz = edgelist[iedge].sz();
		double c12 = (edgelist[iedge].k(0, 0)*sx + edgelist[iedge].k(1, 1)*sy + edgelist[iedge].k(2, 2)*sz) / std::sqrt(sx*sx + sy*sy + sz*sz);
		int i1 = edgelist[iedge].node(0);
		int i2 = edgelist[iedge].node(1);
		double x1 = nodelist[i1].x();
		double y1 = nodelist[i1].y();
		double z1 = nodelist[i1].z();
		double x2 = nodelist[i2].x();
		double y2 = nodelist[i2].y();
		double z2 = nodelist[i2].z();
		double xm = 0.5*(x1 + x2);
		double ym = 0.5*(y1 + y2);
		double zm = 0.5*(z1 + z2);
		double vi1mx = xm - x1;
		double vi1my = ym - y1;
		double vi1mz = zm - z1;
		double Ti1m = c12*(vi1mx*sx + vi1my*sy + vi1mz*sz) / std::sqrt(vi1mx*vi1mx + vi1my*vi1my + vi1mz*vi1mz);
		double vmi2x = x2 - xm;
		double vmi2y = y2 - ym;
		double vmi2z = z2 - zm;
		double Tmi2 = c12*(vmi2x*sx + vmi2y*sy + vmi2z*sz) / std::sqrt(vmi2x*vmi2x + vmi2y*vmi2y + vmi2z*vmi2z);
		double T12 = 1.0 / (1.0 / Ti1m + 1.0 / Tmi2);
		edgelist[iedge].trans(std::abs(T12));
	}
}

//void REGION::computeedgetrans_mf(){
//	for (int iedge = 0; iedge < edgelist.size(); iedge++){
//		int i1 = edgelist[iedge].node(0);
//		int i2 = edgelist[iedge].node(1);
//		double mobit = 0.5*(nodelist[i1].mobit() + nodelist[i2].mobit());
//		double sx = edgelist[iedge].sx();
//		double sy = edgelist[iedge].sy();
//		double sz = edgelist[iedge].sz();
//		double c12 = mobit*(edgelist[iedge].k(0, 0)*sx + edgelist[iedge].k(1, 1)*sy + edgelist[iedge].k(2, 2)*sz) / std::sqrt(sx*sx + sy*sy + sz*sz);
//		double x1 = nodelist[i1].x();
//		double y1 = nodelist[i1].y();
//		double z1 = nodelist[i1].z();
//		double x2 = nodelist[i2].x();
//		double y2 = nodelist[i2].y();
//		double z2 = nodelist[i2].z();
//		double xm = 0.5*(x1 + x2);
//		double ym = 0.5*(y1 + y2);
//		double zm = 0.5*(z1 + z2);
//		double vi1mx = xm - x1;
//		double vi1my = ym - y1;
//		double vi1mz = zm - z1;
//		double Ti1m = c12*(vi1mx*sx + vi1my*sy + vi1mz*sz) / std::sqrt(vi1mx*vi1mx + vi1my*vi1my + vi1mz*vi1mz);
//		double vmi2x = x2 - xm;
//		double vmi2y = y2 - ym;
//		double vmi2z = z2 - zm;
//		double Tmi2 = c12*(vmi2x*sx + vmi2y*sy + vmi2z*sz) / std::sqrt(vmi2x*vmi2x + vmi2y*vmi2y + vmi2z*vmi2z);
//		double T12 = 1.0 / (1.0 / Ti1m + 1.0 / Tmi2);
//		edgelist[iedge].trans(std::abs(T12));
//	}
//}

void REGION::computeedgetrans_tpfa_mf(){
	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		int i1 = edgelist[iedge].node(0);
		int i2 = edgelist[iedge].node(1);
		double sx = edgelist[iedge].sx();
		double sy = edgelist[iedge].sy();
		double sz = edgelist[iedge].sz();
		double c1 = nodelist[i1].mobit()*(edgelist[iedge].k(0, 0)*sx + edgelist[iedge].k(1, 1)*sy + edgelist[iedge].k(2, 2)*sz) / std::sqrt(sx*sx + sy*sy + sz*sz);
		double c2 = nodelist[i2].mobit()*(edgelist[iedge].k(0, 0)*sx + edgelist[iedge].k(1, 1)*sy + edgelist[iedge].k(2, 2)*sz) / std::sqrt(sx*sx + sy*sy + sz*sz);
		double x1 = nodelist[i1].x();
		double y1 = nodelist[i1].y();
		double z1 = nodelist[i1].z();
		double x2 = nodelist[i2].x();
		double y2 = nodelist[i2].y();
		double z2 = nodelist[i2].z();
		double xm = 0.5*(x1 + x2);
		double ym = 0.5*(y1 + y2);
		double zm = 0.5*(z1 + z2);
		double vi1mx = xm - x1;
		double vi1my = ym - y1;
		double vi1mz = zm - z1;
		double Ti1m = c1*(vi1mx*sx + vi1my*sy + vi1mz*sz) / std::sqrt(vi1mx*vi1mx + vi1my*vi1my + vi1mz*vi1mz);
		double vmi2x = x2 - xm;
		double vmi2y = y2 - ym;
		double vmi2z = z2 - zm;
		double Tmi2 = c2*(vmi2x*sx + vmi2y*sy + vmi2z*sz) / std::sqrt(vmi2x*vmi2x + vmi2y*vmi2y + vmi2z*vmi2z);
		double T12 = 1.0 / (1.0 / Ti1m + 1.0 / Tmi2);
		edgelist[iedge].trans(std::abs(T12));
	}
}

void REGION::computeedgetrans_tpfa_mf_nodalperm(){
	for (int iedge = 0; iedge < edgelist.size(); iedge++){
		int i1 = edgelist[iedge].node(0);
		int i2 = edgelist[iedge].node(1);
		double sx = edgelist[iedge].sx();
		double sy = edgelist[iedge].sy();
		double sz = edgelist[iedge].sz();
		double c1 = nodelist[i1].mobit()*(nodelist[i1].k(0, 0)*sx + nodelist[i1].k(1, 1)*sy + nodelist[i1].k(2, 2)*sz) / std::sqrt(sx*sx + sy*sy + sz*sz);
		double c2 = nodelist[i2].mobit()*(nodelist[i2].k(0, 0)*sx + nodelist[i2].k(1, 1)*sy + nodelist[i2].k(2, 2)*sz) / std::sqrt(sx*sx + sy*sy + sz*sz);
		double x1 = nodelist[i1].x();
		double y1 = nodelist[i1].y();
		double z1 = nodelist[i1].z();
		double x2 = nodelist[i2].x();
		double y2 = nodelist[i2].y();
		double z2 = nodelist[i2].z();
		double xm = 0.5*(x1 + x2);
		double ym = 0.5*(y1 + y2);
		double zm = 0.5*(z1 + z2);
		double vi1mx = xm - x1;
		double vi1my = ym - y1;
		double vi1mz = zm - z1;
		double Ti1m = c1*(vi1mx*sx + vi1my*sy + vi1mz*sz) / std::sqrt(vi1mx*vi1mx + vi1my*vi1my + vi1mz*vi1mz);
		double vmi2x = x2 - xm;
		double vmi2y = y2 - ym;
		double vmi2z = z2 - zm;
		double Tmi2 = c2*(vmi2x*sx + vmi2y*sy + vmi2z*sz) / std::sqrt(vmi2x*vmi2x + vmi2y*vmi2y + vmi2z*vmi2z);
		double T12 = 1.0 / (1.0 / Ti1m + 1.0 / Tmi2);
		edgelist[iedge].trans(std::abs(T12));
	}
}

//void REGION::computetofcvfe_matrixoutput(){
//	int *nD2node, *node2nD;
//	double *RHS;
//	std::vector<std::vector<double>> M;
//	int nnode_nD;
//	int inode, jnode;
//	std::vector<double> rowvaluelist;
//	std::vector<int> rowpositionlist;
//	std::vector<ROW> rowlist;
//
//	if (graph.tracingsign() == -1){//backward tof; change the sign of velocity
//		for (int ied = 0; ied < nedge; ied++){
//			edgelist[ied].flux(-edgelist[ied].flux());
//			edgelist[ied].fluxcvfe(-edgelist[ied].fluxcvfe());
//		}
//	}
//
//	int nnode = nodelist.size();
//	int nelement = elementlist.size();
//	int nedge = edgelist.size();
//	node2nD = new int[nnode];
//
//	nnode_nD = 0;
//	for (int i = 0; i < nnode; i++){
//		if (nodelist[i].marktracingb() == -1){
//			nnode_nD++;
//			node2nD[i] = nnode_nD - 1;
//		}
//	}
//	nD2node = new int[nnode_nD];
//	nnode_nD = 0;
//	for (int i = 0; i < nnode; i++){
//		if (nodelist[i].marktracingb() == -1){
//			nD2node[nnode_nD] = i;
//			nnode_nD++;
//		}
//		else{
//			if (graph.tracingsign() == 1){
//				nodelist[i].tof(0);
//			}
//			else if (graph.tracingsign() == -1){
//				nodelist[i].tof_back(0);
//			}
//		}
//	}
//
//	RHS = new double[nnode_nD];
//
//	for (int i = 0; i < nnode_nD; i++){
//		RHS[i] = 0;
//	}
//
//	M.resize(nnode_nD);
//	for (int i = 0; i < nnode_nD; i++){
//		M[i].resize(nnode_nD);
//	}
//
//	rowlist.resize(nnode_nD);
//
//	for (int ie = 0; ie<nedge; ie++){
//		if (edgelist[ie].fluxcvfe()>0){//i to j consider j
//			inode = edgelist[ie].node(0);
//			jnode = edgelist[ie].node(1);
//		}
//		else if (edgelist[ie].fluxcvfe()<0){
//			inode = edgelist[ie].node(1);
//			jnode = edgelist[ie].node(0);
//		}
//		double flux_ = std::abs(edgelist[ie].fluxcvfe());
//		if (nodelist[jnode].marktracingb() == -1){ //i in the E_j^in
//			int jnD = node2nD[jnode];
//			rowlist[jnD].addvalue(flux_, jnD);
//			if (nodelist[inode].marktracingb() == -1){
//				int inD = node2nD[inode];
//				rowlist[jnD].addvalue(-flux_, inD);
//			}
//		}
//	}
//	for (int inD = 0; inD<nnode_nD; inD++){
//		inode = nD2node[inD];
//		RHS[inD] = nodelist[inode].porosity()*nodelist[inode].dualvolume();
//	}
//
//	for (int i = 0; i < nnode_nD; i++){
//		rowvaluelist = rowlist[i].valuevec();
//		rowpositionlist = rowlist[i].positionvec();
//		for (int j = 0; j < rowvaluelist.size(); j++){
//			M[i][rowpositionlist[j]] = rowvaluelist[j];
//
//		}
//
//	}
//
//	if (graph.tracingsign() == 1){
//		std::ofstream output1, output2;
//		output1.open("ftofmatrix.txt");
//		output2.open("ftofrhsvec.txt");
//		for (int i = 0; i < nnode_nD; i++){
//			output2 << RHS[i] << std::endl;
//			for (int j = 0; j < nnode_nD; j++){
//				output1 << M[i][j] << " ";
//			}
//			output1 << std::endl;
//		}
//	}
//	else if (graph.tracingsign() == -1){
//		std::ofstream output1, output2;
//		output1.open("btofmatrix.txt");
//		output2.open("btofrhsvec.txt");
//		for (int i = 0; i < nnode_nD; i++){
//			output2 << RHS[i] << std::endl;
//			for (int j = 0; j < nnode_nD; j++){
//				output1 << M[i][j] << " ";
//			}
//			output1 << std::endl;
//		}
//	}
//}
//
//void REGION::computetofcvfe_read(){
//	int *nD2node, *node2nD;
//	double *RHS;
//	std::vector<double> rowvaluelist;
//	std::vector<int> rowpositionlist;
//	std::vector<double> a_vec;
//	std::vector<int> ja_vec, ia_vec;
//	std::vector<ROW> rowlist;
//	int nnode_nD;
//	int inode, jnode;
//
//	int nnode = nodelist.size();
//	int nelement = elementlist.size();
//	int nedge = edgelist.size();
//	node2nD = new int[nnode];
//
//	nnode_nD = 0;
//	for (int i = 0; i < nnode; i++){
//		if (nodelist[i].marktracingb() == -1){
//			nnode_nD++;
//			node2nD[i] = nnode_nD - 1;
//		}
//	}
//	nD2node = new int[nnode_nD];
//	nnode_nD = 0;
//	for (int i = 0; i < nnode; i++){
//		if (nodelist[i].marktracingb() == -1){
//			nD2node[nnode_nD] = i;
//			nnode_nD++;
//		}
//		else{
//			if (graph.tracingsign() == 1){
//				nodelist[i].tof(0);
//				//std::cout << "ftof boundary node " << i <<" "<< nodelist[i].x() <<" "<< nodelist[i].y() <<" "<< nodelist[i].z() << std::endl;
//			}
//			else if (graph.tracingsign() == -1){
//				nodelist[i].tof_back(0);
//				//std::cout << "btof boundary node " << i <<" "<< nodelist[i].x() << " " << nodelist[i].y() << " " << nodelist[i].z() << std::endl;
//			}
//		}
//	}
//
//	std::vector<double> values;
//	values.resize(nnode_nD);
//	std::ifstream inputtof;
//	if (graph.tracingsign() == 1){
//		inputtof.open("ftofresults.txt");
//		for (int i = 0; i < nnode_nD; i++){
//			inputtof >> values[i];
//		}
//	}
//	else if(graph.tracingsign() == -1){
//		inputtof.open("btofresults.txt");
//		for (int i = 0; i < nnode_nD; i++){
//			inputtof >> values[i];
//		}
//	}
//
//	for (int i = 0; i < nnode_nD; i++){
//		inode = nD2node[i];
//		if (graph.tracingsign() == 1){
//			nodelist[inode].tof(values[i]);
//		}
//		else if (graph.tracingsign() == -1){
//			nodelist[inode].tof_back(values[i]);
//		}
//	}
//}


void REGION::computetofcvfe_matrix(){
	int *nD2node, *node2nD;
	double *RHS;
	std::vector<double> rowvaluelist;
	std::vector<int> rowpositionlist;
	std::vector<double> a_vec;
	std::vector<int> ja_vec, ia_vec;
	std::vector<ROW> rowlist;
	int nnode_nD;
	int inode, jnode;

	if (graph.tracingsign() == -1){//backward tof; change the sign of velocity
		for (int ied = 0; ied < nedge; ied++){
			edgelist[ied].flux(-edgelist[ied].flux());
		}
	}

	int nnode = nodelist.size();
	int nelement = elementlist.size();
	int nedge = edgelist.size();
	node2nD = new int[nnode];

	nnode_nD = 0;
	for (int i = 0; i < nnode; i++){
		if (nodelist[i].marktracingb() ==-1){
			nnode_nD++;
			node2nD[i] = nnode_nD - 1;
		}
	}
	nD2node = new int[nnode_nD];
	nnode_nD = 0;
	for (int i = 0; i < nnode; i++){
		if (nodelist[i].marktracingb() ==-1){
			nD2node[nnode_nD] = i;
			nnode_nD++;
		}
		else{
			if (graph.tracingsign() == 1){
				nodelist[i].tof(0);
			}
			else if (graph.tracingsign() == -1){
				nodelist[i].tof_back(0);
			}
		}
	}

	RHS = new double[nnode_nD];

	for (int i = 0; i < nnode_nD; i++){
		RHS[i] = 0;
	}

	rowlist.resize(nnode_nD);

	for (int ie = 0; ie<nedge; ie++){
		if (edgelist[ie].flux()>0){//i to j consider j
			inode = edgelist[ie].node(0);
			jnode = edgelist[ie].node(1);
		}
		else if (edgelist[ie].flux()<0){
			inode = edgelist[ie].node(1);
			jnode = edgelist[ie].node(0);
		}
		double flux_ = std::abs(edgelist[ie].flux());
		if (nodelist[jnode].marktracingb() == -1){
			int jnD = node2nD[jnode];
			rowlist[jnD].addvalue(flux_, jnD);
			if (nodelist[inode].marktracingb() == -1){
				int inD = node2nD[inode];
				rowlist[jnD].addvalue(-flux_, inD);
			}
		}
	}
	for (int inD = 0; inD<nnode_nD; inD++){
		inode = nD2node[inD];
		RHS[inD] = nodelist[inode].porosity()*nodelist[inode].dualvolume();
	}

	if (linearsolverbutton_ == 1){
		nnu = nnode_nD;
		ia = new int[nnu + 1];
		ia[0] = 1;
		for (int i = 0; i < nnode_nD; i++){
			rowvaluelist = rowlist[i].valuevec();
			rowpositionlist = rowlist[i].positionvec();
			ia[i + 1] = ia[i] + rowpositionlist.size();
			for (int j = 0; j < rowpositionlist.size(); j++){
				a_vec.push_back(rowvaluelist[j]);
				ja_vec.push_back(rowpositionlist[j] + 1); //in ja "1" means the first number
			}
		}

		nna = a_vec.size();

		ja = new int[nna];
		a = new double[nna];

		for (int i = 0; i < nna; i++){
			ja[i] = ja_vec[i];
			a[i] = a_vec[i];
		}

		u = new double[nnu];
		f = new double[nnu];

		for (int i = 0; i < nnu; i++){
			inode = nD2node[i];
			u[i] = 0.0;
			f[i] = RHS[i];
		}

		callsamg_advection();


		for (int i = 0; i < nnode_nD; i++){
			inode = nD2node[i];
			/*if (u[i] < 0){
				std::cout << "tof<0 " <<inode<< std::endl;
				std::exit(EXIT_FAILURE);
			}*/
			if (graph.tracingsign() == 1){
				nodelist[inode].tof(u[i]);
			}
			else if (graph.tracingsign() == -1){
				nodelist[inode].tof_back(u[i]);
			}
		}
	}
	else if (linearsolverbutton_ == 2){
		int solver_id; //see Hypre ex5 for more options; For RRM design default setting, currently AMG alone
		MPI_Comm comm;
		comm = 0;
		int ilower = 0;
		int jlower = 0;
		int iupper, jupper;
		HYPRE_IJMatrix ij_matrix;
		HYPRE_ParCSRMatrix parcsr_matrix;
		int nrows;
		int ncols;
		int *rows;
		int *cols;
		double *values;
		int local_size = nnode_nD;
		iupper = local_size - 1; //nnode_nD is local_size
		jupper = local_size - 1;
		HYPRE_IJMatrixCreate(comm, ilower, iupper, jlower, jupper, &ij_matrix);
		//HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &ij_matrix);
		HYPRE_IJMatrixSetObjectType(ij_matrix, HYPRE_PARCSR);
		HYPRE_IJMatrixInitialize(ij_matrix);

		nrows = 1;

		//set matrix coefficients
		for (int i = ilower; i <= iupper; i++){
			rowvaluelist = rowlist[i].valuevec();
			rowpositionlist = rowlist[i].positionvec();
			ncols = rowvaluelist.size();
			cols = new int[ncols];
			values = new double[ncols];
			for (int j = 0; j < ncols; j++){
				cols[j] = rowpositionlist[j];
				values[j] = rowvaluelist[j];
			}
			HYPRE_IJMatrixSetValues(ij_matrix, 1, &ncols, &i, cols, values);
		}
		HYPRE_IJMatrixAssemble(ij_matrix);
		HYPRE_IJMatrixGetObject(ij_matrix, (void **)&parcsr_matrix);

		//create RHS and solution
		HYPRE_IJVector b;
		HYPRE_ParVector par_b;
		HYPRE_IJVector x;
		HYPRE_ParVector par_x;
		HYPRE_Solver solver, precond;
		HYPRE_IJVectorCreate(comm, ilower, iupper, &b);
		HYPRE_IJVectorSetObjectType(b, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(b);

		HYPRE_IJVectorCreate(comm, ilower, iupper, &x);
		HYPRE_IJVectorSetObjectType(x, HYPRE_PARCSR);
		HYPRE_IJVectorInitialize(x);

		/* Set the rhs values; set the solution to zero */
		double *rhs_values, *x_values;
		rhs_values = new double[local_size];
		x_values = new double[local_size];
		rows = new int[local_size];
		for (int i = 0; i < local_size; i++){
			rhs_values[i] = RHS[i];
			x_values[i] = 0.0;
			rows[i] = i;
		}

		HYPRE_IJVectorSetValues(b, local_size, rows, rhs_values);
		HYPRE_IJVectorSetValues(x, local_size, rows, x_values);
		delete x_values;
		delete rhs_values;
		delete rows;
		HYPRE_IJVectorAssemble(b);
		HYPRE_IJVectorGetObject(b, (void **)&par_b);

		HYPRE_IJVectorAssemble(x);
		HYPRE_IJVectorGetObject(x, (void **)&par_x);
		/*HYPRE_IJMatrixPrint(ij_matrix, "IJ.out.A");
		HYPRE_IJVectorPrint(b, "IJ.out.b");*/
		/* Choose a solver and solve the system */
		solver_id = 0;
		/* AMG */
		if (solver_id == 0){
			int num_iterations;
			double final_res_norm;
			HYPRE_BoomerAMGCreate(&solver);
			/* Set some parameters (See Reference Manual for more parameters) */
			HYPRE_BoomerAMGSetPrintLevel(solver, 3);  /* print solve info + parameters */
			HYPRE_BoomerAMGSetCoarsenType(solver, 6); /* Falgout coarsening */
			HYPRE_BoomerAMGSetRelaxType(solver, 3);   /* G-S/Jacobi hybrid relaxation */
			HYPRE_BoomerAMGSetNumSweeps(solver, 1);   /* Sweeeps on each level */
			HYPRE_BoomerAMGSetMaxLevels(solver, 20);  /* maximum number of levels */
			HYPRE_BoomerAMGSetTol(solver, 1e-7);      /* conv. tolerance */
			/* Now setup and solve! */
			HYPRE_BoomerAMGSetup(solver, parcsr_matrix, par_b, par_x);
			HYPRE_BoomerAMGSolve(solver, parcsr_matrix, par_b, par_x);
			/* Run info - needed logging turned on */
			HYPRE_BoomerAMGGetNumIterations(solver, &num_iterations);
			HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, &final_res_norm);
			printf("\n");
			printf("Iterations = %d\n", num_iterations);
			printf("Final Relative Residual Norm = %e\n", final_res_norm);
			printf("\n");
			HYPRE_BoomerAMGDestroy(solver);
		}
		int nvalues = local_size;
		rows = new int[nvalues];
		for (int i = 0; i < nvalues; i++) rows[i] = i;
		values = new double[nvalues];
		HYPRE_IJVectorGetValues(x, nvalues, rows, values);
		HYPRE_IJMatrixDestroy(ij_matrix);
		HYPRE_IJVectorDestroy(b);
		HYPRE_IJVectorDestroy(x);

		for (int i = 0; i < nnode_nD; i++){
			inode = nD2node[i];
			/*if (values[i] < 0){
				std::cout << "tof<0 " << inode << std::endl;
				std::exit(EXIT_FAILURE);
			}*/
			if (graph.tracingsign() == 1){
				nodelist[inode].tof(values[i]);
			}
			else if (graph.tracingsign() == -1){
				nodelist[inode].tof_back(values[i]);
			}
		}
	}
}

void REGION::markcpgmaxtracer(){

	int nele = cpgelementlist.size();
	std::cout << "mark max tracers for cpgfv" << std::endl;
	//mark nodes with max ftracer and btracer wells
	if (ftracerbutton_ == 1){
		for (int i = 0; i < nele; i++) {
			int markmax_ = -1;
			double max_ = 1e-1;
			for (int j = 0; j < inflowblist.size(); j++) {
				if (cpgelementlist[i].tracer(j) > max_) {
					max_ = cpgelementlist[i].tracer(j);
					markmax_ = j;
				}
			}
			cpgelementlist[i].markmaxtracer(markmax_);
		}
	}
	if (btracerbutton_ == 1){
		for (int i = 0; i < nele; i++) {
			int markmax_ = -1;
			double max_ = 1e-1;
			for (int j = 0; j < outflowblist.size(); j++) {
				if (cpgelementlist[i].tracer_back(j) > max_) {
					max_ = cpgelementlist[i].tracer_back(j);
					markmax_ = j;
				}
			}
			cpgelementlist[i].markmaxtracer_back(markmax_);
		}
	}
}

void REGION::markcpgtracer_fmm(std::vector<int> wellvec){

	int nele = cpgelementlist.size();
	std::cout << "mark tracers for cpgfv" << std::endl;
	//mark nodes with max ftracer and btracer wells

		for (int i = 0; i < nele; i++) {
			int count = 0;
			int imark = 0;
			for (int j = 0; j < wellvec.size(); j++) {
				if (cpgelementlist[i].tracer(j) > 0.1) {
					count++;
					imark = wellvec[j];
				}
			}
			if (count == 1){
				cpgelementlist[i].markmaxtracer(imark);
			}
			else if (count > 1){
				cpgelementlist[i].markmaxtracer(wellvec.size());
			}
		}

	std::cout << "DONE" << std::endl;
}

void REGION::markcpgtracermax_fmm(std::vector<int> wellvec){

	int nele = cpgelementlist.size();
	std::cout << "mark tracers for cpgfv" << std::endl;
	//mark nodes with max ftracer and btracer wells
	if (ftracerbutton_ == 1){
		for (int i = 0; i < nele; i++) {
			int markmax_ = -1;
			double max_ = 0;
			for (int j = 0; j < wellvec.size(); j++) {
				if (cpgelementlist[i].tracer(j) > max_) {
					max_ = cpgelementlist[i].tracer(j);
					markmax_ = wellvec[j];
				}
			}
			cpgelementlist[i].markmaxtracer(markmax_);
		}
	}

	std::cout << "DONE" << std::endl;
}

void REGION::markcpgtracermax_fmm_back(std::vector<int> wellvec){

	int nele = cpgelementlist.size();
	std::cout << "mark tracers for cpgfv" << std::endl;
	//mark nodes with max ftracer and btracer wells
	if (ftracerbutton_ == 1){
		for (int i = 0; i < nele; i++) {
			int markmax_ = -1;
			double max_ = 0;
			for (int j = 0; j < wellvec.size(); j++) {
				if (cpgelementlist[i].tracer_back(j) > max_) {
					max_ = cpgelementlist[i].tracer_back(j);
					markmax_ = wellvec[j];
				}
			}
			cpgelementlist[i].markmaxtracer_back(markmax_);
		}
	}

	std::cout << "DONE" << std::endl;
}

void REGION::markcpgtracer_fmm_back(std::vector<int> wellvec){

	int nele = cpgelementlist.size();
	std::cout << "mark tracers for cpgfv" << std::endl;
	//mark nodes with max ftracer and btracer wells

		for (int i = 0; i < nele; i++) {
			int count = 0;
			int imark = 0;
			for (int j = 0; j < wellvec.size(); j++) {
				if (cpgelementlist[i].tracer_back(j) > 0.1) {
					count++;
					imark = wellvec[j];
				}
			}
			if (count == 1){
				cpgelementlist[i].markmaxtracer_back(imark);
			}
			else if (count > 1){
				cpgelementlist[i].markmaxtracer_back(wellvec.size());
			}
		}

	std::cout << "DONE" << std::endl;
}

void REGION::computetracercvfe_matrix(){
	int i, j, markmax_, n0, nnode, ii, iinode;
	double max_;
	nnode = nodelist.size();
	int nelement = elementlist.size();
	int nedge = edgelist.size();

	if (graph.ntracingb() > 0){
		//initialize tracer vector
		if (graph.tracingsign() == 1){
			for (i = 0; i < nnode; i++) {
				nodelist[i].tracersize(graph.ntracingb());
			}
		}
		else if (graph.tracingsign() == -1){ //backward tracer; change the sign of velocity
			for (int ied = 0; ied < nedge; ied++){
				edgelist[ied].flux(-edgelist[ied].flux());

			}
			for (i = 0; i < nnode; i++) {
				nodelist[i].tracersize_back(graph.ntracingb());
			}
		}

		for (int itracer = 0; itracer < graph.ntracingb(); itracer++) {
			int *nD2node, *node2nD;
			double *RHS;
			std::vector<double> rowvaluelist;
			std::vector<int> rowpositionlist;
			std::vector<double> a_vec;
			std::vector<int> ja_vec, ia_vec;
			std::vector<ROW> rowlist;
			int nnode_nD;
			int inode, jnode;

			node2nD = new int[nnode];

			nnode_nD = 0;
			for (int i = 0; i < nnode; i++){
				if (nodelist[i].marktracingb() <0){
					nnode_nD++;
					node2nD[i] = nnode_nD - 1;
				}
			}
			nD2node = new int[nnode_nD];
			nnode_nD = 0;
			for (int i = 0; i < nnode; i++){
				if (nodelist[i].marktracingb() <0){
					nD2node[nnode_nD] = i;
					nnode_nD++;
				}
				else if (nodelist[i].marktracingb() == itracer){
					if (graph.tracingsign() == 1){
						nodelist[i].tracer(itracer, 1);
					}
					else if (graph.tracingsign() == -1){
						nodelist[i].tracer_back(itracer, 1);
					}
				}
				else{
					if (graph.tracingsign() == 1){
						nodelist[i].tracer(itracer, 0);
					}
					else if (graph.tracingsign() == -1){
						nodelist[i].tracer_back(itracer, 0);
					}
				}
			}

			RHS = new double[nnode_nD];

			for (int i = 0; i < nnode_nD; i++){
				RHS[i] = 0;
			}

			rowlist.resize(nnode_nD);

			for (int ie = 0; ie<nedge; ie++){
				if (edgelist[ie].flux()>0){//i to j consider j
					inode = edgelist[ie].node(0);
					jnode = edgelist[ie].node(1);
				}
				else if (edgelist[ie].flux()<0){
					inode = edgelist[ie].node(1);
					jnode = edgelist[ie].node(0);
				}
				double flux_ = std::abs(edgelist[ie].flux());
				if (nodelist[jnode].marktracingb() <0){
					int jnD = node2nD[jnode];
					rowlist[jnD].addvalue(flux_, jnD);
					if (nodelist[inode].marktracingb() <0){
						int inD = node2nD[inode];
						rowlist[jnD].addvalue(-flux_, inD);
					}
					else if(graph.tracingsign()==1){
						RHS[jnD] = RHS[jnD] + flux_*nodelist[inode].tracer(itracer);//tracer at boundary =1 or 0
					}
					else if (graph.tracingsign() == -1){
						RHS[jnD] = RHS[jnD] + flux_*nodelist[inode].tracer_back(itracer);
					}
				}
			}

			if (linearsolverbutton_ == 1){
				nnu = nnode_nD;
				ia = new int[nnu + 1];
				ia[0] = 1;
				for (int i = 0; i < nnode_nD; i++){
					rowvaluelist = rowlist[i].valuevec();
					rowpositionlist = rowlist[i].positionvec();
					ia[i + 1] = ia[i] + rowpositionlist.size();
					for (int j = 0; j < rowpositionlist.size(); j++){
						a_vec.push_back(rowvaluelist[j]);
						ja_vec.push_back(rowpositionlist[j] + 1); //in ja "1" means the first number
					}
				}

				nna = a_vec.size();

				ja = new int[nna];
				a = new double[nna];

				for (int i = 0; i < nna; i++){
					ja[i] = ja_vec[i];
					a[i] = a_vec[i];
				}

				u = new double[nnu];
				f = new double[nnu];

				for (int i = 0; i < nnu; i++){
					inode = nD2node[i];
					u[i] = 0.0;
					f[i] = RHS[i];
				}

				callsamg_advection_tracer();

				//obtain P from u and boundary conditions
				for (int i = 0; i < nnode_nD; i++){
					inode = nD2node[i];
					if (graph.tracingsign() == 1){
						nodelist[inode].tracer(itracer, u[i]);
					}
					else if (graph.tracingsign() == -1){
						nodelist[inode].tracer_back(itracer, u[i]);
					}
				}

			}
			else if (linearsolverbutton_ == 2){
				int solver_id; //see Hypre ex5 for more options; For RRM design default setting, currently AMG alone
				MPI_Comm comm;
				comm = 0;
				int ilower = 0;
				int jlower = 0;
				int iupper, jupper;
				HYPRE_IJMatrix ij_matrix;
				HYPRE_ParCSRMatrix parcsr_matrix;
				int nrows;
				int ncols;
				int *rows;
				int *cols;
				double *values;
				int local_size = nnode_nD;
				iupper = local_size - 1; //nnode_nD is local_size
				jupper = local_size - 1;
				HYPRE_IJMatrixCreate(comm, ilower, iupper, jlower, jupper, &ij_matrix);
				//HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, jlower, jupper, &ij_matrix);
				HYPRE_IJMatrixSetObjectType(ij_matrix, HYPRE_PARCSR);
				HYPRE_IJMatrixInitialize(ij_matrix);

				nrows = 1;

				//set matrix coefficients
				for (int i = ilower; i <= iupper; i++){
					rowvaluelist = rowlist[i].valuevec();
					rowpositionlist = rowlist[i].positionvec();
					ncols = rowvaluelist.size();
					cols = new int[ncols];
					values = new double[ncols];
					for (int j = 0; j < ncols; j++){
						cols[j] = rowpositionlist[j];
						values[j] = rowvaluelist[j];
					}
					HYPRE_IJMatrixSetValues(ij_matrix, 1, &ncols, &i, cols, values);
				}
				HYPRE_IJMatrixAssemble(ij_matrix);
				HYPRE_IJMatrixGetObject(ij_matrix, (void **)&parcsr_matrix);

				//create RHS and solution
				HYPRE_IJVector b;
				HYPRE_ParVector par_b;
				HYPRE_IJVector x;
				HYPRE_ParVector par_x;
				HYPRE_Solver solver, precond;
				HYPRE_IJVectorCreate(comm, ilower, iupper, &b);
				HYPRE_IJVectorSetObjectType(b, HYPRE_PARCSR);
				HYPRE_IJVectorInitialize(b);

				HYPRE_IJVectorCreate(comm, ilower, iupper, &x);
				HYPRE_IJVectorSetObjectType(x, HYPRE_PARCSR);
				HYPRE_IJVectorInitialize(x);

				/* Set the rhs values; set the solution to zero */
				double *rhs_values, *x_values;
				rhs_values = new double[local_size];
				x_values = new double[local_size];
				rows = new int[local_size];
				for (int i = 0; i < local_size; i++){
					rhs_values[i] = RHS[i];
					x_values[i] = 0.0;
					rows[i] = i;
				}

				HYPRE_IJVectorSetValues(b, local_size, rows, rhs_values);
				HYPRE_IJVectorSetValues(x, local_size, rows, x_values);
				delete x_values;
				delete rhs_values;
				delete rows;
				HYPRE_IJVectorAssemble(b);
				HYPRE_IJVectorGetObject(b, (void **)&par_b);

				HYPRE_IJVectorAssemble(x);
				HYPRE_IJVectorGetObject(x, (void **)&par_x);
				HYPRE_IJMatrixPrint(ij_matrix, "IJ.out.A");
				HYPRE_IJVectorPrint(b, "IJ.out.b");
				/* Choose a solver and solve the system */
				solver_id = 0;
				/* AMG */
				if (solver_id == 0){
					int num_iterations;
					double final_res_norm;
					HYPRE_BoomerAMGCreate(&solver);
					/* Set some parameters (See Reference Manual for more parameters) */
					HYPRE_BoomerAMGSetPrintLevel(solver, 3);  /* print solve info + parameters */
					HYPRE_BoomerAMGSetCoarsenType(solver, 6); /* Falgout coarsening */
					HYPRE_BoomerAMGSetRelaxType(solver, 3);   /* G-S/Jacobi hybrid relaxation */
					HYPRE_BoomerAMGSetNumSweeps(solver, 1);   /* Sweeeps on each level */
					HYPRE_BoomerAMGSetMaxLevels(solver, 20);  /* maximum number of levels */
					HYPRE_BoomerAMGSetTol(solver, 1e-7);      /* conv. tolerance */
					/* Now setup and solve! */
					HYPRE_BoomerAMGSetup(solver, parcsr_matrix, par_b, par_x);
					HYPRE_BoomerAMGSolve(solver, parcsr_matrix, par_b, par_x);
					/* Run info - needed logging turned on */
					HYPRE_BoomerAMGGetNumIterations(solver, &num_iterations);
					HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, &final_res_norm);
					printf("\n");
					printf("Iterations = %d\n", num_iterations);
					printf("Final Relative Residual Norm = %e\n", final_res_norm);
					printf("\n");
					HYPRE_BoomerAMGDestroy(solver);
				}
				int nvalues = local_size;
				rows = new int[nvalues];
				for (int i = 0; i < nvalues; i++) rows[i] = i;
				values = new double[nvalues];
				HYPRE_IJVectorGetValues(x, nvalues, rows, values);
				HYPRE_IJMatrixDestroy(ij_matrix);
				HYPRE_IJVectorDestroy(b);
				HYPRE_IJVectorDestroy(x);

				for (int i = 0; i < nnode_nD; i++){
					inode = nD2node[i];
					for (int i = 0; i < nnode_nD; i++){
						inode = nD2node[i];
						if (graph.tracingsign() == 1){
							nodelist[inode].tracer(itracer, values[i]);
						}
						else if (graph.tracingsign() == -1){
							nodelist[inode].tracer_back(itracer, values[i]);
						}
					}
				}
			}
		}
	}
}

void REGION::marknodesmaxtracer() {
	int i, j, markmax_, n0, nnode, ii, iinode;
	double max_;
	nnode = nodelist.size();

	//mark nodes with max ftracer and btracer wells
	if (ftracerbutton_ == 1){
		for (i = 0; i < nnode; i++) {
			markmax_ = -1;
			max_ = 1e-1;
			for (j = 0; j < inflowblist.size(); j++) {
				if (nodelist[i].tracer(j) > max_) {
					max_ = nodelist[i].tracer(j);
					markmax_ = j;
				}
			}
			nodelist[i].markmaxtracer(markmax_);
		}
	}
	if (btracerbutton_ == 1){
		for (i = 0; i < nnode; i++) {
			markmax_ = -1;
			max_ = 1e-1;
			for (j = 0; j < outflowblist.size(); j++) {
				if (nodelist[i].tracer_back(j) > max_) {
					max_ = nodelist[i].tracer_back(j);
					markmax_ = j;
				}
			}
			nodelist[i].markmaxtracer_back(markmax_);
		}
	}


}



void REGION::meshdimension_max_av(){
	double xmin_, xmax_, ymin_, ymax_, zmin_, zmax_, x, y, z, avxr, avxl, avyf, avyb, avzb, avzt;
	int i, nnode, cxr, cxl, cyf, cyb, czb, czt, mark;
	nnode = nodelist.size();
	xmin_ = 1e+10;
	xmax_ = -1e+10;
	ymin_ = 1e+10;
	ymax_ = -1e+10;
	zmin_ = 1e+10;
	zmax_ = -1e+10;
	cxr = 0; cxl = 0; cyf = 0; cyb = 0; czb = 0; czt = 0;
	avxr = 0; avxl = 0; avyf = 0; avyb = 0; avzb = 0; avzt = 0;

	for (i = 0; i < nnode; i++){
		x = nodelist[i].x();
		y = nodelist[i].y();
		z = nodelist[i].z();
		if (x > xmax_){
			xmax_ = x;
		}
		if (x < xmin_){
			xmin_ = x;
		}
		if (y > ymax_){
			ymax_ = y;
		}
		if (y < ymin_){
			ymin_ = y;
		}
		if (z > zmax_){
			zmax_ = z;
		}
		if (z < zmin_){
			zmin_ = z;
		}
		mark = nodelist[i].markbsurface();
		if (mark == parasurfacelist.size() - 1){
			cxr++;
			avxr = avxr + x;
		}
		else if (mark == parasurfacelist.size() - 2){
			cxl++;
			avxl = avxl + x;
		}
		else if (mark == parasurfacelist.size() - 3){
			cyb++;
			avyb = avyb + y;
		}
		else if (mark == parasurfacelist.size() - 4){
			cyf++;
			avyf = avyf + y;
		}
		else if (mark == bsurfaceid[0]){
			czb++;
			avzb = avzb + z;
		}
		else if (mark == bsurfaceid[1]){
			czt++;
			avzt = avzt + z;
		}
	}
	avxr = avxr / cxr;
	avxl = avxl / cxl;
	avyf = avyf / cyf;
	avyb = avyb / cyb;
	avzb = avzb / czb;
	avzt = avzt / czt;
	meshinfo.xmax(xmax_);
	meshinfo.xmin(xmin_);
	meshinfo.ymax(ymax_);
	meshinfo.ymin(ymin_);
	meshinfo.zmax(zmax_);
	meshinfo.zmin(zmin_);

	meshinfo.xmaxav(avxr);
	meshinfo.xminav(avxl);
	meshinfo.ymaxav(avyb);
	meshinfo.yminav(avyf);
	meshinfo.zmaxav(avzt);
	meshinfo.zminav(avzb);
}

void REGION::meshdimension_max(){
	double xmin_, xmax_, ymin_, ymax_, zmin_, zmax_, x, y, z;
	int i, nnode;
	nnode = nodelist.size();
	xmin_ = 1e+10;
	xmax_ = -1e+10;
	ymin_ = 1e+10;
	ymax_ = -1e+10;
	zmin_ = 1e+10;
	zmax_ = -1e+10;


	for (i = 0; i < nnode; i++){
		x = nodelist[i].x();
		y = nodelist[i].y();
		z = nodelist[i].z();
		if (x > xmax_){
			xmax_ = x;
		}
		if (x < xmin_){
			xmin_ = x;
		}
		if (y > ymax_){
			ymax_ = y;
		}
		if (y < ymin_){
			ymin_ = y;
		}
		if (z > zmax_){
			zmax_ = z;
		}
		if (z < zmin_){
			zmin_ = z;
		}

	}

	meshinfo.xmax(xmax_);
	meshinfo.xmin(xmin_);
	meshinfo.ymax(ymax_);
	meshinfo.ymin(ymin_);
	meshinfo.zmax(zmax_);
	meshinfo.zmin(zmin_);

	std::cout << "mesh dimensions x: " << xmax_ << " " << xmin_ << std::endl;
	std::cout << "mesh dimensions y: " << ymax_ << " " << ymin_ << std::endl;
	std::cout << "mesh dimensions z: " << zmax_ << " " << zmin_ << std::endl;

}


void REGION::computecapillarypressure(){
	int ie, mark_, n[4], j;
	double Pc_, h_;
	//for (ie = 0; ie < elementlist.size(); ie++){
	//	for (j = 0; j < 4; j++){
	//		n[j]=elementlist[ie].node(j);
	//	}
	//	h_ = 0.25*(nodelist[n[0]].z() + nodelist[n[1]].z() + nodelist[n[2]].z() + nodelist[n[3]].z());
	//	Pc_ = (elementlist[ie].rho_w() - elementlist[ie].rho_o())*g_*(h_ - h_fwl);
	//	elementlist[ie].Pc(Pc_);
	//	//approximate nodal Pc
	//	double volume_ = elementlist[ie].volume();
	//	for (j = 0; j < 4; j++){
	//		int jnode = elementlist[ie].node(j);
	//		double dualvolume_ = nodelist[jnode].dualvolume();
	//		nodelist[jnode].Pc(Pc_*volume_*0.25 / dualvolume_ + nodelist[jnode].Pc()); //nodal Pc initialised 0 when constructed
	//	}
	//}

}

void REGION::computesaturation(){
	int ie;
	double Sw_;
	/*for (ie = 0; ie < elementlist.size(); ie++){
		double Siw_ = elementlist[ie].Siw();
		double Pc_ = elementlist[ie].Pc();
		double Pct_ = elementlist[ie].Pct();
		double lamda_ = elementlist[ie].lamda();
		if (Pc_ > Pct_){
			Sw_ = (1 - Siw_) / pow(Pc_ / Pct_, lamda_) + Siw_;
		}
		else{
			Sw_ = 1.0;
		}
		elementlist[ie].Sw(Sw_);
	}*/
}

void REGION::computerelativepermeabilities(){
	int ie;
	double krw_, kro_, Sw_, Siw_, lamda_;
	/*for (ie = 0; ie < elementlist.size(); ie++){
		Sw_ = elementlist[ie].Sw();
		Siw_ = elementlist[ie].Siw();
		lamda_ = elementlist[ie].lamda();
		krw_ = pow((Sw_ - Siw_) / (1.0 - Siw_), (2.0 + 3.0*lamda_) / lamda_);
		kro_ = pow((1 - Sw_) / (1 - Siw_), 2)*(1.0 - pow((Sw_ - Siw_) / (1.0 - Siw_), (2.0 + lamda_) / lamda_));
		elementlist[ie].krw(krw_);
		elementlist[ie].kro(kro_);
	}*/
}


void REGION::inputsurfacetype(int i){
	inputsurfacetype_ = i;
}

int REGION::inputsurfacetype(){
	return inputsurfacetype_;
}

void REGION::numberofphases(int i){
	numberofphases_ = i;
}

int REGION::numberofphases(){
	return numberofphases_;
}



