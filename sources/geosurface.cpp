

#include "geosurface.h"

GEOSURFACE::GEOSURFACE(){
}

void GEOSURFACE::addnode(int i){
	node_.push_back(i);
}

int GEOSURFACE::node(int i){
	return(node_[i]);
}

int GEOSURFACE::nnode(){
	return(node_.size());
}

void GEOSURFACE::addcurve(int i){
	curve_.push_back(i);
}

int GEOSURFACE::curve(int i){
	return(curve_[i]);
}

int GEOSURFACE::ncurve(){
	return(curve_.size());
}

void GEOSURFACE::addcurvelocation(double d){
	curvelocation_.push_back(d);
}
double GEOSURFACE::curvelocation(int i){
	return(curvelocation_[i]);
}

void GEOSURFACE::addcurvemark(int i){
	curvemark_.push_back(i);
}
int GEOSURFACE::curvemark(int i){
	return(curvemark_[i]);
}

void GEOSURFACE::mark(int i){
	mark_ = i;
}

int GEOSURFACE::mark(){
	return(mark_);
}
