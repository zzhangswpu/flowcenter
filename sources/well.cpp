

#include "well.h"

WELL::WELL(){
	length_ = 0;
	type_ = 0;
	sign_ = 0;
	value_ = 0;
}

void WELL::clear(){
	length_ = 0;
	type_ = 0;
	sign_ = 0;
	value_ = 0;
	nodelist_.clear();
	geonodelist_.clear();
}

//void WELL::addnode(NODE node_){
//	nodelist_.push_back(node_);
//}
//
//int WELL::numberofnodes(){
//	return(nodelist_.size());
//}


void WELL::type(int a){
	type_ = a;
}

int WELL::type(){
	return(type_);
}

void WELL::sign(int a){
	sign_ = a;
}

int WELL::sign(){
	return(sign_);
}


void WELL::value(double a){
	value_ = a;
}

double WELL::value(){
	return(value_);
}

//NODE WELL:: node(int i){
//	return(nodelist_[i]);
//}

void WELL::computelength(){
	int i;
	NODE m, n;
	if (geonodelist_.size() > 0){
		for (i = 0; i < geonodelist_.size() - 1; i++){ //piecewise linear
			m = geonodelist_[i];
			n = geonodelist_[i + 1];
			length_ = length_ + sqrt((m.x() - n.x())*(m.x() - n.x()) + (m.y() - n.y())*(m.y() - n.y()) + (m.z() - n.z())*(m.z() - n.z()));
		}
	}
}

double WELL::length(){
	return(length_);
}

std::vector<NODE> WELL::nodelist(){
	return (nodelist_);
}

std::vector<NODE> WELL::geonodelist(){
	return (geonodelist_);
}

void WELL::nodelist(std::vector<NODE> list_){
	nodelist_ = list_;
}

void WELL::geonodelist(std::vector<NODE> list_){
	geonodelist_ = list_;
}