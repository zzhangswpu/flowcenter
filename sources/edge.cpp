

#include"edge.h"

EDGE::EDGE(){
	int i, j;
	sx_ = 0;
	sy_ = 0;
	sz_ = 0;
	fluxh_ = 0;
	fluxc_ = 0;
	flux_ = 0;
	area_ = 0;
	for (i = 0; i < 3; i++){
		fmmv_[i] = 0;
		for (j = 0; j < 3; j++){
			k_[i][j] = 0.0;
		}
	}

}
void EDGE::addsuele(int i){
	suele_.push_back(i);
}

void EDGE::fmmv(int i, double d){
	fmmv_[i] = d;
}

double EDGE::fmmv(int i){
	return fmmv_[i];
}

int EDGE::suele(int i){
	return(suele_[i]);
}

int EDGE::numberofsuele(){
	return(suele_.size());
}

unsigned int EDGE::node(unsigned int i) const{
	if (i > 1 || i < 0){
		std::cout << "get node index i out of range " << i << std::endl;
		std::exit(EXIT_FAILURE);
	}
	return (node1[i]);
}

void EDGE::node(unsigned int i, unsigned int node_in){ //set edge node numbers
	if (i > 1 || i < 0){
		std::cout << "set node index i out of range " << i << std::endl;
		std::exit(EXIT_FAILURE);
	}
	node1[i] = node_in;
}

void EDGE::sx(double d){
	sx_ = d;
}

void EDGE::sy(double d){
	sy_ = d;
}

void EDGE::sz(double d){
	sz_ = d;
}

double EDGE::sx(){
	return(sx_);
}

double EDGE::sy(){
	return(sy_);
}

double EDGE::sz(){
	return(sz_);
}

void EDGE::area(double d){
	area_ = d;
}

double EDGE::area(){
	return(area_);
}

void EDGE::length(double d){
	length_ = d;
}

double EDGE::length(){
	return(length_);
}


void EDGE::trans(double d){
	trans_ = d;
}

double EDGE::trans(){
	return(trans_);
}

double EDGE::k(int i, int j) const{ // get permeability
	return k_[i][j];
}

void EDGE::k(int i, int j, double input){ // set permeability
	k_[i][j] = input;
}

void EDGE::flux(double d){
	flux_ = d;
}

double EDGE::flux(){
	return(flux_);
}

void EDGE::fluxc(double d){
	fluxc_ = d;
}

double EDGE::fluxc(){
	return(fluxc_);
}

void EDGE::fluxh(double d){
	fluxh_ = d;
}

double EDGE::fluxh(){
	return(fluxh_);
}

