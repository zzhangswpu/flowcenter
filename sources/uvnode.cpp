

#include"uvnode.h"

UVNODE::UVNODE(){
	flag_ = 0;
}
void UVNODE::u_current(double d){
	u_current_ = d;
}

void UVNODE::flag(int i){
	flag_ = i;
}

int UVNODE::flag(){
	return flag_;
}

void UVNODE::v_current(double d){
	v_current_ = d;
}

void UVNODE::u_origin(double d){
	u_origin_ = d;
}

void UVNODE::v_origin(double d){
	v_origin_ = d;
}

double UVNODE::u_current(){
	return(u_current_);
}

double UVNODE::v_current(){
	return(v_current_);
}

double UVNODE::u_origin(){
	return(u_origin_);
}

double UVNODE::v_origin(){
	return(v_origin_);
}

void UVNODE::u_flat(double d){
	u_flat_ = d;
}

void UVNODE::v_flat(double d){
	v_flat_ = d;
}

double UVNODE::u_flat(){
	return(u_flat_);
}

double UVNODE::v_flat(){
	return(v_flat_);
}

void UVNODE::markoriginsurface(int i){
	markoriginsurface_ = i;
}

int UVNODE::markoriginsurface(){
	return(markoriginsurface_);
}

void UVNODE::u_inter(double d){
	u_inter_ = d;
}

void UVNODE::v_inter(double d){
	v_inter_ = d;
}

double UVNODE::u_inter(){
	return(u_inter_);
}

double UVNODE::v_inter(){
	return(v_inter_);
}

void UVNODE::x(double d){
	x_ = d;
}
void UVNODE::y(double d){
	y_ = d;
}
void UVNODE::z(double d){
	z_ = d;
}

double UVNODE::x(){
	return(x_);
}
double UVNODE::y(){
	return(y_);
}
double UVNODE::z(){
	return(z_);
}